import Bugsnag from '@bugsnag/react-native';
import 'react-native-gesture-handler';
import {enableScreens} from 'react-native-screens';
import {AppRegistry} from 'react-native';
import App from './app/index';
import {BaseSetting} from '@config';

Bugsnag.start();
enableScreens();
AppRegistry.registerComponent(BaseSetting.name, () => App);
