import { useSelector } from "react-redux";
import { useDarkMode } from "react-native-dark-mode";

/**
 * Define Const color use for whole application
 */
export const BaseColor = {
  dimGreyColor: '#DCDCDC',
  lightGrey: '#C0C0C0',
  grayColor: "#9B9B9B",
  dividerColor: "#BDBDBD",
  whiteColor: "#FFFFFF",
  blackColor: '#000',
  fieldColor: "#F5F5F5",
  yellowColor: "#FDC60A",
  navyBlue: "#3C5A99",
  kashmir: "#5D6D7E",
  orangeColor: "#E5634D",
  blueColor: "#5DADE2",
  pinkColor: "#A569BD",
  greenColor: "#58D68D",
  ThemeOrange: '#ff1d25',
  ThemeYellow: '#ffba0c',
  ThemeBlue: '#000023',
  textPrimaryColor: '#212121',
  ThemeRedColor: '#FF0000',
  lightBlack: '#696969',
  pureOrangeColor: '#FF4500',
};

/**
 * Define Const list theme use for whole application
 */

/**
 * Define default theme use for whole application
 */

export const DefaultTheme = {
  theme: "pink",
  light: {
    dark: false,
    colors: {
      primary: "#FF2D55",
      primaryDark: "#F90030",
      primaryLight: "#FF5E80",
      accent: "#4A90A4",
      background: "white",
      card: "#F5F5F5",
      text: "#212121",
      border: "#c7c7cc",
      black: '#000',
      white: '#fff',
    },
  },
  dark: {
    dark: true,
    colors: {
      primary: "#FF2D55",
      primaryDark: "#F90030",
      primaryLight: "#FF5E80",
      accent: "#4A90A4",
      background: "#010101",
      card: "#121212",
      text: "#e5e5e7",
      border: "#272729",
      black: '#fff',
      white: '#000',
    },
  },
};

/**
 * export theme and colors for application
 * @returns theme,colors
 */
export const useTheme = () => {
  const isDarkMode = useDarkMode();
  const theme = DefaultTheme;

  return isDarkMode
    ? { theme: theme.light, colors: theme.light.colors }
    : { theme: theme.light, colors: theme.light.colors };
};
