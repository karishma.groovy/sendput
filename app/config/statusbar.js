import React from 'react';
import {StatusBar} from 'react-native';
import {BaseColor} from '../config/theme';

export function setStatusbar(type = 'light') {
  if (type === 'light') {
    StatusBar.setBackgroundColor(BaseColor.whiteColor, true);
    StatusBar.setBarStyle('dark-content', true);
  } else if (type === 'blue') {
    StatusBar.setBackgroundColor(BaseColor.ThemeBlue, true);
    StatusBar.setBarStyle('light-content', true);
  } else if (type === 'orange') {
    StatusBar.setBackgroundColor(BaseColor.ThemeOrange, true);
    StatusBar.setBarStyle('light-content', true);
  }
}

export const codePushVersion = 34;
