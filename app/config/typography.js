import {StyleSheet, Platform} from 'react-native';

export const FontFamily = {
  regular: 'Rubik-Regular',
  medium: 'Rubik-Light',
  mediumItalic: 'Rubik-LightItalic',
  italic: 'Rubik-Italic',
  bold: 'Rubik-Bold',
  boldItalic: 'Rubik-BoldItalic',
  black: 'Rubik-Black',
  blackItalic: 'Rubik-BlackItalic',
  semiBold: 'Rubik-Medium',
  semiBoldItalic: 'Rubik-MediumItalic',
};

export const FontWeight = {
  thin: '100',
  ultraLight: '200',
  light: '300',
  regular: '400',
  medium: '500',
  semibold: '600',
  bold: '700',
  heavy: '800',
  black: '900',
};

export const Typography = StyleSheet.create({
  default: {
    fontWeight: FontWeight.regular,
    fontFamily: FontFamily.regular,
    letterSpacing:0.5,

  },
  header: {
    fontSize: 34,
    fontWeight: FontWeight.bold,
    fontFamily: FontFamily.semiBold,
    letterSpacing:0.5,
  },
  title1: {
    fontSize: 28,
    fontWeight: FontWeight.medium,
    fontFamily: FontFamily.medium,
    letterSpacing:0.5,
  },
  title2: {
    fontSize: 22,
    fontWeight: FontWeight.medium,
    fontFamily: FontFamily.medium,
    letterSpacing:0.5,
  },
  title3: {
    fontSize: 20,
    fontWeight: FontWeight.regular,
    fontFamily: FontFamily.regular,
    letterSpacing:0.5,
  },
  headline: {
    fontSize: 17,
    fontWeight: FontWeight.regular,
    fontFamily: FontFamily.regular,
    letterSpacing:0.5,
  },
  body1: {
    fontSize: 17,
    fontWeight: FontWeight.regular,
    fontFamily: FontFamily.regular,
    letterSpacing:0.5,
  },
  body2: {
    fontSize: 14,
    fontWeight: FontWeight.regular,
    fontFamily: FontFamily.regular,
    letterSpacing:0.5,
  },
  callout: {
    fontSize: 17,
    fontWeight: FontWeight.regular,
    fontFamily: FontFamily.regular,
    letterSpacing:0.5,
  },
  subhead: {
    fontSize: 15,
    fontWeight: FontWeight.regular,
    fontFamily: FontFamily.regular,
    letterSpacing:0.5,
  },
  footnote: {
    fontSize: 13,
    fontWeight: FontWeight.regular,
    fontFamily: FontFamily.regular,
    letterSpacing:0.5,
  },
  caption1: {
    fontSize: 12,
    fontWeight: FontWeight.regular,
    fontFamily: FontFamily.regular,
    letterSpacing:0.5,
  },
  caption2: {
    fontSize: 11,
    fontWeight: FontWeight.regular,
    fontFamily: FontFamily.regular,
    letterSpacing:0.5,
  },
  overline: {
    fontSize: 10,
    fontWeight: FontWeight.regular,
    fontFamily: FontFamily.regular,
    letterSpacing:0.5,
  },
});
