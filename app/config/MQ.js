import {BaseSetting} from './setting';

const nWidth = BaseSetting.nWidth;
const FORTAB = nWidth <= 2736 && nWidth >= 600;
const TABLANDSCAPE = nWidth <= 2736 && nWidth >= 600;
const TABPORTRAIT = nWidth >= 600 && nWidth <= 600;

module.exports = {
  FORTAB,
  TABLANDSCAPE,
  TABPORTRAIT,
};
