/**
 * Images Defines
 * @author Passion UI <passionui.com>
 */

export const Images = {
  logoWhiteImg: require('../assets/images/whiteLogo.png'),
  logoBlackImg: require('../assets/images/logoBlack.png'),
  sliderImg1: require('../assets/images/splash1.png'),
  sliderImg2: require('../assets/images/splash2.png'),
  sliderImg3: require('../assets/images/splash3.png'),
  hero: require('../assets/images/hero.jpeg'),
  userImage: require('../assets/images/user.jpg'),
  FAQimage: require('../assets/images/FAQ.png'),
  masterCard: require('../assets/images/masterCard.png'),
  staticMap: require('../assets/images/Maps.jpg'),
  usFlag: require('../assets/images/usFlag.png'),
  emptyCart: require('../assets/lottie/emptyCart.json'),
  success: require('../assets/lottie/placeOrderSuccess.json'),
};
