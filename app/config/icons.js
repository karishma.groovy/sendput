import {PixelRatio, Platform} from 'react-native';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import icoMoonConfig from './selection.json';

const CustomIcon = createIconSetFromIcoMoon(icoMoonConfig);

const iconSize = 22;
const navIconSize =
  __DEV__ === false && Platform.OS === 'android'
    ? PixelRatio.getPixelSizeForLayoutSize(8)
    : iconSize;
const replaceSuffixPattern = /--(active|big|small|very-big)/g;

const CustIcon = {
  'smartphone': [navIconSize, '#FFFFFF'],
};
const iconsArray = [
  [CustIcon, CustomIcon],
];

const iconsMap = {};
const iconsLoaded = new Promise(resolve => {
  const allFonts = [iconsArray].map(iconArrayMain => {
    return Promise.all(
      iconArrayMain.map(iconArray => {
        return Promise.all(
          Object.keys(iconArray[0]).map(iconName =>
            iconArray[1].getImageSource(
              iconName.replace(replaceSuffixPattern, ''),
              iconArray[0][iconName][0],
              iconArray[0][iconName][1],
            ),
          ),
        ).then(sources => {
          return Object.keys(iconArray[0]).forEach(
            (iconName, idx) => (iconsMap[iconName] = sources[idx]),
          );
        });
      }),
    ).then(() => {
      resolve(true);
    });
  });

  return Promise.all(allFonts);
});

export {iconsMap, iconsLoaded, CustomIcon};
