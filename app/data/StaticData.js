import { Images } from '../config/images';

export const slides = [
  {
    key: 1,
    title: 'Explore Store & Products',
    text: 'Browse and explore collections of marijuana strains and product insights at online storefront. Quick and easy place order.',
    image: Images.sliderImg1,
  },
  {
    key: 2,
    title: 'Multiple Payment',
    text: 'Users can pay for the marijuana strains and product via cash or card.',
    image: Images.sliderImg2,
  },
  {
    key: 3,
    title: 'Get Delivery at Your Home',
    text: 'Carry out door-to-door instant delivery all through our web and app on demand solutions.',
    image: Images.sliderImg3,
  },
];

export const productListData = [
  {
    id: 1,
    image:
      'https://www.buzzdelivery.org/wp-content/uploads/2019/12/3954037e-bb6b-46a9-b6c1-aaf862791b63.jpeg',
    length: 0.7,
    name: 'STIIIZY Starter Kit-\nBlack',
    yellowStar: 5,
    price: '23.00',
    pricePer: 'each',
    isFav: true,
  },
  {
    id: 2,
    image:
      'https://aph-uploads-production.s3.amazonaws.com/uploads/photo/photo/57697/Stiiizy-Juicy-Melon-1-1-Vape-Pod-Hero-CA-0556-791700.jpg',
    length: 0.7,
    name: 'STIIIZY Starter Kit-Black',
    yellowStar: 5,
    price: '23.00',
    pricePer: 'each',
    isFav: true,
  },
  {
    id: 3,
    image:
      'https://www.buzzdelivery.org/wp-content/uploads/2019/12/3954037e-bb6b-46a9-b6c1-aaf862791b63.jpeg',
    length: 0.7,
    name: 'STIIIZY Starter Kit-Black',
    yellowStar: 5,
    price: '23.00',
    pricePer: 'each',
    isFav: true,
  },
  {
    id: 4,
    image:
      'https://aph-uploads-production.s3.amazonaws.com/uploads/photo/photo/57697/Stiiizy-Juicy-Melon-1-1-Vape-Pod-Hero-CA-0556-791700.jpg',
    length: 0.7,
    name: 'STIIIZY Starter Kit-Black',
    yellowStar: 5,
    price: '23.00',
    pricePer: 'each',
    isFav: true,
  },
  {
    id: 5,
    image:
      'https://www.buzzdelivery.org/wp-content/uploads/2019/12/3954037e-bb6b-46a9-b6c1-aaf862791b63.jpeg',
    length: 0.7,
    name: 'STIIIZY Starter Kit-Black',
    yellowStar: 5,
    price: '23.00',
    pricePer: 'each',
    isFav: false,
  },
  {
    id: 6,
    image:
      'https://aph-uploads-production.s3.amazonaws.com/uploads/photo/photo/57697/Stiiizy-Juicy-Melon-1-1-Vape-Pod-Hero-CA-0556-791700.jpg',
    length: 0.7,
    name: 'STIIIZY Starter Kit-Black',
    yellowStar: 5,
    price: '23.00',
    pricePer: 'each',
    isFav: false,
  },
  {
    id: 7,
    image:
      'https://www.buzzdelivery.org/wp-content/uploads/2019/12/3954037e-bb6b-46a9-b6c1-aaf862791b63.jpeg',
    length: 0.7,
    name: 'STIIIZY Starter Kit-Black',
    yellowStar: 5,
    price: '23.00',
    pricePer: 'each',
    isFav: false,
  },
  {
    id: 8,
    image:
      'https://aph-uploads-production.s3.amazonaws.com/uploads/photo/photo/57697/Stiiizy-Juicy-Melon-1-1-Vape-Pod-Hero-CA-0556-791700.jpg',
    length: 0.7,
    name: 'STIIIZY Starter Kit-Black',
    yellowStar: 5,
    price: '23.00',
    pricePer: 'each',
    isFav: true,
  },
];

export const NotificationData = [
  {
    id: 1,
    text1: 'Kanha BOGO 50% OFF!',
    text2: 'Project Cannabis DTLA has a new deal!',
    date: 'March 15th',
    img:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQJ8C1MGvXlQ3T4aO2ZKF7gYFXcGHS2jIvjbw&usqp=CAU',
    isRead: true,
  },
  {
    id: 2,
    text1: 'GRASSDOOR is offering a promotion near you!',
    text2: 'Use promo code MAR23 at checkout to save 23%',
    date: 'March 8th',
    img:
      'https://static.grassdoor.com/grassdoor.com/web_static_files/og_grassdoor.png',
    isRead: false,
  },
  {
    id: 3,
    text1: 'Happy Hour 8am - 10am *15% off*',
    text2: 'PAC LA - Downtown LA has a new deal!',
    date: 'March 1th',
    img:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTOddFLfckg_Re_j2yHpd9Mh8GfowN69vOgMQ&usqp=CAU',
    isRead: true,
  },
  {
    id: 4,
    text1: 'Kanha BOGO 50% OFF!',
    text2: 'Project Cannabis DTLA has a new deal!',
    date: 'March 15th',
    img:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQJ8C1MGvXlQ3T4aO2ZKF7gYFXcGHS2jIvjbw&usqp=CAU',
    isRead: false,
  },
  {
    id: 5,
    text1: 'GRASSDOOR is offering a promotion near you!',
    text2: 'Use promo code MAR23 at checkout to save 23%',
    date: 'March 8th',
    img:
      'https://static.grassdoor.com/grassdoor.com/web_static_files/og_grassdoor.png',
    isRead: true,
  },
  {
    id: 6,
    text1: 'Happy Hour 8am - 10am *15% off*',
    text2: 'PAC LA - Downtown LA has a new deal!',
    date: 'March 1th',
    img:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTOddFLfckg_Re_j2yHpd9Mh8GfowN69vOgMQ&usqp=CAU',
    isRead: false,
  },
];

export const ratingUsData = [
  {
    name: 'Chris Hemsworth',
    yellowStar: 4,
    whiteStar: 1,
    date: 'June 5, 2021',
    message:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
  },
  {
    name: 'Chris Hemsworth',
    yellowStar: 4,
    whiteStar: 1,
    date: 'June 5, 2021',
    message:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
  },
  {
    name: 'Chris Hemsworth',
    yellowStar: 4,
    whiteStar: 1,
    date: 'June 5, 2021',
    message:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
  },
  {
    name: 'Chris Hemsworth',
    yellowStar: 4,
    whiteStar: 1,
    date: 'June 5, 2021',
    message:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
  },
];

export const orderTabList = [
  {
    id: 1,
    category_name: 'All',
    status: '',
  },
  {
    id: 2,
    category_name: 'Ongoing',
    status: 'ongoing',
  },
  {
    id: 3,
    category_name: 'Picked up',
    status: 'picked',
  },
  {
    id: 4,
    category_name: 'Delivered',
    status: 'delivered',
  },
  {
    id: 5,
    category_name: 'Cancelled',
    status: 'cancelled',
  },
  {
    id: 6,
    category_name: 'Return',
    status: 'return',
  },
];

export const settingScreenList = [
  {
    id: 1,
    title: 'Edit Profile',
    screen: 'EditProfile',
  },
  {
    id: 2,
    title: 'Change Password',
    screen: 'ChangePassword',
  },
  {
    id: 3,
    title: 'FAQ',
    screen: 'FAQ',
  },
  {
    id: 5,
    title: 'Driver Guideline',
    screen: 'DriverGuideLine',
  },
  {
    id: 6,
    title: 'Job Status',
    screen: 'Switch',
  },
  {
    id: 4,
    title: 'Logout',
    screen: 'Logout',
  },
];

export const genderTypes = [
  {
    id: 1,
    Type: 'Male',
  },
  {
    id: 2,
    Type: 'Female',
  },
  {
    id: 3,
    Type: 'Other',
  },
];

export const UserSettingScreenList = [
  // {
  //   id: 1,
  //   title: 'About Us',
  //   screenName: 'TermsConditions',
  // },
  {
    id: 2,
    title: 'Edit Profile',
    screenName: 'EditProfile',
  },
  {
    id: 3,
    title: 'Change Password',
    screenName: 'ChangePassword',
  },
  {
    id: 4,
    title: 'Shipping Address',
    screenName: 'ShippingAddress',
  },
  {
    id: 5,
    title: 'Payment Method',
    screenName: 'PaymentMethod',
  },
  {
    id: 1,
    title: 'About Us',
    screenName: 'TermsConditions',
  },
  {
    id: 6,
    title: 'Contact us',
    screenName: 'ContactUs',
  },
  {
    id: 7,
    title: 'FAQ',
    screenName: 'FAQ',
  },
  {
    id: 8,
    title: 'Terms & Condition',
    screenName: 'TermsConditions',
  },
  // {
  //   id: 7,
  //   title: 'FavItemList', // Change Title Name //
  //   screenName: 'FavItemList',
  // },
  {
    id: 9,
    title: 'Log Out',
    screenName: 'Logout',
  },
];

export const drawerList = [
  {
    id: 1,
    title: 'Home',
    iconName: 'home-outline',
    screenName: 'Home',
  },
  {
    id: 2,
    title: 'Dispensaries',
    iconName: 'card-outline',
    screenName: 'Dispensaries',
  },
  {
    id: 3,
    title: 'Products',
    iconName: 'flower-outline',
    screenName: 'Products',
  },
  {
    id: 4,
    title: 'Brands',
    iconName: 'checkmark-circle-outline',
    screenName: 'Brands',
  },
  {
    id: 5,
    title: 'Deals',
    iconName: 'pricetag-outline',
    screenName: 'Deals',
  },
  {
    id: 6,
    title: 'Account Settings',
    iconName: 'settings-outline',
    screenName: 'Account',
  },
  {
    id: 7,
    title: 'Favorites',
    iconName: 'heart-outline',
    screenName: 'Favorites',
  },
  {
    id: 8,
    title: 'Order History',
    iconName: 'cart-outline',
    screenName: 'UserOrder',
  },
  {
    id: 9,
    title: 'About SendPut',
    iconName: 'information-circle-outline',
    screenName: 'TermsConditions',
  },
  {
    id: 10,
    title: 'Contact us',
    iconName: 'call-outline',
    screenName: 'ContactUs',
  },
  // {
  //   id: 11,
  //   title: 'Rate us',
  //   iconName: 'star-half-sharp',
  //   screenName: 'Rate',
  // },
  {
    id: 12,
    title: 'FAQ',
    iconName: 'chatbubbles-outline',
    screenName: 'FAQ',
  },
  {
    id: 13,
    title: 'Terms & Condition',
    iconName: 'shield-checkmark-outline',
    screenName: 'TermsConditions',
  },
  {
    id: 14,
    title: 'Log Out',
    iconName: 'log-out-outline',
    screenName: 'Logout',
  },
];

export const userOrderTab = [
  {
    id: 1,
    category_name: 'Delivered',
    status: 'delivered',
  },
  {
    id: 2,
    category_name: 'Processing',
    status: 'ongoing',
  },
  {
    id: 3,
    category_name: 'Cancelled',
    status: 'cancelled',
  },
];

export const flatListData = [
  {
    id: 1,
    title: 'All Cat',
  },
  {
    id: 3,
    title: 'Dispensaries',
  },
  {
    id: 2,
    title: 'Sub Cat',
  },
];

export const mainCatData = [
  {
    id: 'bd7asffgsgcbea-c1b1-46c2-aed5-3ad53abb28ba',
    Images:
      'https://us.123rf.com/450wm/grispb/grispb2007/grispb200701153/151882434-vaping-concept-on-orange-background-vape-pen-vape-smoke.jpg?ver=6',
    text: 'Vape Pens',
  },
  {
    id: '3acsghht68afc-c605-48d3-a4f8-fbd91aa97f63',
    Images:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR4iGuUFd7pAi30JcRLqZXy6wKkVyLOAg4Xww&usqp=CAU',
    text: 'Flower',
  },
  {
    id: '58694rthrta0f-3da1-471f-bd96-145571e29d72',
    Images:
      'https://thumbs.dreamstime.com/b/pink-crayon-pencil-paper-background-business-concept-155437062.jpg',
    text: 'Concentrates',
  },
  {
    id: 'bd7arthrhcbea-cfdgdf1b1-46c2-aed5-3ad53abb28ba',
    Images:
      'https://thumbs.dreamstime.com/b/edible-flowers-plate-violet-purple-white-dessert-fork-over-blue-knitted-background-flat-lay-space-182972868.jpg',
    text: 'Edibles',
  },
  {
    id: '3ac6rthrth8afc-c6dhgjjhk05-48d3-a4f8-fbd91aa97f63',
    Images:
      'https://as1.ftcdn.net/jpg/03/39/48/10/500_F_339481037_DI6TzwPRiBK3J2TQVj879bPhVqH9nygr.jpg',
    text: 'CBD',
  },
  {
    id: '586rthgrh94a0f-3da1-47jkk1f-bd96-145571e29d72',
    Images:
      'https://media.gettyimages.com/videos/gears-complex-video-id1225033094?s=640x640',
    text: 'Gear',
  },
  {
    id: '586rthgrh94a0f-3da1-47jkk1f-bd96-145571e29d72',
    Images:
      'https://c8.alamy.com/comp/F071GH/soybean-crops-in-field-soya-bean-growing-on-plantation-blue-sky-in-F071GH.jpg',
    text: 'Cultivation',
  },
  {
    id: '586rthgrh94a0f-3da1-47jkk1f-bd96-145571e29d72',
    Images:
      'https://c8.alamy.com/comp/2EWTFMB/thick-white-cbd-topical-lotion-or-cream-with-cannabis-leaf-on-a-green-background-2EWTFMB.jpg',
    text: 'Topicals',
  },
  {
    id: '586rthgrh94a0f-3da1-47jkk1f-bd96-145571e29d72',
    Images:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRDKu31kf6MlXJA2gkpcamZERHXcYzsbFoTXQ&usqp=CAU',
    text: 'Pre Roll',
  },
];