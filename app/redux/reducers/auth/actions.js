import Bugsnag from '@bugsnag/react-native';
import _ from 'lodash';
import {checkObject} from '../../../utils/commonFunction';

const actions = {
  SET_DATA: 'auth/SET_DATA',
  SET_NETWORK_STATUS: 'auth/SET_NETWORK_STATUS',
  SET_SELECTED_TAB_INDEX: 'auth/SET_SELECTED_TAB_INDEX',
  CLEAR_LOCAL_STORAGE_DATA: 'auth/CLEAR_LOCAL_STORAGE_DATA',
  SET_LOGOUT_LOAD: 'auth/SET_LOGOUT_LOAD',
  SET_INTRO_SLIDER_DATA: 'auth/SET_INTRO_SLIDER_DATA',
  SET_USER_TYPE: 'auth/SET_USER_TYPE',
  SET_ORDER_LIST: 'auth/SET_ORDER_LIST',
  SET_UUID: 'auth/SET_UUID',
  SET_NOTIFICATION_BADGE: 'auth/SET_NOTIFICATION_BADGE',
  SET_USER_LOCATION_DATA: 'auth/SET_USER_LOCATION_DATA',
  SET_SHOW_AGE_PERMISSION_UI: 'auth/SET_SHOW_AGE_PERMISSION_UI',
  SET_CART_BADGE_COUNT: 'auth/SET_CART_BADGE_COUNT',
  SET_STATUSBAR_HEIGHT: 'auth/SET_STATUSBAR_HEIGHT',
  SET_PUSH_NOTIFICATION_STATUS: 'auth/SET_PUSH_NOTIFICATION_STATUS',

  getNewNotification: (NotiNumber) => (dispatch) =>
    dispatch({
      type: actions.SET_PUSH_NOTIFICATION_STATUS,
      NotiNumber,
    }),

  setStatusBarHeight: (statusBarHeight) => (dispatch) =>
    dispatch({
      type: actions.SET_STATUSBAR_HEIGHT,
      statusBarHeight,
    }),

  setCartBadgeCount: (cartBadge) => (dispatch) =>
    dispatch({
      type: actions.SET_CART_BADGE_COUNT,
      cartBadge,
    }),

  setShowAgePermissonUI: (isDisplayAgePermissionUI) => (dispatch) =>
    dispatch({
      type: actions.SET_SHOW_AGE_PERMISSION_UI,
      isDisplayAgePermissionUI,
    }),

  setUserLocationsData: (locationsData) => (dispatch) =>
    dispatch({
      type: actions.SET_USER_LOCATION_DATA,
      locationsData,
    }),

  setNotificationBadge: (notificationCount) => (dispatch) =>
    dispatch({
      type: actions.SET_NOTIFICATION_BADGE,
      notificationCount,
    }),

  setUUID: (uuid) => (dispatch) =>
    dispatch({
      type: actions.SET_UUID,
      uuid,
    }),

  setOrderList: (orderList) => (dispatch) =>
    dispatch({
      type: actions.SET_ORDER_LIST,
      orderList,
    }),

  setUserType: (userType) => (dispatch) =>
    dispatch({
      type: actions.SET_USER_TYPE,
      userType,
    }),

  setIntroSlider: (displayIntro) => (dispatch) =>
    dispatch({
      type: actions.SET_INTRO_SLIDER_DATA,
      displayIntro,
    }),

  setLogoutLoad: (logoutLoad) => (dispatch) =>
    dispatch({
      type: actions.SET_LOGOUT_LOAD,
      logoutLoad,
    }),

  clearLocalStorageData: () => (dispatch) => {
    return dispatch({
      type: actions.CLEAR_LOCAL_STORAGE_DATA,
    });
  },

  setSelectedTab: (tabNo) => (dispatch) =>
    dispatch({
      type: actions.SET_SELECTED_TAB_INDEX,
      tabNo,
    }),

  setNetworkStatus: (isConnected) => (dispatch) =>
    dispatch({
      type: actions.SET_NETWORK_STATUS,
      isConnected,
    }),

  setUserData: (data) => {
    let uData = {};
    if (data !== undefined && data !== null && Object.keys(data).length > 0) {
      uData = data;
    }

    const fName = checkObject(uData, 'firstname');
    const lName = checkObject(uData, 'lastname');
    const userID = checkObject(uData, 'id');
    const userName = `${fName} ${lName}`;
    const userEmail = checkObject(uData, 'email');

    if (Bugsnag.setUser) {
      Bugsnag.setUser(
        _.toString(userID),
        _.toString(userEmail),
        _.toString(userName),
      );
    }

    return (dispatch) =>
      dispatch({
        type: actions.SET_DATA,
        userData: uData,
      });
  },
};

export default actions;
