import types from './actions';

const initialState = {
  userData: {},
  isConnected: true,
  uuid: '',
  tabNo: 0,
  cartBadge: 0,
  notificationCount: 0,
  logoutLoad: false,
  displayIntro: true,
  userType: 'user',
  orderList: [],
  locationsData: {},
  isDisplayAgePermissionUI: true,
  statusBarHeight: 20,
  NotiNumber: 0,
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case 'persist/REHYDRATE':
      if (action.payload && action.payload.auth) {
        return {
          ...state,
          ...action.payload.auth,
        };
      }
      return state;

    case types.CLEAR_LOCAL_STORAGE_DATA:
      return {
        ...state,
        userData: {},
        isConnected: true,
        tabNo: 0,
        uuid: '',
        logoutLoad: false,
        userType: 'user',
        orderList: [],
        notificationCount: 0,
        cartBadge: 0,
        locationsData: {},
        NotiNumber: 0,
      };

    case types.SET_PUSH_NOTIFICATION_STATUS:
      console.log('=============================== SET PUSH NOTIFICATION COUNT ==============================================');
      console.log(action.NotiNumber);
      console.log('=============================== SET PUSH NOTIFICATION COUNT ==============================================');
      return {
        ...state,
        NotiNumber: action.NotiNumber,
      };

    case types.SET_STATUSBAR_HEIGHT:
      return {
        ...state,
        statusBarHeight: action.statusBarHeight,
      };

    case types.SET_CART_BADGE_COUNT:
      return {
        ...state,
        cartBadge: action.cartBadge,
      };

    case types.SET_SHOW_AGE_PERMISSION_UI:
      return {
        ...state,
        isDisplayAgePermissionUI: action.isDisplayAgePermissionUI,
      };

    case types.SET_USER_LOCATION_DATA:
      return {
        ...state,
        locationsData: action.locationsData,
      };

    case types.SET_UUID:
      return {
        ...state,
        uuid: action.uuid,
      };

    case types.SET_NOTIFICATION_BADGE:
      return {
        ...state,
        notificationCount: action.notificationCount,
      };

    case types.SET_USER_TYPE:
      return {
        ...state,
        userType: action.userType,
      };

    case types.SET_ORDER_LIST:
      console.log(JSON.stringify(action.orderList));
      return {
        ...state,
        orderList: action.orderList,
      };

    case types.SET_INTRO_SLIDER_DATA:
      return {
        ...state,
        displayIntro: action.displayIntro,
      };

    case types.SET_LOGOUT_LOAD:
      return {
        ...state,
        logoutLoad: action.logoutLoad,
      };

    case types.SET_DATA:
      return {
        ...state,
        userData: action.userData,
      };

    case types.SET_SELECTED_TAB_INDEX:
      return {
        ...state,
        tabNo: action.tabNo,
      };

    case types.SET_NETWORK_STATUS:
      return {
        ...state,
        isConnected: action.isConnected,
      };

    default:
      return state;
  }
}
