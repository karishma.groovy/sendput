import React, {useCallback, useEffect, useRef, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {DarkModeProvider} from 'react-native-dark-mode';
import SplashScreen from 'react-native-splash-screen';
import {Platform, StatusBar, NativeModules} from 'react-native';
import _ from 'lodash';
import UserBottomTab from './UserBottomTab';
import {BaseColor, useTheme} from '@config';
import CustomerDrawer from './CustomerDrawer';
import DriverBottomTab from './DriverBottomTab';
import {checkObject} from '../utils/commonFunction';

// Default WelCome Screen //
import WelCome from '../screens/WelCome/WelCome';

// Common Screen For Both Driver & Normal User //
import FAQ from '../screens/CommomScreen/FAQ';
import Login from '../screens/CommomScreen/Login';
import VerifyOtp from '../screens/CommomScreen/VerifyOtp';
import ForgotPwd from '../screens/CommomScreen/ForgotPwd';
import EditProfile from '../screens/CommomScreen/EditProfile';
import ChangePassword from '../screens/CommomScreen/ChangePassword';
import TermsConditions from '../screens/CommomScreen/TermsConditions';

// Normal User Screens //
import Home from '../screens/NormalUser/Home/Home';
import Cart from '../screens/NormalUser/Cart/Cart';
import Rate from '../screens/NormalUser/Rate/Rate';
import SignUp from '../screens/NormalUser/SignUp/SignUp';
import UserOrder from '../screens/NormalUser/Order/UserOrder';
import Products from '../screens/NormalUser/Products/Products';
import ContactUs from '../screens/NormalUser/ContactUs/ContactUs';
import IntroSlider from '../screens/NormalUser/IntroSlider/IntroSlider';
import UserOrderDetails from '../screens/NormalUser/Order/UserOrderDetails';
import Notifications from '../screens/NormalUser/Notifications/Notifications';
import DispensariesDetail from '../screens/NormalUser/Dispensaries/DispensariesDetail';

import Favorites from '../screens/NormalUser/Favorites/Favorites';
import Account from '../screens/NormalUser/Account/Account';
import Brands from '../screens/NormalUser/Brands/Brands';
import Deals from '../screens/NormalUser/Deals/Deals';
import OtherCategory from '../screens/NormalUser/OtherCategory/OtherCategory';
import Dispensaries from '../screens/NormalUser/Dispensaries/Dispensaries';
import ProductDetails from '../screens/NormalUser/Products/ProductDetails';
import ShippingAddress from '../screens/NormalUser/ShippingAddress/ShippingAddress';
import AddAddress from '../screens/NormalUser/AddAddress/AddAddress';
import PaymentMethod from '../screens/NormalUser/PaymentMethod/PaymentMethod';
import DealsDetail from '../screens/NormalUser/Deals/DealsDetail';
import PushNotification from '../components/PushNotification';
import AgePermission from '../screens/NormalUser/AgePermissionScreen/AgePermission';
import LocationAccess from '../screens/NormalUser/LocationAccess/LocationAccess';

import NoInternet from '../components/NoInternet/index';
import authActions from '../redux/reducers/auth/actions';

const {setStatusBarHeight} = authActions;
const isIOS = Platform.OS === 'ios';
const {StatusBarManager} = NativeModules;

function App() {
  const dispatch = useDispatch();
  const {theme} = useTheme();
  const navigationRef = useRef();
  const userDataValue = useSelector((uState) => uState.auth.userData);
  const displayIntroSlider = useSelector((uState) => uState.auth.displayIntro);

  const uType = checkObject(userDataValue, 'user_type');

  useEffect(() => {
    SplashScreen.hide();

    if (isIOS) {
      StatusBar.setBarStyle('dark-content', true);
      StatusBarManager.getHeight((statusBarHeight) => {
        const sHeight =
          statusBarHeight && statusBarHeight.height
            ? statusBarHeight.height
            : 0;
        dispatch(setStatusBarHeight(sHeight));
      });
    } else {
      StatusBar.setBackgroundColor(BaseColor.WhiteColor, true);
      dispatch(setStatusBarHeight(StatusBarManager.HEIGHT));
    }
  }, []);

  const normalStack = createStackNavigator();
  const UserDrawer = createDrawerNavigator();
  const LoginStack = createStackNavigator();
  const IntroStack = createStackNavigator();

  const IntroStackScreen = ({}) => (
    <IntroStack.Navigator>
      <IntroStack.Screen
        name="IntroSlider"
        component={IntroSlider}
        options={{headerShown: false}}
      />
    </IntroStack.Navigator>
  );

  const LoginStackScreen = ({}) => (
    <LoginStack.Navigator>
      <LoginStack.Screen
        name="WelCome"
        component={WelCome}
        options={{headerShown: false}}
      />
      <LoginStack.Screen
        name="Login"
        component={Login}
        options={{headerShown: false}}
      />
      <LoginStack.Screen
        name="SignUp"
        component={SignUp}
        options={{headerShown: false}}
      />
      <LoginStack.Screen
        name="ForgotPwd"
        component={ForgotPwd}
        options={{headerShown: false}}
      />
      <LoginStack.Screen
        name="TermsConditions"
        component={TermsConditions}
        options={{headerShown: false}}
      />
      <LoginStack.Screen
        name="AgePermission"
        component={AgePermission}
        options={{headerShown: false}}
      />
      <LoginStack.Screen
        name="LocationAccess"
        component={LocationAccess}
        options={{headerShown: false}}
      />
      <LoginStack.Screen
        name="VerifyOtp"
        component={VerifyOtp}
        options={{headerShown: false}}
      />
    </LoginStack.Navigator>
  );

  const normalStackScreens = ({navigation}) => (
    <normalStack.Navigator>
      <normalStack.Screen
        name="Home"
        component={Home}
        options={{headerShown: false}}
      />
      <normalStack.Screen
        name="Products"
        component={Products}
        options={{headerShown: false}}
      />
      <normalStack.Screen
        name="ProductDetails"
        component={ProductDetails}
        options={{headerShown: false}}
      />
      <normalStack.Screen
        name="Cart"
        component={Cart}
        options={{headerShown: false}}
      />
      <normalStack.Screen
        name="Rate"
        component={Rate}
        options={{headerShown: false}}
      />
      <normalStack.Screen
        name="UserOrder"
        component={UserOrder}
        options={{headerShown: false}}
      />
      <normalStack.Screen
        name="ContactUs"
        component={ContactUs}
        options={{headerShown: false}}
      />
      <normalStack.Screen
        name="UserOrderDetails"
        component={UserOrderDetails}
        options={{headerShown: false}}
      />
      <normalStack.Screen
        name="Notifications"
        component={Notifications}
        options={{headerShown: false}}
      />
      <normalStack.Screen
        name="FAQ"
        component={FAQ}
        options={{headerShown: false}}
      />
      <normalStack.Screen
        name="EditProfile"
        component={EditProfile}
        options={{headerShown: false}}
      />
      <normalStack.Screen
        name="ChangePassword"
        component={ChangePassword}
        options={{headerShown: false}}
      />
      <normalStack.Screen
        name="Favorites"
        component={Favorites}
        options={{headerShown: false}}
      />
      <normalStack.Screen
        name="Account"
        component={Account}
        options={{headerShown: false}}
      />
      <normalStack.Screen
        name="Dispensaries"
        component={Dispensaries}
        options={{headerShown: false}}
      />
      <normalStack.Screen
        name="DispensariesDetail"
        component={DispensariesDetail}
        options={{headerShown: false}}
      />
      <normalStack.Screen
        name="Brands"
        component={Brands}
        options={{headerShown: false}}
      />
      <normalStack.Screen
        name="Deals"
        component={Deals}
        options={{headerShown: false}}
      />
      <normalStack.Screen
        name="DealsDetail"
        component={DealsDetail}
        options={{headerShown: false}}
      />
      <normalStack.Screen
        name="TermsConditions"
        component={TermsConditions}
        options={{headerShown: false}}
      />
      <normalStack.Screen
        name="OtherCategory"
        component={OtherCategory}
        options={{headerShown: false}}
      />
      <normalStack.Screen
        name="ShippingAddress"
        component={ShippingAddress}
        options={{headerShown: false}}
      />
      <normalStack.Screen
        name="AddAddress"
        component={AddAddress}
        options={{headerShown: false}}
      />
      <normalStack.Screen
        name="PaymentMethod"
        component={PaymentMethod}
        options={{headerShown: false}}
      />
    </normalStack.Navigator>
  );

  const UserDrawerScreen = ({navigation}) => (
    <UserDrawer.Navigator
      drawerContent={(props) => <CustomerDrawer {...props} />}
      drawerType="slide"
      drawerStyle={{
        width: '85%',
        marginTop: 0,
      }}>
      <UserDrawer.Screen
        name="Home"
        component={normalStackScreens}
        options={{headerShown: false}}
      />
    </UserDrawer.Navigator>
  );

  return (
    <DarkModeProvider>
      <NavigationContainer ref={navigationRef} theme={theme}>
        {/* {displayIntroSlider ? <IntroStackScreen /> : _.isEmpty(userDataValue) ? <LoginStackScreen /> : _.toUpper(uType) === 'DRIVER' ? <DriverBottomTab /> : <UserDrawerScreen />} */}
        {displayIntroSlider ? (
          <IntroStackScreen />
        ) : _.isEmpty(userDataValue) ? (
          <LoginStackScreen />
        ) : _.toUpper(uType) === 'DRIVER' ? (
          <DriverBottomTab />
        ) : (
          <UserBottomTab />
        )}
      </NavigationContainer>

      <PushNotification navigation={navigationRef} />
      <NoInternet />
    </DarkModeProvider>
  );
}

export default App;
