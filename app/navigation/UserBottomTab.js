import React from 'react';
import {View, StyleSheet} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import _ from 'lodash';
import Icon from 'react-native-vector-icons/Ionicons';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

// Common Screen For Both Driver & Normal User //
import FAQ from '../screens/CommomScreen/FAQ';
import EditProfile from '../screens/CommomScreen/EditProfile';
import ChangePassword from '../screens/CommomScreen/ChangePassword';
import TermsConditions from '../screens/CommomScreen/TermsConditions';

// Normal User Screens //
import Home from '../screens/NormalUser/Home/Home';
import Cart from '../screens/NormalUser/Cart/Cart';
import Rate from '../screens/NormalUser/Rate/Rate';
import UserOrder from '../screens/NormalUser/Order/UserOrder';
import Products from '../screens/NormalUser/Products/Products';
import ContactUs from '../screens/NormalUser/ContactUs/ContactUs';
import UserOrderDetails from '../screens/NormalUser/Order/UserOrderDetails';
import Notifications from '../screens/NormalUser/Notifications/Notifications';
import DispensariesDetail from '../screens/NormalUser/Dispensaries/DispensariesDetail';

import Favorites from '../screens/NormalUser/Favorites/Favorites';
import Account from '../screens/NormalUser/Account/Account';
import Brands from '../screens/NormalUser/Brands/Brands';
import Deals from '../screens/NormalUser/Deals/Deals';
import OtherCategory from '../screens/NormalUser/OtherCategory/OtherCategory';
import Dispensaries from '../screens/NormalUser/Dispensaries/Dispensaries';
import ProductDetails from '../screens/NormalUser/Products/ProductDetails';
import ShippingAddress from '../screens/NormalUser/ShippingAddress/ShippingAddress';
import AddAddress from '../screens/NormalUser/AddAddress/AddAddress';
import PaymentMethod from '../screens/NormalUser/PaymentMethod/PaymentMethod';
import DealsDetail from '../screens/NormalUser/Deals/DealsDetail';
import FavItemList from '../screens/NormalUser/Favorites/FavItemList';

import OrderPlaced from '../screens/NormalUser/Cart/OrderPlaced';

import {BaseColor} from '../config/theme';
import Text from '../components/Text/index';

const styles = StyleSheet.create({
  badgeCon: {
    top: 0,
    right: 0,
    width: 14,
    height: 14,
    borderRadius: 7,
    alignItems: 'center',
    position: 'absolute',
    justifyContent: 'center',
    backgroundColor: BaseColor.ThemeOrange,
  },
});

function UserBottomTab() {
  const getActiveRouteName = (state) => {
    const route = state.routes[state.index];
    if (route.state) {
      return getActiveRouteName(route.state);
    }
    return route.name;
  };

  const UserStack = createStackNavigator();
  const UserTabs = createBottomTabNavigator();

  const UserHomeStack = createStackNavigator();
  const UserOrderStack = createStackNavigator();
  const UserCartStack = createStackNavigator();
  const UserNotificationStack = createStackNavigator();
  const UserSettingStack = createStackNavigator();

  const UserHomeStackScreen = ({navigation}) => (
    <UserHomeStack.Navigator>
      <UserHomeStack.Screen
        name="Home"
        component={Home}
        options={{headerShown: false}}
      />
      <UserHomeStack.Screen
        name="Rate"
        component={Rate}
        options={{headerShown: false}}
      />
      <UserHomeStack.Screen
        name="Products"
        component={Products}
        options={{headerShown: false}}
      />
      <UserHomeStack.Screen
        name="DispensariesDetail"
        component={DispensariesDetail}
        options={{headerShown: false}}
      />
      <UserHomeStack.Screen
        name="Favorites"
        component={Favorites}
        options={{headerShown: false}}
      />
      <UserHomeStack.Screen
        name="Brands"
        component={Brands}
        options={{headerShown: false}}
      />
      <UserHomeStack.Screen
        name="Deals"
        component={Deals}
        options={{headerShown: false}}
      />
      <UserHomeStack.Screen
        name="OtherCategory"
        component={OtherCategory}
        options={{headerShown: false}}
      />
      <UserHomeStack.Screen
        name="Dispensaries"
        component={Dispensaries}
        options={{headerShown: false}}
      />
      <UserHomeStack.Screen
        name="ProductDetails"
        component={ProductDetails}
        options={{headerShown: false}}
      />
      <UserHomeStack.Screen
        name="DealsDetail"
        component={DealsDetail}
        options={{headerShown: false}}
      />
    </UserHomeStack.Navigator>
  );

  const UserOrderStackScreen = ({navigation}) => (
    <UserOrderStack.Navigator>
      <UserOrderStack.Screen
        name="UserOrder"
        component={UserOrder}
        options={{headerShown: false}}
      />
      <UserOrderStack.Screen
        name="UserOrderDetails"
        component={UserOrderDetails}
        options={{headerShown: false}}
      />
    </UserOrderStack.Navigator>
  );

  const UserCartStackScreen = ({navigation}) => (
    <UserCartStack.Navigator>
      <UserCartStack.Screen
        name="Cart"
        component={Cart}
        options={{headerShown: false}}
      />
      <UserCartStack.Screen
        name="OrderPlaced"
        component={OrderPlaced}
        options={{headerShown: false}}
      />
      <UserCartStack.Screen
        name="AddAddress"
        component={AddAddress}
        options={{headerShown: false}}
      />
      <UserHomeStack.Screen
        name="Dispensaries"
        component={Dispensaries}
        options={{headerShown: false}}
      />
      <UserCartStack.Screen
        name="DispensariesDetail"
        component={DispensariesDetail}
        options={{headerShown: false}}
      />
      <UserCartStack.Screen
        name="ProductDetails"
        component={ProductDetails}
        options={{headerShown: false}}
      />
    </UserCartStack.Navigator>
  );

  const UserNotificationStackScreen = ({navigation}) => (
    <UserNotificationStack.Navigator>
      <UserNotificationStack.Screen
        name="Notifications"
        component={Notifications}
        options={{headerShown: false}}
      />
    </UserNotificationStack.Navigator>
  );

  const UserSettingStackScreen = ({navigation}) => (
    <UserSettingStack.Navigator>
      <UserSettingStack.Screen
        name="Account"
        component={Account}
        options={{headerShown: false}}
      />
      <UserSettingStack.Screen
        name="TermsConditions"
        component={TermsConditions}
        options={{headerShown: false}}
      />
      <UserSettingStack.Screen
        name="EditProfile"
        component={EditProfile}
        options={{headerShown: false}}
      />
      <UserSettingStack.Screen
        name="ChangePassword"
        component={ChangePassword}
        options={{headerShown: false}}
      />
      <UserSettingStack.Screen
        name="ShippingAddress"
        component={ShippingAddress}
        options={{headerShown: false}}
      />
      <UserSettingStack.Screen
        name="PaymentMethod"
        component={PaymentMethod}
        options={{headerShown: false}}
      />
      <UserSettingStack.Screen
        name="ContactUs"
        component={ContactUs}
        options={{headerShown: false}}
      />
      <UserSettingStack.Screen
        name="FAQ"
        component={FAQ}
        options={{headerShown: false}}
      />
      <UserSettingStack.Screen
        name="FavItemList"
        component={FavItemList}
        options={{headerShown: false}}
      />
      <UserCartStack.Screen
        name="AddAddress"
        component={AddAddress}
        options={{headerShown: false}}
      />
    </UserSettingStack.Navigator>
  );

  const getTabBarVisibility = (route) => {
    const routeIndex = route.state && route.state.index ? route.state.index : 0;
    if (routeIndex >= 1) {
      return false;
    }
    return true;
  };

  const UserProviderTabsScreen = () => {
    const notificationCount = useSelector((uState) => uState.auth.notificationCount);
    const cartBadge = useSelector((uState) => uState.auth.cartBadge);
    return (
      <UserTabs.Navigator
        screenOptions={({route}) => ({
          tabBarIcon: ({focused, color, size}) => {
            let iconName;
            if (route.name === 'Home') {
              iconName = focused ? 'home' : 'home-outline';
            } else if (route.name === 'Orders') {
              iconName = focused ? 'gift' : 'gift-outline';
            } else if (route.name === 'Cart') {
              iconName = focused ? 'cart' : 'cart-outline';
            } else if (route.name === 'Notification') {
              iconName = focused ? 'notifications' : 'notifications-outline';
            } else if (route.name === 'Settings') {
              iconName = focused ? 'settings' : 'settings-outline';
            }

            if (route.name === 'Notification' && notificationCount > 0) {
              return (
                <View style={{position: 'relative'}}>
                  <Icon name={iconName} size={size} color={color} />
                  <View style={styles.badgeCon}>
                    <Text
                      bold
                      numberOfLines={1}
                      textAlignCenter
                      caption2
                      whiteColor
                      style={{
                        fontSize: 8,
                      }}
                    >
                      {notificationCount}
                    </Text>
                  </View>
                </View>
              );
            }

            if (route.name === 'Cart' && cartBadge > 0) {
              return (
                <View style={{position: 'relative'}}>
                  <Icon name={iconName} size={size} color={color} />
                  <View style={styles.badgeCon}>
                    <Text
                      bold
                      numberOfLines={1}
                      textAlignCenter
                      caption2
                      whiteColor
                      style={{
                        fontSize: 8,
                      }}
                    >
                      {cartBadge}
                    </Text>
                  </View>
                </View>
              );
            }

            return <Icon name={iconName} size={size} color={color} />;
          },
        })}
        tabBarOptions={{
          activeTintColor: BaseColor.ThemeOrange,
          inactiveTintColor: BaseColor.grayColor,
          keyboardHidesTabBar: true,
          showLabel: true,
          activeBackgroundColor: BaseColor.WhiteColor,
          inactiveBackgroundColor: BaseColor.WhiteColor,
          allowFontScaling: false,
        }}>
        <UserTabs.Screen
          options={({route}) => ({
            tabBarVisible: getTabBarVisibility(route),
            unmountOnBlur: true,
          })}
          name="Home"
          component={UserHomeStackScreen}
        />
        <UserTabs.Screen
          options={({route}) => ({
            tabBarVisible: getTabBarVisibility(route),
            unmountOnBlur: true,
          })}
          name="Orders"
          component={UserOrderStackScreen}
        />
        <UserTabs.Screen
          options={({route}) => ({
            tabBarVisible: getTabBarVisibility(route),
            unmountOnBlur: true,
          })}
          name="Cart"
          component={UserCartStackScreen}
        />
        <UserTabs.Screen
          options={({route}) => ({
            tabBarVisible: getTabBarVisibility(route),
            unmountOnBlur: true,
          })}
          name="Notification"
          component={UserNotificationStackScreen}
        />
        <UserTabs.Screen
          options={({route}) => ({
            tabBarVisible: getTabBarVisibility(route),
            unmountOnBlur: true,
          })}
          name="Settings"
          component={UserSettingStackScreen}
        />
      </UserTabs.Navigator>
    );
  }

  const UserStackNavigator = () => {
    return (
      <UserStack.Navigator>
        <UserStack.Screen
          name="Home"
          component={UserProviderTabsScreen}
          options={{headerShown: false}}
        />
      </UserStack.Navigator>
    );
  };

  return <UserStackNavigator />;
}

export default UserBottomTab;
