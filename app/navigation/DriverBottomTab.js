import React from 'react';
import _ from 'lodash';
import Icon from 'react-native-vector-icons/Ionicons';
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

// Driver App Screens //
import Feeds from '../screens/Driver/Feeds/Feeds';
import Orders from '../screens/Driver/Orders/Orders';
import Settings from '../screens/Driver/Settings/Settings';
import OrderDetails from '../screens/Driver/Orders/OrderDetails';
import DriverGuideLine from '../screens/Driver/Settings/DriverGuideLine';

// Common Screen For Both Driver & Normal User //
import FAQ from '../screens/CommomScreen/FAQ';
import EditProfile from "../screens/CommomScreen/EditProfile";
import ChangePassword from "../screens/CommomScreen/ChangePassword";

import { BaseColor } from "../config/theme";

function DriverBottomTab() {
  const getActiveRouteName = (state) => {
    const route = state.routes[state.index];
    if (route.state) {
      return getActiveRouteName(route.state);
    }
    return route.name;
  };

  const DriverStack = createStackNavigator();
  const DriverTabs = createBottomTabNavigator();
  const DriverFeedStack = createStackNavigator();
  const DriverOrderStack = createStackNavigator();
  const DriverSettingStack = createStackNavigator();

  const DriverFeedStackScreen = ({ navigation }) => (
    <DriverFeedStack.Navigator>
      <DriverFeedStack.Screen
        name="Feeds"
        component={Feeds}
        options={{ headerShown: false }}
      />
      <DriverFeedStack.Screen
        name="OrderDetails"
        component={OrderDetails}
        options={{ headerShown: false }}
      />
    </DriverFeedStack.Navigator>
  );

  const DriverOrderStackScreen = ({ navigation }) => (
    <DriverOrderStack.Navigator>
      <DriverOrderStack.Screen
        name="Orders"
        component={Orders}
        options={{ headerShown: false }}
      />
      <DriverOrderStack.Screen
        name="OrderDetails"
        component={OrderDetails}
        options={{ headerShown: false }}
      />
    </DriverOrderStack.Navigator>
  );

  const DriverSettingStackScreen = ({ navigation }) => (
    <DriverSettingStack.Navigator>
      <DriverSettingStack.Screen
        name="Settings"
        component={Settings}
        options={{ headerShown: false }}
      />
      <DriverSettingStack.Screen
        name="EditProfile"
        component={EditProfile}
        options={{ headerShown: false }}
      />
      <DriverSettingStack.Screen
        name="ChangePassword"
        component={ChangePassword}
        options={{ headerShown: false }}
      />
      <DriverSettingStack.Screen
        name="FAQ"
        component={FAQ}
        options={{ headerShown: false }}
      />
      <DriverSettingStack.Screen
        name="DriverGuideLine"
        component={DriverGuideLine}
        options={{ headerShown: false }}
      />
    </DriverSettingStack.Navigator>
  );

  const getTabBarVisibility = (route) => {
    const routeIndex = route.state && route.state.index ? route.state.index : 0;
    if (routeIndex >= 1) {
      return false;
    }
    return true;
  };

  const DriverProviderTabsScreen = () => (
    <DriverTabs.Navigator
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused, color, size }) => {
          let iconName;
          if (route.name === 'Job Feed') {
            iconName = focused ? 'grid' : 'grid-outline';
          } else if (route.name === 'Orders') {
            iconName = focused ? 'cart' : 'cart-outline';
          } else if (route.name === 'Settings') {
            iconName = focused ? 'settings' : 'settings-outline';
          }
          return (
            <Icon name={iconName} size={size} color={color} />
          );
        },
      })}
      tabBarOptions={{
        activeTintColor: BaseColor.ThemeOrange,
        inactiveTintColor: BaseColor.grayColor,
        keyboardHidesTabBar: true,
        showLabel: true,
        activeBackgroundColor: BaseColor.WhiteColor,
        inactiveBackgroundColor: BaseColor.WhiteColor,
        allowFontScaling: false,
      }}>
      <DriverTabs.Screen
        options={({ route }) => ({
          tabBarVisible: getTabBarVisibility(route),
          unmountOnBlur: true,
        })}
        name="Job Feed"
        component={DriverFeedStackScreen}
      />
      <DriverTabs.Screen
        options={({ route }) => ({
          tabBarVisible: getTabBarVisibility(route),
          unmountOnBlur: true,
        })}
        name="Orders"
        component={DriverOrderStackScreen}
      />
      <DriverTabs.Screen
        options={({ route }) => ({
          tabBarVisible: getTabBarVisibility(route),
          unmountOnBlur: true,
        })}
        name="Settings"
        component={DriverSettingStackScreen}
      />
    </DriverTabs.Navigator>
  );

  const DriverStackNavigator = () => {
    return (
      <DriverStack.Navigator>
        <DriverStack.Screen
          name="Feed"
          component={DriverProviderTabsScreen}
          options={{ headerShown: false }}
        />
      </DriverStack.Navigator>
    );
  };

  return (
    <DriverStackNavigator />
  );

}

export default DriverBottomTab;