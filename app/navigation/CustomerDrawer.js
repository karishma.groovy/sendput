/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect} from 'react';
import {DrawerItem} from '@react-navigation/drawer';
import {connect, useDispatch, useSelector} from 'react-redux';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/Ionicons';
import _ from 'lodash';
import {
  View,
  Image,
  ScrollView,
  StyleSheet,
  ActivityIndicator,
} from 'react-native';
import CAlert from '../components/CAlert';
import {BaseColor} from '../config/theme';
import Text from '../components/Text/index';
import {drawerList} from '../data/StaticData';
import {BaseSetting} from '../config/setting';
import {getApiData} from '../utils/apiHelper';
import {checkObject} from '../utils/commonFunction';
import authActions from '../redux/reducers/auth/actions';
import {StaticAlertMsg, StaticHeader} from '../config/StaticAlertMsg';

const {clearLocalStorageData, setLogoutLoad} = authActions;

const styles = StyleSheet.create({
  main: {
    flex: 1,
  },
  otherCon: {
    flexGrow: 1,
  },
});

export default function CustomerDrawer(props) {
  const {isConnected, logoutLoad, userData, uuid} = useSelector(
    (state) => state.auth,
  );
  const dispatch = useDispatch();

  const userToken = checkObject(userData, 'access_token');

  const header = {
    Authorization: `Bearer ${userToken}`,
  };

  async function logoutAction() {
    if (isConnected === true) {
      const logoutData = {
        uuid: uuid,
      };

      try {
        let endPoint = `${BaseSetting.endpoints.logout}?uuid=${uuid}`;
        const response = await getApiData(
          props.navigation,
          endPoint,
          'GET',
          null,
          header,
        );
        if (
          _.isObject(response) &&
          !_.isEmpty(response) &&
          response.success === true
        ) {
          backToLogin();
        } else {
          const errorMessage = checkObject(response, 'message');
          CAlert(errorMessage, StaticHeader.Oops, () => {
            dispatch(setLogoutLoad(false));
          });
        }
      } catch (err) {
        console.log('Catch Part', err);
        CAlert(StaticAlertMsg.someThing_Went_Wrong, StaticHeader.Oops, () => {
          dispatch(setLogoutLoad(false));
        });
      }
    } else {
      CAlert(StaticAlertMsg.Network_Problem, StaticHeader.Oops, () => {
        dispatch(setLogoutLoad(false));
      });
    }
  }

  function backToLogin() {
    dispatch(clearLocalStorageData());
    setTimeout(() => {
      props.navigation.closeDrawer();
      props.navigation.navigate('WelCome');
    }, 2000);
  }

  function routeScreen(obj) {
    const screenName = checkObject(obj, 'screenName');
    const title = checkObject(obj, 'title');

    if (screenName === 'Logout') {
      CAlert(
        StaticAlertMsg.logout,
        StaticHeader.Confirm,
        () => {
          dispatch(setLogoutLoad(true));
          logoutAction();
        },
        () => {
          return null;
        },
        'YES',
        'NO',
      );
    }
    if (title === 'Terms & Condition' || title === 'About Us') {
      props.navigation.closeDrawer();
      props.navigation.navigate(screenName, title);
    } else {
      props.navigation.closeDrawer();
      props.navigation.navigate(screenName);
    }
  }

  return (
    <View style={styles.main}>
      <LinearGradient
        start={{x: 0.3, y: 0.25}}
        end={{x: 0.8, y: 1.0}}
        colors={[BaseColor.ThemeOrange, BaseColor.ThemeYellow]}
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
          paddingHorizontal: 15,
          paddingTop: 40,
          paddingBottom: 15,
        }}>
        <Image
          style={{
            width: 60,
            height: 60,
            borderRadius: 30,
          }}
          source={{
            uri:
              'https://images.unsplash.com/photo-1495231916356-a86217efff12?ixlib=rb-1.2.1',
          }}
        />
        <View
          style={{
            flex: 1,
            alignItems: 'flex-start',
            justifyContent: 'center',
            paddingHorizontal: 10,
          }}>
          <Text body1 whiteColor>
            {'Groovy Web LLP'}
          </Text>
          <Text subhead whiteColor>
            {'View Profile'}
          </Text>
        </View>
      </LinearGradient>
      <ScrollView
        bounces={false}
        contentContainerStyle={styles.otherCon}
        showsVerticalScrollIndicator={false}>
        {drawerList.map((obj, index) => {
          const screenName = checkObject(obj, 'screenName');
          const isLogout = screenName === 'Logout';
          return (
            <DrawerItem
              key={index}
              style={{
                marginBottom: drawerList.length - 1 === index ? 100 : 2,
              }}
              icon={({color, size}) => {
                if (isLogout && logoutLoad) {
                  return (
                    <ActivityIndicator
                      size={'small'}
                      color={BaseColor.ThemeOrange}
                      animating
                    />
                  );
                } else {
                  return (
                    <Icon
                      name={obj.iconName}
                      color={BaseColor.blackColor}
                      size={size}
                    />
                  );
                }
              }}
              label={({focused, color}) => {
                return (
                  <Text numberOfLines={1} subhead blackColor>
                    {obj.title}
                  </Text>
                );
              }}
              onPress={
                logoutLoad
                  ? null
                  : () => {
                      routeScreen(obj);
                    }
              }
            />
          );
        })}
      </ScrollView>
    </View>
  );
}
