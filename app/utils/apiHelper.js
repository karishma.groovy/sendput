import _ from 'lodash';
import Bugsnag from '@bugsnag/react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {store} from '../redux/store/configureStore';
import AuthActions from '../redux/reducers/auth/actions';
import {BaseSetting} from '../config/setting';

async function logOut(navigation) {
  const keys = ['token', 'userData'];
  try {
    await AsyncStorage.multiRemove(keys);
    store.dispatch(AuthActions.clearLocalStorageData());
    navigation.navigate('Login');
  } catch (e) {
    console.log(e);
    navigation.navigate('Login');
  }
}

export function getApiData(navigation, endpoint, method, data, headers) {
  return new Promise((resolve, reject) => {
    let query = '';
    let qs = '';
    // for (key in data) {
    //   query += `${encodeURIComponent(key)}=${encodeURIComponent(data[key])}&`;
    // }
    const params = {};
    params.method = method.toLowerCase() === 'get' ? 'get' : 'post';

    if (headers) {
      const obj = headers;
      obj['cache-control'] =
        'no-store, no-cache, must-revalidate, post-check=0, pre-check=0';
      params.headers = obj;
      console.log('obj===>>', obj);
    } else {
      params.headers = {
        'Content-Type': 'application/json',
        'cache-control':
          'no-store, no-cache, must-revalidate, post-check=0, pre-check=0',
      };
    }
    console.log(params.headers);
    if (params.method === 'post') {
      if (
        params.headers &&
        params.headers['Content-Type'] &&
        params.headers['Content-Type'] === 'application/json'
      ) {
        params.body = JSON.stringify(data);
      } else {
        params.body = query;
      }
    } else {
      qs = `?${query}`;
    }

    console.log(params);

    if (
      params.method === 'post' &&
      params.headers &&
      params.headers['Content-Type'] &&
      params.headers['Content-Type'] === 'application/json'
    ) {
      console.log(JSON.stringify(data));
    } else {
      let str = '';
      if (data && Object.keys(data).length > 0) {
        Object.keys(data).map((dk) => {
          str += `${dk}:${data[dk]}\n`;
        });
      }
      console.log(str);
    }
    console.log('params', params);
    console.log(
      '=============================================================================================',
    );
    console.log(BaseSetting.api + endpoint + qs);
    console.log(
      '=============================================================================================',
    );

    let url = BaseSetting.api + endpoint + qs;
    let length = url.length;
    if (url.charAt(length - 1) === "?") url = url.slice(0, length - 1);

    fetch(url, params)
      .then((response) => response.json())
      .then((resposeJson) => {
        console.log(resposeJson);
        if (
          _.isObject(resposeJson) &&
          _.has(resposeJson, 'status') &&
          _.toNumber(resposeJson.status) === 401
        ) {
          logOut(navigation);
        } else {
          resolve(resposeJson);
        }
      })
      .catch((err) => {
        console.log(err);
        reject(err);
        Bugsnag.notify(err, function (report) {
          report.metadata = {
            data: {
              endpoint,
              qs,
              params,
            },
          };
        });
      });
  });
}

export function getApiDataProgress(
  navigation,
  endpoint,
  method,
  data,
  headers = null,
  onProgress = null,
) {
  return new Promise((resolve, reject) => {
    const url = BaseSetting.api + endpoint;
    const oReq = new XMLHttpRequest();
    oReq.upload.addEventListener('progress', (event) => {
      if (event.lengthComputable) {
        const progress = (event.loaded * 100) / event.total;
        if (onProgress) {
          onProgress(progress);
        }
      }
    });
    let str = '';
    if (data && Object.keys(data).length > 0) {
      Object.keys(data).map((dk) => {
        str += `${dk}:${data[dk]}\n`;
      });
    }
    console.log(str);
    const query = new FormData();
    if (data && Object.keys(data).length > 0) {
      Object.keys(data).map((k) => query.append(k, data[k]));
    }
    const params = query;
    console.log('query', JSON.stringify(query));
    oReq.open(method, url, true);
    console.log('params', JSON.stringify(params));
    console.log(
      '=============================================================================================',
    );
    console.log(url);
    console.log(
      '=============================================================================================',
    );
    oReq.setRequestHeader('Content-Type', 'multipart/form-data');
    oReq.setRequestHeader(
      'cache-control',
      'no-store, no-cache, must-revalidate, post-check=0, pre-check=0',
    );
    if (_.isObject(headers)) {
      Object.keys(headers).map((hK) => {
        oReq.setRequestHeader(hK, headers[hK]);
      });
    }
    oReq.send(params);
    oReq.onreadystatechange = () => {
      if (oReq.readyState === XMLHttpRequest.DONE) {
        try {
          console.log(oReq.responseText);
          const resposeJson = JSON.parse(oReq.responseText);
          console.log(resposeJson);
          if (
            _.isObject(resposeJson) &&
            _.has(resposeJson, 'status') &&
            _.toNumber(resposeJson.status) === 401
          ) {
            logOut(navigation);
          } else {
            resolve(resposeJson);
          }
        } catch (exe) {
          console.log(exe);
          reject(exe);
          Bugsnag.notify(exe);
        }
      }
    };
  });
}
