
import _ from 'lodash';
import { LayoutAnimation } from 'react-native';
import { UIManager } from 'react-native';
import { BaseColor } from '../config';
import { store } from '../redux/store/configureStore';

export function getAPIHeaderValue() {
  const { auth: { userData } } = store.getState();

  const userToken = checkObject(userData, 'access_token');

  const header = {
    Authorization: `Bearer ${userToken}`,
  };

  return header;
}

export function getTimeStempFromDate(dateTimeValue) {
  const dateTimeParts = dateTimeValue.split(' ');
  const timeParts = dateTimeParts[1].split(':');
  const dateParts = dateTimeParts[0].split('-');

  const date = new Date(
    dateParts[0],
    parseInt(dateParts[1], 10) - 1,
    dateParts[2],
    timeParts[0],
    timeParts[1],
  );
  const timeStempValue = date.getTime() / 1000;
  return timeStempValue;
}

export function checkObject(data, str) {
  if (_.has(data, str)) {
    return data[str];
  } else {
    return '-';
  }
}

export function getNumPriceFromStrToNum(price) {
  var result = price.replace('$',' ').replace(',','').replace('USD', '');
  var finalStr = result.trim();
  var finalPrice = parseFloat(finalStr);

  return finalPrice;
}

export function priceFormat(value) {
  return new Number(value).toLocaleString("en-US");
}

export function getUserCurrentLatLog() {
  const { auth: { locationsData } } = store.getState();
  const currentLat = locationsData && locationsData.geometry && locationsData.geometry.location && locationsData.geometry.location.lat ? locationsData.geometry.location.lat : 0;
  const currentLong = locationsData && locationsData.geometry && locationsData.geometry.location && locationsData.geometry.location.lng ? locationsData.geometry.location.lng : 0;

  const locationData = {
    currentLat,
    currentLong,
  };

  return locationData;
}

export function getOrderStatusClr(str) {
  const isOrderPending = str === 'pending';
  const isOrderAssigned = str === 'assigned';
  const isOrderOnGoing = str === 'ongoing';
  const isOrderPicked = str === 'picked';
  const isOrderDelivered = str === 'delivered';
  const isOrderCancelled = str === 'cancelled';
  const isOrderRejected = str === 'rejected';

  let colorCode = BaseColor.grayColor;

  if (isOrderCancelled || isOrderRejected) {
    colorCode = BaseColor.ThemeRedColor;
  } else {
    colorCode = BaseColor.greenColor;
  }

  return colorCode;
}

export const enableAnimateInEaseOut = () => {
  if (Platform.OS === "android") {
    UIManager.setLayoutAnimationEnabledExperimental(true);
  }
  LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
};

export const enableAnimateLinear = () => {
  if (Platform.OS === 'android') {
    UIManager.setLayoutAnimationEnabledExperimental(true);
  }
  LayoutAnimation.configureNext(LayoutAnimation.Presets.linear);
};

export const enableAnimateSpring = () => {
  if (Platform.OS === 'android') {
    UIManager.setLayoutAnimationEnabledExperimental(true);
  }

  LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
};
