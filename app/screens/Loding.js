import React, {useCallback, useEffect, useRef, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {
  View,
  ActivityIndicator,
} from 'react-native';
import NetInfo from "@react-native-community/netinfo";
import { useTheme } from "@config";
import authActions from '../redux/reducers/auth/actions';

const {setNetworkStatus} = authActions;

export default function Loding({navigation}) {
  const { colors } = useTheme();
  const dispatch = useDispatch();

  function checkNetWorkStatus() {
    NetInfo.addEventListener((state) => {
      dispatch(setNetworkStatus(state.isConnected));
    });
  }
  
  React.useEffect(
    () => navigation.addListener('focus', () => {
      checkNetWorkStatus();
    }),
    []
  );

  return (
    <View style={{
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
    }}>
      <ActivityIndicator size={'large'} color={colors.primary} animating/>
    </View>
  );
}
