import React, {useCallback, useEffect, useRef, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import _ from 'lodash';
import {
  View,
  Image,
  FlatList,
  StyleSheet,
  RefreshControl,
  ActivityIndicator,
} from 'react-native';
import CAlert from '../../../components/CAlert';
import {BaseColor} from '../../../config/theme';
import Text from '../../../components/Text/index';
import {BaseSetting} from '../../../config/setting';
import {getApiData} from '../../../utils/apiHelper';
import {checkObject} from '../../../utils/commonFunction';
import GredientHeader from '../../../components/DriverApp/GredientHeader';
import {StaticAlertMsg, StaticHeader} from '../../../config/StaticAlertMsg';
import {DealsLoader} from '../../../components/ContentLoader/ContentLoader';
import COffer from '../../../components/COffer/COffer';

const styles = StyleSheet.create({
  mainCon: {
    flex: 1,
    backgroundColor: BaseColor.whiteColor,
  },
  flexMainCon: {
    flexGrow: 1,
  },
  listFooterView: {
    width: '100%',
    height: 30,
  },
  loaderFooterView: {
    width: '100%',
    padding: 30,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: BaseColor.whiteColor,
  },
});

export default function Deals({navigation}) {
  const {isConnected, userData} = useSelector((state) => state.auth);

  const [refreshLoader, setRefreshLoader] = useState(false);
  const [pageLoader, setPageLoader] = useState(true);
  const [moreLoad, setMoreLoad] = useState(false);
  const [listData, setListData] = useState({});
  const [offerDetails, setOfferDetails] = useState({});

  const mailListRef = useRef();

  const userToken = checkObject(userData, 'access_token');

  const header = {
    Authorization: `Bearer ${userToken}`,
  };

  React.useEffect(
    () =>
      navigation.addListener('focus', () => {
        getList(true);
      }),
    [],
  );

  async function getList(bool) {
    const cPage =
      listData && listData.pagination && listData.pagination.currentPage
        ? _.toNumber(listData.pagination.currentPage)
        : 0;
    if (isConnected === true) {
      let PageNo = 0;
      if (bool === true) {
        PageNo = 1;
      } else {
        PageNo = cPage + 1;
      }

      const lData = {
        page: PageNo,
      };

      try {
        let endPoint = `${BaseSetting.endpoints.offer_list}?page=${PageNo}`;
        const response = await getApiData(
          navigation,
          endPoint,
          'GET',
          null,
          header,
        );
        if (
          _.isObject(response) &&
          !_.isEmpty(response) &&
          response.success === true
        ) {
          const obj = bool ? {} : _.cloneDeep(listData);

          const newListData =
            response && response.data && response.data.rows
              ? response.data.rows
              : [];
          const paginationDatas =
            response && response.data && response.data.pagination
              ? response.data.pagination
              : {};

          if (_.isArray(newListData)) {
            if (_.isArray(obj.data) && obj.data.length > 0) {
              obj.data = _.flattenDeep([...obj.data, newListData]);
            } else {
              obj.data = newListData;
            }
            obj.pagination = paginationDatas;
          }
          setListData(obj);
          handleAllLoader();
        } else {
          const errorMessage = checkObject(response, 'message');
          CAlert(errorMessage, StaticHeader.Oops, () => {
            setListData(listData);
            handleAllLoader();
          });
        }
      } catch (err) {
        console.log('Catch Part', err);
        CAlert(StaticAlertMsg.someThing_Went_Wrong, StaticHeader.Oops, () => {
          handleAllLoader();
        });
      }
    } else {
      CAlert(StaticAlertMsg.Network_Problem, StaticHeader.Oops, () => {
        handleAllLoader();
      });
    }
  }

  function handleAllLoader() {
    setTimeout(() => {
      setPageLoader(false);
      setRefreshLoader(false);
      setMoreLoad(false);
    }, 200);
  }

  async function getMoreData() {
    const cPage =
      listData && listData.pagination && listData.pagination.currentPage
        ? _.toNumber(listData.pagination.currentPage)
        : 0;
    const tPage =
      listData && listData.pagination && listData.pagination.totalPage
        ? _.toNumber(listData.pagination.totalPage)
        : 0;

    if (listData.pagination.isMore === true && cPage < tPage) {
      setMoreLoad(true);
      getList(false);
    }
  }

  function onRefresh() {
    setRefreshLoader(true);
    setPageLoader(true);
    setTimeout(() => {
      getList(true);
    }, 500);
  }

  function renderFooterComponent() {
    if (moreLoad) {
      return (
        <View style={styles.loaderFooterView}>
          <ActivityIndicator
            size={'small'}
            animating
            color={BaseColor.ThemeOrange}
          />
        </View>
      );
    } else {
      return <View style={styles.listFooterView} />;
    }
  }

  function renderEmptyComponent() {
    return (
      <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
        <Text headline>{'Sorry currently store data is empty.'}</Text>
      </View>
    );
  }

  const renderItem = ({item, index}) => {
    return (
      <COffer
        data={item}
        onPress={() => {
          navigation.navigate('DealsDetail', item);
        }}
      />
    );
  };

  function renderList() {
    return (
      <FlatList
        ref={mailListRef}
        data={listData.data}
        scrollsToTop={false}
        numColumns={2}
        horizontal={false}
        renderItem={renderItem}
        refreshing={refreshLoader}
        keyExtractor={(item) => item.id}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={styles.flexMainCon}
        ListFooterComponent={renderFooterComponent}
        ListEmptyComponent={renderEmptyComponent}
        refreshControl={
          <RefreshControl
            colors={[BaseColor.ThemeOrange]}
            tintColor={BaseColor.ThemeOrange}
            refreshing={refreshLoader}
            onRefresh={() => {
              onRefresh();
            }}
          />
        }
        onEndReached={getMoreData}
        onEndReachedThreshold={0.5}
      />
    );
  }

  function renderLoader() {
    return <DealsLoader />;
  }

  function renderHeader() {
    return (
      <GredientHeader
        leftIcon
        title={'Deals'}
        onLeftAction={() => {
          navigation.pop();
        }}
      />
    );
  }

  return (
    <View style={styles.mainCon}>
      {renderHeader()}
      {pageLoader ? renderLoader() : renderList()}
    </View>
  );
}
