import React, {useState} from 'react';
import {useSelector} from 'react-redux';
import _ from 'lodash';
import {
  View,
  Image,
  ScrollView,
  StyleSheet,
  RefreshControl,
} from 'react-native';
import CAlert from '../../../components/CAlert';
import {BaseColor} from '../../../config/theme';
import Text from '../../../components/Text/index';
import {BaseSetting} from '../../../config/setting';
import {getApiData} from '../../../utils/apiHelper';
import {checkObject} from '../../../utils/commonFunction';
import GredientHeader from '../../../components/DriverApp/GredientHeader';
import {StaticAlertMsg, StaticHeader} from '../../../config/StaticAlertMsg';
import {DealsDetailsLoader} from '../../../components/ContentLoader/ContentLoader';
import LinearGradient from 'react-native-linear-gradient';

const styles = StyleSheet.create({
  mainCon: {
    flex: 1,
    backgroundColor: BaseColor.whiteColor,
  },
  scrollCon: {
    flexGrow: 1,
  },
  centeredView: {
    flex: 1,
    alignContent: 'center',
    alignItems: 'center',
  },
  img: {
    height: BaseSetting.nHeight / 3,
    width: BaseSetting.nWidth,
  },
  bottomView: {
    width: '100%',
    flex: 1,
    marginTop: 10,
    padding: 10,
  },
  gradientView: {
    width: '60%',
    alignItems: 'center',
    padding: 15,
    alignSelf: 'center',
    marginVertical: 20,
    borderRadius: 30,
  },
  descView: {
    borderTopWidth: 0.5,
    borderTopColor: BaseColor.grayColor,
    borderBottomColor: BaseColor.grayColor,
    paddingVertical: 10,
    marginTop: 15,
    borderBottomWidth: 0.5,
  },
  metaDescView: {
    borderBottomColor: BaseColor.grayColor,
    paddingVertical: 10,
    borderBottomWidth: 0.5,
  },
  dateView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop: 20,
  },
  ptop10: {
    paddingTop: 10,
  },
});

export default function DealsDetail({route, navigation}) {
  const {isConnected, userData} = useSelector((state) => state.auth);

  const item = route.params;
  const offerId = checkObject(item, 'id');

  const [refreshLoader, setRefreshLoader] = useState(false);
  const [pageLoader, setPageLoader] = useState(true);
  const [offerDetails, setOfferDetails] = useState({});

  const userToken = checkObject(userData, 'access_token');

  const header = {
    Authorization: `Bearer ${userToken}`,
  };

  React.useEffect(
    () =>
      navigation.addListener('focus', () => {
        getOfferDetail();
      }),
    [],
  );

  async function getOfferDetail() {
    const offerData = {
      offer_id: offerId,
    };

    if (isConnected === true) {
      try {
        let endPoint = `${BaseSetting.endpoints.view_offer}?offer_id=${offerId}`;
        const response = await getApiData(
          navigation,
          endPoint,
          'GET',
          null,
          header,
        );
        if (
          _.isObject(response) &&
          !_.isEmpty(response) &&
          response.success === true
        ) {
          const details = checkObject(response, 'data');
          setOfferDetails(details);
          handleAllLoader();
        } else {
          const errorMessage = checkObject(response, 'message');
          CAlert(errorMessage, StaticHeader.Oops, () => {
            handleAllLoader();
          });
        }
      } catch (err) {
        console.log('Catch Part', err);
        CAlert(StaticAlertMsg.someThing_Went_Wrong, StaticHeader.Oops, () => {
          handleAllLoader();
        });
      }
    } else {
      CAlert(StaticAlertMsg.Network_Problem, StaticHeader.Oops, () => {
        handleAllLoader();
      });
    }
  }

  function handleAllLoader() {
    setTimeout(() => {
      setPageLoader(false);
      setRefreshLoader(false);
    }, 200);
  }

  function onRefresh() {
    setRefreshLoader(true);
    setPageLoader(true);
    setTimeout(() => {
      getOfferDetail();
    }, 500);
  }

  function renderScrollCom() {
    const storeId = checkObject(offerDetails, 'store_id');
    const title = checkObject(offerDetails, 'title');
    const offerCode = checkObject(offerDetails, 'offer_code');
    const startDate = checkObject(offerDetails, 'start_date');
    const endDate = checkObject(offerDetails, 'end_date');
    const discountType = checkObject(offerDetails, 'discount_type');
    const discountValue = checkObject(offerDetails, 'discount_value');
    const offerStatus = checkObject(offerDetails, 'status');
    const offerImg = checkObject(offerDetails, 'img');
    const offerDesc = checkObject(offerDetails, 'description');
    const createdAt = checkObject(offerDetails, 'created_at');
    const updatedAt = checkObject(offerDetails, 'updated_at');
    const metaTitle = checkObject(offerDetails, 'meta_title');
    const metaKeyword = checkObject(offerDetails, 'meta_keyword');
    const metaDescription = checkObject(offerDetails, 'meta_description');

    return (
      <ScrollView
        bounces={false}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={styles.scrollCon}
        refreshControl={
          <RefreshControl
            colors={[BaseColor.ThemeOrange]}
            tintColor={BaseColor.ThemeOrange}
            refreshing={refreshLoader}
            onRefresh={() => {
              onRefresh();
            }}
          />
        }>
        <View style={styles.centeredView}>
          <Image
            source={{
              uri: offerImg,
            }}
            style={styles.img}
          />
          <View style={styles.bottomView}>
            <Text
              textAlignCenter
              bold
              title2
              style={{textDecorationLine: 'underline'}}>
              {title}
            </Text>
            <LinearGradient
              start={{x: 0.3, y: 0.25}}
              end={{x: 0.8, y: 1.0}}
              colors={[BaseColor.ThemeOrange, BaseColor.ThemeYellow]}
              style={styles.gradientView}>
              <Text semibold textAlignLeft body1 whiteColor>
                Offer Code:{' '}
                <Text bold body1 whiteColor>
                  {offerCode}
                </Text>
              </Text>
            </LinearGradient>
            <Text body1 style={styles.ptop10}>
              Discount Value:{' '}
              <Text bold style={{color: BaseColor.ThemeRedColor}}>
                {discountValue}%
              </Text>{' '}
            </Text>
            <View style={styles.dateView}>
              <Text textAlignLeft semibold body2>
                Start Date: {startDate}
              </Text>
              <Text textAlignRight semibold body2>
                End Date: {endDate}
              </Text>
            </View>
            <View style={styles.descView}>
              <Text body2>
                <Text bold body1>
                  Description:
                </Text>
                {offerDesc}
              </Text>
            </View>
            <View style={styles.metaDescView}>
              <Text body2 style={{marginTop: 10}}>
                <Text bold body1>
                  Meta Description :
                </Text>
                {metaKeyword}
              </Text>
            </View>
            <Text style={{paddingVertical: 10}}>{metaDescription}</Text>
          </View>
        </View>
      </ScrollView>
    );
  }

  function renderLoader() {
    return <DealsDetailsLoader />;
  }

  function renderHeader() {
    return (
      <GredientHeader
        leftIcon
        title={'Deals Detail'}
        onLeftAction={() => {
          navigation.pop();
        }}
      />
    );
  }

  return (
    <View style={styles.mainCon}>
      {renderHeader()}
      {pageLoader ? renderLoader() : renderScrollCom()}
    </View>
  );
}
