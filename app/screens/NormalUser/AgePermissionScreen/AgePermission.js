import React from 'react';
import {useDispatch} from 'react-redux';
import {View, Image, StyleSheet} from 'react-native';
import Text from '../../../components/Text';
import {Images} from '../../../config/images';
import {BaseColor} from '../../../config/theme';
import Button from '../../../components/Button/index';
import authActions from '../../../redux/reducers/auth/actions';
import {BaseSetting} from '../../../config';

const {setShowAgePermissonUI} = authActions;

const styles = StyleSheet.create({
  mainCon: {
    flex: 1,
  },
  scrollCon: {
    flex: 1,
    alignItems: 'center',
    paddingHorizontal: 20,
    justifyContent: 'center',
  },
  image: {
    width: BaseSetting.nWidth / 2.5,
    height: BaseSetting.nHeight / 2.5,
    resizeMode: 'contain',
    alignSelf: 'center',
  },
  btnWidth: {
    width: '45%',
  },
  agreeBtn: {
    // borderRadius: 5,
    marginTop: 50,
  },
  paddingT20: {
    paddingTop: 20,
    lineHeight: 20,
    paddingBottom: 10,
  },
  tncTxt: {
    color: BaseColor.ThemeRedColor,
    textDecorationLine: 'underline',
    zIndex: 10,
  },
  declineBtnTxt: {
    color: BaseColor.blackColor,
  },
  cmnFlex: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
});

const AgePermission = ({navigation}) => {
  const dispatch = useDispatch();

  return (
    <View style={styles.mainCon}>
      <View style={styles.scrollCon}>
        <Image source={Images.logoBlackImg} style={styles.image} />
        <Text bold title2>
          Are you 18 or older?
        </Text>
        <Text textAlignCenter body2 style={styles.paddingT20}>
          SendPut requires that you meet the legal age requirements of your
          area to view cannabis information through the SendPut app.
        </Text>

        <View style={styles.cmnFlex}>
          <Text textAlignCenter>
            I confirm that I am 18 or over and that I agree to the SendPut{' '}
            <Text
              style={styles.tncTxt}
              onPress={() => navigation.navigate('TermsConditions')}>
              Terms of Use
            </Text>
            <Text> and </Text>
            <Text
              style={styles.tncTxt}
              onPress={() =>
                navigation.navigate('TermsConditions', 'Privacy Policy')
              }>
              Privacy Policy
            </Text>
          </Text>
        </View>

        <Button
          style={styles.agreeBtn}
          buttonText={'I AGREE'}
          onPress={() => {
            dispatch(setShowAgePermissonUI(false));
            navigation.navigate('LocationAccess');
          }}
        />
        <Button
          styleText={styles.declineBtnTxt}
          style={{
            elevation: 0,
            shadowColor: '#0000',
          }}
          colorAry={[BaseColor.whiteColor, BaseColor.whiteColor]}
          buttonText={'DECLINE'}
          onPress={() => {
            navigation.navigate('WelCome');
          }}
        />
      </View>
    </View>
  );
};

export default AgePermission;
