/* eslint-disable react-native/no-inline-styles */
import React, {useCallback, useEffect, useRef, useState} from 'react';
import MIcon from 'react-native-vector-icons/MaterialIcons';
import LinearGradient from 'react-native-linear-gradient';
import NetInfo from '@react-native-community/netinfo';
import Icon from 'react-native-vector-icons/Ionicons';
import {useDispatch, useSelector} from 'react-redux';
import _ from 'lodash';
import {
  View,
  Image,
  Platform,
  Keyboard,
  FlatList,
  ScrollView,
  StyleSheet,
  Dimensions,
  RefreshControl,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import {BaseColor} from '../../../config/theme';
import CAlert from '../../../components/CAlert';
import Text from '../../../components/Text/index';
import Button from '../../../components/Button';
import {getApiData} from '../../../utils/apiHelper';
import {BaseSetting} from '../../../config/setting';
import TextInput from '../../../components/TextInput/index';
import authActions from '../../../redux/reducers/auth/actions';
import EmptyView from '../../../components/DriverApp/EmptyView';
import CGoogleAutoComplete from '../../../components/CGoogleAutoComplete';
import GredientHeader from '../../../components/DriverApp/GredientHeader';
import {StaticAlertMsg, StaticHeader} from '../../../config/StaticAlertMsg';
import {HomeLoader} from '../../../components/ContentLoader/ContentLoader';
import {
  checkObject,
  enableAnimateInEaseOut,
  getUserCurrentLatLog,
} from '../../../utils/commonFunction';
import {SafeAreaView} from 'react-native';

const {
  setNetworkStatus,
  setUserLocationsData,
  setNotificationBadge,
} = authActions;

const isIOS = Platform.OS === 'ios';
const nWidth = BaseSetting.nWidth;

const styles = StyleSheet.create({
  mainCon: {
    flex: 1,
  },
  searchBarCon: {
    width: '100%',
    height: 55,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    overflow: 'hidden',
    paddingHorizontal: 10,
  },
  searchInputCon: {
    width: '100%',
    height: 40,
    borderRadius: 0,
    backgroundColor: BaseColor.whiteColor,
    borderWidth: 0,
    marginTop: 0,
    borderTopLeftRadius: 5,
    borderBottomLeftRadius: 5,
  },
  searchIconClr: {
    color: BaseColor.grayColor,
  },
  searchBtnSty: {
    height: 40,
    width: '20%',
    borderRadius: 0,
    marginTop: 0,
    borderTopRightRadius: 5,
    borderBottomRightRadius: 5,
  },
  flexMainCon: {
    flexGrow: 1,
  },
  listFooterView: {
    width: '100%',
    height: 30,
  },
  loaderFooterView: {
    width: '100%',
    padding: 30,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: BaseColor.whiteColor,
  },
});

export default function Home({navigation}) {
  const {isConnected, userData, locationsData} = useSelector(
    (state) => state.auth,
  );
  const [refreshLoader, setRefreshLoader] = useState(false);
  const [pageLoader, setPageLoader] = useState(true);
  const [moreLoad, setMoreLoad] = useState(false);
  const [storeList, setStoreList] = useState({});
  const [searchTxt, setSearchTxt] = useState('');
  const [searchBtnLoad, setSearchBtnLoad] = useState(false);
  const [categoryList, setCategoryList] = useState([]);
  const [selectedCatId, setSelectedCatId] = useState({});

  const dispatch = useDispatch();

  const mailListRef = useRef();
  const catListRef = useRef();

  const searchInputRef = useRef('');

  const CGoogleAutoCompleteRef = useRef('');

  const userToken = checkObject(userData, 'access_token');

  const header = {
    Authorization: `Bearer ${userToken}`,
  };

  React.useEffect(
    () =>
      navigation.addListener('focus', () => {
        checkNetWorkStatus();
        setSearchTxt('');
        setTimeout(() => {
          getHomeData();
        }, 100);
      }),
    [],
  );

  useEffect(() => {
    if (_.isObject(selectedCatId) && !_.isEmpty(selectedCatId)) {
      setPageLoader(true);
      setTimeout(() => {
        getStoreList(true);
      }, 1000);
    }
  }, [selectedCatId]);

  function getHomeData() {
    getNotificationBadge();
    getCategoryList();
  }

  async function getNotificationBadge() {
    if (isConnected === true) {
      try {
        let endPoint = `${BaseSetting.endpoints.badge_count}`;
        const response = await getApiData(
          navigation,
          endPoint,
          'GET',
          null,
          header,
        );
        if (
          _.isObject(response) &&
          !_.isEmpty(response) &&
          response.success === true
        ) {
          const responseData = checkObject(response, 'data');
          const badgeNo = checkObject(responseData, 'badge');
          dispatch(setNotificationBadge(badgeNo));
        } else {
          const errorMessage = checkObject(response, 'message');
          CAlert(errorMessage, StaticHeader.Oops);
        }
      } catch (err) {
        console.log('Catch Part', err);
        CAlert(StaticAlertMsg.someThing_Went_Wrong, StaticHeader.Oops);
      }
    } else {
      CAlert(StaticAlertMsg.Network_Problem, StaticHeader.Oops);
    }
  }

  async function getCategoryList() {
    if (isConnected === true) {
      try {
        let endPoint = `${BaseSetting.endpoints.category_list}`;
        const response = await getApiData(
          navigation,
          endPoint,
          'GET',
          null,
          header,
        );
        if (
          _.isObject(response) &&
          !_.isEmpty(response) &&
          response.success === true
        ) {
          const responseData = checkObject(response, 'data');
          const cListData = checkObject(responseData, 'category');
          const obj = {
            id: 1,
            category_name: 'All',
            category_photo: '',
          };
          cListData.unshift(obj);
          setCategoryList(cListData);
          setSelectedCatId(cListData[0]);
        } else {
          const errorMessage = checkObject(response, 'message');
          console.log(errorMessage);
          setCategoryList([]);
        }
      } catch (err) {
        console.log('Catch Part', err);
        setCategoryList([]);
      }
    } else {
      CAlert(StaticAlertMsg.Network_Problem, StaticHeader.Oops, () => {
        handleAllLoader(false);
      });
    }
  }

  async function getSearchStoreList() {
    if (isConnected === true) {
      const storeName = {
        store_name: searchTxt,
      };
      try {
        let endPoint = `${BaseSetting.endpoints.search_store}?store_name=${searchTxt}`;
        const response = await getApiData(
          navigation,
          endPoint,
          'GET',
          null,
          header,
        );
        if (
          _.isObject(response) &&
          !_.isEmpty(response) &&
          response.success === true
        ) {
          const responseData = checkObject(response, 'data');
          responseData.data =
            responseData && responseData.rows ? responseData.rows : [];
          responseData.pagination =
            responseData && responseData.pagination
              ? responseData.pagination
              : {};
          setStoreList(responseData);
          handleAllLoader();
        } else {
          const errorMessage = checkObject(response, 'message');
          CAlert(errorMessage, StaticHeader.Oops, () => {
            handleAllLoader();
          });
        }
      } catch (err) {
        console.log('Catch Part', err);
        CAlert(StaticAlertMsg.someThing_Went_Wrong, StaticHeader.Oops, () => {
          handleAllLoader();
        });
      }
    } else {
      CAlert(StaticAlertMsg.Network_Problem, StaticHeader.Oops, () => {
        handleAllLoader();
      });
    }
  }

  async function getStoreList(bool) {
    const cPage =
      storeList && storeList.pagination && storeList.pagination.currentPage
        ? _.toNumber(storeList.pagination.currentPage)
        : 0;

    const LData = getUserCurrentLatLog();
    const cLat = checkObject(LData, 'currentLat');
    const cLng = checkObject(LData, 'currentLong');

    if (isConnected === true) {
      let PageNo = 0;
      if (bool === true) {
        PageNo = 1;
      } else {
        PageNo = cPage + 1;
      }

      const locationData = {
        page: PageNo,
        lat: cLat,
        lng: cLng,
      };

      if (_.isObject(selectedCatId) && !_.isEmpty(selectedCatId)) {
        const selectedId = checkObject(selectedCatId, 'id');
        // locationData.cat_id = checkObject(selectedCatId, 'id');
        if (selectedId !== 1) {
          locationData.cat_id = selectedId;
        }
      }

      try {
        let endPoint = `${
          BaseSetting.endpoints.store_list
        }?page=${PageNo}&lat=${cLat}&lng=${cLng}&cat_id=${
          checkObject(selectedCatId, 'id') !== 1
            ? checkObject(selectedCatId, 'id')
            : null
        }`;
        const response = await getApiData(
          navigation,
          endPoint,
          'GET',
          null,
          header,
        );
        if (
          _.isObject(response) &&
          !_.isEmpty(response) &&
          response.success === true
        ) {
          const obj = bool ? {} : _.cloneDeep(storeList);

          const newListData =
            response && response.data && response.data.rows
              ? response.data.rows
              : [];

          const paginationDatas =
            response && response.data && response.data.pagination
              ? response.data.pagination
              : {};

          if (_.isArray(newListData)) {
            if (_.isArray(obj.data) && obj.data.length > 0) {
              obj.data = _.flattenDeep([...obj.data, newListData]);
            } else {
              obj.data = newListData;
            }
            obj.pagination = paginationDatas;
          }

          setStoreList(obj);
          handleAllLoader();
        } else {
          const errorMessage = checkObject(response, 'message');
          CAlert(errorMessage, StaticHeader.Oops, () => {
            handleAllLoader();
          });
        }
      } catch (err) {
        console.log('Catch Part', err);
        CAlert(StaticAlertMsg.someThing_Went_Wrong, StaticHeader.Oops, () => {
          handleAllLoader();
        });
      }
    } else {
      CAlert(StaticAlertMsg.Network_Problem, StaticHeader.Oops, () => {
        handleAllLoader();
      });
    }
  }

  function checkNetWorkStatus() {
    NetInfo.addEventListener((state) => {
      dispatch(setNetworkStatus(state.isConnected));
    });
  }

  const renderItem = ({item, index}) => {
    const storeName = checkObject(item, 'store_name');
    const storeImg = checkObject(item, 'store_image');
    const storeAddress = checkObject(item, 'store_address');
    const storeRating = checkObject(item, 'rating');
    const storeDistance = checkObject(item, 'distance');
    return (
      <TouchableOpacity
        key={index}
        activeOpacity={0.7}
        onPress={() => {
          navigation.navigate('DispensariesDetail', item);
        }}
        style={{
          flex: 1,
          padding: 10,
          elevation: 5,
          marginTop: 10,
          borderRadius: 5,
          flexDirection: 'row',
          position: 'relative',
          alignItems: 'flex-start',
          marginHorizontal: 10,
          justifyContent: 'space-between',
          backgroundColor: BaseColor.whiteColor,
          shadowColor: BaseColor.blackColor,
          shadowOffset: {
            width: 0,
            height: 2,
          },
          shadowOpacity: 0.25,
          shadowRadius: 3.84,
        }}>
        <View
          style={{
            width: nWidth / 4,
            height: nWidth / 4,
            borderRadius: 5,
            padding: 5,
            overflow: 'hidden',
            borderWidth: StyleSheet.hairlineWidth,
            borderColor: BaseColor.grayColor,
            backgroundColor: BaseColor.whiteColor,
          }}>
          <Image
            style={{
              width: '100%',
              height: '100%',
              resizeMode: 'contain',
              borderRadius: 5,
            }}
            source={{uri: storeImg}}
          />
        </View>

        <View
          style={{
            flex: 1,
            justifyContent: 'space-between',
            paddingLeft: 10,
            height: nWidth / 4,
          }}>
          <View>
            <Text footnote bold>
              {storeName}
            </Text>
            <Text numberOfLines={2} caption1 grayColor style={{paddingTop: 3}}>
              {storeAddress}
            </Text>
          </View>

          <View style={{paddingVertical: 5}}>
            <View
              style={{
                width: '100%',
                height: StyleSheet.hairlineWidth,
                backgroundColor: '#D3D3D3',
              }}
            />
            <View
              style={{
                marginTop: 8,
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <MIcon name="star" size={15} color={BaseColor.ThemeOrange} />
                <Text caption2 grayColor style={{paddingLeft: 2}}>
                  {storeRating}
                </Text>
              </View>
              <Text
                caption2
                textAlignRight>{`${storeDistance} away from you`}</Text>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  function onRefresh() {
    setRefreshLoader(true);
    setPageLoader(true);
    setTimeout(() => {
      getStoreList(true);
    }, 1000);
  }

  function handleAllLoader() {
    setTimeout(() => {
      setRefreshLoader(false);
      setPageLoader(false);
      setSearchBtnLoad(false);
      setMoreLoad(false);
    }, 100);
  }

  async function getMoreData() {
    const cPage =
      storeList && storeList.pagination && storeList.pagination.currentPage
        ? _.toNumber(storeList.pagination.currentPage)
        : 0;
    const tPage =
      storeList && storeList.pagination && storeList.pagination.totalPage
        ? _.toNumber(storeList.pagination.totalPage)
        : 0;

    if (storeList.pagination.isMore === true && cPage < tPage) {
      setMoreLoad(true);
      getStoreList(false);
    }
  }

  function renderHeaderComponent() {
    return (
      <View style={{paddingVertical: 10}}>
        <FlatList
          ref={catListRef}
          horizontal={true}
          data={categoryList}
          showsHorizontalScrollIndicator={false}
          renderItem={({item, index}) => {
            const aryLength = categoryList.length;
            const isSelected = _.isEqual(selectedCatId, item);
            const catImg = checkObject(item, 'category_photo');
            const catName = checkObject(item, 'category_name');
            const imgUrl =
              catImg !== '-' && !_.isEmpty(catImg)
                ? catImg
                : 'https://images.unsplash.com/photo-1495231916356-a86217efff12?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxleHBsb3JlLWZlZWR8M3x8fGVufDB8fHw%3D&w=1000&q=80';
            return (
              <TouchableOpacity
                key={index}
                onPress={() => {
                  enableAnimateInEaseOut();
                  setSearchTxt('');
                  setSelectedCatId(item);
                }}
                style={{
                  marginRight: aryLength - 1 === index ? 15 : 0,
                  alignItems: 'center',
                  justifyContent: 'center',
                  marginLeft: 15,
                }}>
                {index === 0 ? (
                  <View
                    style={{
                      width: nWidth / 7,
                      height: nWidth / 7,
                      alignItems: 'center',
                      justifyContent: 'center',
                      borderRadius: nWidth / 7 / 2,
                      borderColor: BaseColor.blackColor,
                      borderWidth: isSelected ? 2 : StyleSheet.hairlineWidth,
                    }}>
                    <Text body1 bold>
                      ALL
                    </Text>
                  </View>
                ) : (
                  <Image
                    style={{
                      width: nWidth / 7,
                      height: nWidth / 7,
                      borderRadius: nWidth / 7 / 2,
                      borderColor: BaseColor.blackColor,
                      borderWidth: isSelected ? 2 : StyleSheet.hairlineWidth,
                    }}
                    source={{uri: imgUrl}}
                  />
                )}
                {isSelected ? (
                  <Text
                    bold
                    caption2
                    textAlignCenter
                    numberOfLines={1}
                    style={{paddingVertical: 10}}>
                    {catName}
                  </Text>
                ) : (
                  <Text
                    caption2
                    textAlignCenter
                    numberOfLines={1}
                    style={{paddingVertical: 10}}>
                    {catName}
                  </Text>
                )}
              </TouchableOpacity>
            );
          }}
          keyExtractor={(item) => item.id}
          ListEmptyComponent={() => {
            return (
              <View
                style={{
                  width: '100%',
                  height: 50,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Text bold textAlignCenter body1 lightGrey>
                  {"Sorry, We don't have product category yet."}
                </Text>
              </View>
            );
          }}
          contentContainerStyle={{flexGrow: 1}}
          showsVerticalScrollIndicator={false}
        />
      </View>
    );
  }

  function renderFooterComponent() {
    if (moreLoad) {
      return (
        <View style={styles.loaderFooterView}>
          <ActivityIndicator
            size={'small'}
            animating
            color={BaseColor.ThemeOrange}
          />
        </View>
      );
    } else {
      return <View style={styles.listFooterView} />;
    }
  }

  function renderEmptyComponent() {
    const categoryName = checkObject(selectedCatId, 'category_name');
    const catName =
      categoryName !== '=' && categoryName !== ''
        ? _.toUpper(categoryName)
        : 'product';
    return (
      <EmptyView
        emptyText={`Sorry, We don't have any store in\n ${catName} category & selected location.`}
        btnLoader={refreshLoader}
        onPress={() => {
          onRefresh();
        }}
      />
    );
  }

  function renderList() {
    return (
      <FlatList
        ref={mailListRef}
        data={storeList.data}
        scrollsToTop={false}
        horizontal={false}
        showsVerticalScrollIndicator={false}
        renderItem={renderItem}
        keyExtractor={(item) => item.id}
        contentContainerStyle={styles.flexMainCon}
        ListFooterComponent={renderFooterComponent}
        ListEmptyComponent={renderEmptyComponent}
        onEndReached={getMoreData}
        onEndReachedThreshold={0.5}
        refreshing={refreshLoader}
        refreshControl={
          <RefreshControl
            colors={[BaseColor.ThemeOrange]}
            tintColor={BaseColor.ThemeOrange}
            refreshing={refreshLoader}
            onRefresh={() => {
              onRefresh();
            }}
          />
        }
      />
    );
  }

  function renderSearchBar() {
    const isTestEmpty = searchTxt === '';
    return (
      <LinearGradient
        start={{x: 0.3, y: 0.25}}
        end={{x: 0.8, y: 1.0}}
        colors={[BaseColor.ThemeOrange, BaseColor.ThemeYellow]}
        style={styles.searchBarCon}>
        <View
          style={[
            styles.mainCon,
            {
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
            },
          ]}>
          <View style={styles.mainCon}>
            <TextInput
              ref={searchInputRef}
              leftIconName={'search'}
              otherCon={[
                styles.searchInputCon,
                {
                  borderTopRightRadius: isTestEmpty ? 5 : 0,
                  borderBottomRightRadius: isTestEmpty ? 5 : 0,
                },
              ]}
              otherIconCss={styles.searchIconClr}
              placeholder="Search Store..."
              onChangeText={(t) => {
                enableAnimateInEaseOut();
                setSearchTxt(t);
                if (t === '') {
                  onClearInputCon();
                }
              }}
              onSubmitEditing={
                searchBtnLoad
                  ? null
                  : () => {
                      onGoAction();
                    }
              }
              value={searchTxt}
            />
          </View>
          {!isTestEmpty ? (
            <TouchableOpacity
              activeOpacity={0.8}
              onPress={
                searchBtnLoad
                  ? null
                  : () => {
                      onClearInputCon();
                    }
              }
              style={{
                paddingHorizontal: 10,
                height: 40,
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: BaseColor.whiteColor,
              }}>
              <Icon name={'close'} size={12} color={BaseColor.blackColor} />
            </TouchableOpacity>
          ) : null}
        </View>

        {!isTestEmpty ? (
          <Button
            buttonText={'Go'}
            loading={searchBtnLoad}
            style={styles.searchBtnSty}
            otherGredientCss={{
              borderRadius: 0,
              height: 40,
              borderTopRightRadius: 5,
              borderBottomRightRadius: 5,
            }}
            onPress={
              searchBtnLoad
                ? null
                : () => {
                    onGoAction();
                  }
            }
          />
        ) : null}
      </LinearGradient>
    );
  }

  function onClearInputCon() {
    Keyboard.dismiss();
    searchInputRef.current.blur();
    enableAnimateInEaseOut();
    setSearchTxt('');
    setPageLoader(true);
    setTimeout(() => {
      getStoreList(true);
    }, 100);
  }

  function onGoAction() {
    Keyboard.dismiss();
    searchInputRef.current.blur();
    setSearchBtnLoad(true);
    setPageLoader(true);
    setTimeout(() => {
      getSearchStoreList();
    }, 100);
  }

  function renderHeader() {
    const addCom = checkObject(locationsData, 'address_components');
    let cityName = '-';
    if (_.isArray(addCom) && !_.isEmpty(addCom)) {
      cityName = checkObject(addCom[0], 'short_name');
    }
    return (
      <GredientHeader
        title={cityName}
        pageLoader={pageLoader}
        onTitlePress={() => {
          CGoogleAutoCompleteRef.current.openModal();
        }}
        homeRightIcon
      />
    );
  }

  function renderLoader() {
    return <HomeLoader />;
  }

  function onSave(loc) {
    dispatch(setUserLocationsData(loc));
    onRefresh();
    CGoogleAutoCompleteRef.current.closeModal();
  }

  function onCloseAuto() {
    console.log('Close Event');
  }

  function renderGooglePlaceInput() {
    return (
      <CGoogleAutoComplete
        ref={CGoogleAutoCompleteRef}
        onSave={(loc) => {
          onSave(loc);
        }}
        onClose={() => {
          onCloseAuto();
        }}
      />
    );
  }

  return (
    <View style={styles.mainCon}>
      {renderHeader()}
      {pageLoader ? null : renderSearchBar()}
      {pageLoader ? renderLoader() : null}
      {pageLoader ? null : renderHeaderComponent()}
      {pageLoader ? null : renderList()}
      {renderGooglePlaceInput()}
    </View>
  );
}
