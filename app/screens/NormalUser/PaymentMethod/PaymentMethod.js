import {View, FlatList, StyleSheet, RefreshControl} from 'react-native';
import React, {useRef, useState} from 'react';
import {useSelector} from 'react-redux';
import _ from 'lodash';
import {BaseColor} from '../../../config';
import CAlert from '../../../components/CAlert';
import Button from '../../../components/Button';
import Toast from '../../../components/Toast/index';
import {BaseSetting} from '../../../config/setting';
import {getApiData} from '../../../utils/apiHelper';
import {checkObject} from '../../../utils/commonFunction';
import EmptyView from '../../../components/DriverApp/EmptyView';
import CPaymentCard from '../../../components/Payment/CPaymentCard';
import PaymentModal from '../../../components/paymentModal/paymentModal';
import GredientHeader from '../../../components/DriverApp/GredientHeader';
import {StaticAlertMsg, StaticHeader} from '../../../config/StaticAlertMsg';
import {PaymentMethodLoader} from '../../../components/ContentLoader/ContentLoader';

const styles = StyleSheet.create({
  mainCon: {
    flex: 1,
    position: 'relative',
    backgroundColor: BaseColor.whiteColor,
  },
  flexMainCon: {
    flexGrow: 1,
  },
  listFooterView: {
    width: '100%',
    height: 100,
  },
  paddingB10: {
    paddingBottom: 10,
  },
  ItemSeparatorCss: {
    height: 10,
    width: '100%',
  },
  bottomFixedBtn: {
    height: 50,
    width: 50,
    borderRadius: 25,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 0,
  },
  otherCSS: {
    position: 'absolute',
    bottom: 10,
    right: 10,
    zIndex: 10,
  },
});

const PaymentMethod = ({navigation}) => {
  const {isConnected, userData} = useSelector((state) => state.auth);
  const [ismodalVisible, setIsmodalVisible] = useState(false);
  const [refreshLoader, setRefreshLoader] = useState(false);
  const [pageLoader, setPageLoader] = useState(true);
  const [listData, setListData] = useState([]);

  const mailListRef = useRef();
  const ToastRef = useRef();

  const userToken = checkObject(userData, 'access_token');

  const header = {
    Authorization: `Bearer ${userToken}`,
  };

  React.useEffect(
    () =>
      navigation.addListener('focus', () => {
        getPaymentList();
      }),
    [],
  );

  function handleAllLoader() {
    setPageLoader(false);
    setRefreshLoader(false);
  }

  function onRefresh() {
    setRefreshLoader(true);
    setPageLoader(true);
    setTimeout(() => {
      getPaymentList();
    }, 500);
  }

  async function getPaymentList() {
    if (isConnected === true) {
      try {
        let endPoint = BaseSetting.endpoints.card_list;
        const response = await getApiData(
          navigation,
          endPoint,
          'GET',
          null,
          header,
        );
        if (
          _.isObject(response) &&
          !_.isEmpty(response) &&
          response.success === true
        ) {
          const newListData =
            response && response.data
              ? response.data.cards && response.data.cards.data
              : [];
          setListData(newListData);
          handleAllLoader();
        } else {
          const errorMessage = checkObject(response, 'message');
          CAlert(errorMessage, StaticHeader.Oops, () => {
            handleAllLoader();
          });
        }
      } catch (err) {
        console.log('Catch Part', err);
        CAlert(StaticAlertMsg.someThing_Went_Wrong, StaticHeader.Oops, () => {
          handleAllLoader();
        });
      }
    } else {
      CAlert(StaticAlertMsg.Network_Problem, StaticHeader.Oops, () => {
        handleAllLoader();
      });
    }
  }

  async function deleteCardAction(deleteCardObj) {
    const deleteId = checkObject(deleteCardObj, 'id');
    if (isConnected === true) {
      try {
        let endPoint = `${BaseSetting.endpoints.delete_card}?token=${deleteId}`;
        const response = await getApiData(
          navigation,
          endPoint,
          'GET',
          null,
          header,
        );
        if (
          _.isObject(response) &&
          !_.isEmpty(response) &&
          response.success === true
        ) {
          const successMessage = checkObject(response, 'message');
          ToastRef.current.show(successMessage, 2000);
          getPaymentList();
        } else {
          const errorMessage = checkObject(response, 'message');
          CAlert(errorMessage, StaticHeader.Oops, () => {
            handleAllLoader();
          });
        }
      } catch (err) {
        console.log('Catch Part', err);
        CAlert(StaticAlertMsg.someThing_Went_Wrong, StaticHeader.Oops, () => {
          handleAllLoader();
        });
      }
    } else {
      CAlert(StaticAlertMsg.Network_Problem, StaticHeader.Oops, () => {
        handleAllLoader();
      });
    }
  }

  const renderItem = ({item, index}) => {
    return (
      <CPaymentCard
        key={index}
        data={item}
        onDelete={() => {
          CAlert(
            StaticAlertMsg.cardRemove,
            StaticHeader.Confirm,
            () => {
              setPageLoader(true);
              deleteCardAction(item);
            },
            () => {
              return null;
            },
          );
        }}
      />
    );
  };

  function renderFooterComponent() {
    return <View style={styles.listFooterView} />;
  }

  function renderEmptyComponent() {
    return (
      <EmptyView
        emptyText={
          'Sorry currently your card list is empty.\nAdd your first payment card.'
        }
        centerIconName={'add-sharp'}
        onPress={() => {
          setIsmodalVisible(true);
        }}
      />
    );
  }

  function renderList() {
    return (
      <FlatList
        ref={mailListRef}
        data={listData}
        horizontal={false}
        renderItem={renderItem}
        refreshing={refreshLoader}
        keyExtractor={(item) => item.id}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={styles.flexMainCon}
        ListFooterComponent={renderFooterComponent}
        ListEmptyComponent={renderEmptyComponent}
        ItemSeparatorComponent={() => {
          return <View style={styles.ItemSeparatorCss} />;
        }}
        refreshControl={
          <RefreshControl
            colors={[BaseColor.ThemeOrange]}
            tintColor={BaseColor.ThemeOrange}
            refreshing={refreshLoader}
            onRefresh={() => {
              onRefresh();
            }}
          />
        }
      />
    );
  }

  function renderLoader() {
    return <PaymentMethodLoader />;
  }

  function renderHeader() {
    return (
      <GredientHeader
        leftIcon
        homeRightIcon={false}
        title={'PAYMENT METHOD'}
        onLeftAction={() => {
          navigation.pop();
        }}
      />
    );
  }

  function BottomFixedButton() {
    return (
      <Button
        style={[styles.bottomFixedBtn, styles.otherCSS]}
        otherGredientCss={styles.bottomFixedBtn}
        otherIconSty={{
          fontSize: 35,
        }}
        displayIcon
        centerIconName={'add-sharp'}
        onPress={() => {
          setIsmodalVisible(true);
        }}
      />
    );
  }

  function renderModal() {
    return (
      <PaymentModal
        navigation={navigation}
        ismodalVisible={ismodalVisible}
        dismiss={() => {
          setIsmodalVisible(false);
        }}
        onRefreshList={() => {
          setIsmodalVisible(false);
          setPageLoader(true);
          getPaymentList();
        }}
      />
    );
  }

  return (
    <View style={styles.mainCon}>
      {renderHeader()}
      {pageLoader ? renderLoader() : renderList()}
      {pageLoader ? null : renderModal()}
      {pageLoader || _.isEmpty(listData) ? null : BottomFixedButton()}
      <Toast
        ref={ToastRef}
        position="bottom"
        positionValue={150}
        fadeInDuration={750}
        fadeOutDuration={2000}
        opacity={0.8}
      />
    </View>
  );
};
export default PaymentMethod;
