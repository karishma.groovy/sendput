import React, {useState} from 'react';
import {View, StyleSheet, FlatList, Dimensions} from 'react-native';
import Text from '../../../components/Text/index';
import GredientHeader from '../../../components/DriverApp/GredientHeader';
import {productListData} from '../../../data/StaticData';
import CFavorites from '../../../components/favorite/Cfavorite';
import {FavoriteLoader} from '../../../components/ContentLoader/ContentLoader';

const styles = StyleSheet.create({
  main: {
    flex: 1,
  },
  emptytextitem3: {
    marginVertical: 20,
    paddingLeft: 10,
  },
  footerView: {
    marginBottom: 150,
    width: '100%',
  },
  resultText: {
    marginTop: 15,
    paddingLeft: 10,
  },
  paddingLeft: {
    paddingLeft: 10,
  },
});

export default function Favorites({navigation}) {
  const [loading, setLoading] = useState(false);
  const renderItem = ({item, index}) => {
    if (item.isFav) {
      return <CFavorites item={item} index={index} />;
    } else {
      return null;
    }
  };
  function renderHeader() {
    return (
      <GredientHeader
        leftIcon
        title={'Favorites'}
        onLeftAction={() => {
          navigation.pop();
        }}
      />
    );
  }

  return (
    <View style={styles.main}>
      {renderHeader()}
      {loading ? (
        <FavoriteLoader />
      ) : (
        <View>
          <Text grayColor semibold body1 style={styles.resultText}>
            4 results
          </Text>
          <FlatList
            showsHorizontalScrollIndicator={false}
            horizontal={false}
            data={productListData}
            refreshing={loading}
            onRefresh={() => {
              setTimeout(() => {
                setLoading(false);
              }, 500);
              setLoading(true);
            }}
            renderItem={renderItem}
            keyExtractor={(item) => item.id}
            numColumns={2}
            ListEmptyComponent={() => {
              return (
                <Text body1 bold blackColor style={styles.emptytextitem3}>
                  Sorry no data available for stores!!!
                </Text>
              );
            }}
            ListFooterComponent={() => {
              return <View style={styles.footerView} />;
            }}
          />
        </View>
      )}
    </View>
  );
}
