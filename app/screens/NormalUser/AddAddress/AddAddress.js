import React, {useCallback, useEffect, useRef, useState} from 'react';
import Icon from 'react-native-vector-icons/Ionicons';
import {useDispatch, useSelector} from 'react-redux';
import _ from 'lodash';
import {
  View,
  Keyboard,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import CAlert from '../../../components/CAlert';
import {BaseColor} from '../../../config/theme';
import Text from '../../../components/Text/index';
import {BaseSetting} from '../../../config/setting';
import Button from '../../../components/Button/index';
import {FontFamily} from '../../../config/typography';
import {checkObject} from '../../../utils/commonFunction';
import {getApiDataProgress} from '../../../utils/apiHelper';
import TextInput from '../../../components/TextInput/index';
import CGoogleAutoComplete from '../../../components/CGoogleAutoComplete';
import GredientHeader from '../../../components/DriverApp/GredientHeader';
import {StaticAlertMsg, StaticHeader} from '../../../config/StaticAlertMsg';

const AddType = [
  {
    id: 1,
    title: 'Home',
  },
  {
    id: 2,
    title: 'Office',
  },
  {
    id: 3,
    title: 'Other',
  },
];

const styles = StyleSheet.create({
  main: {
    flex: 1,
  },
  scrollMainCon: {
    flexGrow: 1,
    paddingTop: 10,
    paddingHorizontal: 10,
  },
  addCon: {
    width: '100%',
    height: 50,
    paddingHorizontal: 20,
    borderRadius: 25,
    alignItems: 'flex-start',
    justifyContent: 'center',
    backgroundColor: BaseColor.whiteColor,
    borderColor: '#000',
    marginTop: 10,
    borderWidth: 1,
  },
  addText: {
    fontFamily: FontFamily.medium,
    fontSize: 16,
    letterSpacing: 1,
    backgroundColor: '#0000',
  },
  PL20: {
    paddingLeft: 20,
  },
  MT15: {
    marginTop: 15,
  },
  addTypeCon: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingTop: 15,
    paddingLeft: 15,
  },
  radioBtnCon: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingRight: 20,
  },
});

const AddAddress = ({route, navigation}) => {
  const {isConnected, userData} = useSelector((state) => state.auth);
  const updateOrDeleteObj = route.params;

  const isUpdateData =
    _.isObject(updateOrDeleteObj) && !_.isEmpty(updateOrDeleteObj);
  const HeaderTitle = isUpdateData ? 'Modify Address' : 'Add Address';
  const buttonName = isUpdateData ? 'Update Address' : 'Save Address';
  const APIURLEndPoint = isUpdateData
    ? BaseSetting.endpoints.update_address
    : BaseSetting.endpoints.add_address;

  const updatedAddressId = isUpdateData
    ? checkObject(updateOrDeleteObj, 'id')
    : '-';
  const updatedAddress = isUpdateData
    ? checkObject(updateOrDeleteObj, 'address')
    : 'Address';
  const updatedCity = isUpdateData
    ? checkObject(updateOrDeleteObj, 'city')
    : '';
  const updatedState = isUpdateData
    ? checkObject(updateOrDeleteObj, 'state')
    : '';
  const updatedZipCode = isUpdateData
    ? checkObject(updateOrDeleteObj, 'zipcode')
    : '';
  const updatedCountry = isUpdateData
    ? checkObject(updateOrDeleteObj, 'country')
    : '';
  const updatedAddTypeValue = isUpdateData
    ? checkObject(updateOrDeleteObj, 'type')
    : '';

  const updatedAddressLat = isUpdateData
    ? checkObject(updateOrDeleteObj, 'lat')
    : '';
  const updatedAddressLng = isUpdateData
    ? checkObject(updateOrDeleteObj, 'lng')
    : '';

  const fAddData = {
    geometry: {
      location: {
        lat: updatedAddressLat,
        lng: updatedAddressLng,
      },
    },
  };

  const selectedAddType = AddType.find(
    (v) => _.toLower(v.title) === _.toLower(updatedAddTypeValue),
  );
  const [loader, setLoader] = useState(false);

  const cityId = useRef();
  const stateId = useRef();
  const zipId = useRef();
  const countryId = useRef();
  const CGoogleAutoCompleteRef = useRef('');

  const [fullAddressDetails, setFullAddressDetails] = useState(fAddData);
  const [saveAddLoader, setSaveAddLoader] = useState(false);

  const [name, setName] = useState('Chris Hamaworth');
  const [address, setAddress] = useState(updatedAddress);
  const [city, setCity] = useState(updatedCity);
  const [state, setState] = useState(updatedState);
  const [zip, setZip] = useState(updatedZipCode);
  const [country, setCountry] = useState(updatedCountry);
  const [addType, setAddType] = useState(
    !_.isEmpty(selectedAddType) ? selectedAddType : AddType[0],
  );

  const userToken = checkObject(userData, 'access_token');

  const dispatch = useDispatch();

  const header = {
    Authorization: `Bearer ${userToken}`,
  };

  async function addOrUpdateAddressAction() {
    Keyboard.dismiss();
    setLoader(true);
    if (isConnected === true) {
      const latValue =
        fullAddressDetails &&
        fullAddressDetails.geometry &&
        fullAddressDetails.geometry.location &&
        fullAddressDetails.geometry.location.lat
          ? fullAddressDetails.geometry.location.lat
          : '-';
      const lngValue =
        fullAddressDetails &&
        fullAddressDetails.geometry &&
        fullAddressDetails.geometry.location &&
        fullAddressDetails.geometry.location.lng
          ? fullAddressDetails.geometry.location.lng
          : '-';

      let addressDetails = {
        'CustomerAddress[address]': address,
        'CustomerAddress[city]': city,
        'CustomerAddress[zipcode]': zip,
        'CustomerAddress[state]': state,
        // 'CustomerAddress[country]': country,
        'CustomerAddress[country]': 'USA',
        'CustomerAddress[lat]': latValue,
        'CustomerAddress[lng]': lngValue,
        'CustomerAddress[type]': _.toLower(addType.title),
      };

      if (isUpdateData) {
        addressDetails['CustomerAddress[address_id]'] = updatedAddressId;
      }

      try {
        const response = await getApiDataProgress(
          navigation,
          APIURLEndPoint,
          'POST',
          addressDetails,
          header,
        );
        if (
          _.isObject(response) &&
          !_.isEmpty(response) &&
          response.success === true
        ) {
          setName('');
          setAddress('');
          setCity('');
          setState('');
          setZip('');
          setCountry('');
          navigation.pop();
        } else {
          const errorMessage = checkObject(response, 'message');
          CAlert(errorMessage, StaticHeader.Oops, () => {
            setLoader(false);
          });
        }
      } catch (err) {
        console.log('Catch Part', err);
        CAlert(StaticAlertMsg.someThing_Went_Wrong, StaticHeader.Oops, () => {
          setLoader(false);
        });
      }
    } else {
      CAlert(StaticAlertMsg.Network_Problem, StaticHeader.Oops, () => {
        setLoader(false);
      });
    }
  }

  function onSave(loc) {
    setSaveAddLoader(true);
    console.log(loc);
    const addressName =
      loc && loc.formatted_address ? loc.formatted_address : '-';
    setAddress(addressName);
    setFullAddressDetails(loc);
    setTimeout(() => {
      setSaveAddLoader(false);
      CGoogleAutoCompleteRef.current.closeModal();
    }, 1000);
  }

  function renderForm() {
    return (
      <ScrollView
        keyboardShouldPersistTaps="never"
        showsVerticalScrollIndicator={false}
        contentContainerStyle={styles.scrollMainCon}>
        <TouchableOpacity
          activeOpacity={0.8}
          onPress={() => {
            CGoogleAutoCompleteRef.current.openModal();
          }}
          style={styles.addCon}>
          <Text numberOfLines={2} style={styles.addText}>
            {address}
          </Text>
        </TouchableOpacity>

        <TextInput
          ref={cityId}
          placeholder="City"
          onChangeText={(t) => {
            setCity(t);
          }}
          value={city}
          keyboardType={'default'}
          onSubmitEditing={() => stateId.current.focus()}
          returnKeyType="next"
          inputStyle={styles.PL20}
          otherCon={styles.MT15}
        />

        <TextInput
          ref={stateId}
          placeholder="state"
          onChangeText={(t) => {
            setState(t);
          }}
          value={state}
          keyboardType={'default'}
          onSubmitEditing={() => zipId.current.focus()}
          returnKeyType="next"
          inputStyle={styles.PL20}
          otherCon={styles.MT15}
        />

        <TextInput
          ref={zipId}
          placeholder="Zip Code"
          onChangeText={(t) => {
            setZip(t);
          }}
          value={zip}
          keyboardType={'number-pad'}
          // onSubmitEditing={() => countryId.current.focus()}
          onSubmitEditing={() => {
            Keyboard.dismiss();
          }}
          returnKeyType="next"
          inputStyle={styles.PL20}
          otherCon={styles.MT15}
        />
        {/*
        <TextInput
          ref={countryId}
          placeholder="Country"
          onChangeText={(t) => {
            setCountry(t);
          }}
          value={country}
          keyboardType={'default'}
          onSubmitEditing={() => {
            Keyboard.dismiss();
          }}
          returnKeyType="done"
          inputStyle={styles.PL20}
          otherCon={styles.MT15}
        /> */}

        <View style={styles.addTypeCon}>
          {AddType.map((obj, index) => {
            const isCheck = _.isEqual(addType, obj);
            const iconName = isCheck
              ? 'radio-button-on'
              : 'radio-button-off-sharp';
            return (
              <TouchableOpacity
                key={index}
                onPress={() => {
                  setAddType(obj);
                }}
                style={styles.radioBtnCon}>
                <Icon name={iconName} size={20} color={'#696969'} />
                <Text subhead style={{paddingLeft: 3}}>
                  {obj.title}
                </Text>
              </TouchableOpacity>
            );
          })}
        </View>

        <Button
          loading={loader}
          buttonText={buttonName}
          onPress={() => {
            addOrUpdateAddressAction();
          }}
        />
      </ScrollView>
    );
  }

  function renderGooglePlaceInput() {
    return (
      <CGoogleAutoComplete
        ref={CGoogleAutoCompleteRef}
        saveAddLoader={saveAddLoader}
        onSave={(loc) => {
          onSave(loc);
        }}
        onClose={() => {
          console.log('Close Event');
        }}
      />
    );
  }

  function renderHeader() {
    return (
      <GredientHeader
        leftIcon
        title={HeaderTitle}
        onLeftAction={() => {
          navigation.pop();
        }}
      />
    );
  }

  return (
    <View style={styles.main}>
      {renderHeader()}
      {renderForm()}
      {renderGooglePlaceInput()}
    </View>
  );
};
export default AddAddress;
