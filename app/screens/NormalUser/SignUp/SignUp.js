import React, {useCallback, useEffect, useRef, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {
  View,
  Image,
  StyleSheet,
  Keyboard,
  Platform,
  SafeAreaView,
  ScrollView,
  TouchableOpacity,
  KeyboardAvoidingView,
} from 'react-native';
import {checkObject} from '../../../utils/commonFunction';
import _ from 'lodash';
import OTPTextView from 'react-native-otp-textinput';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import authActions from '../../../redux/reducers/auth/actions';
import TextInput from '../../../components/TextInput';
import Text from '../../../components/Text';
import Button from '../../../components/Button';
import {BaseColor} from '../../../config/theme';
import {Images} from '../../../config/images';
import {getApiDataProgress} from '../../../utils/apiHelper';
import {BaseSetting} from '../../../config/setting';
import CAlert from '../../../components/CAlert';
import TopBar from '../../../components/TopBars/index';
import {StaticAlertMsg, StaticHeader} from '../../../config/StaticAlertMsg';

const styles = StyleSheet.create({
  main: {
    flex: 1,
  },
  img: {
    width: BaseSetting.nWidth / 3,
    height: BaseSetting.nWidth / 3,
    resizeMode: 'contain',
    alignSelf: 'center',
  },
  title: {
    paddingVertical: 15,
  },
  agreeTxtView: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: 15,
    paddingVertical: 15,
  },
  agreeTxt: {
    paddingLeft: 8,
  },
  roundedTextInput: {
    borderRadius: 10,
    borderBottomWidth: 0.8,
    borderWidth: 0.8,
  },
  verifyOtpText: {
    textAlign: 'center',
    width: '35%',
    alignSelf: 'center',
    marginTop: 25,
  },
  middleView: {
    flex: 1,
    paddingHorizontal: 40,
  },
  iconView: {
    width: 80,
    height: 80,
    borderRadius: 40,
    borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    marginBottom: 30,
  },
  resendView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  scroll: {
    flexGrow: 1,
    paddingHorizontal: 20,
    paddingBottom: 20,
  },
  trmsNconditnText: {
    textDecorationLine: 'underline',
    paddingLeft: 5,
  },
});

export default function SignUp({navigation}) {
  const {isConnected} = useSelector((state) => state.auth);
  const dispatch = useDispatch();

  const [buttonLoad, setButtonLoad] = useState(false);

  const [checked, setChecked] = useState(false);
  const [name, setName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [phone, setPhone] = useState('');
  const firstNameID = useRef();
  const lastNameID = useRef();
  const mailID = useRef();
  const passwordId = useRef();
  const phnNumberId = useRef();
  const otpInput = useRef('');

  async function signUpAction() {
    if (isConnected === true) {
      const signUpData = {
        'SignupForm[firstname]': name,
        'SignupForm[lastname]': lastName,
        'SignupForm[email]': email,
        'SignupForm[phone]': phone,
        'SignupForm[password]': password,
        'SignupForm[phone_code]': '+1',
      };

      try {
        let endPoint = BaseSetting.endpoints.signup;
        const response = await getApiDataProgress(
          navigation,
          endPoint,
          'POST',
          signUpData,
        );
        if (
          _.isObject(response) &&
          !_.isEmpty(response) &&
          response.success === true
        ) {
          const successMessage = checkObject(response, 'message');
          CAlert(successMessage, StaticHeader.success, () => {
            const verifyData = {
              phoneNo: phone,
              from: 'SignUp',
            };
            navigation.navigate('VerifyOtp', verifyData);
            setButtonLoad(false);
          });
        } else {
          const errorMessage = checkObject(response, 'message');
          CAlert(errorMessage, StaticHeader.Oops, () => {
            setButtonLoad(false);
          });
        }
      } catch (err) {
        console.log('Catch Part', err);
        CAlert(StaticAlertMsg.someThing_Went_Wrong, StaticHeader.Oops, () => {
          setButtonLoad(false);
        });
      }
    } else {
      CAlert(StaticAlertMsg.Network_Problem, StaticHeader.Oops, () => {
        setButtonLoad(false);
      });
    }
  }

  function validate() {
    Keyboard.dismiss();
    let valid = true;
    // let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    //   if(_.isEmpty(name) ||_.isEmpty(email) || _.isEmpty(lastName) ||_.isEmpty(phone)){
    //     valid = false;
    //     CAlert(StaticAlertMsg.fillAllData, StaticHeader.Alert);
    //     return false;
    //   }
    //   if(_.trim(name).length < 3){
    //     valid = false;
    //     CAlert(StaticAlertMsg.firstName , StaticHeader.alert);
    //     return false;
    //   }
    //   if (reg.test(email) === false) {
    //     valid = false;
    //     CAlert(StaticAlertMsg.validEmail , StaticHeader.alert)
    //     return false;

    //   }
    //   if(_.trim(lastName.length) < 3){
    //     valid = false;
    //     CAlert(StaticAlertMsg.lastNameLength , StaticHeader.alert)
    //     return false;
    //   }
    //   if(!((_.trim(password.length) >=6) && (_.trim(password.length) <=8))){
    //     valid = false;
    //     CAlert(StaticAlertMsg.pwdlength , StaticHeader.alert)
    //     return false;
    //   }
    //   if(_.trim(phone.length) != 10){
    //     valid = false;
    //     CAlert(StaticAlertMsg.phoneNumberlength , StaticHeader.alert)
    //     return false;
    //   }
    if (checked === false) {
      valid = false;
      CAlert(StaticAlertMsg.termsNCONDITION, StaticHeader.Oops);
      return false;
    } else if (valid === true) {
      setButtonLoad(true);
      signUpAction();
    }
  }

  function renderForm() {
    return (
      <KeyboardAvoidingView
        style={{
          flex: 1,
          backgroundColor: BaseColor.whiteColor,
        }}
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
        <ScrollView
          keyboardShouldPersistTaps="never"
          showsVerticalScrollIndicator={false}
          contentContainerStyle={styles.scroll}>
          <Image source={Images.logoBlackImg} style={styles.img} />
          <Text title2 textAlignCenter blackColor bold style={styles.title}>
            Signup
          </Text>

          <TextInput
            ref={firstNameID}
            placeholder="First name"
            leftIconName={'user'}
            onChangeText={(t) => {
              setName(t);
            }}
            value={name}
            onSubmitEditing={() => lastNameID.current.focus()}
            returnKeyType="next"
          />
          <TextInput
            ref={lastNameID}
            placeholder="Last name"
            leftIconName={'user'}
            onChangeText={(t) => {
              setLastName(t);
            }}
            value={lastName}
            onSubmitEditing={() => mailID.current.focus()}
            returnKeyType="next"
          />
          <TextInput
            ref={mailID}
            placeholder="Email id"
            leftIconName={'envelope'}
            onChangeText={(t) => {
              setEmail(t);
            }}
            value={email}
            onSubmitEditing={() => passwordId.current.focus()}
            returnKeyType="next"
            keyboardType={'email-address'}
          />
          <TextInput
            ref={passwordId}
            placeholder="Password"
            leftIconName={'unlock-alt'}
            secureTextEntry
            onChangeText={(t) => {
              setPassword(t);
            }}
            value={password}
            onSubmitEditing={() => phnNumberId.current.focus()}
            returnKeyType="next"
          />

          <TextInput
            ref={phnNumberId}
            phone
            placeholder="Phone number"
            onChangeText={(t) => {
              setPhone(t);
            }}
            value={phone}
            onSubmitEditing={() => {
              Keyboard.dismiss();
            }}
            returnKeyType="done"
            keyboardType={'numeric'}
          />

          <View style={styles.agreeTxtView}>
            <TouchableOpacity
              activeOpacity={0.8}
              onPress={() => {
                setChecked(!checked);
              }}>
              <Icon
                name={checked ? 'radiobox-marked' : 'radiobox-blank'}
                size={25}
                color={BaseColor.ThemeBlue}
              />
            </TouchableOpacity>
            <Text footnote blackColor style={styles.agreeTxt}>
              AGREE TO
            </Text>
            <TouchableOpacity
              activeOpacity={0.8}
              onPress={() => {
                navigation.navigate('TermsConditions', 'Terms & Condition');
              }}>
              <Text footnote blackColor style={styles.trmsNconditnText}>
                TERMS & CONDITION
              </Text>
            </TouchableOpacity>
          </View>

          <Button
            buttonText={'REGISTER'}
            loading={buttonLoad}
            onPress={() => {
              validate();
            }}
          />
        </ScrollView>
      </KeyboardAvoidingView>
    );
  }

  return (
    <SafeAreaView style={styles.main}>
      <TopBar
        leftIcon
        title=""
        onPressLeft={() => {
          navigation.pop();
        }}
      />
      {renderForm()}
    </SafeAreaView>
  );
}
