/* eslint-disable react-native/no-inline-styles */
import React, {useEffect, useRef, useState} from 'react';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/Ionicons';
import {useSelector} from 'react-redux';
import _ from 'lodash';
import {
  View,
  Keyboard,
  FlatList,
  StyleSheet,
  RefreshControl,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import Button from '../../../components/Button';
import {BaseColor} from '../../../config/theme';
import CAlert from '../../../components/CAlert';
import {getApiData} from '../../../utils/apiHelper';
import {BaseSetting} from '../../../config/setting';
import {userOrderTab} from '../../../data/StaticData';
import TextInput from '../../../components/TextInput';
import TopTabs from '../../../components/TopTabs/TopTabs';
import EmptyView from '../../../components/DriverApp/EmptyView';
import GredientHeader from '../../../components/DriverApp/GredientHeader';
import {StaticAlertMsg, StaticHeader} from '../../../config/StaticAlertMsg';
import OrderDetailCard from '../../../components/DriverApp/OrderDetailCard';
import {MyOrderLoader} from '../../../components/ContentLoader/ContentLoader';
import {
  checkObject,
  enableAnimateInEaseOut,
} from '../../../utils/commonFunction';

const styles = StyleSheet.create({
  mainCon: {
    flex: 1,
  },
  searchBarCon: {
    width: '100%',
    height: 55,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    overflow: 'hidden',
    paddingHorizontal: 10,
  },
  searchInputCon: {
    width: '100%',
    height: 40,
    borderRadius: 0,
    backgroundColor: BaseColor.whiteColor,
    borderWidth: 0,
    marginTop: 0,
    borderTopLeftRadius: 5,
    borderBottomLeftRadius: 5,
  },
  searchIconClr: {
    color: BaseColor.grayColor,
  },
  searchBtnSty: {
    height: 40,
    width: '20%',
    borderRadius: 0,
    marginTop: 0,
    borderTopRightRadius: 5,
    borderBottomRightRadius: 5,
  },
  flatListMainCon: {
    flexGrow: 1,
  },
  headerMainCon: {
    flex: 1,
    padding: 10,
    backgroundColor: BaseColor.whiteColor,
  },
  listFooterView: {
    width: '100%',
    height: 30,
  },
  textInput: {
    marginTop: 0,
    borderRadius: 0,
    borderColor: BaseColor.grayColor,
  },
  colorGray: {
    color: BaseColor.grayColor,
  },
  loaderFooterView: {
    width: '100%',
    padding: 30,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: BaseColor.whiteColor,
  },
  headerSubCon: {
    flex: 1,
    paddingVertical: 10,
    backgroundColor: BaseColor.whiteColor,
  },
  otherTabCon: {
    flexGrow: 1,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  tabButtonSty: {
    marginRight: 0,
    width: BaseSetting.nWidth / 3 - 15,
  },
  tabTitle: {fontSize: 13},
});

export default function UserOrder({navigation}) {
  const {isConnected, NotiNumber, userData} = useSelector(
    (state) => state.auth,
  );
  const [listData, setListData] = useState({});
  const [pageLoader, setPageLoader] = useState(true);
  const [refreshLoader, setRefreshLoader] = useState(false);
  const [moreLoad, setMoreLoad] = useState(false);
  const [active, setActive] = useState(0);

  const [searchTxt, setSearchTxt] = useState('');
  const [searchBtnLoad, setSearchBtnLoad] = useState(false);

  const mailListRef = useRef();
  const searchInputRef = useRef('');

  const userToken = checkObject(userData, 'access_token');

  const header = {
    Authorization: `Bearer ${userToken}`,
  };

  React.useEffect(
    () =>
      navigation.addListener('focus', () => {
        setSearchTxt('');
        getList(true);
      }),
    [],
  );

  useEffect(() => {
    onRefresh();
  }, [NotiNumber]);

  useEffect(() => {
    setPageLoader(true);
    setTimeout(() => {
      getList(true);
    }, 1000);
  }, [active]);

  async function getList(bool) {
    const cPage =
      listData && listData.pagination && listData.pagination.currentPage
        ? _.toNumber(listData.pagination.currentPage)
        : 0;
    if (isConnected === true) {
      let PageNo = 0;
      if (bool === true) {
        PageNo = 1;
      } else {
        PageNo = cPage + 1;
      }

      const oData = {
        page: PageNo,
      };

      try {
        let endPoint = `${BaseSetting.endpoints.customer_order_list}?page=${PageNo}`;
        const response = await getApiData(
          navigation,
          endPoint,
          'GET',
          null,
          header,
        );
        if (
          _.isObject(response) &&
          !_.isEmpty(response) &&
          response.success === true
        ) {
          const obj = bool ? {} : _.cloneDeep(listData);

          const newListData =
            response && response.data && response.data.rows
              ? response.data.rows
              : [];
          const paginationDatas =
            response && response.data && response.data.pagination
              ? response.data.pagination
              : {};

          if (_.isArray(newListData)) {
            if (_.isArray(obj.data) && obj.data.length > 0) {
              obj.data = _.flattenDeep([...obj.data, newListData]);
            } else {
              obj.data = newListData;
            }
            obj.pagination = paginationDatas;
          }
          setListData(obj);
          handleAllLoader();
        } else {
          const errorMessage = checkObject(response, 'message');
          CAlert(errorMessage, StaticHeader.Oops, () => {
            setListData(listData);
            handleAllLoader();
          });
        }
      } catch (err) {
        console.log('Catch Part', err);
        CAlert(StaticAlertMsg.someThing_Went_Wrong, StaticHeader.Oops, () => {
          handleAllLoader();
        });
      }
    } else {
      CAlert(StaticAlertMsg.Network_Problem, StaticHeader.Oops, () => {
        handleAllLoader();
      });
    }
  }

  async function getFilterList() {
    const cPage =
      listData && listData.pagination && listData.pagination.currentPage
        ? _.toNumber(listData.pagination.currentPage)
        : 0;
    if (isConnected === true) {
      const filterOrderData = {
        order_no: _.toNumber(searchTxt),
      };
      try {
        let endPoint = `${
          BaseSetting.endpoints.order_list
        }?order_no=${_.toNumber(searchTxt)}`;
        const response = await getApiData(
          navigation,
          endPoint,
          'GET',
          null,
          header,
        );
        if (
          _.isObject(response) &&
          !_.isEmpty(response) &&
          response.success === true
        ) {
          const obj = {};

          const newListData =
            response && response.data && response.data.rows
              ? response.data.rows
              : [];
          const paginationDatas =
            response && response.data && response.data.pagination
              ? response.data.pagination
              : {};

          if (_.isArray(newListData)) {
            obj.data = newListData;
            obj.pagination = paginationDatas;
          }
          setListData(obj);
          handleAllLoader();
        } else {
          const errorMessage = checkObject(response, 'message');
          CAlert(errorMessage, StaticHeader.Oops, () => {
            setListData(listData);
            handleAllLoader();
          });
        }
      } catch (err) {
        console.log('Catch Part', err);
        CAlert(StaticAlertMsg.someThing_Went_Wrong, StaticHeader.Oops, () => {
          handleAllLoader();
        });
      }
    } else {
      CAlert(StaticAlertMsg.Network_Problem, StaticHeader.Oops, () => {
        handleAllLoader();
      });
    }
  }

  function onRefresh() {
    setSearchTxt('');
    setRefreshLoader(true);
    setPageLoader(true);
    setTimeout(() => {
      getList(true);
    }, 500);
  }

  function handleAllLoader() {
    setTimeout(() => {
      setPageLoader(false);
      setRefreshLoader(false);
      setMoreLoad(false);
      setSearchBtnLoad(false);
    }, 200);
  }

  async function getMoreData() {
    const cPage =
      listData && listData.pagination && listData.pagination.currentPage
        ? _.toNumber(listData.pagination.currentPage)
        : 0;
    const tPage =
      listData && listData.pagination && listData.pagination.totalPage
        ? _.toNumber(listData.pagination.totalPage)
        : 0;

    if (listData.pagination.isMore === true && cPage < tPage) {
      setMoreLoad(true);
      getList(false);
    }
  }

  function changeTab(no) {
    setActive(no);
  }

  const renderItem = ({item, index}) => {
    return (
      <OrderDetailCard
        key={index}
        data={item}
        from="User"
        onShowOrderDetail={() => {
          navigation.navigate('UserOrderDetails', item);
        }}
      />
    );
  };

  function renderFooterComponent() {
    if (moreLoad) {
      return (
        <View style={styles.loaderFooterView}>
          <ActivityIndicator
            size={'small'}
            animating
            color={BaseColor.ThemeOrange}
          />
        </View>
      );
    } else {
      return <View style={styles.listFooterView} />;
    }
  }

  function renderEmptyComponent() {
    return (
      <EmptyView
        emptyText={"Sorry you don't have any order yet."}
        btnLoader={refreshLoader}
        onPress={() => {
          onRefresh();
        }}
      />
    );
  }

  function renderHeaderComponent() {
    return (
      <View style={styles.headerMainCon}>
        <TopTabs
          onPress={(no) => {
            changeTab(no);
          }}
          active={active}
          btnArray={userOrderTab}
          otherMain={styles.otherTabCon}
          otherSty={styles.tabButtonSty}
          otherFontSty={styles.tabTitle}
        />
      </View>
    );
  }

  function renderList() {
    return (
      <FlatList
        ref={mailListRef}
        data={listData.data}
        scrollsToTop={false}
        showsVerticalScrollIndicator={false}
        onEndReachedThreshold={0.5}
        keyExtractor={(item) => item.id}
        renderItem={renderItem}
        contentContainerStyle={styles.flatListMainCon}
        // ListHeaderComponent={renderHeaderComponent} // Top tab Removed by sir Now not in functionality. Default Display All Orders
        ListFooterComponent={renderFooterComponent}
        ListEmptyComponent={renderEmptyComponent}
        // stickyHeaderIndices={[0]}
        onEndReached={getMoreData}
        refreshControl={
          <RefreshControl
            colors={[BaseColor.ThemeOrange]}
            tintColor={BaseColor.ThemeOrange}
            refreshing={refreshLoader}
            onRefresh={() => {
              onRefresh();
            }}
          />
        }
      />
    );
  }

  function renderLoader() {
    return <MyOrderLoader />;
  }

  function renderHeader() {
    return <GredientHeader title={'My Orders'} />;
  }

  function onClearInputCon() {
    Keyboard.dismiss();
    searchInputRef.current.blur();
    enableAnimateInEaseOut();
    setSearchTxt('');
    setPageLoader(true);
    setTimeout(() => {
      getList(true);
    }, 100);
  }

  function onGoAction() {
    Keyboard.dismiss();
    searchInputRef.current.blur();
    setSearchBtnLoad(true);
    setPageLoader(true);
    setTimeout(() => {
      getFilterList();
    }, 100);
  }

  function renderSearchBar() {
    const isTestEmpty = searchTxt === '';
    return (
      <LinearGradient
        start={{x: 0.3, y: 0.25}}
        end={{x: 0.8, y: 1.0}}
        colors={[BaseColor.ThemeOrange, BaseColor.ThemeYellow]}
        style={styles.searchBarCon}>
        <View
          style={[
            styles.mainCon,
            {
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
            },
          ]}>
          <View style={styles.mainCon}>
            <TextInput
              ref={searchInputRef}
              leftIconName={'search'}
              otherCon={[
                styles.searchInputCon,
                {
                  borderTopRightRadius: isTestEmpty ? 5 : 0,
                  borderBottomRightRadius: isTestEmpty ? 5 : 0,
                },
              ]}
              otherIconCss={styles.searchIconClr}
              placeholder="Search order by order no..."
              onChangeText={(t) => {
                enableAnimateInEaseOut();
                setSearchTxt(t);
                if (t === '') {
                  onClearInputCon();
                }
              }}
              onSubmitEditing={
                searchBtnLoad
                  ? null
                  : () => {
                      onGoAction();
                    }
              }
              value={searchTxt}
              keyboardType={'numeric'}
            />
          </View>
          {!isTestEmpty ? (
            <TouchableOpacity
              activeOpacity={0.8}
              onPress={
                searchBtnLoad
                  ? null
                  : () => {
                      onClearInputCon();
                    }
              }
              style={{
                paddingHorizontal: 10,
                height: 40,
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: BaseColor.whiteColor,
              }}>
              <Icon name={'close'} size={12} color={BaseColor.blackColor} />
            </TouchableOpacity>
          ) : null}
        </View>

        {!isTestEmpty ? (
          <Button
            buttonText={'Go'}
            loading={searchBtnLoad}
            style={styles.searchBtnSty}
            otherGredientCss={{
              borderRadius: 0,
              height: 40,
              borderTopRightRadius: 5,
              borderBottomRightRadius: 5,
            }}
            onPress={
              searchBtnLoad
                ? null
                : () => {
                    onGoAction();
                  }
            }
          />
        ) : null}
      </LinearGradient>
    );
  }

  return (
    <View style={styles.mainCon}>
      {renderHeader()}
      {renderSearchBar()}
      {pageLoader ? renderLoader() : renderList()}
    </View>
  );
}
