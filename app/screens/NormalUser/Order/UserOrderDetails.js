/* eslint-disable react-native/no-inline-styles */
import React, {useCallback, useEffect, useRef, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import _ from 'lodash';
import Moment from 'moment';
import {
  View,
  Image,
  FlatList,
  StyleSheet,
  Dimensions,
  ScrollView,
  RefreshControl,
  ActivityIndicator,
} from 'react-native';
import Text from '../../../components/Text';
import {Images} from '../../../config/images';
import {BaseColor} from '../../../config/theme';
import CAlert from '../../../components/CAlert';
import {BaseSetting} from '../../../config/setting';
import Button from '../../../components/Button/index';
import Toast from '../../../components/Toast/index';
import OrderedItems from '../../../components/MyOrders/OrderedItems';
import {getApiData, getApiDataProgress} from '../../../utils/apiHelper';
import GredientHeader from '../../../components/DriverApp/GredientHeader';
import {StaticAlertMsg, StaticHeader} from '../../../config/StaticAlertMsg';
import {checkObject, getOrderStatusClr} from '../../../utils/commonFunction';
import {OrderDetailLoader} from '../../../components/ContentLoader/ContentLoader';

const nWidth = BaseSetting.nWidth;
const nHeight = BaseSetting.nHeight;

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: BaseColor.whiteColor,
  },
  commonFlexSty: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 10,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderColor: BaseColor.lightGrey,
  },
  scrollCon: {
    flexGrow: 1,
  },
  btnView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 10,
    marginBottom: 30,
  },
  W45: {
    width: '48%',
  },
  btnTxtColor: {
    color: BaseColor.blackColor,
  },
  orderInfoCon: {
    flex: 1,
  },
  cmnFlexCon: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  PT10: {
    paddingTop: 10,
  },
  orderAddCon: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'space-between',
    paddingTop: 10,
  },
  imgView: {
    flexDirection: 'row',
    paddingLeft: 3,
    alignItems: 'center',
  },
  imageStyle: {
    width: nWidth / 9,
    height: nHeight / 25,
    resizeMode: 'contain',
    elevation: 5,
    shadowColor: BaseColor.blackColor,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
  },
  PL5: {
    paddingLeft: 5,
  },
  PV5: {
    paddingVertical: 5,
  },
  P15: {
    padding: 15,
  },
  bottomView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 15,
  },
});

const UserOrderDetails = ({route, navigation}) => {
  const ToastRef = useRef();

  const item = route.params;
  const {isConnected, NotiNumber, userData} = useSelector((state) => state.auth);
  const [pageLoader, setPageLoader] = useState(false);
  const [orderData, setOrderData] = useState(item);
  const [refreshLoader, setRefreshLoader] = useState(false);
  const [reOrderBtnLoader, setReorderButtonLoader] = useState(false);
  const [cancelOrderBtnLoad, setCancelOrderBtnLoad] = useState(false);
  const [returnBtnLoad, setReturnBtnLoad] = useState(false);

  const orderId = checkObject(orderData, 'order_id');
  const userToken = checkObject(userData, 'access_token');

  const header = {
    Authorization: `Bearer ${userToken}`,
  };

  React.useEffect(
    () =>
      navigation.addListener('focus', () => {
        // getOrderDetail();
      }),
    [],
  );

  useEffect(() => {
    onRefresh();
  }, [NotiNumber]);

  function onRefresh() {
    setRefreshLoader(true);
    setPageLoader(true);
    setTimeout(() => {
      getOrderDetail();
    }, 500);
  }

  function handleAllLoader() {
    setTimeout(() => {
      setPageLoader(false);
      setReturnBtnLoad(false);
      setRefreshLoader(false);
      setCancelOrderBtnLoad(false);
      setReorderButtonLoader(false);
    }, 200);
  }

  async function getOrderDetail() {
    const orderData = {
      id: orderId,
    };

    if (isConnected === true) {
      try {
        let endPoint = `${BaseSetting.endpoints.customer_order_details}?id=${orderId}`;
        const response = await getApiData(
          navigation,
          endPoint,
          'GET',
          null,
          header,
        );
        if (
          _.isObject(response) &&
          !_.isEmpty(response) &&
          response.success === true
        ) {
          const details = checkObject(response, 'data');
          setOrderData(details);
          handleAllLoader();
        } else {
          const errorMessage = checkObject(response, 'message');
          CAlert(errorMessage, StaticHeader.Oops, () => {
            handleAllLoader();
          });
        }
      } catch (err) {
        console.log('Catch Part', err);
        CAlert(StaticAlertMsg.someThing_Went_Wrong, StaticHeader.Oops, () => {
          handleAllLoader();
        });
      }
    } else {
      CAlert(StaticAlertMsg.Network_Problem, StaticHeader.Oops, () => {
        handleAllLoader();
      });
    }
  }

  async function reOrderAction() {
    const orderData = {
      id: orderId,
    };

    if (isConnected === true) {
      try {
        let endPoint = `${BaseSetting.endpoints.re_Order}?id=${orderId}`;
        const response = await getApiData(
          navigation,
          endPoint,
          'GET',
          null,
          header,
        );
        if (
          _.isObject(response) &&
          !_.isEmpty(response) &&
          response.success === true
        ) {
          const successMessage = checkObject(response, 'message');
          ToastRef.current.show(successMessage, 2000);
          onRefresh();
        } else {
          const errorMessage = checkObject(response, 'message');
          CAlert(errorMessage, StaticHeader.Oops, () => {
            setReorderButtonLoader(false);
          });
        }
      } catch (err) {
        console.log('Catch Part', err);
        CAlert(StaticAlertMsg.someThing_Went_Wrong, StaticHeader.Oops, () => {
          setReorderButtonLoader(false);
        });
      }
    } else {
      CAlert(StaticAlertMsg.Network_Problem, StaticHeader.Oops, () => {
        setReorderButtonLoader(false);
      });
    }
  }

  async function updateOrderStatus(status) {
    const returnOrderData = {
      order_id: orderId,
      status: status,
    };

    if (isConnected === true) {
      try {
        let endPoint = `${BaseSetting.endpoints.update_order_status}?order_id=${orderId}&status=${status}`;
        const response = await getApiData(
          navigation,
          endPoint,
          'GET',
          null,
          header,
        );
        if (
          _.isObject(response) &&
          !_.isEmpty(response) &&
          response.success === true
        ) {
          const successMessage = checkObject(response, 'message');
          onRefresh();
          ToastRef.current.show(successMessage, 2000);
        } else {
          const errorMessage = checkObject(response, 'message');
          CAlert(errorMessage, StaticHeader.Oops, () => {
            handleAllLoader();
          });
        }
      } catch (err) {
        console.log('Catch Part', err);
        CAlert(StaticAlertMsg.someThing_Went_Wrong, StaticHeader.Oops, () => {
          handleAllLoader();
        });
      }
    } else {
      CAlert(StaticAlertMsg.Network_Problem, StaticHeader.Oops, () => {
        handleAllLoader();
      });
    }
  }

  function renderOrderDetails() {
    const orderNo = checkObject(orderData, 'order_no');
    const orderStatus = checkObject(orderData, 'order_status');
    const orderDate = checkObject(orderData, 'order_date');
    const deliveryDate = checkObject(orderData, 'deliver_date');
    const deliveryType = checkObject(orderData, 'delivery_type');
    const productList = checkObject(orderData, 'product');
    const totalProduct = checkObject(orderData, 'total_qty');
    const paymentType = checkObject(orderData, 'payment_mode');
    const address = checkObject(orderData, 'delivery_address');

    const storeData = checkObject(orderData, 'store');
    const storeImg = checkObject(storeData, 'store_thumb');
    const storeName = checkObject(storeData, 'store_name');
    const storeAddress = checkObject(storeData, 'address');

    const orderAmount = checkObject(orderData, 'order_amount');
    const orderDiscount = checkObject(orderData, 'discount');
    const totalAmount = checkObject(orderData, 'total_amount');

    return (
      <ScrollView
        bounces={false}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={styles.scrollCon}
        refreshControl={
          <RefreshControl
            colors={[BaseColor.ThemeOrange]}
            tintColor={BaseColor.ThemeOrange}
            refreshing={refreshLoader}
            onRefresh={() => {
              onRefresh();
            }}
          />
        }>
        <View style={styles.orderInfoCon}>
          <View
            style={{
              paddingTop: 10,
              flexDirection: 'row',
              alignItems: 'flex-start',
              justifyContent: 'space-between',
              paddingHorizontal: 10,
            }}>
            <View
              style={{
                width: nWidth / 6,
                height: nWidth / 6,
                overflow: 'hidden',
                borderRadius: 5,
                padding: 3,
                borderWidth: StyleSheet.hairlineWidth,
                borderColor: BaseColor.grayColor,
              }}>
              <Image
                source={{uri: storeImg}}
                style={{
                  width: '100%',
                  height: '100%',
                  borderRadius: 5,
                  resizeMode: 'contain',
                }}
              />
            </View>

            <View style={{flex: 1, paddingLeft: 10}}>
              <Text numberOfLines={2} body1 blackColor bold>
                {storeName}
              </Text>
              <Text
                caption1
                numberOfLines={2}
                grayColor
                style={{paddingVertical: 5}}>
                {storeAddress}
              </Text>
            </View>
          </View>

          <View style={styles.commonFlexSty}>
            <View style={{width: '50%'}}>
              <Text numberOfLines={1} grayColor semibold body2 textAlignLeft>
                Order No
              </Text>
              <Text
                numberOfLines={1}
                blackColor
                bold
                body2
                textAlignLeft>{`${orderNo}`}</Text>
            </View>

            <View style={{width: '50%'}}>
              <Text numberOfLines={1} grayColor semibold body2 textAlignRight>
                Order Status
              </Text>
              <Text
                body2
                bold
                textAlignRight
                style={{color: getOrderStatusClr(orderStatus)}}>
                {_.toUpper(orderStatus)}
              </Text>
            </View>
          </View>

          <View
            style={{
              padding: 10,
              borderBottomWidth: StyleSheet.hairlineWidth,
              borderColor: BaseColor.lightGrey,
            }}>
            <Text numberOfLines={1} grayColor semibold body2>
              Shipping Address:
            </Text>
            <Text blackColor bold body2 style={{paddingTop: 5}}>
              {address}
            </Text>
          </View>

          <View style={styles.commonFlexSty}>
            <View style={styles.width50}>
              <Text numberOfLines={1} grayColor semibold body2 textAlignLeft>
                Delivery Date & Time
              </Text>
              <Text
                numberOfLines={1}
                blackColor
                bold
                body2
                textAlignLeft
                style={{paddingTop: 2}}>
                {deliveryDate}
              </Text>
            </View>

            <View style={styles.width50}>
              <Text numberOfLines={1} grayColor semibold body2 textAlignRight>
                Order Date
              </Text>
              <Text
                numberOfLines={1}
                blackColor
                bold
                body2
                textAlignRight
                style={{paddingTop: 2}}>
                {orderDate}
              </Text>
            </View>
          </View>

          {/* {_.isArray(productList) && !_.isEmpty(productList) ?
            <View style={styles.PV5}>
              <Text body2>{productList.length} items</Text>
              {productList.map((item, index) => {
                return <OrderedItems key={index} data={item} />;;
              })}
            </View>
          : null} */}

          <View style={styles.commonFlexSty}>
            <View style={styles.width50}>
              <Text numberOfLines={1} grayColor semibold body2 textAlignLeft>
                Payment Type
              </Text>
              <Text numberOfLines={1} blackColor bold body2 textAlignLeft>
                {paymentType}
              </Text>
            </View>
            {paymentType === 'CARD' ? (
              <View style={styles.width50}>
                <Text numberOfLines={1} grayColor semibold body2 textAlignRight>
                  Card No
                </Text>
                <Text numberOfLines={1} blackColor bold body2 textAlignRight>
                  {'**** **** **** 9010'}
                </Text>
              </View>
            ) : (
              <View style={styles.width50} />
            )}
          </View>

          {_.isArray(productList) && !_.isEmpty(productList) ? (
            <View>
              <View style={[styles.commonFlexSty, {borderBottomWidth: 0}]}>
                <Text numberOfLines={1} grayColor semibold body2 textAlignLeft>
                  Ordered items
                </Text>
                <Text numberOfLines={1} grayColor bold body2 textAlignRight>
                  {totalProduct}
                </Text>
              </View>
              {productList.map((item, index) => {
                return <OrderedItems key={index} data={item} />;
              })}
            </View>
          ) : null}

          <View style={styles.bottomView}>
            <View>
              <Text numberOfLines={1} grayColor semibold body2 textAlignLeft>
                Total Amount
              </Text>
              <Text
                numberOfLines={1}
                blackColor
                bold
                body2
                textAlignLeft
                style={{paddingTop: 2}}>
                {`${orderAmount}`}
              </Text>
            </View>
            <View>
              <Text numberOfLines={1} grayColor semibold body2 textAlignCenter>
                Discount
              </Text>
              <Text
                numberOfLines={1}
                blackColor
                bold
                body2
                textAlignCenter
                style={{paddingTop: 2}}>
                {`${orderDiscount}`}
              </Text>
            </View>
            <View>
              <Text numberOfLines={1} grayColor semibold body2 textAlignRight>
                Net Amount
              </Text>
              <Text
                numberOfLines={1}
                blackColor
                bold
                body2
                textAlignRight
                style={{paddingTop: 2}}>
                {`${totalAmount}`}
              </Text>
            </View>
          </View>
        </View>
        {renderBottomButton()}
      </ScrollView>
    );
  }

  function renderBottomButton() {
    const orderStatus = checkObject(orderData, 'order_status');

    if (orderStatus === 'ongoing') {
      return (
        <View style={styles.P15}>
          <Button
            otherGredientCss={{borderWidth: StyleSheet.hairlineWidth}}
            loading={cancelOrderBtnLoad}
            styleText={styles.btnTxtColor}
            loaderColor={BaseColor.ThemeBlue}
            colorAry={[BaseColor.whiteColor, BaseColor.whiteColor]}
            buttonText={'Cancel Order'}
            onPress={() => {
              CAlert(
                StaticAlertMsg.cancelOrder,
                StaticHeader.Confirm,
                () => {
                  setCancelOrderBtnLoad(true);
                  updateOrderStatus('cancelled');
                },
                () => {
                  return null;
                },
                'YES',
                'NO',
              );
            }}
          />
        </View>
      );
    }

    if (orderStatus === 'delivered') {
      return (
        <View style={styles.btnView}>
          <View style={styles.W45}>
            <Button
              loading={reOrderBtnLoader}
              otherGredientCss={{borderWidth: StyleSheet.hairlineWidth}}
              colorAry={[BaseColor.whiteColor, BaseColor.whiteColor]}
              buttonText={'Reorder'}
              onPress={() => {
                setReorderButtonLoader(true);
                reOrderAction();
              }}
              styleText={styles.btnTxtColor}
              loaderColor={BaseColor.ThemeBlue}
            />
          </View>

          <View style={styles.W45}>
            <Button
              colorAry={[BaseColor.ThemeBlue, BaseColor.ThemeBlue]}
              loading={returnBtnLoad}
              buttonText={'Return Order'}
              onPress={() => {
                CAlert(
                  StaticAlertMsg.returnOrder,
                  StaticHeader.Confirm,
                  () => {
                    setReturnBtnLoad(true);
                    updateOrderStatus('return');
                  },
                  () => {
                    return null;
                  },
                  'YES',
                  'NO',
                );
              }}
            />
          </View>
        </View>
      );
    }

    return null;
  }

  function renderLoader() {
    return <OrderDetailLoader />;
  }

  function renderHeader() {
    return (
      <GredientHeader
        leftIcon
        title={'Order Details'}
        onLeftAction={() => {
          navigation.pop();
        }}
      />
    );
  }

  return (
    <View style={styles.main}>
      {renderHeader()}
      {pageLoader ? renderLoader() : renderOrderDetails()}
      <Toast
        ref={ToastRef}
        position="bottom"
        positionValue={150}
        fadeInDuration={750}
        fadeOutDuration={2000}
        opacity={0.8}
      />
    </View>
  );
};
export default UserOrderDetails;
