import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/MaterialIcons';
import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import {
  View,
  Image,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import Animated, {
  useAnimatedStyle,
  useSharedValue,
  withTiming,
  withSpring,
} from 'react-native-reanimated';
import {slides} from '../../../data/StaticData';
import {BaseColor} from '../../../config/theme';
import {BaseSetting} from '../../../config/setting';
import {FontFamily} from '../../../config/typography';
import {setStatusbar} from '../../../config/statusbar';
import authActions from '../../../redux/reducers/auth/actions';
import { enableAnimateInEaseOut } from '../../../utils/commonFunction';

const { setIntroSlider } = authActions;
const nWidth = BaseSetting.nWidth;
const nHeight = BaseSetting.nHeight;

const styles = StyleSheet.create({
  mainCon: {
    flex: 1,
    position: 'relative',
    backgroundColor: BaseColor.whiteColor,
  },
  imageCon:{
    padding: 10,
    width: nWidth,
    height: nHeight - 275,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: BaseColor.whiteColor,
  },
  imgSty: {
    width: '100%',
    height: '100%',
  },
  contentView:{
    flex:1,
    width: nWidth,
    paddingVertical: 22,
    position: 'relative', 
    paddingHorizontal: 34,
    borderTopLeftRadius: 50, 
  },
  titleText:{
    color: BaseColor.whiteColor,
    fontSize: 26, 
    textAlign: 'center', 
    paddingBottom: 20,
    paddingTop:14,
    fontFamily: FontFamily.regular,
  },
  descriptionText:{
    color: BaseColor.whiteColor, 
    fontSize: 14, 
    textAlign: 'center',
    fontFamily: FontFamily.regular,
  },
  paginationDotCon: {
    bottom: 25,
    alignSelf: 'center',
    flexDirection: 'row',
    position: 'absolute',
    alignItems: 'center',
    backgroundColor: '#0000',
  },
  dotSty: {
    backgroundColor: BaseColor.whiteColor,
    margin: 4,
    borderRadius: 5,
    height: 8,
  },
  buttonCircle: {
    width: 40,
    height: 40,
    backgroundColor: 'rgba(0, 0, 0, .2)',
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    bottom: 20,
    right: 20,
  },
});

const IntroSlider = ({ navigation }) => {
  React.useEffect(
    () => navigation.addListener('focus', () => setStatusbar('light')),
    [],
  );

  React.useEffect(
    () => navigation.addListener('blur', () => setStatusbar('blue')),
    [],
  );

  const animTime = 200;
  const [currentIndex, setcurrentIndex] = useState(0);

  const dispatch = useDispatch();

  const anim = useSharedValue(1);
  const nextImgAnim = useSharedValue(0);

  const offset = useSharedValue(0);

  const defaultSpringStyles = useAnimatedStyle(() => {
    return {
      transform: [{ translateX: withSpring(offset.value * 255) }],
    };
  });

  const titleStyle = useAnimatedStyle(() => ({
    opacity: withTiming(
      anim.value,
      {
        duration: animTime,
      },
      () => {
        anim.value = 1;
      }
    ),
  }));

  const desStyle = useAnimatedStyle(() => ({
    opacity: withTiming(
      anim.value,
      {
        duration: animTime,
      },
      () => {
        anim.value = 1;
      }
    ),
  }));

  const changeIndex = () => {
    if (currentIndex !== 2) {
      nextImgAnim.value = 0;
      anim.value = 0.2;
      setTimeout(() => {
        const nIndex = currentIndex + 1;
        setcurrentIndex(nIndex);
      }, animTime);
    }
  };

  const preIndex = () => {
    if (currentIndex !== 0) {
      anim.value = 0.2;
      setTimeout(() => {
        const nIndex = currentIndex - 1;
        setcurrentIndex(nIndex);
      }, animTime);
    }
  };

  enableAnimateInEaseOut();
  return (
    <View style={styles.mainCon}>
      <Animated.View style={[styles.imageCon, defaultSpringStyles]}>
        <Image
          style={styles.imgSty}
          source={slides[currentIndex].image}
          resizeMode="contain"
        />
      </Animated.View>
      
      <LinearGradient
        start={{x: 0.2, y: 0}}
        end={{x: 0.8, y: 1.0}}
        colors={[BaseColor.ThemeOrange, BaseColor.ThemeYellow]}
        style={styles.contentView}
      >
        <Animated.Text style={[styles.titleText, titleStyle]}>
          {slides[currentIndex].title}
        </Animated.Text>
        <Animated.Text style={[styles.descriptionText, desStyle]}>
          {slides[currentIndex].text}
        </Animated.Text>
        <View style={styles.paginationDotCon}>
          {slides.map((item, index) => (
            <Animated.View
              key={index}
              style={[styles.dotSty, {
                width: index === currentIndex ? 20 : 8,
              }]}
            />
          ))}
        </View>
      </LinearGradient>

      {currentIndex >= 1 ? (
        <TouchableOpacity
          activeOpacity={0.8}
          style={[styles.buttonCircle, {
            left: 20,
          }]}
          onPress={preIndex}
        >
          <Icon name="keyboard-arrow-left" color={BaseColor.whiteColor} size={25} />
        </TouchableOpacity>
      ) : null}

      {currentIndex >= 0 && currentIndex < 2 ? (
        <TouchableOpacity
          activeOpacity={0.8}
          style={styles.buttonCircle}
          onPress={changeIndex}
        >
          <Icon name="keyboard-arrow-right" color={BaseColor.whiteColor} size={25} />
        </TouchableOpacity>
      ) : null}

      {currentIndex === 2 ? (
        <TouchableOpacity
          activeOpacity={0.8}
          style={styles.buttonCircle}
          onPress={() => {
            dispatch(setIntroSlider(false));
            navigation.navigate('WelCome');
          }}
        >
          <Icon name="check" color={BaseColor.whiteColor} size={25} />
        </TouchableOpacity>
      ) : null}
    </View>
  );
};

export default IntroSlider;
