import React from 'react';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {useDispatch, useSelector} from 'react-redux';
import _ from 'lodash';
import {
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  ActivityIndicator,
  View,
} from 'react-native';
import CAlert from '../../../components/CAlert';
import {BaseColor} from '../../../config/theme';
import Text from '../../../components/Text/index';
import {getApiData} from '../../../utils/apiHelper';
import {BaseSetting} from '../../../config/setting';
import {checkObject} from '../../../utils/commonFunction';
import {UserSettingScreenList} from '../../../data/StaticData';
import authActions from '../../../redux/reducers/auth/actions';
import GredientHeader from '../../../components/DriverApp/GredientHeader';
import {StaticAlertMsg, StaticHeader} from '../../../config/StaticAlertMsg';

const {clearLocalStorageData, setLogoutLoad} = authActions;

const styles = StyleSheet.create({
  mainCon: {
    flex: 1,
    backgroundColor: BaseColor.whiteColor,
  },
  scrollMainCon: {
    flexGrow: 1,
    backgroundColor: BaseColor.whiteColor,
  },
  mainListCon: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 10,
    paddingVertical: 20,
    borderTopColor: BaseColor.lightGrey,
    borderTopWidth: StyleSheet.hairlineWidth,
  },
  iconSty: {
    fontSize: 22,
    color: BaseColor.blackColor,
  },
});

export default function Account({navigation}) {
  const {isConnected, userData, logoutLoad, uuid} = useSelector(
    (state) => state.auth,
  );
  const userToken = checkObject(userData, 'access_token');

  const header = {
    Authorization: `Bearer ${userToken}`,
  };

  const dispatch = useDispatch();

  function routeScreen(obj) {
    const screenName = checkObject(obj, 'screenName');
    const title = checkObject(obj, 'title');

    if (screenName === 'Logout') {
      CAlert(
        StaticAlertMsg.logout,
        StaticHeader.Confirm,
        () => {
          dispatch(setLogoutLoad(true));
          logoutAction();
        },
        () => {
          return null;
        },
        'YES',
        'NO',
      );
    }
    if (title === 'Terms & Condition' || title === 'About Us') {
      navigation.navigate(screenName, title);
    } else {
      navigation.navigate(screenName);
    }
  }

  async function logoutAction() {
    if (isConnected === true) {
      const logoutData = {
        uuid: uuid,
      };

      try {
        let endPoint = `${BaseSetting.endpoints.logout}?uuid=${uuid}`;
        const response = await getApiData(
          navigation,
          endPoint,
          'GET',
          null,
          header,
        );
        if (
          _.isObject(response) &&
          !_.isEmpty(response) &&
          response.success === true
        ) {
          backToLogin();
        } else {
          const errorMessage = checkObject(response, 'message');
          CAlert(errorMessage, StaticHeader.Oops, () => {
            dispatch(setLogoutLoad(false));
          });
        }
      } catch (err) {
        console.log('Catch Part', err);
        CAlert(StaticAlertMsg.someThing_Went_Wrong, StaticHeader.Oops, () => {
          dispatch(setLogoutLoad(false));
        });
      }
    } else {
      CAlert(StaticAlertMsg.Network_Problem, StaticHeader.Oops, () => {
        dispatch(setLogoutLoad(false));
      });
    }
  }

  function backToLogin() {
    dispatch(clearLocalStorageData());
    setTimeout(() => {
      navigation.navigate('WelCome');
    }, 2000);
  }

  return (
    <View style={styles.mainCon}>
      <GredientHeader title={'Setting'} />
      <ScrollView
        bounces={false}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={styles.scrollMainCon}>
        {UserSettingScreenList.map((obj, index) => {
          const title = checkObject(obj, 'title');
          const screenName = checkObject(obj, 'screenName');
          const isLogout = screenName === 'Logout';
          return (
            <TouchableOpacity
              key={index}
              activeOpacity={0.8}
              onPress={
                logoutLoad
                  ? null
                  : () => {
                      routeScreen(obj);
                    }
              }
              style={styles.mainListCon}>
              <Text body1 bold>
                {title}
              </Text>
              {isLogout && logoutLoad ? (
                <ActivityIndicator
                  size={'small'}
                  color={BaseColor.ThemeOrange}
                  animating
                />
              ) : null}
              {isLogout ? null : (
                <Icon name="keyboard-arrow-right" style={styles.iconSty} />
              )}
            </TouchableOpacity>
          );
        })}
      </ScrollView>
    </View>
  );
}
