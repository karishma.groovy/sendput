import React, {useRef} from 'react';
import LottieView from 'lottie-react-native';
import Geolocation from '@react-native-community/geolocation';
import {useDispatch, useSelector} from 'react-redux';
import _ from 'lodash';
import {View, StyleSheet} from 'react-native';
import {BaseColor} from '../../../config/theme';
import Text from '../../../components/Text/index';
import {BaseSetting} from '../../../config/setting';
import Button from '../../../components/Button/index';
import {FontFamily} from '../../../config/typography';
import {checkObject} from '../../../utils/commonFunction';
import authActions from '../../../redux/reducers/auth/actions';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import CGoogleAutoComplete from '../../../components/CGoogleAutoComplete';

const {setUserLocationsData} = authActions;

const styles = StyleSheet.create({
  image: {
    width: BaseSetting.nWidth / 5,
    height: BaseSetting.nHeight / 5,
    resizeMode: 'contain',
    alignSelf: 'center',
  },
  addCon: {
    height: 50,
    width: '100%',
    elevation: 5,
    marginTop: 15,
    borderRadius: 5,
    paddingHorizontal: 20,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: BaseColor.whiteColor,
    shadowColor: BaseColor.blackColor,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
  },
  addText: {
    fontFamily: FontFamily.medium,
    fontSize: 16,
    letterSpacing: 1,
    backgroundColor: '#0000',
    color: BaseColor.grayColor,
  },
  mapImage: {
    width: BaseSetting.nWidth / 3,
    height: BaseSetting.nHeight / 3,
    resizeMode: 'contain',
    alignSelf: 'center',
  },
  btn: {
    // borderRadius: 5,
  },
  scrollCon: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 20,
  },
});

const LocationAccess = ({navigation}) => {
  const {locationsData} = useSelector((state) => state.auth);
  const CGoogleAutoCompleteRef = useRef('');
  const animation = useRef();
  const dispatch = useDispatch();

  async function getLocationData() {
    Geolocation.getCurrentPosition(
      (position) => {
        const initialPosition = JSON.stringify(position);
        getCurrentLocationData({initialPosition});
      },
      (error) => console.log(error),
      {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000},
    );
  }

  async function getCurrentLocationData(data) {
    const cLat =
      data && data.coords && data.coords.latitude
        ? data.coords.latitude
        : 37.78825;
    const cLong =
      data && data.coords && data.coords.longitude
        ? data.coords.longitude
        : -122.4324;

    fetch(
      `https://maps.googleapis.com/maps/api/geocode/json?latlng=${cLat},${cLong}&key=${BaseSetting.googleMapAPIKey}`,
    )
      .then((response) => response.json())
      .then((resposeJson) => {
        console.log(
          '<=============================== Main resposeJson ===============================>',
        );
        console.log(resposeJson);
        if (resposeJson && resposeJson.status === 'OK') {
          if (
            _.isArray(resposeJson.results) &&
            !_.isEmpty(resposeJson.results)
          ) {
            let AddressName = resposeJson.results[0].formatted_address;
            console.log(
              '<=============================== resposeJson ===============================>',
            );
            console.log(resposeJson);
          }
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }

  function renderList() {
    const addressName = checkObject(locationsData, 'formatted_address');
    return (
      <View style={styles.scrollCon}>
        <LottieView
          ref={animation}
          autoSize={false}
          style={{
            width: BaseSetting.nWidth,
            height: BaseSetting.nWidth,
          }}
          source={require('../../../assets/lottie/mapPin.json')}
          autoPlay={true}
          loop={true}
        />
        {/* <Icon
          name="map-marker-radius-outline"
          size={200}
          color={BaseColor.blackColor}
        /> */}
        <View style={{paddingVertical: 70}}>
          <Text textAlignCenter body1>
            For the most accurate storefront locations and deals,{' '}
            <Text bold>Set your location</Text>
          </Text>
        </View>

        {/* <Button
            style={styles.btn}
            buttonText={'ENABLE LOCATION'}
            onPress={() => {
              getLocationData();
            }}
          /> */}

        <Button
          style={styles.btn}
          buttonText={'Set your location'}
          onPress={() => {
            CGoogleAutoCompleteRef.current.openModal();
          }}
        />

        {/* <TouchableOpacity
            activeOpacity={0.8}
            onPress={() => {
              CGoogleAutoCompleteRef.current.openModal();
            }}
            style={styles.addCon}
          >
            <Text numberOfLines={2} style={styles.addText}>{_.toUpper('Set your location manually')}</Text>
          </TouchableOpacity> */}
      </View>
    );
  }

  function onSave(loc) {
    dispatch(setUserLocationsData(loc));
    navigation.navigate('Login');
    CGoogleAutoCompleteRef.current.closeModal();
  }

  function onCloseAuto() {
    console.log('Close Event');
  }

  function renderGooglePlaceInput() {
    return (
      <CGoogleAutoComplete
        ref={CGoogleAutoCompleteRef}
        onSave={(loc) => {
          onSave(loc);
        }}
        onClose={() => {
          onCloseAuto();
        }}
      />
    );
  }

  return (
    <View style={{flex: 1}}>
      {renderList()}
      {renderGooglePlaceInput()}
    </View>
  );
};

export default LocationAccess;
