import React, {useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {
  View,
  StyleSheet,
  FlatList,
  Dimensions,
  Modal,
  Image,
  Keyboard,
  KeyboardAvoidingView,
} from 'react-native';
import {getApiDataProgress} from '../../../utils/apiHelper';
import authActions from '../../../redux/reducers/auth/actions';
import {BaseSetting} from '../../../config/setting';
import {StaticAlertMsg, StaticHeader} from '../../../config/StaticAlertMsg';
import CAlert from '../../../components/CAlert';
import {checkObject} from '../../../utils/commonFunction';
import Icon from 'react-native-vector-icons/AntDesign';
import Button from '../../../components/Button';
import Text from '../../../components/Text';
import {BaseColor} from '../../../config/theme';
import {Images} from '../../../config/images';
import {Rating} from '../../../components/RatingMsg/ratingMsg';
import TextInput from '../../../components/TextInput';
import {ratingUsData} from '../../../data/StaticData';
import GredientHeader from '../../../components/DriverApp/GredientHeader';
import _ from 'lodash';
const {setUserData, setUserType} = authActions;
import {ReviewsLoader} from '../../../components/ContentLoader/ContentLoader';
const styles = StyleSheet.create({
  main: {
    flex: 1,
  },
  flexCon: {
    flexGrow: 1,
  },
  ratingTopView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 30,
  },
  starsView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 10,
  },
  listFooter: {
    width: '100%',
    height: 30,
  },
  oopsTxt: {
    alignSelf: 'center',
  },
  oopsView: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  redViews: {
    backgroundColor: BaseColor.ThemeOrange,
    width: 90,
    height: 8,
    borderRadius: 5,
  },
  redViews2: {
    backgroundColor: BaseColor.ThemeOrange,
    width: 70,
    height: 8,
    borderRadius: 5,
  },
  redViews3: {
    backgroundColor: BaseColor.ThemeOrange,
    width: 50,
    height: 8,
    borderRadius: 5,
  },
  redViews4: {
    backgroundColor: BaseColor.ThemeOrange,
    width: 30,
    height: 8,
    borderRadius: 5,
  },
  redViews5: {
    backgroundColor: BaseColor.ThemeOrange,
    width: 10,
    height: 8,
    borderRadius: 5,
  },
  paddingR2: {
    paddingRight: 5,
  },
  oopsImage: {
    width: BaseSetting.nWidth / 2,
    height: BaseSetting.nHeight / 2,
    resizeMode: 'contain',
  },
  centerView: {
    elevation: 5,
    backgroundColor: BaseColor.whiteColor,
    borderRadius: 10,
    marginTop: 15,
    // overflow: 'hidden',
    marginHorizontal: 30,
    marginBottom: 10,
    position: 'relative',
    paddingLeft: 40,
    paddingRight: 20,
    paddingTop: 20,
    shadowColor: BaseColor.blackColor,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
  },
  centeredView: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  modalView: {
    backgroundColor: BaseColor.whiteColor,
    borderTopLeftRadius: 40,
    width: BaseSetting.nWidth,
    padding: 20,
    alignItems: 'center',
    shadowColor: BaseColor.blackColor,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  btnContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  btnWidth: {
    width: '30%',
    borderWidth: 1,
  },
  flexColumn: {
    flexDirection: 'column',
  },
  pddingTp10: {
    paddingTop: 10,
  },
  countView: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  pddingTp5: {
    paddingTop: 5,
  },
  pdingTp3: {
    paddingTop: 3,
  },
  reviewTextView: {
    paddingLeft: 15,
    paddingBottom: 10,
  },
  btn: {
    marginTop: 10,
    width: '50%',
    alignSelf: 'flex-end',
    marginRight: 20,
    marginBottom: 20,
  },
  modalTopEmptyView: {
    width: BaseSetting.nWidth / 6,
    height: 5,
    borderRadius: 10,
    backgroundColor: BaseColor.grayColor,
    marginBottom: 20,
  },
  alignCenter: {
    alignSelf: 'center',
  },
  starView: {
    flexDirection: 'row',
    paddingVertical: 20,
  },
  txt: {
    paddingBottom: 20,
  },
});

export default function Rate({navigation}) {
  const [loading, setLoading] = useState(false);
  const [modalVisible, setModalVisible] = useState(false);
  const [message, setMessage] = useState('');
  const [loader, setLoader] = useState(false);
  const {isConnected, userData} = useSelector((state) => state.auth);
  const userToken = checkObject(userData, 'access_token');
  const userId = checkObject(userData, 'id');
  const dispatch = useDispatch();

  React.useEffect(
    () =>
      navigation.addListener('focus', () => {
        reviewListActions();
      }),
    [],
  );

  const header = {
    Authorization: `Bearer ${userToken}`,
  };

  function Validate() {
    let valid = true;
    if (_.isEmpty(message)) {
      valid = false;
      CAlert(StaticAlertMsg.fillAllData, StaticHeader.Alert);
      return false;
    }
    if (_.trim(message).length >= 255) {
      valid = false;
      CAlert('Message should be of 255 chars only');
      return false;
    }
    if (valid === true) {
      Keyboard.dismiss();
      console.log('valid');
      setLoader(true);
      setTimeout(() => {
        reviewActions();
      }, 100);
    }
  }
  async function reviewListActions() {
    Keyboard.dismiss();
    // setLoader(true);
    if (isConnected === true) {
      const reviewListData = {
        id: userId,
      };

      try {
        let endPoint = `${BaseSetting.endpoints.review_list}?id=${userId}`;
        const response = await getApiDataProgress(
          navigation,
          endPoint,
          'GET',
          reviewListData,
          header,
        );
        if (
          _.isObject(response) &&
          !_.isEmpty(response) &&
          response.success === true
        ) {
          const msg = checkObject(response, 'message');
          // const userData = checkObject(response, 'data');
          // const uType = (userData, 'user_type');
          // dispatch(setUserType(uType));
          // dispatch(setUserData(userData));
          // CAlert(msg, StaticHeader.Success, () => {
          // navigation.pop();
          // setMessage('');
          // setLoader(false);
          // setModalVisible(false);

          // });
        } else {
          const successMessage = checkObject(response, 'message');
          CAlert(successMessage, StaticHeader.Oops, () => {
            setLoader(false);
          });
        }
      } catch (err) {
        console.log('Catch Part', err);
        CAlert(StaticAlertMsg.someThing_Went_Wrong, StaticHeader.Oops, () => {
          setLoader(false);
        });
      }
    } else {
      CAlert(StaticAlertMsg.Network_Problem, StaticHeader.Oops, () => {
        setLoader(false);
      });
    }
  }

  async function reviewActions() {
    Keyboard.dismiss();
    setLoader(true);
    if (isConnected === true) {
      const updatedData = {
        'Feedback[order_id]': 39,
        'Feedback[product_id]': 66,
        'Feedback[rating]': 3,
        'Feedback[comment]': message,
      };

      try {
        let endPoint = BaseSetting.endpoints.order_review;
        const response = await getApiDataProgress(
          navigation,
          endPoint,
          'POST',
          updatedData,
          header,
        );
        if (
          _.isObject(response) &&
          !_.isEmpty(response) &&
          response.success === true
        ) {
          const msg = checkObject(response, 'message');
          // const userData = checkObject(response, 'data');
          // const uType = (userData, 'user_type');
          // dispatch(setUserType(uType));
          // dispatch(setUserData(userData));
          CAlert(msg, StaticHeader.Success, () => {
            // navigation.pop();
            setMessage('');
            setLoader(false);
            setModalVisible(false);
          });
        } else {
          const successMessage = checkObject(response, 'message');
          CAlert(successMessage, StaticHeader.Oops, () => {
            setLoader(false);
            // setMessage('');
          });
        }
      } catch (err) {
        console.log('Catch Part', err);
        CAlert(StaticAlertMsg.someThing_Went_Wrong, StaticHeader.Oops, () => {
          setLoader(false);
          // setMessage('');
        });
      }
    } else {
      CAlert(StaticAlertMsg.Network_Problem, StaticHeader.Oops, () => {
        setLoader(false);
      });
    }
  }

  const createYellowStar = (no) => {
    let stars = [];

    for (let i = 0; i < no; i++) {
      stars.push(
        <Icon
          name="star"
          size={12}
          color={BaseColor.ThemeYellow}
          style={styles.paddingR2}
        />,
      );
    }
    return stars;
  };

  const createWhiteStar = (no, size) => {
    let stars = [];

    for (let i = 0; i < no; i++) {
      stars.push(
        <Icon
          name="staro"
          size={size}
          color={BaseColor.grayColor}
          style={styles.paddingR2}
        />,
      );
    }
    return stars;
  };

  const renderItem = ({item, index}) => {
    return (
      <Rating
        name={item.name}
        yellowStar={item.yellowStar}
        whiteStar={item.whiteStar}
        message={item.message}
        date={item.date}
      />
    );
  };
  const headerComponent = () => {
    return (
      <View>
        {/* <View style={styles.ratingTopView}>
          <View style={styles.pddingTp10}>
            <Text bold header blackColor>
              4.3
            </Text>
            <Text semibold body2 blackColor>
              23 rating
            </Text>
          </View>
          <View style={styles.flexColumn}>
            <View style={styles.starsView}>
              {createYellowStar(5)}
              <View style={styles.redViews}></View>
            </View>
            <View style={styles.starsView}>
              {createYellowStar(4)}
              <View style={styles.redViews2}></View>
            </View>
            <View style={styles.starsView}>
              {createYellowStar(3)}
              <View style={styles.redViews3}></View>
            </View>
            <View style={styles.starsView}>
              {createYellowStar(2)}
              <View style={styles.redViews4}></View>
            </View>
            <View style={styles.starsView}>
              {createYellowStar(1)}
              <View style={styles.redViews5}></View>
            </View>
          </View>
          <View style={styles.countView}>
            <Text semibold blackColor body2 style={styles.pddingTp5}>
              12
            </Text>
            <Text semibold blackColor body2 style={styles.pddingTp5}>
              5
            </Text>
            <Text semibold blackColor body2 style={styles.pddingTp5}>
              4
            </Text>
            <Text semibold blackColor body2 style={styles.pdingTp3}>
              2
            </Text>
            <Text semibold blackColor body2 style={styles.pdingTp3}>
              0
            </Text>
          </View>
        </View> */}
        {/* <View style={styles.reviewTextView}>
          <Text bold blackColor title2>
            8 reviews
          </Text>
        </View> */}
      </View>
    );
  };

  function renderHeader() {
    return (
      <GredientHeader
        leftIcon
        title={'Reviews'}
        onLeftAction={() => {
          navigation.pop();
        }}
      />
    );
  }

  return (
    <View style={styles.main}>
      {renderHeader()}
      {loading ? (
        <ReviewsLoader />
      ) : (
        <FlatList
          data={ratingUsData}
          renderItem={renderItem}
          keyExtractor={(item) => item.id}
          contentContainerStyle={styles.flexCon}
          ListFooterComponent={() => {
            return <View style={styles.listFooter} />;
          }}
          ListEmptyComponent={() => {
            return (
              <View style={styles.oopsView}>
                <Text title1 bold blackColor style={styles.oopsTxt}>
                  oops!!!
                </Text>
              </View>
            );
          }}
          ListHeaderComponent={() => {
            return headerComponent();
          }}
          refreshing={loading}
          onRefresh={() => {
            setTimeout(() => {
              setLoading(false);
            }, 500);
            setLoading(true);
          }}
        />
      )}

      {loading ? (
        <View />
      ) : (
        <Button
          style={styles.btn}
          displayLeftIcon
          iconName="pencil"
          colorAry={[BaseColor.ThemeOrange, BaseColor.ThemeYellow]}
          buttonText={'Write a review'}
          onPress={() => {
            setModalVisible(true);
          }}
          iconSty={{
            color: BaseColor.whiteColor,
          }}
        />
      )}

      <Modal
        transparent={true}
        // statusBarTranslucent
        animationType="slide"
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(false);
        }}>
        <View style={styles.centeredView}>
          {/* <KeyboardAvoidingView
            behavior="position"
          > */}
          <View style={styles.modalView}>
            <View style={styles.modalTopEmptyView} />
            <Text
              bold
              body1
              blackColor
              textAlignCenter
              style={styles.alignCenter}>
              What is your review?
            </Text>
            <View style={styles.starView}>{createWhiteStar(5, 36)}</View>
            <Text
              style={styles.txt}
              semibold
              body1
              blackColor
              textAlignCenter
              numberOfLines={2}>
              Please share your opinion{'\n'}
              about the product
            </Text>
            <TextInput
              placeholder="Your reviews"
              onChangeText={(t) => {
                setMessage(t);
              }}
              value={message}
              keyboardType={'default'}
              textArea
              otherCon={{
                borderWidth: 1,
                borderColor: BaseColor.blackColor,
                backgroundColor: BaseColor.whiteColor,
                elevation: 5,
                borderRadius: 0,
                shadowColor: BaseColor.blackColor,
                shadowOffset: {
                  width: 0,
                  height: 2,
                },
                shadowOpacity: 0.25,
                shadowRadius: 3.84,
              }}
            />
            <Button
              loading={loader}
              buttonText={'Send Review'}
              onPress={() => {
                // setModalVisible(false);
                Validate();
              }}
            />
          </View>
          {/* </KeyboardAvoidingView> */}
        </View>
      </Modal>
    </View>
  );
}
