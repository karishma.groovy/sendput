import React, {useRef, useState} from 'react';
// import LinearGradient from 'react-native-linear-gradient';
// import Icon from 'react-native-vector-icons/Ionicons';
import {useDispatch, useSelector} from 'react-redux';
import _ from 'lodash';
import {
  View,
  Keyboard,
  FlatList,
  StyleSheet,
  RefreshControl,
  ActivityIndicator,
  // TouchableOpacity,
} from 'react-native';
import Button from '../../../components/Button';
import CAlert from '../../../components/CAlert';
import {BaseColor} from '../../../config/theme';
import Text from '../../../components/Text/index';
import {BaseSetting} from '../../../config/setting';
import {getApiData} from '../../../utils/apiHelper';
// import TextInput from '../../../components/TextInput/index';
import authActions from '../../../redux/reducers/auth/actions';
import EmptyView from '../../../components/DriverApp/EmptyView';
import DispensariesCard from '../../../components/CHome/DispensariesCard';
import GredientHeader from '../../../components/DriverApp/GredientHeader';
import CGoogleAutoComplete from '../../../components/CGoogleAutoComplete';
import {StaticAlertMsg, StaticHeader} from '../../../config/StaticAlertMsg';
import {DispensariesLoader} from '../../../components/ContentLoader/ContentLoader';
import {
  checkObject,
  enableAnimateInEaseOut,
  getUserCurrentLatLog,
} from '../../../utils/commonFunction';

const {setUserLocationsData} = authActions;

const styles = StyleSheet.create({
  mainCon: {
    flex: 1,
    backgroundColor: BaseColor.whiteColor,
  },
  flexMainCon: {
    flexGrow: 1,
  },
  listFooterView: {
    width: '100%',
    height: 30,
  },
  loaderFooterView: {
    width: '100%',
    padding: 30,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: BaseColor.whiteColor,
  },
  searchBarCon: {
    width: '100%',
    height: 55,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    overflow: 'hidden',
    paddingHorizontal: 10,
  },
  searchInputCon: {
    width: '100%',
    height: 40,
    borderRadius: 0,
    backgroundColor: BaseColor.whiteColor,
    borderWidth: 0,
    marginTop: 0,
    borderTopLeftRadius: 5,
    borderBottomLeftRadius: 5,
  },
  searchIconClr: {
    color: BaseColor.grayColor,
  },
  searchBtnSty: {
    height: 40,
    width: '20%',
    borderRadius: 0,
    marginTop: 0,
    borderTopRightRadius: 5,
    borderBottomRightRadius: 5,
  },
});

export default function Dispensaries({navigation}) {
  const {isConnected, locationsData, userData} = useSelector(
    (state) => state.auth,
  );

  const [refreshLoader, setRefreshLoader] = useState(false);
  const [pageLoader, setPageLoader] = useState(true);
  const [moreLoad, setMoreLoad] = useState(false);
  const [listData, setListData] = useState({});
  const [searchTxt, setSearchTxt] = useState('');
  const [searchBtnLoad, setSearchBtnLoad] = useState(false);

  const mailListRef = useRef();
  const searchInputRef = useRef('');
  const CGoogleAutoCompleteRef = useRef('');

  const dispatch = useDispatch();

  const userToken = checkObject(userData, 'access_token');

  const header = {
    Authorization: `Bearer ${userToken}`,
  };

  React.useEffect(
    () =>
      navigation.addListener('focus', () => {
        setSearchTxt('');
        getList(true);
      }),
    [],
  );

  async function getList(bool, isStringBlank) {
    const LData = getUserCurrentLatLog();
    const cLat = checkObject(LData, 'currentLat');
    const cLng = checkObject(LData, 'currentLong');

    const cPage =
      listData && listData.pagination && listData.pagination.currentPage
        ? _.toNumber(listData.pagination.currentPage)
        : 0;
    if (isConnected === true) {
      let PageNo = 0;
      if (bool === true) {
        PageNo = 1;
      } else {
        PageNo = cPage + 1;
      }

      const lData = {
        page: PageNo,
        lat: cLat,
        lng: cLng,
        store_name: isStringBlank ? '' : searchTxt,
      };

      try {
        let endPoint = `${
          BaseSetting.endpoints.store_list
        }?page=${PageNo}&lat=${cLat}&lng=${cLng}&store_name=${
          isStringBlank ? '' : searchTxt
        }`;
        const response = await getApiData(
          navigation,
          endPoint,
          'GET',
          null,
          header,
        );
        if (
          _.isObject(response) &&
          !_.isEmpty(response) &&
          response.success === true
        ) {
          const obj = bool ? {} : _.cloneDeep(listData);

          const newListData =
            response && response.data && response.data.rows
              ? response.data.rows
              : [];
          const paginationDatas =
            response && response.data && response.data.pagination
              ? response.data.pagination
              : {};

          if (_.isArray(newListData)) {
            if (_.isArray(obj.data) && obj.data.length > 0) {
              obj.data = _.flattenDeep([...obj.data, newListData]);
            } else {
              obj.data = newListData;
            }
            obj.pagination = paginationDatas;
          }
          setListData(obj);
          handleAllLoader();
        } else {
          const errorMessage = checkObject(response, 'message');
          CAlert(errorMessage, StaticHeader.Oops, () => {
            setListData(listData);
            handleAllLoader();
          });
        }
      } catch (err) {
        console.log('Catch Part', err);
        CAlert(StaticAlertMsg.someThing_Went_Wrong, StaticHeader.Oops, () => {
          handleAllLoader();
        });
      }
    } else {
      CAlert(StaticAlertMsg.Network_Problem, StaticHeader.Oops, () => {
        handleAllLoader();
      });
    }
  }

  function handleAllLoader() {
    setTimeout(() => {
      setPageLoader(false);
      setRefreshLoader(false);
      setMoreLoad(false);
      setSearchBtnLoad(false);
    }, 200);
  }

  function onClearInputCon() {
    Keyboard.dismiss();
    searchInputRef.current.blur();
    enableAnimateInEaseOut();
    setSearchTxt('');
    setPageLoader(true);
    setTimeout(() => {
      getList(true, true);
    }, 100);
  }

  function onGoAction() {
    Keyboard.dismiss();
    searchInputRef.current.blur();
    setSearchBtnLoad(true);
    setPageLoader(true);
    setTimeout(() => {
      getList(true);
    }, 100);
  }

  async function getMoreData() {
    const cPage =
      listData && listData.pagination && listData.pagination.currentPage
        ? _.toNumber(listData.pagination.currentPage)
        : 0;
    const tPage =
      listData && listData.pagination && listData.pagination.totalPage
        ? _.toNumber(listData.pagination.totalPage)
        : 0;

    if (listData.pagination.isMore === true && cPage < tPage) {
      setMoreLoad(true);
      getList(false);
    }
  }

  function onRefresh() {
    setRefreshLoader(true);
    setPageLoader(true);
    setTimeout(() => {
      getList(true);
    }, 500);
  }

  function renderFooterComponent() {
    if (moreLoad) {
      return (
        <View style={styles.loaderFooterView}>
          <ActivityIndicator
            size={'small'}
            animating
            color={BaseColor.ThemeOrange}
          />
        </View>
      );
    } else {
      return <View style={styles.listFooterView} />;
    }
  }

  function renderEmptyComponent() {
    return (
      <EmptyView
        emptyText={'Sorry currently store data is empty.'}
        btnLoader={refreshLoader}
        onPress={() => {
          onRefresh();
        }}
      />
    );
  }

  const renderItem = ({item, index}) => {
    return (
      <DispensariesCard
        key={index}
        data={item}
        onPress={() => {
          navigation.navigate('DispensariesDetail', item);
        }}
      />
    );
  };

  function renderList() {
    return (
      <FlatList
        ref={mailListRef}
        data={listData.data}
        scrollsToTop={false}
        numColumns={2}
        horizontal={false}
        renderItem={renderItem}
        refreshing={refreshLoader}
        keyExtractor={(item) => item.id}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={styles.flexMainCon}
        ListFooterComponent={renderFooterComponent}
        ListEmptyComponent={renderEmptyComponent}
        refreshControl={
          <RefreshControl
            colors={[BaseColor.ThemeOrange]}
            tintColor={BaseColor.ThemeOrange}
            refreshing={refreshLoader}
            onRefresh={() => {
              onRefresh();
            }}
          />
        }
        onEndReached={getMoreData}
        onEndReachedThreshold={0.5}
      />
    );
  }

  function renderLoader() {
    return <DispensariesLoader />;
  }

  // function renderSearchBar() {
  //   const isTestEmpty = searchTxt === '';
  //   return (
  //     <LinearGradient
  //       start={{x: 0.3, y: 0.25}}
  //       end={{x: 0.8, y: 1.0}}
  //       colors={[BaseColor.ThemeOrange, BaseColor.ThemeYellow]}
  //       style={styles.searchBarCon}>
  //       <View
  //         style={[
  //           styles.mainCon,
  //           {
  //             flexDirection: 'row',
  //             alignItems: 'center',
  //             justifyContent: 'space-between',
  //           },
  //         ]}>
  //         <View style={styles.mainCon}>
  //           <TextInput
  //             ref={searchInputRef}
  //             leftIconName={'search'}
  //             otherCon={[
  //               styles.searchInputCon,
  //               {
  //                 borderTopRightRadius: isTestEmpty ? 5 : 0,
  //                 borderBottomRightRadius: isTestEmpty ? 5 : 0,
  //               },
  //             ]}
  //             otherIconCss={styles.searchIconClr}
  //             placeholder="Search by Store name..."
  //             onChangeText={(t) => {
  //               enableAnimateInEaseOut();
  //               setSearchTxt(t);
  //               if (t === '') {
  //                 onClearInputCon();
  //               }
  //             }}
  //             onSubmitEditing={
  //               searchBtnLoad
  //                 ? null
  //                 : () => {
  //                     onGoAction();
  //                   }
  //             }
  //             value={searchTxt}
  //           />
  //         </View>
  //         {!isTestEmpty ? (
  //           <TouchableOpacity
  //             activeOpacity={0.8}
  //             onPress={
  //               searchBtnLoad
  //                 ? null
  //                 : () => {
  //                     onClearInputCon();
  //                   }
  //             }
  //             style={{
  //               paddingHorizontal: 10,
  //               height: 40,
  //               alignItems: 'center',
  //               justifyContent: 'center',
  //               backgroundColor: BaseColor.whiteColor,
  //             }}>
  //             <Icon name={'close'} size={12} color={BaseColor.blackColor} />
  //           </TouchableOpacity>
  //         ) : null}
  //       </View>

  //       {!isTestEmpty ? (
  //         <Button
  //           buttonText={'Go'}
  //           loading={searchBtnLoad}
  //           style={styles.searchBtnSty}
  //           otherGredientCss={{
  //             borderRadius: 0,
  //             height: 40,
  //             borderTopRightRadius: 5,
  //             borderBottomRightRadius: 5,
  //           }}
  //           onPress={
  //             searchBtnLoad
  //               ? null
  //               : () => {
  //                   onGoAction();
  //                 }
  //           }
  //         />
  //       ) : null}
  //     </LinearGradient>
  //   );
  // }

  function renderHeader() {
    const addressName = checkObject(locationsData, 'formatted_address');
    return (
      <GredientHeader
        leftIcon
        onLeftAction={() => {
          navigation.pop();
        }}
        title={pageLoader ? '-' : addressName === '-' ? '-' : addressName}
        onRightAction={() => {
          CGoogleAutoCompleteRef.current.openModal();
        }}
        rightIcon
        rightIconName={'location-outline'}
      />
    );
  }

  function onSave(loc) {
    dispatch(setUserLocationsData(loc));
    onRefresh();
    CGoogleAutoCompleteRef.current.closeModal();
  }

  function onCloseAuto() {
    console.log('Close Event');
  }

  function renderGooglePlaceInput() {
    return (
      <CGoogleAutoComplete
        ref={CGoogleAutoCompleteRef}
        onSave={(loc) => {
          onSave(loc);
        }}
        onClose={() => {
          onCloseAuto();
        }}
      />
    );
  }

  return (
    <View style={styles.mainCon}>
      {renderHeader()}
      {/* {renderSearchBar()} */}
      {pageLoader ? renderLoader() : renderList()}
      {renderGooglePlaceInput()}
    </View>
  );
}
