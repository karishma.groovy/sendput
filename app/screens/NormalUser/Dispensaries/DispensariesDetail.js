/* eslint-disable react-native/no-inline-styles */
import React, {useCallback, useEffect, useRef, useState} from 'react';
import Icon from 'react-native-vector-icons/Ionicons';
import {useDispatch, useSelector} from 'react-redux';
import HTML from 'react-native-render-html';
import _ from 'lodash';
import {
  View,
  Modal,
  Image,
  Platform,
  Linking,
  Keyboard,
  FlatList,
  StyleSheet,
  ScrollView,
  RefreshControl,
  TouchableOpacity,
  ActivityIndicator,
  KeyboardAvoidingView,
} from 'react-native';
import Text from '../../../components/Text';
import {Images} from '../../../config/images';
import {BaseColor} from '../../../config/theme';
import CAlert from '../../../components/CAlert';
import {BaseSetting} from '../../../config/setting';
import TextInput from '../../../components/TextInput';
import Toast from '../../../components/Toast/index';
import Button from '../../../components/Button/index';
import EmptyView from '../../../components/DriverApp/EmptyView';
import {getApiData, getApiDataProgress} from '../../../utils/apiHelper';
import GredientHeader from '../../../components/DriverApp/GredientHeader';
import {StaticAlertMsg, StaticHeader} from '../../../config/StaticAlertMsg';
import StoreDealsCard from '../../../components/StoreDetails/StoreDealsCard';
import StoreReviewCard from '../../../components/StoreDetails/StoreReviewCard';
import StoreProductCard from '../../../components/StoreDetails/StoreProductCard';
import {
  checkObject,
  getUserCurrentLatLog,
  enableAnimateInEaseOut,
} from '../../../utils/commonFunction';
import {DispensariesDetailLoader} from '../../../components/ContentLoader/ContentLoader';

const nWidth = BaseSetting.nWidth;

const tabList = [
  {
    id: 1,
    title: 'PRODUCTS',
  },
  {
    id: 2,
    title: 'DETAILS',
  },
  {
    id: 3,
    title: 'DEALS',
  },
  {
    id: 4,
    title: 'REVIEWS',
  },
];

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: BaseColor.whiteColor,
  },
  FlatMainCon: {
    flexGrow: 1,
  },
  listFooterView: {
    width: '100%',
    height: 30,
  },
  loaderFooterView: {
    width: '100%',
    padding: 30,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: BaseColor.whiteColor,
  },
  storeInfoCon: {
    width: nWidth / 5,
    height: nWidth / 5,
    borderRadius: 5,
    padding: 3,
    overflow: 'hidden',
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: BaseColor.grayColor,
  },
  leftStoreInfoCon: {
    flex: 1,
    paddingLeft: 10,
  },
  infoMainCon: {
    padding: 10,
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'space-between',
  },
  rateStarCon: {
    paddingRight: 2,
    paddingVertical: 5,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  PR3: {
    paddingRight: 3,
  },
  PL3: {
    paddingLeft: 5,
  },
  buttonCon: {
    padding: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  btnSty: {
    elevation: 0,
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: BaseColor.lightBlack,
    width: '48%',
    borderRadius: 2,
    marginTop: 0,
    overflow: 'hidden',
  },
  tabsCon: {
    width: nWidth,
    height: 55,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderTopWidth: StyleSheet.hairlineWidth,
    borderTopColor: BaseColor.greyColor,
  },
  cmnFlex: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  addReviewBtn: {
    height: 50,
    width: 50,
    borderRadius: 25,
    alignItems: 'center',
    justifyContent: 'center',
  },
  reviewCon: {
    flex: 1,
    position: 'relative',
  },
  addReviewFixedCon: {
    marginTop: 0,
    position: 'absolute',
    zIndex: 10,
    bottom: 10,
    right: 10,
  },
  paddingR2: {
    paddingRight: 5,
    paddingTop: 3,
  },
});

const DispensariesDetail = ({route, navigation}) => {
  const {isConnected, userData} = useSelector((state) => state.auth);
  const item = route.params;

  const mailListRef = useRef();
  const ToastRef = useRef();

  const [message, setMessage] = useState('');
  const [listData, setListData] = useState({});
  const [moreLoad, setMoreLoad] = useState(false);
  const [dealsModel, setDealsModel] = useState(-1);
  const [Description, setDiscription] = useState('');
  const [pageLoader, setPageLoader] = useState(true);
  const [isDealsModal, setIsDealsModal] = useState(false);
  const [totalStarCount, setTotalStarCount] = useState(0);
  const [refreshLoader, setRefreshLoader] = useState(false);
  const [isAddReviewModal, setAddReviewModal] = useState(false);
  const [submitReviewLoad, setSubmitReviewLoad] = useState(false);

  const [productListData, setProductListData] = useState({});
  const [dealListData, setDealListData] = useState({});
  const [reviewListData, setReviewListData] = useState({});

  const storeID = checkObject(item, 'id');
  const startArray = [1, 2, 3, 4, 5];

  const [storeDetail, setStoreDetails] = useState({});
  const [selectedTab, setSelectedTab] = useState(0);

  const userToken = checkObject(userData, 'access_token');
  const header = {
    Authorization: `Bearer ${userToken}`,
  };

  React.useEffect(
    () =>
      navigation.addListener('focus', () => {
        getDispensariesDetails(false);
      }),
    [],
  );

  useEffect(() => {
    if (selectedTab === 0) {
      setListData(productListData);
    } else if (selectedTab === 2) {
      setListData(dealListData);
    } else if (selectedTab === 3) {
      setListData(reviewListData);
    }
  }, [selectedTab]);

  function handleAllLoader() {
    setTimeout(() => {
      setPageLoader(false);
      setRefreshLoader(false);
      setMoreLoad(false);
    }, 200);
  }

  async function getDispensariesDetails(bool) {
    if (isConnected === true) {
      console.log('store id =====>>>>>> ', storeID);
      const storeData = {
        id: storeID,
      };

      try {
        let endPoint = `${BaseSetting.endpoints.store_detail}?id=${storeID}`;
        const response = await getApiData(
          navigation,
          endPoint,
          'GET',
          null,
          header,
        );
        if (
          _.isObject(response) &&
          !_.isEmpty(response) &&
          response.success === true
        ) {
          const detailsData = checkObject(response, 'data');
          setStoreDetails(detailsData);
          if (bool === false) {
            getFirstPageData('P');
          } else {
            handleAllLoader();
          }
        } else {
          const errorMessage = checkObject(response, 'message');
          CAlert(errorMessage, StaticHeader.Oops, () => {
            handleAllLoader();
          });
        }
      } catch (err) {
        console.log('Catch Part', err);
        CAlert(StaticAlertMsg.someThing_Went_Wrong, StaticHeader.Oops, () => {
          handleAllLoader();
        });
      }
    } else {
      CAlert(StaticAlertMsg.Network_Problem, StaticHeader.Oops, () => {
        handleAllLoader();
      });
    }
  }

  async function getFirstPageData(str) {
    if (isConnected === true) {
      let otherBodyData = {
        page: 1,
      };

      let endPoint = '';

      if (str === 'P') {
        endPoint = `${BaseSetting.endpoints.product_list}?store_id=${storeID}`;
        otherBodyData.store_id = storeID;
      } else if (str === 'D') {
        endPoint = `${BaseSetting.endpoints.offer_list}?store_id=${storeID}`;
        otherBodyData.store_id = storeID;
      } else if (str === 'R') {
        endPoint = `${BaseSetting.endpoints.store_review}?id=${storeID}`;
        otherBodyData.id = storeID;
      }

      try {
        const response = await getApiData(
          navigation,
          endPoint,
          'GET',
          null,
          header,
        );
        if (
          _.isObject(response) &&
          !_.isEmpty(response) &&
          response.success === true
        ) {
          const obj = {};

          const newListData =
            response && response.data && response.data.rows
              ? response.data.rows
              : [];
          const paginationDatas =
            response && response.data && response.data.pagination
              ? response.data.pagination
              : {};

          if (_.isArray(newListData)) {
            obj.data = newListData;
            obj.pagination = paginationDatas;
          }

          console.log(JSON.stringify(obj));

          if (str === 'P') {
            setProductListData(obj);
            if (selectedTab === 0) {
              setListData(obj);
            }
            getFirstPageData('D');
          } else if (str === 'D') {
            setDealListData(obj);
            if (selectedTab === 2) {
              setListData(obj);
            }
            getFirstPageData('R');
          } else if (str === 'R') {
            setReviewListData(obj);
            if (selectedTab === 3) {
              setListData(obj);
            }
            handleAllLoader();
          }
        } else {
          const errorMessage = checkObject(response, 'message');
          CAlert(errorMessage, StaticHeader.Oops, () => {
            handleAllLoader();
          });
        }
      } catch (err) {
        console.log('Catch Part', err);
        CAlert(StaticAlertMsg.someThing_Went_Wrong, StaticHeader.Oops, () => {
          handleAllLoader();
        });
      }
    } else {
      CAlert(StaticAlertMsg.Network_Problem, StaticHeader.Oops, () => {
        handleAllLoader();
      });
    }
  }

  async function getList(bool) {
    const cPage =
      listData && listData.pagination && listData.pagination.currentPage
        ? _.toNumber(listData.pagination.currentPage)
        : 0;

    if (isConnected === true) {
      let PageNo = 0;
      if (bool === true) {
        PageNo = 1;
      } else {
        PageNo = cPage + 1;
      }

      let otherBodyData = {
        page: PageNo,
      };

      let endPoint = '';

      if (selectedTab === 0) {
        endPoint = `${BaseSetting.endpoints.product_list}?store_id=${storeID}`;
        otherBodyData.store_id = storeID;
      } else if (selectedTab === 2) {
        endPoint = `${BaseSetting.endpoints.offer_list}?store_id=${storeID}`;
        otherBodyData.store_id = storeID;
      } else if (selectedTab === 3) {
        endPoint = `${BaseSetting.endpoints.store_review}?id=${storeID}`;
        otherBodyData.id = storeID;
      }

      try {
        const response = await getApiData(
          navigation,
          endPoint,
          'GET',
          null,
          header,
        );
        if (
          _.isObject(response) &&
          !_.isEmpty(response) &&
          response.success === true
        ) {
          const obj = bool ? {} : _.cloneDeep(listData);

          const newListData =
            response && response.data && response.data.rows
              ? response.data.rows
              : [];
          const paginationDatas =
            response && response.data && response.data.pagination
              ? response.data.pagination
              : {};

          if (_.isArray(newListData)) {
            if (_.isArray(obj.data) && obj.data.length > 0) {
              obj.data = _.flattenDeep([...obj.data, newListData]);
            } else {
              obj.data = newListData;
            }
            obj.pagination = paginationDatas;
          }
          setListData(obj);
          handleAllLoader();
        } else {
          const errorMessage = checkObject(response, 'message');
          CAlert(errorMessage, StaticHeader.Oops, () => {
            setListData(listData);
            handleAllLoader();
          });
        }
      } catch (err) {
        console.log('Catch Part', err);
        CAlert(StaticAlertMsg.someThing_Went_Wrong, StaticHeader.Oops, () => {
          handleAllLoader();
        });
      }
    } else {
      CAlert(StaticAlertMsg.Network_Problem, StaticHeader.Oops, () => {
        handleAllLoader();
      });
    }
  }

  async function getMoreData() {
    const cPage =
      listData && listData.pagination && listData.pagination.currentPage
        ? _.toNumber(listData.pagination.currentPage)
        : 0;
    const tPage =
      listData && listData.pagination && listData.pagination.totalPage
        ? _.toNumber(listData.pagination.totalPage)
        : 0;

    if (listData.pagination.isMore === true && cPage < tPage) {
      setMoreLoad(true);
      getList(false);
    }
  }

  function onRefresh() {
    setRefreshLoader(true);
    setPageLoader(true);
    setTimeout(() => {
      getDispensariesDetails(true);
      if (selectedTab !== 1) {
        getList(true);
      }
    }, 500);
  }

  function validateReviewData() {
    Keyboard.dismiss();
    let valid = true;
    if (_.isEmpty(message) || totalStarCount === 0) {
      valid = false;
      CAlert(StaticAlertMsg.fillAllData, StaticHeader.Alert);
      return false;
    }
    if (message.length > 255) {
      valid = false;
      CAlert(StaticAlertMsg.storeReview, StaticHeader.Oops);
      return false;
    }

    if (valid === true) {
      setSubmitReviewLoad(true);
      setTimeout(() => {
        reviewActions();
      }, 100);
    }
  }

  function closeRateModal() {
    setAddReviewModal(false);
    setTotalStarCount(0);
    setMessage('');
    setSubmitReviewLoad(false);
  }

  async function reviewActions() {
    if (isConnected === true) {
      const reviewData = {
        'StoreReview[store_id]': storeID,
        'StoreReview[rating]': totalStarCount,
        'StoreReview[comment]': message,
      };

      try {
        let endPoint = BaseSetting.endpoints.add_store_review;
        const response = await getApiDataProgress(
          navigation,
          endPoint,
          'POST',
          reviewData,
          header,
        );
        if (
          _.isObject(response) &&
          !_.isEmpty(response) &&
          response.success === true
        ) {
          const successMessage = checkObject(response, 'message');
          ToastRef.current.show(successMessage, 2000);
          onRefresh();
          closeRateModal();
        } else {
          const errorMessage = checkObject(response, 'message');
          CAlert(errorMessage, StaticHeader.Oops, () => {
            setSubmitReviewLoad(false);
          });
        }
      } catch (err) {
        console.log('Catch Part', err);
        CAlert(StaticAlertMsg.someThing_Went_Wrong, StaticHeader.Oops, () => {
          setSubmitReviewLoad(false);
        });
      }
    } else {
      CAlert(StaticAlertMsg.Network_Problem, StaticHeader.Oops, () => {
        setSubmitReviewLoad(false);
      });
    }
  }

  function renderDealsDescModal() {
    // const desc = dealsModel >= 0 && dealsData[dealsModel] ? dealsData[dealsModel].description : '';
    // const desc = '';
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={isDealsModal}
        onRequestClose={() => {
          closeRateModal();
        }}>
        <View
          style={{
            flex: 1,
            backgroundColor: 'rgba(0,0,0,0.3)',
            justifyContent: 'flex-end',
          }}>
          <TouchableOpacity
            activeOpacity={0.8}
            onPress={() => {
              setDealsModel(-1);
              setIsDealsModal(false);
            }}
            style={{
              flex: 1,
            }}
          />

          <View
            style={{
              width: nWidth,
              paddingHorizontal: 15,
              paddingTop: 20,
              paddingBottom: 30,
              backgroundColor: BaseColor.whiteColor,
              borderTopLeftRadius: 40,
            }}>
            <HTML
              tagsStyles={{p: {marginTop: 0, marginBottom: 5}}}
              source={{html: Description}}
              contentWidth={nWidth}
            />
            <Button
              style={{marginTop: 30}}
              buttonText={'Okay'}
              onPress={() => {
                setDealsModel(-1);
                setIsDealsModal(false);
              }}
            />
          </View>
        </View>
      </Modal>
    );
  }

  function renderAddReviewModal() {
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={isAddReviewModal}
        onRequestClose={() => {
          closeRateModal();
        }}>
        <View
          style={{
            flex: 1,
            backgroundColor: 'rgba(0,0,0,0.3)',
            justifyContent: 'flex-end',
          }}>
          <TouchableOpacity
            activeOpacity={0.8}
            onPress={() => {
              closeRateModal();
            }}
            style={{
              flex: 1,
            }}
          />
          <KeyboardAvoidingView
            style={{
              flex: 1,
              paddingHorizontal: 15,
              paddingTop: 20,
              paddingBottom: 30,
              backgroundColor: BaseColor.whiteColor,
              borderTopLeftRadius: 40,
            }}
            behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
            <ScrollView
              keyboardShouldPersistTaps="never"
              showsVerticalScrollIndicator={false}>
              <Text bold body1 blackColor textAlignCenter>
                What is your review?
              </Text>
              <Text
                semibold
                body1
                blackColor
                textAlignCenter
                style={{
                  paddingVertical: 15,
                }}>
                {'Please share your opinion\nabout our store.'}
              </Text>

              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'center',
                  paddingBottom: 15,
                }}>
                {startArray.map((obj, index) => {
                  const isBool = obj <= totalStarCount;
                  return (
                    <Icon
                      name={isBool ? 'star' : 'star-outline'}
                      size={30}
                      color={BaseColor.ThemeYellow}
                      style={{
                        paddingHorizontal: 10,
                      }}
                      onPress={() => {
                        setTotalStarCount(obj);
                      }}
                    />
                  );
                })}
              </View>

              <TextInput
                placeholder="Your reviews"
                onChangeText={(t) => {
                  setMessage(t);
                }}
                value={message}
                keyboardType={'default'}
                textArea
                otherCon={{
                  borderRadius: 5,
                  borderWidth: 0,
                  backgroundColor: BaseColor.whiteColor,
                  elevation: 5,
                  shadowColor: BaseColor.blackColor,
                  shadowOffset: {
                    width: 0,
                    height: 2,
                  },
                  shadowOpacity: 0.25,
                  shadowRadius: 3.84,
                  overflow: 'visible',
                  width: '95%',
                  alignSelf: 'center',
                }}
              />
              <Button
                style={{
                  marginVertical: 30,
                  alignSelf: 'center',
                  width: '95%',
                }}
                loading={submitReviewLoad}
                buttonText={'Submit'}
                onPress={() => {
                  validateReviewData();
                }}
              />
            </ScrollView>
          </KeyboardAvoidingView>
        </View>
      </Modal>
    );
  }

  function storeDetailsCon() {
    const store_Desc = checkObject(storeDetail, 'description');

    return (
      <ScrollView
        bounces={false}
        contentContainerStyle={{flexGrow: 1, padding: 10}}>
        <HTML source={{html: store_Desc}} contentWidth={nWidth} />
      </ScrollView>
    );
  }

  function renderInfoCon({item, index}) {
    if (selectedTab === 0) {
      return (
        <StoreProductCard
          key={index}
          data={item}
          onPress={() => {
            navigation.navigate('ProductDetails', item);
          }}
        />
      );
    } else if (selectedTab === 2) {
      return (
        <StoreDealsCard
          key={index}
          data={item}
          onCopyCode={() => {
            ToastRef.current.show('Code Copied', 2000);
          }}
          onPress={() => {
            setDealsModel(index);
            setDiscription(item.description);
            setIsDealsModal(true);
          }}
        />
      );
    } else if (selectedTab === 3) {
      return <StoreReviewCard key={index} data={item} />;
    }
  }

  function onChangeTabAction(index) {
    enableAnimateInEaseOut();
    setSelectedTab(index);
  }

  function renderTabBarCon() {
    const tabWidth = nWidth / tabList.length;
    return (
      <View style={styles.tabsCon}>
        {tabList.map((obj, index) => {
          const isSelected = index === selectedTab;
          const btnCss = {
            width: tabWidth,
            height: '100%',
            alignItems: 'center',
            justifyContent: 'center',
            borderBottomWidth: isSelected ? 4 : StyleSheet.hairlineWidth,
            borderBottomColor: isSelected
              ? BaseColor.ThemeOrange
              : BaseColor.lightGrey,
          };
          return (
            <TouchableOpacity
              key={index}
              activeOpacity={0.8}
              onPress={() => {
                onChangeTabAction(index);
              }}
              style={btnCss}>
              {isSelected ? (
                <Text subhead bold textAlignCenter>
                  {obj.title}
                </Text>
              ) : (
                <Text subhead textAlignCenter>
                  {obj.title}
                </Text>
              )}
            </TouchableOpacity>
          );
        })}
      </View>
    );
  }

  function openDialScreen() {
    let phoneNumber = checkObject(storeDetail, 'owner_mobile');

    if (Platform.OS === 'ios') {
      phoneNumber = `telprompt:${phoneNumber}`;
    } else {
      phoneNumber = `tel:${phoneNumber}`;
    }

    Linking.canOpenURL(phoneNumber)
      .then((supported) => {
        if (!supported) {
          CAlert('Phone number is not available');
        } else {
          return Linking.openURL(phoneNumber);
        }
      })
      .catch((err) => console.log(err));
  }

  function handleLocation() {
    const userCurrentLoc = getUserCurrentLatLog();

    let storeLat = checkObject(storeDetail, 'lat');
    let storeLng = checkObject(storeDetail, 'lang');

    const userCurrentLat = checkObject(userCurrentLoc, 'currentLat');
    const userCurrentLng = checkObject(userCurrentLoc, 'currentLong');

    let url = `http://www.google.com/maps/dir/${userCurrentLat},${userCurrentLng}/${storeLat},${storeLng}`;
    Linking.canOpenURL(url)
      .then((supported) => {
        if (supported) {
          return Linking.openURL(url);
        }
      })
      .catch((err) => {
        console.error('An error occurred', err);
      });
  }

  function renderButtonCon() {
    return (
      <View style={styles.buttonCon}>
        <Button
          styleText={{color: BaseColor.blackColor}}
          loaderColor={BaseColor.ThemeBlue}
          style={styles.btnSty}
          otherGredientCss={{
            borderWidth: StyleSheet.hairlineWidth,
            borderColor: BaseColor.lightBlack,
            borderRadius: 2,
          }}
          buttonText={'Call'}
          colorAry={[BaseColor.whiteColor, BaseColor.whiteColor]}
          onPress={() => {
            openDialScreen();
          }}
        />

        <Button
          styleText={{color: BaseColor.blackColor}}
          loaderColor={BaseColor.ThemeBlue}
          style={styles.btnSty}
          otherGredientCss={{
            borderWidth: StyleSheet.hairlineWidth,
            borderColor: BaseColor.lightBlack,
            borderRadius: 2,
          }}
          buttonText={'Direction'}
          colorAry={[BaseColor.whiteColor, BaseColor.whiteColor]}
          onPress={() => {
            handleLocation();
          }}
        />
      </View>
    );
  }

  function renderTopInfo() {
    const storeImage = checkObject(storeDetail, 'store_image');
    const address = checkObject(storeDetail, 'address');
    const storeName = checkObject(storeDetail, 'store_name');
    const startCount = checkObject(storeDetail, 'average_star');
    const countReview = checkObject(storeDetail, 'count_review');
    const ownerMobile = checkObject(storeDetail, 'owner_mobile');

    return (
      <View style={styles.infoMainCon}>
        <View style={styles.storeInfoCon}>
          <Image
            source={
              storeImage !== '-' && storeImage !== ''
                ? {uri: storeImage}
                : Images.staticMap
            }
            style={{
              width: '100%',
              height: '100%',
              resizeMode: 'contain',
            }}
          />
        </View>

        <View style={styles.leftStoreInfoCon}>
          <Text body1 bold blackColor>
            {storeName}
          </Text>
          <View style={styles.rateStarCon}>
            {startArray.map((obj, index) => {
              const isBool = obj <= startCount;
              return (
                <Icon
                  name={isBool ? 'star' : 'star-outline'}
                  size={14}
                  color={BaseColor.ThemeYellow}
                  style={styles.PR3}
                />
              );
            })}
            <Text caption1 semibold blackColor style={styles.PL3}>
              {startCount}
              <Text grayTextColor>{` (${countReview})`}</Text>
            </Text>
          </View>
          <View style={styles.cmnFlex}>
            <Icon
              name="call"
              size={14}
              color={BaseColor.ThemeOrange}
              style={{paddingBottom: 3, paddingRight: 5}}
            />
            <Text caption1 grayColor>
              {ownerMobile}
            </Text>
          </View>
          <Text caption1 grayColor style={{paddingVertical: 5}}>
            {address}
          </Text>
        </View>
      </View>
    );
  }

  function renderFooterComponent() {
    if (moreLoad) {
      return (
        <View style={styles.loaderFooterView}>
          <ActivityIndicator
            size={'small'}
            animating
            color={BaseColor.ThemeOrange}
          />
        </View>
      );
    } else {
      return <View style={styles.listFooterView} />;
    }
  }

  function renderEmptyComponent() {
    return (
      <EmptyView
        emptyText={`Sorry we don't have\nany ${_.toLower(
          tabList[selectedTab].title,
        )} list yet.`}
        btnLoader={refreshLoader}
        onPress={() => {
          onRefresh();
        }}
      />
    );
  }

  function renderHeaderComponent() {
    return (
      <View>
        {renderTopInfo()}
        {renderButtonCon()}
        {renderTabBarCon()}
      </View>
    );
  }

  function renderDispensariesDetails() {
    if (selectedTab === 1) {
      return (
        <ScrollView
          bounces={false}
          contentContainerStyle={{flexGrow: 1}}
          refreshControl={
            <RefreshControl
              colors={[BaseColor.ThemeOrange]}
              tintColor={BaseColor.ThemeOrange}
              refreshing={refreshLoader}
              onRefresh={() => {
                onRefresh();
              }}
            />
          }>
          {renderHeaderComponent()}
          {storeDetailsCon()}
        </ScrollView>
      );
    } else {
      return (
        <View style={{flex: 1, position: 'relative'}}>
          <FlatList
            ref={mailListRef}
            key={selectedTab}
            data={listData.data}
            scrollsToTop={false}
            numColumns={selectedTab === 0 ? 2 : 1}
            horizontal={false}
            onEndReachedThreshold={0.5}
            showsVerticalScrollIndicator={false}
            keyExtractor={(item) => item.id}
            renderItem={renderInfoCon}
            contentContainerStyle={styles.FlatMainCon}
            ListHeaderComponent={renderHeaderComponent}
            ListFooterComponent={renderFooterComponent}
            ListEmptyComponent={renderEmptyComponent}
            onEndReached={getMoreData}
            refreshControl={
              <RefreshControl
                colors={[BaseColor.ThemeOrange]}
                tintColor={BaseColor.ThemeOrange}
                refreshing={refreshLoader}
                onRefresh={() => {
                  onRefresh();
                }}
              />
            }
          />
          {selectedTab === 3 && pageLoader === false ? (
            <Button
              style={[styles.addReviewBtn, styles.addReviewFixedCon]}
              otherGredientCss={styles.addReviewBtn}
              otherIconSty={{
                fontSize: 35,
              }}
              displayIcon
              centerIconName={'add-sharp'}
              onPress={() => {
                setAddReviewModal(true);
              }}
            />
          ) : null}
        </View>
      );
    }
  }

  function onRefreshData() {
    setPageLoader(true);
    setTimeout(() => {
      getDispensariesDetails(false);
    }, 1000);
  }

  function renderLoader() {
    return <DispensariesDetailLoader selectedTab={selectedTab} />;
  }

  function renderHeader() {
    return (
      <GredientHeader
        leftIcon
        // rightIcon
        // rightLoader={pageLoader}
        // onRightAction={() => {
        //   onRefreshData();
        // }}
        title={'Dispensaries Details'}
        onLeftAction={() => {
          navigation.pop();
        }}
      />
    );
  }

  return (
    <View style={styles.main}>
      {renderHeader()}
      {pageLoader ? renderLoader() : renderDispensariesDetails()}
      {renderAddReviewModal()}
      {renderDealsDescModal()}
      <Toast
        ref={ToastRef}
        position="bottom"
        positionValue={150}
        fadeInDuration={750}
        fadeOutDuration={2000}
        opacity={0.8}
      />
    </View>
  );
};

export default DispensariesDetail;
