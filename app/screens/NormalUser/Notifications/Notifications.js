import React, {useRef, useEffect, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import _ from 'lodash';
import {
  View,
  FlatList,
  StyleSheet,
  RefreshControl,
  ActivityIndicator,
} from 'react-native';
import CAlert from '../../../components/CAlert';
import {BaseColor} from '../../../config/theme';
import {getApiData} from '../../../utils/apiHelper';
import {BaseSetting} from '../../../config/setting';
import {checkObject} from '../../../utils/commonFunction';
import authActions from '../../../redux/reducers/auth/actions';
import EmptyView from '../../../components/DriverApp/EmptyView';
import GredientHeader from '../../../components/DriverApp/GredientHeader';
import CNotification from '../../../components/Notification/CNotification';
import {StaticAlertMsg, StaticHeader} from '../../../config/StaticAlertMsg';
import {NotificationLoader} from '../../../components/ContentLoader/ContentLoader';

const {setNotificationBadge} = authActions;

const styles = StyleSheet.create({
  mainCon: {
    flex: 1,
    backgroundColor: BaseColor.whiteColor,
  },
  flexMainCon: {
    flexGrow: 1,
  },
  listFooterView: {
    width: '100%',
    height: 30,
  },
  loaderFooterView: {
    width: '100%',
    padding: 30,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: BaseColor.whiteColor,
  },
});

export default function Notifications({navigation}) {
  const {isConnected, NotiNumber, userData} = useSelector((state) => state.auth);
  const [refreshLoader, setRefreshLoader] = useState(false);
  const [pageLoader, setPageLoader] = useState(true);
  const [moreLoad, setMoreLoad] = useState(false);
  const [listData, setListData] = useState({});

  const dispatch = useDispatch();
  const mailListRef = useRef();

  const userToken = checkObject(userData, 'access_token');

  const header = {
    Authorization: `Bearer ${userToken}`,
  };

  useEffect(() => {
    onRefresh();
  }, [NotiNumber]);

  React.useEffect(
    () =>
      navigation.addListener('focus', () => {
        dispatch(setNotificationBadge(0));
        getList(true);
      }),
    [],
  );

  async function removeAllNotifications() {
    setPageLoader(true);
    if (isConnected === true) {
      try {
        let endPoint = `${BaseSetting.endpoints.removeAll}`;
        const response = await getApiData(
          navigation,
          endPoint,
          'GET',
          null,
          header,
        );
        if (
          _.isObject(response) &&
          !_.isEmpty(response) &&
          response.success === true
        ) {
          setListData({});
          setTimeout(() => {
            setPageLoader(false);
          }, 100);
        } else {
          const errorMessage = checkObject(response, 'message');
          CAlert(errorMessage, StaticHeader.Oops, () => {
            setPageLoader(false);
          });
        }
      } catch (err) {
        console.log('Catch Part', err);
        CAlert(StaticAlertMsg.someThing_Went_Wrong, StaticHeader.Oops, () => {
          setPageLoader(false);
        });
      }
    } else {
      CAlert(StaticAlertMsg.Network_Problem, StaticHeader.Oops, () => {
        setPageLoader(false);
      });
    }
  }

  async function removeSingleNotifications(removeId) {
    setPageLoader(true);
    if (isConnected === true) {
      const removeNotificationData = {
        id: removeId,
      };
      try {
        let endPoint = `${BaseSetting.endpoints.remove}?id=${removeId}`;
        const response = await getApiData(
          navigation,
          endPoint,
          'GET',
          null,
          header,
        );
        if (
          _.isObject(response) &&
          !_.isEmpty(response) &&
          response.success === true
        ) {
          getList(true);
        } else {
          const errorMessage = checkObject(response, 'message');
          CAlert(errorMessage, StaticHeader.Oops, () => {
            setPageLoader(false);
          });
        }
      } catch (err) {
        console.log('Catch Part', err);
        CAlert(StaticAlertMsg.someThing_Went_Wrong, StaticHeader.Oops, () => {
          setPageLoader(false);
        });
      }
    } else {
      CAlert(StaticAlertMsg.Network_Problem, StaticHeader.Oops, () => {
        setPageLoader(false);
      });
    }
  }

  async function getList(bool) {
    const cPage =
      listData && listData.pagination && listData.pagination.currentPage
        ? _.toNumber(listData.pagination.currentPage)
        : 0;
    if (isConnected === true) {
      let PageNo = 0;
      if (bool === true) {
        PageNo = 1;
      } else {
        PageNo = cPage + 1;
      }

      const nData = {
        page: PageNo,
      };

      try {
        let endPoint = `${BaseSetting.endpoints.notificationList}?page=${PageNo}`;
        const response = await getApiData(
          navigation,
          endPoint,
          'GET',
          null,
          header,
        );
        if (
          _.isObject(response) &&
          !_.isEmpty(response) &&
          response.success === true
        ) {
          const obj = bool ? {} : _.cloneDeep(listData);

          const newListData =
            response && response.data && response.data.rows
              ? response.data.rows
              : [];
          const paginationDatas =
            response && response.data && response.data.pagination
              ? response.data.pagination
              : {};

          if (_.isArray(newListData)) {
            if (_.isArray(obj.data) && obj.data.length > 0) {
              obj.data = _.flattenDeep([...obj.data, newListData]);
            } else {
              obj.data = newListData;
            }
            obj.pagination = paginationDatas;
          }
          setListData(obj);
          handleAllLoader();
        } else {
          const errorMessage = checkObject(response, 'message');
          CAlert(errorMessage, StaticHeader.Oops, () => {
            setListData(listData);
            handleAllLoader();
          });
        }
      } catch (err) {
        console.log('Catch Part', err);
        CAlert(StaticAlertMsg.someThing_Went_Wrong, StaticHeader.Oops, () => {
          handleAllLoader();
        });
      }
    } else {
      CAlert(StaticAlertMsg.Network_Problem, StaticHeader.Oops, () => {
        handleAllLoader();
      });
    }
  }

  function handleAllLoader() {
    setTimeout(() => {
      setPageLoader(false);
      setRefreshLoader(false);
      setMoreLoad(false);
    }, 200);
  }

  async function getMoreData() {
    const cPage =
      listData && listData.pagination && listData.pagination.currentPage
        ? _.toNumber(listData.pagination.currentPage)
        : 0;
    const tPage =
      listData && listData.pagination && listData.pagination.totalPage
        ? _.toNumber(listData.pagination.totalPage)
        : 0;

    if (listData.pagination.isMore === true && cPage < tPage) {
      setMoreLoad(true);
      getList(false);
    }
  }

  function onRefresh() {
    setRefreshLoader(true);
    setPageLoader(true);
    setTimeout(() => {
      getList(true);
    }, 500);
  }

  function renderFooterComponent() {
    if (moreLoad) {
      return (
        <View style={styles.loaderFooterView}>
          <ActivityIndicator
            size={'small'}
            animating
            color={BaseColor.ThemeOrange}
          />
        </View>
      );
    } else {
      return <View style={styles.listFooterView} />;
    }
  }

  function renderEmptyComponent() {
    return (
      <EmptyView
        emptyText={"Sorry you don't have any notification yet."}
        btnLoader={refreshLoader}
        onPress={() => {
          onRefresh();
        }}
      />
    );
  }

  const renderItem = ({item, index}) => {
    return (
      <CNotification
        key={index}
        data={item}
        onPress={
          pageLoader
            ? null
            : () => {
                const removeId = checkObject(item, 'id');
                CAlert(
                  StaticAlertMsg.singleNotificationRemove,
                  StaticHeader.Confirm,
                  () => {
                    removeSingleNotifications(removeId);
                  },
                  () => {
                    return null;
                  },
                );
              }
        }
      />
    );
  };

  function renderList() {
    return (
      <FlatList
        ref={mailListRef}
        data={listData.data}
        scrollsToTop={false}
        horizontal={false}
        renderItem={renderItem}
        refreshing={refreshLoader}
        keyExtractor={(item) => item.id}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={styles.flexMainCon}
        ListFooterComponent={renderFooterComponent}
        ListEmptyComponent={renderEmptyComponent}
        refreshControl={
          <RefreshControl
            colors={[BaseColor.ThemeOrange]}
            tintColor={BaseColor.ThemeOrange}
            refreshing={refreshLoader}
            onRefresh={() => {
              onRefresh();
            }}
          />
        }
        onEndReached={getMoreData}
        onEndReachedThreshold={0.5}
      />
    );
  }

  function renderLoader() {
    return <NotificationLoader />;
  }

  function renderHeader() {
    const isDisplayIcon = _.isArray(listData.data) && !_.isEmpty(listData.data);
    return (
      <GredientHeader
        rightIcon={isDisplayIcon}
        rightIconName={'trash-outline'}
        title={'Notifications'}
        onRightAction={
          pageLoader
            ? null
            : () => {
                CAlert(
                  StaticAlertMsg.removeAllNotification,
                  StaticHeader.Confirm,
                  () => {
                    removeAllNotifications();
                  },
                  () => {
                    return null;
                  },
                );
              }
        }
      />
    );
  }

  return (
    <View style={styles.mainCon}>
      {renderHeader()}
      {pageLoader ? renderLoader() : renderList()}
    </View>
  );
}
