import React, {useCallback, useEffect, useRef, useState} from 'react';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/Ionicons';
import {useDispatch, useSelector} from 'react-redux';
import _ from 'lodash';
import {
  View,
  StyleSheet,
  Keyboard,
  FlatList,
  Dimensions,
  RefreshControl,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import CAlert from '../../../components/CAlert';
import {BaseColor} from '../../../config/theme';
import Text from '../../../components/Text/index';
import {BaseSetting} from '../../../config/setting';
import {getApiData} from '../../../utils/apiHelper';
import Button from '../../../components/Button/index';
import TopTabs from '../../../components/TopTabs/TopTabs';
import TextInput from '../../../components/TextInput/index';
import GredientHeader from '../../../components/DriverApp/GredientHeader';
import {StaticAlertMsg, StaticHeader} from '../../../config/StaticAlertMsg';
import {
  checkObject,
  enableAnimateInEaseOut,
} from '../../../utils/commonFunction';
import ProductCategory from '../../../components/ProductCategory/ProductCategory';
import {OtherCategorLoader} from '../../../components/ContentLoader/ContentLoader';

const styles = StyleSheet.create({
  mainCon: {
    flex: 1,
    backgroundColor: BaseColor.whiteColor,
  },
  searchBarCon: {
    width: '100%',
    height: 55,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    overflow: 'hidden',
    paddingHorizontal: 10,
  },
  searchInputCon: {
    width: '100%',
    height: 40,
    borderRadius: 0,
    backgroundColor: BaseColor.whiteColor,
    borderWidth: 0,
    marginTop: 0,
  },
  searchIconClr: {
    color: BaseColor.grayColor,
  },
  searchBtnSty: {
    height: 40,
    width: '20%',
    borderRadius: 0,
    marginTop: 0,
    borderTopRightRadius: 5,
    borderBottomRightRadius: 5,
  },
  flexMainCon: {
    flexGrow: 1,
  },
  headerMainCon: {
    flex: 1,
    paddingHorizontal: 8,
    paddingVertical: 10,
    backgroundColor: BaseColor.whiteColor,
  },
  listFooterView: {
    width: '100%',
    height: 30,
  },
  listHeaderView: {
    width: '100%',
    height: 50,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 7,
    justifyContent: 'space-between',
  },
  emptyViewCon: {
    width: '100%',
    height: BaseSetting.nWidth / 3,
    alignItems: 'center',
    justifyContent: 'center',
  },
  loaderFooterView: {
    width: '100%',
    padding: 30,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: BaseColor.whiteColor,
  },
});

const OtherCategory = ({route, navigation}) => {
  const {isConnected, userData} = useSelector((state) => state.auth);
  const productData = route.params;
  const headerTitle =
    productData && productData.mainCat && productData.mainCat.category_name
      ? productData.mainCat.category_name
      : '-';
  const categoryId =
    productData && productData.mainCat && productData.mainCat.id
      ? productData.mainCat.id
      : '-';
  const subCategoryId =
    productData && productData.subCat && productData.subCat.id
      ? productData.subCat.id
      : 0;

  const [searchBtnLoad, setSearchBtnLoad] = useState(false);
  const [refreshLoader, setRefreshLoader] = useState(false);
  const [pageLoader, setPageLoader] = useState(true);
  const [moreLoad, setMoreLoad] = useState(false);
  const [searchTxt, setSearchTxt] = useState('');
  const [listData, setListData] = useState({});
  const [tabList, setTabList] = useState([]);
  const [active, setActive] = useState(subCategoryId);

  const mailListRef = useRef();
  const searchInputRef = useRef('');

  const userToken = checkObject(userData, 'access_token');

  const header = {
    Authorization: `Bearer ${userToken}`,
  };

  React.useEffect(
    () =>
      navigation.addListener('focus', () => {
        setSearchTxt('');
        getList(true);
      }),
    [],
  );

  useEffect(() => {
    setPageLoader(true);
    setTimeout(() => {
      getList(true);
    }, 1000);
  }, [active]);

  async function getSearchList() {
    const subCatId = tabList[active].id;
    if (isConnected === true) {
      const lData = {
        page: 1,
        id: _.toNumber(categoryId),
        sub_cat_id: _.toNumber(subCatId),
        product_name: searchTxt,
      };

      try {
        let endPoint = `${
          BaseSetting.endpoints.sub_category_list
        }?page=${1}&id=${_.toNumber(categoryId)}&sub_cat_id=${_.toNumber(
          subCatId,
        )}&product_name=${searchTxt}`;
        const response = await getApiData(
          navigation,
          endPoint,
          'GET',
          null,
          header,
        );
        if (
          _.isObject(response) &&
          !_.isEmpty(response) &&
          response.success === true
        ) {
          const obj = {};

          const newListData =
            response && response.data && response.data.product_list
              ? response.data.product_list
              : [];
          const paginationDatas =
            response && response.data && response.data.pagination
              ? response.data.pagination
              : {};

          let tabData =
            response && response.data && response.data.subcategoryList
              ? response.data.subcategoryList
              : [];

          if (_.isArray(tabData) && !_.isEmpty(tabData)) {
            let otherObj = {
              id: 0,
              category_name: 'All',
            };
            tabData.unshift(otherObj);
          }

          if (_.isArray(newListData)) {
            obj.data = newListData;
            obj.pagination = paginationDatas;
          }

          const isEmptyArray = _.isArray(tabData) && _.isEmpty(tabData);
          const findTabIndex =
            isEmptyArray === false
              ? tabData.findIndex(
                  (v) => _.toNumber(v.id) === _.toNumber(subCatId),
                )
              : null;
          const activeTab =
            isEmptyArray === false && findTabIndex >= 0 ? findTabIndex : null;
          setTabList(tabData);
          setActive(activeTab);
          setListData(obj);
          handleAllLoader();
        } else {
          const errorMessage = checkObject(response, 'message');
          CAlert(errorMessage, StaticHeader.Oops, () => {
            setListData(listData);
            handleAllLoader();
          });
        }
      } catch (err) {
        console.log('Catch Part', err);
        CAlert(StaticAlertMsg.someThing_Went_Wrong, StaticHeader.Oops, () => {
          handleAllLoader();
        });
      }
    } else {
      CAlert(StaticAlertMsg.Network_Problem, StaticHeader.Oops, () => {
        handleAllLoader();
      });
    }
  }

  async function getList(bool) {
    const currentTabId = _.toNumber(tabList[active].id);
    const cPage =
      listData && listData.pagination && listData.pagination.currentPage
        ? _.toNumber(listData.pagination.currentPage)
        : 0;
    if (isConnected === true) {
      let PageNo = 0;
      if (bool === true) {
        PageNo = 1;
      } else {
        PageNo = cPage + 1;
      }

      const lData = {
        page: PageNo,
        id: _.toNumber(categoryId),
        sub_cat_id: currentTabId,
      };

      try {
        let endPoint = `${
          BaseSetting.endpoints.sub_category_list
        }?page=${PageNo}&id=${_.toNumber(
          categoryId,
        )}&sub_cat_id=${currentTabId}`;
        const response = await getApiData(
          navigation,
          endPoint,
          'GET',
          null,
          header,
        );
        if (
          _.isObject(response) &&
          !_.isEmpty(response) &&
          response.success === true
        ) {
          const obj = bool ? {} : _.cloneDeep(listData);

          const newListData =
            response && response.data && response.data.product_list
              ? response.data.product_list
              : [];
          const paginationDatas =
            response && response.data && response.data.pagination
              ? response.data.pagination
              : {};

          let tabData =
            response && response.data && response.data.subcategoryList
              ? response.data.subcategoryList
              : [];

          if (_.isArray(tabData) && !_.isEmpty(tabData)) {
            let otherObj = {
              id: 0,
              category_name: 'All',
            };
            tabData.unshift(otherObj);
          }

          if (_.isArray(newListData)) {
            if (_.isArray(obj.data) && obj.data.length > 0) {
              obj.data = _.flattenDeep([...obj.data, newListData]);
            } else {
              obj.data = newListData;
            }
            obj.pagination = paginationDatas;
          }

          const isEmptyArray = _.isArray(tabData) && _.isEmpty(tabData);
          const findTabIndex =
            isEmptyArray === false
              ? tabData.findIndex(
                  (v) => _.toNumber(v.id) === _.toNumber(subCatId),
                )
              : null;
          const activeTab =
            isEmptyArray === false && findTabIndex >= 0 ? findTabIndex : null;
          setTabList(tabData);
          setActive(activeTab);

          setListData(obj);
          handleAllLoader();
        } else {
          const errorMessage = checkObject(response, 'message');
          CAlert(errorMessage, StaticHeader.Oops, () => {
            setListData(listData);
            handleAllLoader();
          });
        }
      } catch (err) {
        console.log('Catch Part', err);
        CAlert(StaticAlertMsg.someThing_Went_Wrong, StaticHeader.Oops, () => {
          handleAllLoader();
        });
      }
    } else {
      CAlert(StaticAlertMsg.Network_Problem, StaticHeader.Oops, () => {
        handleAllLoader();
      });
    }
  }

  function handleAllLoader() {
    setTimeout(() => {
      setPageLoader(false);
      setRefreshLoader(false);
      setMoreLoad(false);
      setSearchBtnLoad(false);
    }, 200);
  }

  async function getMoreData() {
    const cPage =
      listData && listData.pagination && listData.pagination.currentPage
        ? _.toNumber(listData.pagination.currentPage)
        : 0;
    const tPage =
      listData && listData.pagination && listData.pagination.totalPage
        ? _.toNumber(listData.pagination.totalPage)
        : 0;

    if (listData.pagination.isMore === true && cPage < tPage) {
      setMoreLoad(true);
      getList(false);
    }
  }

  function onRefresh() {
    setRefreshLoader(true);
    setPageLoader(true);
    setTimeout(() => {
      getList(true);
    }, 500);
  }

  function renderEmptyComponent() {
    return (
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
          paddingHorizontal: 30,
        }}>
        <Text
          textAlignCenter
          headline
          style={{
            lineHeight: 25,
          }}>{`Sorry, Currently ${tabList[active].category_name} products are not available.`}</Text>
      </View>
    );
  }

  function renderHeaderComponent() {
    if (_.isEmpty(tabList)) {
      return null;
    }

    return (
      <View style={styles.headerMainCon}>
        <TopTabs
          onPress={(no) => {
            setActive(no);
            setSearchTxt('');
          }}
          active={active}
          btnArray={tabList}
        />
      </View>
    );
  }

  const renderItem = ({item, index}) => {
    return (
      <ProductCategory
        key={index}
        data={item}
        onPress={() => {
          navigation.navigate('ProductDetails', item);
        }}
      />
    );
  };

  function renderList() {
    return (
      <FlatList
        ref={mailListRef}
        data={listData.data}
        scrollsToTop={false}
        numColumns={2}
        horizontal={false}
        renderItem={renderItem}
        refreshing={refreshLoader}
        keyExtractor={(item) => item.id}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={styles.flexMainCon}
        ListFooterComponent={renderFooterComponent}
        ListEmptyComponent={renderEmptyComponent}
        ListHeaderComponent={renderHeaderComponent}
        stickyHeaderIndices={[0]}
        refreshControl={
          <RefreshControl
            colors={[BaseColor.ThemeOrange]}
            tintColor={BaseColor.ThemeOrange}
            refreshing={refreshLoader}
            onRefresh={() => {
              onRefresh();
            }}
          />
        }
        onEndReached={getMoreData}
        onEndReachedThreshold={0.5}
      />
    );
  }

  function renderLoader() {
    return <OtherCategorLoader />;
  }

  function renderSearchBar() {
    const isTestEmpty = searchTxt === '';
    return (
      <LinearGradient
        start={{x: 0.3, y: 0.25}}
        end={{x: 0.8, y: 1.0}}
        colors={[BaseColor.ThemeOrange, BaseColor.ThemeYellow]}
        style={styles.searchBarCon}>
        <View
          style={[
            styles.mainCon,
            {
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
              overflow: 'hidden',
              borderTopLeftRadius: 5,
              borderBottomLeftRadius: 5,
              borderTopRightRadius: isTestEmpty ? 5 : 0,
              borderBottomRightRadius: isTestEmpty ? 5 : 0,
            },
          ]}>
          <View style={styles.mainCon}>
            <TextInput
              ref={searchInputRef}
              leftIconName={'search'}
              otherCon={styles.searchInputCon}
              otherIconCss={styles.searchIconClr}
              placeholder="Search by product name..."
              onChangeText={(t) => {
                enableAnimateInEaseOut();
                setSearchTxt(t);
                if (t === '') {
                  onClearInputCon();
                }
              }}
              onSubmitEditing={
                searchBtnLoad
                  ? null
                  : () => {
                      onGoAction();
                    }
              }
              value={searchTxt}
            />
          </View>
          {!isTestEmpty ? (
            <TouchableOpacity
              activeOpacity={0.8}
              onPress={
                searchBtnLoad
                  ? null
                  : () => {
                      onClearInputCon();
                    }
              }
              style={{
                paddingHorizontal: 10,
                height: 40,
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: BaseColor.whiteColor,
              }}>
              <Icon name={'close'} size={12} color={BaseColor.blackColor} />
            </TouchableOpacity>
          ) : null}
        </View>

        {!isTestEmpty ? (
          <Button
            buttonText={'Go'}
            loading={searchBtnLoad}
            style={styles.searchBtnSty}
            otherGredientCss={{
              borderRadius: 0,
              height: 40,
              borderTopRightRadius: 5,
              borderBottomRightRadius: 5,
            }}
            onPress={
              searchBtnLoad
                ? null
                : () => {
                    onGoAction();
                  }
            }
          />
        ) : null}
      </LinearGradient>
    );
  }

  function onClearInputCon() {
    Keyboard.dismiss();
    searchInputRef.current.blur();
    enableAnimateInEaseOut();
    setSearchTxt('');
    setTimeout(() => {
      onRefresh();
    }, 100);
  }

  function onGoAction() {
    Keyboard.dismiss();
    searchInputRef.current.blur();
    setSearchBtnLoad(true);
    setPageLoader(true);
    setTimeout(() => {
      getSearchList();
    }, 100);
  }

  function renderHeader() {
    return (
      <GredientHeader
        leftIcon
        title={headerTitle}
        onLeftAction={() => {
          navigation.pop();
        }}
      />
    );
  }

  function renderFooterComponent() {
    if (moreLoad) {
      return (
        <View style={styles.loaderFooterView}>
          <ActivityIndicator
            size={'small'}
            animating
            color={BaseColor.ThemeOrange}
          />
        </View>
      );
    } else {
      return <View style={styles.listFooterView} />;
    }
  }

  return (
    <View style={styles.mainCon}>
      {renderHeader()}
      {renderSearchBar()}
      {pageLoader ? renderLoader() : renderList()}
    </View>
  );
};

export default OtherCategory;
