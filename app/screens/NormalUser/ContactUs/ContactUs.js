import React, {useCallback, useEffect, useRef, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {View, StatusBar, StyleSheet, Keyboard, ScrollView} from 'react-native';
import {getApiDataProgress} from '../../../utils/apiHelper';
import authActions from '../../../redux/reducers/auth/actions';
import {BaseSetting} from '../../../config/setting';
import TextInput from '../../../components/TextInput/index';
import Button from '../../../components/Button/index';
import Text from '../../../components/Text/index';
import GredientHeader from '../../../components/DriverApp/GredientHeader';
import {StaticAlertMsg, StaticHeader} from '../../../config/StaticAlertMsg';
import _ from 'lodash';
import CAlert from '../../../components/CAlert';
import {checkObject} from '../../../utils/commonFunction';
const {setUserData, setUserType} = authActions;

const styles = StyleSheet.create({
  main: {
    flex: 1,
  },
  scrollCon: {
    flexGrow: 1,
    paddingHorizontal: 10,
  },
  PT30: {
    paddingTop: 30,
  },
  MT20: {
    marginTop: 20,
  },
  PT50: {
    paddingTop: 30,
  },
  PL20: {
    paddingLeft: 20,
  },
});

export default function ContactUs({navigation}) {
  const {isConnected, userData} = useSelector((state) => state.auth);
  const [loader, setLoader] = useState(false);
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [message, setMessage] = useState('');
  const nameId = useRef();
  const emailId = useRef();
  const messageId = useRef();
  const userToken = checkObject(userData, 'access_token');
  const dispatch = useDispatch();

  const header = {
    Authorization: `Bearer ${userToken}`,
  };
  function Validate() {
    // let valid = true;
    // let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    // if (_.isEmpty(name) || _.isEmpty(email) || _.isEmpty(message)) {
    //   valid = false;
    //   CAlert(StaticAlertMsg.fillAllData, StaticHeader.Alert);
    //   return false;
    // }
    // if (_.trim(name).length < 4 || _.trim(name).length > 7) {
    //   valid = false;
    //   CAlert(StaticAlertMsg.nameLength, StaticHeader.Alert);
    //   return false;
    // }
    // if (reg.test(email) === false) {
    //   valid = false;
    //   CAlert('please enter a valid email address ');
    //   return false;
    // }
    // if (_.trim(message).length >= 255) {
    //   valid = false;
    //   CAlert('Message should be of 255 chars only');
    //   return false;
    // }
    // if (valid === true) {
      Keyboard.dismiss();
      setLoader(true);
      setTimeout(() => {
        contactUsAction();
      }, 100);
    // }
  }

  async function contactUsAction() {
    Keyboard.dismiss();
    setLoader(true);
    if (isConnected === true) {
      const updatedData = {
        'ContactForm[name]': name,
        'ContactForm[email]': email,
        'ContactForm[body]': message,
      };

      try {
        let endPoint = BaseSetting.endpoints.contact_us;
        const response = await getApiDataProgress(
          navigation,
          endPoint,
          'POST',
          updatedData,
          header,
        );
        if (
          _.isObject(response) &&
          !_.isEmpty(response) &&
          response.success === true
        ) {
          const msg = checkObject(response, 'message');
          CAlert(msg, StaticHeader.Success, () => {
            setName('');
            setEmail('');
            setMessage('');
            setLoader(false);
          });
        } else {
          const successMessage = checkObject(response, 'message');
          CAlert(successMessage, StaticHeader.Oops, () => {
            setLoader(false);
          });
        }
      } catch (err) {
        console.log('Catch Part', err);
        CAlert(StaticAlertMsg.someThing_Went_Wrong, StaticHeader.Oops, () => {
          setLoader(false);
        });
      }
    } else {
      CAlert(StaticAlertMsg.Network_Problem, StaticHeader.Oops, () => {
        setLoader(false);
      });
    }
  }

  function renderHeader() {
    return (
      <GredientHeader
        leftIcon
        title={'Contact Us'}
        onLeftAction={() => {
          navigation.pop();
        }}
      />
    );
  }

  function renderForm() {
    return (
      <ScrollView
        keyboardShouldPersistTaps="never"
        showsVerticalScrollIndicator={false}
        contentContainerStyle={styles.scrollCon}>
        {/* <Text body1 medium blackColor style={styles.PT30}>
          Contact Details
        </Text> */}

        <TextInput
          ref={nameId}
          placeholder="Name"
          onChangeText={(t) => {
            setName(t);
          }}
          value={name}
          inputStyle={styles.PL20}
          onSubmitEditing={() => emailId.current.focus()}
          returnKeyType="next"
          keyboardType={'default'}
          otherCon={styles.MT20}
        />
        <TextInput
          ref={emailId}
          placeholder="Email"
          onChangeText={(t) => {
            setEmail(t);
          }}
          value={email}
          inputStyle={styles.PL20}
          onSubmitEditing={() => messageId.current.focus()}
          returnKeyType="next"
          keyboardType={'email-address'}
          otherCon={styles.MT20}
        />
        <TextInput
          ref={messageId}
          placeholder="Message"
          onChangeText={(t) => {
            setMessage(t);
          }}
          value={message}
          onSubmitEditing={() => {
            Keyboard.dismiss();
          }}
          returnKeyType="done"
          keyboardType={'default'}
          textArea
          otherCon={styles.MT20}
        />

        <View style={styles.PT50}>
          <Button
            loading={loader}
            buttonText={'SUBMIT'}
            onPress={() => {
              Validate();
              // setLoader(true);
              // navigation.pop();
            }}
          />
        </View>
      </ScrollView>
    );
  }

  return (
    <View style={styles.main}>
      {renderHeader()}
      {renderForm()}
    </View>
  );
}
