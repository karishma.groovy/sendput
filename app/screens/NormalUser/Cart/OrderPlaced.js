
import React, {useRef, useState, useEffect} from 'react';
import {View, StyleSheet} from 'react-native';
import LottieView from 'lottie-react-native';
import _ from 'lodash';
import {Images} from '../../../config';
import {BaseColor} from '../../../config/theme';
import Text from '../../../components/Text/index';
import {BaseSetting} from '../../../config/setting';
import Button from '../../../components/Button/index';
import {setStatusbar} from '../../../config/statusbar';
import {checkObject} from '../../../utils/commonFunction';

const nWidth = BaseSetting.nWidth;

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: '#f8f8ff',
  },
  subCon: {
    flex: 1,
    paddingHorizontal: 30,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: BaseColor.whiteColor,
  },
  imageStyle: {
    height: nWidth / 3,
    width: nWidth / 3,
  },
  PB30: {
    paddingBottom: 30,
  },
  PT10: {
    paddingTop: 10,
  },
});


export default function OrderPlaced({route, navigation}) {
  const confirmOrderData = route.params;
  const orderNo = checkObject(confirmOrderData, 'order_no');

  const animation = useRef();

  useEffect(() => {
    setStatusbar('light');
  }, []);

  return (
    <View style={styles.main}>
      <View style={styles.subCon}>
        <LottieView
          ref={animation}
          autoSize={false}
          style={styles.imageStyle}
          source={Images.success}
          autoPlay={true}
          loop={true}
        />

        <Text title1 bold textAlignCenter style={{color: '#2EA45E'}}>Thank you</Text>
        <Text textAlignCenter body1 style={[styles.PB30, styles.PT10]}>Your order has been placed successfully</Text>

        <Text textAlignCenter body1 style={styles.PB30}>Order No:- <Text bold>{orderNo}</Text></Text>

        <Button
          buttonText={'Okay'}
          onPress={() => {
            navigation.pop();
          }}
        />
      </View>
    </View>
  );
}