import React, {useRef, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import _ from 'lodash';
import {View, FlatList, StyleSheet, RefreshControl} from 'react-native';
import {Images} from '../../../config';
import CAlert from '../../../components/CAlert';
import {BaseColor} from '../../../config/theme';
import Toast from '../../../components/Toast/index';
import CNoData from '../../../components/CNoData';
import {BaseSetting} from '../../../config/setting';
import {checkObject} from '../../../utils/commonFunction';
import CartCard from '../../../components/CartCard/CartCard';
import authActions from '../../../redux/reducers/auth/actions';
import {getApiData, getApiDataProgress} from '../../../utils/apiHelper';
import PaymentModal from '../../../components/paymentModal/paymentModal';
import GredientHeader from '../../../components/DriverApp/GredientHeader';
import {CartLoader} from '../../../components/ContentLoader/ContentLoader';
import {StaticAlertMsg, StaticHeader} from '../../../config/StaticAlertMsg';

const nWidth = BaseSetting.nWidth;

const {setOrderList, setCartBadgeCount} = authActions;

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: '#f8f8ff',
  },
  FlatMainCon: {
    flexGrow: 1,
  },
  listFooterView: {
    width: '100%',
    height: 50,
  },
  emptyViewCon: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  imageStyle: {
    height: nWidth / 3,
    width: nWidth / 3,
  },
  modalCon: {
    flex: 1,
    paddingHorizontal: 30,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: BaseColor.whiteColor,
  },
  PB30: {
    paddingBottom: 30,
  },
  PT10: {
    paddingTop: 10,
  },
  PT5: {
    paddingTop: 5,
  },
});

export default function Cart({navigation}) {
  const {isConnected, userData, orderList} = useSelector((state) => state.auth);
  const [ismodalVisible, setIsmodalVisible] = useState(false);
  const [refreshLoader, setRefreshLoader] = useState(false);
  const [pageLoader, setPageLoader] = useState(true);
  const [cartListData, setCartistData] = useState([]);
  const [paymentList, setPaymentList] = useState([]);

  const mailListRef = useRef();
  const ToastRef = useRef();
  const dispatch = useDispatch();

  const userToken = checkObject(userData, 'access_token');
  const header = {
    Authorization: `Bearer ${userToken}`,
  };

  React.useEffect(
    () =>
      navigation.addListener('focus', () => {
        setPageLoader(true);
        getCartList();
      }),
    [],
  );

  function handleAllLoader() {
    setTimeout(() => {
      setPageLoader(false);
      setRefreshLoader(false);
    }, 100);
  }

  function onRefresh() {
    setRefreshLoader(true);
    setPageLoader(true);
    setTimeout(() => {
      getCartList();
    }, 100);
  }

  async function getCartList() {
    dispatch(setOrderList({}));
    if (isConnected === true) {
      try {
        let endPoint = BaseSetting.endpoints.cart_list;
        const response = await getApiData(
          navigation,
          endPoint,
          'GET',
          null,
          header,
        );
        if (
          _.isObject(response) &&
          !_.isEmpty(response) &&
          response.success === true
        ) {
          const cList = checkObject(response, 'data');
          setCartistData(cList);
          dispatch(setOrderList(cList));
          getPaymentList();
        } else {
          const errorMessage = checkObject(response, 'message');
          CAlert(errorMessage, StaticHeader.Oops, () => {
            handleAllLoader();
          });
        }
      } catch (err) {
        console.log('Catch Part', err);
        CAlert(StaticAlertMsg.someThing_Went_Wrong, StaticHeader.Oops, () => {
          handleAllLoader();
        });
      }
    } else {
      CAlert(StaticAlertMsg.Network_Problem, StaticHeader.Oops, () => {
        handleAllLoader();
      });
    }
  }

  async function getPaymentList() {
    if (isConnected === true) {
      try {
        let endPoint = BaseSetting.endpoints.card_list;
        const response = await getApiData(
          navigation,
          endPoint,
          'GET',
          null,
          header,
        );
        if (
          _.isObject(response) &&
          !_.isEmpty(response) &&
          response.success === true
        ) {
          const payListData =
            response && response.data
              ? response.data.cards && response.data.cards.data
              : [];
          setPaymentList(payListData);
          handleAllLoader();
        } else {
          const errorMessage = checkObject(response, 'message');
          CAlert(errorMessage, StaticHeader.Oops, () => {
            handleAllLoader();
          });
        }
      } catch (err) {
        console.log('Catch Part', err);
        CAlert(StaticAlertMsg.someThing_Went_Wrong, StaticHeader.Oops, () => {
          handleAllLoader();
        });
      }
    } else {
      CAlert(StaticAlertMsg.Network_Problem, StaticHeader.Oops, () => {
        handleAllLoader();
      });
    }
  }

  async function deleteStore(data) {
    const storeData = checkObject(data, 'store');
    const storeId = checkObject(storeData, 'id');
    if (isConnected === true) {
      const removeData = {
        store_id: storeId,
      };

      try {
        let endPoint = BaseSetting.endpoints.remove_store;
        const response = await getApiDataProgress(
          navigation,
          endPoint,
          'POST',
          removeData,
          header,
        );
        if (
          _.isObject(response) &&
          !_.isEmpty(response) &&
          response.success === true
        ) {
          const successMessage = checkObject(response, 'message');
          const cBadgeNo = checkObject(response, 'data');
          dispatch(setCartBadgeCount(_.toNumber(cBadgeNo)));
          getCartList();
          ToastRef.current.show(successMessage, 2000);
        } else {
          const errorMessage = checkObject(response, 'message');
          CAlert(errorMessage, StaticHeader.Oops, () => {
            handleAllLoader();
          });
        }
      } catch (err) {
        console.log('Catch Part', err);
        CAlert(StaticAlertMsg.someThing_Went_Wrong, StaticHeader.Oops, () => {
          handleAllLoader();
        });
      }
    } else {
      CAlert(StaticAlertMsg.Network_Problem, StaticHeader.Oops, () => {
        handleAllLoader();
      });
    }
  }

  function renderItem({item, index}) {
    return (
      <CartCard
        key={index}
        data={item}
        paymentList={paymentList}
        navigation={navigation}
        onDeleteStore={
          pageLoader
            ? null
            : () => {
                CAlert(
                  StaticAlertMsg.removeEntireProduct,
                  StaticHeader.Confirm,
                  () => {
                    setPageLoader(true);
                    setTimeout(() => {
                      deleteStore(item);
                    }, 1000);
                  },
                  () => {
                    return null;
                  },
                  'YES',
                  'NO',
                );
              }
        }
        onlyRefreshData={(msg) => {
          getCartList();
        }}
        onAddAddressAction={() => {
          navigation.navigate('AddAddress');
        }}
        onPaymentCard={() => {
          setIsmodalVisible(true);
        }}
        onRefreshList={(orderData) => {
          navigation.navigate('OrderPlaced', orderData);
        }}
      />
    );
  }

  function renderFooterComponent() {
    return <View style={styles.listFooterView} />;
  }

  function renderEmptyComponent() {
    return (
      <View style={styles.emptyViewCon}>
        <CNoData imageSource={Images.emptyCart} msgNoData={'Cart is empty'} />
      </View>
    );
  }

  function renderList() {
    return (
      <FlatList
        ref={mailListRef}
        data={cartListData}
        scrollsToTop={false}
        onEndReachedThreshold={0.5}
        showsVerticalScrollIndicator={false}
        keyExtractor={(item) => item.id}
        renderItem={renderItem}
        contentContainerStyle={styles.FlatMainCon}
        ListFooterComponent={renderFooterComponent}
        ListEmptyComponent={renderEmptyComponent}
        refreshControl={
          <RefreshControl
            colors={[BaseColor.ThemeOrange]}
            tintColor={BaseColor.ThemeOrange}
            refreshing={refreshLoader}
            onRefresh={() => {
              onRefresh();
            }}
          />
        }
      />
    );
  }

  function renderAddCardModal() {
    return (
      <PaymentModal
        navigation={navigation}
        ismodalVisible={ismodalVisible}
        dismiss={() => {
          setIsmodalVisible(false);
        }}
        onRefreshList={() => {
          setIsmodalVisible(false);
          getCartList();
        }}
      />
    );
  }

  function renderLoader() {
    return <CartLoader />;
  }

  function renderHeader() {
    return (
      <GredientHeader
        rightIcon={pageLoader ? false : true}
        rightIconName={'add'}
        onRightAction={
          pageLoader
            ? null
            : () => {
                navigation.navigate('Dispensaries');
              }
        }
        title={'Cart'}
      />
    );
  }

  return (
    <View style={styles.main}>
      {renderHeader()}
      {pageLoader ? renderLoader() : renderList()}
      {renderAddCardModal()}
      <Toast
        ref={ToastRef}
        position="bottom"
        positionValue={150}
        fadeInDuration={750}
        fadeOutDuration={2000}
        opacity={0.8}
      />
    </View>
  );
}
