/* eslint-disable react-native/no-inline-styles */
import React, {useRef, useState} from 'react';
import Carousel, {Pagination} from 'react-native-snap-carousel';
import Icon from 'react-native-vector-icons/Ionicons';
import {useDispatch, useSelector} from 'react-redux';
import HTML from 'react-native-render-html';
import _ from 'lodash';
import {
  View,
  Modal,
  Image,
  Keyboard,
  StyleSheet,
  ScrollView,
  RefreshControl,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import Button from '../../../components/Button';
import CAlert from '../../../components/CAlert';
import {BaseColor} from '../../../config/theme';
import Text from '../../../components/Text/index';
import {BaseSetting} from '../../../config/setting';
import {FontFamily} from '../../../config/typography';
import Toast from '../../../components/Toast/index';
import {
  checkObject,
  enableAnimateInEaseOut,
} from '../../../utils/commonFunction';
import TextInput from '../../../components/TextInput/index';
import authActions from '../../../redux/reducers/auth/actions';
import {getApiData, getApiDataProgress} from '../../../utils/apiHelper';
import GredientHeader from '../../../components/DriverApp/GredientHeader';
import {StaticAlertMsg, StaticHeader} from '../../../config/StaticAlertMsg';
import {ProductDetailLoader} from '../../../components/ContentLoader/ContentLoader';

const {setOrderList, setCartBadgeCount} = authActions;
const nWidth = BaseSetting.nWidth;

const styles = StyleSheet.create({
  main: {
    flex: 1,
    position: 'relative',
  },
  mainScrollCon: {
    flexGrow: 1,
  },
  sliderMinCon: {
    width: nWidth,
    height: nWidth / 2,
    position: 'relative',
  },
  paginationContainer: {
    backgroundColor: '#0000',
    position: 'absolute',
    marginHorizontal: 0,
    paddingVertical: 5,
    bottom: 0,
    left: 0,
    right: 0,
  },
  dotConSty: {
    marginHorizontal: 2,
  },
  activeDotStyle: {
    width: 10,
    height: 10,
    borderRadius: 5,
    backgroundColor: BaseColor.ThemeRedColor,
  },
  inActiveDotSty: {
    width: 8,
    height: 8,
    borderRadius: 4,
    backgroundColor: BaseColor.grayColor,
  },
  courasalImage: {
    width: '100%',
    height: '100%',
    resizeMode: 'contain',
  },
  btnContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    paddingBottom: 20,
  },
  btnWidth: {
    width: '45%',
    borderWidth: 1,
  },
  btn1: {
    width: '45%',
  },
  infoMainCon: {
    flex: 1,
    paddingHorizontal: 10,
    paddingTop: 15,
  },
  cmnFlex: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingTop: 10,
    paddingBottom: 15,
  },
  reviewText: {
    color: BaseColor.ThemeRedColor,
    paddingLeft: 8,
  },
  starView: {
    flexDirection: 'row',
    paddingTop: 5,
  },
  centeredView: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  middleView: {
    paddingHorizontal: 20,
    paddingBottom: 100,
    backgroundColor: BaseColor.whiteColor,
    width: BaseSetting.nWidth,
    marginTop: 200,
    position: 'relative',
  },
  MT15: {
    marginTop: 20,
  },
  MT30: {
    marginLeft: 10,
    width: '40%',
  },
  PL20: {
    paddingLeft: 20,
  },
  dropDown: {
    height: 45,
    borderWidth: 1,
    borderRadius: 3,
    borderColor: '#ddd',
    alignSelf: 'center',
    backgroundColor: BaseColor.whiteColor,
  },
  pickerStyle: {
    height: BaseSetting.nWidth / 6,
    backgroundColor: BaseColor.whiteColor,
  },
  dropdownTitleSty: {
    color: BaseColor.grayColor,
    fontFamily: FontFamily.regular,
  },
  PT0: {
    paddingTop: 0,
  },
});

const ProductDetails = ({route, navigation}) => {
  const item = route.params;
  const productId = checkObject(item, 'id');
  const starData = [1, 2, 3, 4, 5];
  const courasalId = useRef();

  const ToastRef = useRef();

  const dispatch = useDispatch();

  const {isConnected, userData, cartBadge, orderList} = useSelector(
    (state) => state.auth,
  );
  const [pageLoader, setPageLoader] = useState(true);
  const [productData, setProductData] = useState({});
  const [CreateItemListData, setCreateItemListData] = useState({});
  const [listData, setListData] = useState([]);

  const [paginationNum, setPaginationNum] = useState(0);
  const [refreshLoader, setRefreshLoader] = useState(false);
  const [modalVisible, setModalVisible] = useState(false);
  const userToken = checkObject(userData, 'access_token');
  const [favName, setFavName] = useState('');
  const [isNewCategory, setIsNewCategory] = useState(false);
  const [selectedId, setSelectedID] = useState({});

  const [showItemList, setShowItemList] = useState(false);

  const [addCartLoad, setAddCartLoad] = useState(false);
  const [setAddCatLoad, setAddCatLoader] = useState(false);
  const [saveCatLoad, setSaveCatLoad] = useState(false);

  const header = {
    Authorization: `Bearer ${userToken}`,
  };

  React.useEffect(
    () =>
      navigation.addListener('focus', () => {
        getProductDetail();
      }),
    [],
  );

  function onRefresh() {
    setRefreshLoader(true);
    setPageLoader(true);
    setTimeout(() => {
      getProductDetail();
    }, 500);
  }

  function handleAllLoader() {
    setTimeout(() => {
      setPageLoader(false);
      setRefreshLoader(false);
      setSaveCatLoad(false);
    }, 200);
  }

  async function getProductDetail() {
    const pData = {
      product_id: productId,
    };

    if (isConnected === true) {
      try {
        let endPoint = `${BaseSetting.endpoints.product_detail}?product_id=${productId}`;
        const response = await getApiData(
          navigation,
          endPoint,
          'GET',
          null,
          header,
        );
        if (
          _.isObject(response) &&
          !_.isEmpty(response) &&
          response.success === true
        ) {
          const details = checkObject(response, 'data');
          setProductData(details);
          getItemCatList();
        } else {
          const errorMessage = checkObject(response, 'message');
          CAlert(errorMessage, StaticHeader.Oops, () => {
            handleAllLoader();
          });
        }
      } catch (err) {
        console.log('Catch Part', err);
        CAlert(StaticAlertMsg.someThing_Went_Wrong, StaticHeader.Oops, () => {
          handleAllLoader();
        });
      }
    } else {
      CAlert(StaticAlertMsg.Network_Problem, StaticHeader.Oops, () => {
        handleAllLoader();
      });
    }
  }

  async function CreateItemList() {
    const cData = {
      'MyItem[list_name]': favName,
    };

    if (isConnected === true) {
      try {
        let endPoint = `${BaseSetting.endpoints.create_item_list}`;
        const response = await getApiDataProgress(
          navigation,
          endPoint,
          'POST',
          cData,
          header,
        );
        if (
          _.isObject(response) &&
          !_.isEmpty(response) &&
          response.success === true
        ) {
          getItemCatList();
        } else {
          const errorMessage = checkObject(response, 'message');
          CAlert(errorMessage, StaticHeader.Oops, () => {
            handleAllLoader();
          });
        }
      } catch (err) {
        CAlert(StaticAlertMsg.someThing_Went_Wrong, StaticHeader.Oops, () => {
          handleAllLoader();
        });
      }
    } else {
      CAlert(StaticAlertMsg.Network_Problem, StaticHeader.Oops, () => {
        handleAllLoader();
      });
    }
  }

  async function getItemCatList() {
    if (isConnected === true) {
      try {
        let endPoint = `${BaseSetting.endpoints.item_list}`;
        const response = await getApiData(
          navigation,
          endPoint,
          'GET',
          null,
          header,
        );
        if (
          _.isObject(response) &&
          !_.isEmpty(response) &&
          response.success === true
        ) {
          const details = checkObject(response, 'data');
          const itemList = checkObject(details, 'rows');
          setListData(itemList);
          handleAllLoader();
          setFavName('');
          setIsNewCategory(false);
        } else {
          console.log('🚀 ~ file:~ response', response);
          const errorMessage = checkObject(response, 'message');
          CAlert(errorMessage, StaticHeader.Oops, () => {
            handleAllLoader();
          });
        }
      } catch (err) {
        CAlert(StaticAlertMsg.someThing_Went_Wrong, StaticHeader.Oops, () => {
          handleAllLoader();
        });
      }
    } else {
      CAlert(StaticAlertMsg.Network_Problem, StaticHeader.Oops, () => {
        handleAllLoader();
      });
    }
  }

  async function addProductAction() {
    const productItemId = checkObject(selectedId, 'value');
    const pData = {
      'MyItemList[my_item_id]': productItemId,
      'MyItemList[product_id]': productId,
    };

    if (isConnected === true) {
      try {
        let endPoint = `${BaseSetting.endpoints.add_product}`;
        const response = await getApiDataProgress(
          navigation,
          endPoint,
          'POST',
          pData,
          header,
        );
        if (
          _.isObject(response) &&
          !_.isEmpty(response) &&
          response.success === true
        ) {
          setAddCatLoader(false);
          setIsNewCategory(false);
          setModalVisible(false);
          getProductDetail();
          setSelectedID({});
          setShowItemList(false);
        } else {
          const errorMessage = checkObject(response, 'message');
          CAlert(errorMessage, StaticHeader.Oops, () => {
            setAddCatLoader(false);
          });
        }
      } catch (err) {
        console.log('Catch Part', err);
        CAlert(StaticAlertMsg.someThing_Went_Wrong, StaticHeader.Oops, () => {
          setAddCatLoader(false);
        });
      }
    } else {
      CAlert(StaticAlertMsg.Network_Problem, StaticHeader.Oops, () => {
        setAddCatLoader(false);
      });
    }
  }

  async function checkoutAction() {
    const storeId = checkObject(productData, 'store_id');
    const cData = {
      'Cart[product_id]': productId,
      'Cart[store_id]': storeId,
      'Cart[qty]': 1,
    };

    if (isConnected === true) {
      try {
        let endPoint = `${BaseSetting.endpoints.create_cart}`;
        const response = await getApiDataProgress(
          navigation,
          endPoint,
          'POST',
          cData,
          header,
        );
        if (
          _.isObject(response) &&
          !_.isEmpty(response) &&
          response.success === true
        ) {
          const successMessage = checkObject(response, 'message');
          const cBadgeNo = checkObject(response, 'data');
          dispatch(setCartBadgeCount(_.toNumber(cBadgeNo)));
          ToastRef.current.show(successMessage, 2000);
          setAddCartLoad(false);
        } else {
          const errorMessage = checkObject(response, 'message');
          CAlert(errorMessage, StaticHeader.Oops, () => {
            setAddCartLoad(false);
          });
        }
      } catch (err) {
        CAlert(StaticAlertMsg.someThing_Went_Wrong, StaticHeader.Oops, () => {
          setAddCartLoad(false);
        });
      }
    } else {
      CAlert(StaticAlertMsg.Network_Problem, StaticHeader.Oops, () => {
        setAddCartLoad(false);
      });
    }
  }

  function pagination() {
    const sliderImages = checkObject(productData, 'product_image');
    const length = sliderImages.length;
    return (
      <Pagination
        tappableDots={true}
        animatedTension={100}
        dotsLength={length}
        activeDotIndex={paginationNum}
        containerStyle={styles.paginationContainer}
        dotContainerStyle={styles.dotConSty}
        dotStyle={styles.activeDotStyle}
        inactiveDotStyle={styles.inActiveDotSty}
        inactiveDotOpacity={1}
        inactiveDotScale={0.6}
      />
    );
  }

  function otherInformationCon() {
    const productName = checkObject(productData, 'name');
    const productPrice = checkObject(productData, 'price');
    const productDesc = checkObject(productData, 'description');
    const noOfReview = checkObject(productData, 'number_review');
    const startCount = checkObject(productData, 'average_star');
    const dateTime = checkObject(productData, 'created_at');
    const productBrandName = checkObject(productData, 'brand_name');
    const ProductCategory = checkObject(productData, 'category');
    // console.log("🚀 ~ file: ProductDetails.js ~ line 477 ~ otherInformationCon ~ ProductCategory.join()", ProductCategory.map((x)=>{x}))
    // {
    //   ProductCategory.map((x)=>{
    //     console.log("🚀 ~ file: ProductDetails.js ~ line 477 ~ otherInformationCon ~ ProductCategory.join()", x)
    //   })
    // }
    return (
      <View style={styles.infoMainCon}>
        <Text numberOfLines={2} bold title2 style={{paddingRight: nWidth / 5}}>
          {productName}
        </Text>

        <View style={styles.cmnFlex}>
          <View style={styles.main}>
            <Text>
              Pull{' '}
              <Text bold body2>
                {productBrandName}
              </Text>
            </Text>
            <View style={styles.starView}>
              {starData.map((obj) => {
                const isYellow = obj <= _.toNumber(startCount);
                return (
                  <Icon
                    name={isYellow ? 'star' : 'star-outline'}
                    size={13}
                    color={
                      isYellow ? BaseColor.ThemeYellow : BaseColor.grayColor
                    }
                    style={{marginRight: 1}}
                  />
                );
              })}
              <Text
                caption1
                style={styles.reviewText}>{`${noOfReview} Reviews`}</Text>
            </View>
          </View>
          <Text bold title3>
            {productPrice}
          </Text>
        </View>
        {/* {
              ProductCategory.map((x)=>{
                <View style={{flexDirection:'row' }}>
                return(
                    <Text>{x}</Text>
                  </View>
                )
              })
            }
             */}

        {_.isEmpty(productDesc) ? null : (
          <View>
            <Text grayColor body2>
              Description
            </Text>
            <HTML
              tagsStyles={{
                p: {marginTop: 10, fontSize: 14, color: BaseColor.blackColor},
              }}
              source={{html: productDesc}}
              contentWidth={nWidth}
            />
          </View>
        )}
      </View>
    );
  }

  function topSliderCon() {
    const sliderImages = checkObject(productData, 'product_image');
    return (
      <View style={styles.sliderMinCon}>
        <Carousel
          ref={courasalId}
          itemWidth={nWidth}
          sliderWidth={nWidth}
          data={sliderImages}
          renderItem={({item, index}) => {
            console.log(item);
            return (
              <Image
                key={index}
                source={{uri: item}}
                style={styles.courasalImage}
              />
            );
          }}
          onSnapToItem={(index) => setPaginationNum(index)}
        />
        {pagination()}
      </View>
    );
  }

  function renderMainScroll() {
    return (
      <ScrollView
        bounces={false}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={styles.mainScrollCon}
        refreshControl={
          <RefreshControl
            colors={[BaseColor.ThemeOrange]}
            tintColor={BaseColor.ThemeOrange}
            refreshing={refreshLoader}
            onRefresh={() => {
              onRefresh();
            }}
          />
        }>
        {topSliderCon()}
        {otherInformationCon()}
      </ScrollView>
    );
  }

  function addProductInCart() {
    setAddCartLoad(true);
    setTimeout(() => {
      checkoutAction();
    }, 1000);
  }

  function renderBottomButtons() {
    return (
      <View style={[styles.btnContainer, {paddingHorizontal: 10}]}>
        <Button
          loading={addCartLoad}
          // style={styles.btn1}
          colorAry={[BaseColor.ThemeBlue, BaseColor.ThemeBlue]}
          buttonText={'ADD TO CART'}
          onPress={() => {
            addProductInCart();
          }}
        />
        {/* <Button
          styleText={{color: BaseColor.blackColor}}
          style={styles.btnWidth}
          colorAry={['#ffffff', '#ffffff']}
          buttonText={'BUY NOW'}
          onPress={() => {
            return null;
          }}
        /> */}
      </View>
    );
  }

  function gotoCartTab() {
    navigation.navigate('Cart');
  }

  function renderHeader() {
    return (
      <GredientHeader
        leftIcon
        rightIcon
        rightIconName={'cart-outline'}
        cartBadgeNo={cartBadge}
        title={'Product Detail'}
        // productDetailIcon={pageLoader ? false : true}
        onLeftAction={() => {
          navigation.pop();
        }}
        onRightAction={() => {
          gotoCartTab();
        }}
        // onHeartAction={pageLoader ? null : () => {
        //   setModalVisible(true);
        // }}
        // heartFilled={productData.is_favourite}
        // onShareAction={() => {
        //   return null;
        // }}
      />
    );
  }

  function renderLoader() {
    return <ProductDetailLoader />;
  }

  function renderModal() {
    const isFav = checkObject(productData, 'is_favourite');
    const productName = checkObject(productData, 'name');
    const isArrayBlank = _.isArray(listData) && _.isEmpty(listData);

    return (
      <Modal
        animationType="slide"
        visible={modalVisible}
        transparent={true}
        onRequestClose={
          setAddCatLoad || saveCatLoad
            ? null
            : () => {
                setIsNewCategory(false);
                setModalVisible(false);
                setShowItemList(false);
                setSelectedID({});
              }
        }>
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'flex-end',
            backgroundColor: 'rgba(0,0,0,0.5)',
          }}>
          {isNewCategory ? (
            <View
              style={{
                width: '100%',
                paddingHorizontal: 10,
                backgroundColor: BaseColor.whiteColor,
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  height: 60,
                }}>
                <Text title3 bold>
                  Create your category{' '}
                </Text>
              </View>

              <TextInput
                value={favName}
                placeholder="Enter category name"
                keyboardType={'default'}
                otherCon={styles.MT15}
                inputStyle={styles.PL20}
                onChangeText={(t) => {
                  setFavName(t.trim());
                }}
                onSubmitEditing={() => Keyboard.dismiss()}
              />

              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'flex-end',
                  marginVertical: 20,
                }}>
                <TouchableOpacity
                  activeOpacity={0.8}
                  onPress={
                    saveCatLoad
                      ? null
                      : () => {
                          enableAnimateInEaseOut();
                          setIsNewCategory(false);
                          setFavName('');
                          Keyboard.dismiss();
                        }
                  }
                  style={{
                    paddingHorizontal: 18,
                    paddingVertical: 8,
                    borderRadius: 50,
                    elevation: 3,
                    shadowColor: BaseColor.blackColor,
                    shadowOffset: {
                      width: 0,
                      height: 2,
                    },
                    shadowOpacity: 0.25,
                    shadowRadius: 3.84,
                    alignItems: 'center',
                    justifyContent: 'center',
                    borderWidth: StyleSheet.hairlineWidth,
                    borderColor: BaseColor.ThemeBlue,
                    backgroundColor: BaseColor.ThemeBlue,
                  }}>
                  <Text body2 whiteColor>
                    Cancel
                  </Text>
                </TouchableOpacity>

                <TouchableOpacity
                  activeOpacity={0.8}
                  onPress={
                    saveCatLoad
                      ? null
                      : () => {
                          Keyboard.dismiss();
                          setSaveCatLoad(true);
                          setTimeout(() => {
                            CreateItemList();
                          }, 1000);
                        }
                  }
                  style={{
                    paddingHorizontal: 18,
                    paddingVertical: 8,
                    borderRadius: 50,
                    elevation: 3,
                    shadowColor: BaseColor.blackColor,
                    shadowOffset: {
                      width: 0,
                      height: 2,
                    },
                    shadowOpacity: 0.25,
                    shadowRadius: 3.84,
                    marginLeft: 10,
                    alignItems: 'center',
                    justifyContent: 'center',
                    borderWidth: StyleSheet.hairlineWidth,
                    borderColor: BaseColor.greyColor,
                    backgroundColor: BaseColor.whiteColor,
                  }}>
                  {saveCatLoad ? (
                    <ActivityIndicator
                      size={'small'}
                      color={BaseColor.ThemeBlue}
                      animating
                    />
                  ) : (
                    <Text body2 greyColor>
                      Save
                    </Text>
                  )}
                </TouchableOpacity>
              </View>
            </View>
          ) : (
            <View
              style={{
                width: '100%',
                paddingHorizontal: 10,
                backgroundColor: BaseColor.whiteColor,
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  height: 60,
                }}>
                <Text title3 bold>
                  {isFav === 'Yes' ? 'Remove From' : 'Save From'}
                </Text>
                <Icon
                  name={isFav === 'Yes' ? 'heart-outline' : 'heart-sharp'}
                  size={20}
                  color={BaseColor.ThemeOrange}
                />
              </View>

              {isArrayBlank === false ? (
                <Text
                  caption1
                  greyColor
                  style={{
                    lineHeight: 18,
                    letterSpacing: 0.5,
                    paddingBottom: 10,
                  }}>
                  {'Select one category in which you want to add these'}
                  <Text bold>{` ${productName} `}</Text>
                  {
                    'products as favorite product. or click on add (+) button to add New category in list.'
                  }
                </Text>
              ) : (
                <Text
                  caption1
                  greyColor
                  style={{
                    lineHeight: 18,
                    letterSpacing: 0.5,
                    paddingBottom: 10,
                  }}>
                  {
                    "Sorry, Currently you haven't created any category, just click on add button & create your first Category to save your favorite product on it."
                  }
                </Text>
              )}

              <View>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    marginBottom: isArrayBlank ? 40 : 0,
                  }}>
                  {isArrayBlank === false ? (
                    <TouchableOpacity
                      activeOpacity={0.8}
                      onPress={() => {
                        enableAnimateInEaseOut();
                        setShowItemList(!showItemList);
                      }}
                      style={{
                        flex: 1,
                        width: '100%',
                        height: 40,
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                        borderRadius: 5,
                        borderWidth: StyleSheet.hairlineWidth,
                        borderColor: BaseColor.grayColor,
                        backgroundColor: BaseColor.whiteColor,
                      }}>
                      <View style={{flex: 1, paddingLeft: 10}}>
                        <Text caption1 greyColor>
                          {selectedId && selectedId.label
                            ? selectedId.label
                            : 'Select Favorite Chategory'}
                        </Text>
                      </View>
                      <View style={{width: 24}}>
                        <Icon
                          name={showItemList ? 'caret-up' : 'caret-down'}
                          size={15}
                          color={'#CCC'}
                        />
                      </View>
                    </TouchableOpacity>
                  ) : null}

                  {showItemList ? null : (
                    <TouchableOpacity
                      activeOpacity={0.8}
                      onPress={() => {
                        enableAnimateInEaseOut();
                        setIsNewCategory(true);
                        setSelectedID({});
                      }}
                      style={{
                        width: isArrayBlank ? '100%' : 40,
                        height: 40,
                        marginLeft: isArrayBlank ? 0 : 10,
                        alignItems: 'center',
                        justifyContent: 'center',
                        borderRadius: 5,
                        borderWidth: StyleSheet.hairlineWidth,
                        borderColor: BaseColor.grayColor,
                        backgroundColor: BaseColor.whiteColor,
                        flexDirection: 'row',
                      }}>
                      <Icon
                        name={'add-sharp'}
                        size={15}
                        color={BaseColor.ThemeBlue}
                      />
                      {isArrayBlank ? (
                        <Text caption1 style={{paddingLeft: 2}}>
                          Add
                        </Text>
                      ) : null}
                    </TouchableOpacity>
                  )}
                </View>

                {showItemList ? (
                  <View
                    style={{width: '100%', height: nWidth / 3, marginTop: 5}}>
                    <ScrollView
                      bounces={false}
                      nestedScrollEnabled
                      contentContainerStyle={{flexGrow: 1}}>
                      {listData.map((obj, index) => {
                        return (
                          <TouchableOpacity
                            key={index}
                            onPress={() => {
                              enableAnimateInEaseOut();
                              setShowItemList(false);
                              setSelectedID(obj);
                            }}
                            style={{
                              width: '98%',
                              height: 40,
                              paddingHorizontal: 10,
                              alignItems: 'flex-start',
                              justifyContent: 'center',
                              alignSelf: 'center',
                              elevation: 2,
                              marginTop: 5,
                              marginBottom:
                                listData.length - 1 === index ? 10 : 0,
                              borderRadius: 5,
                              backgroundColor: BaseColor.whiteColor,
                              shadowColor: BaseColor.blackColor,
                              shadowOffset: {
                                width: 0,
                                height: 2,
                              },
                              shadowOpacity: 0.25,
                              shadowRadius: 3.84,
                            }}>
                            <Text
                              subhead
                              bold
                              style={{letterSpacing: 4, color: '#4c4c4c'}}>
                              {obj.label}
                            </Text>
                          </TouchableOpacity>
                        );
                      })}
                    </ScrollView>
                  </View>
                ) : null}
              </View>

              {isArrayBlank ? null : (
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'flex-end',
                    marginVertical: 20,
                  }}>
                  <TouchableOpacity
                    activeOpacity={0.8}
                    onPress={
                      setAddCatLoad
                        ? null
                        : () => {
                            setModalVisible(false);
                            setIsNewCategory(false);
                            setShowItemList(false);
                            Keyboard.dismiss();
                            setSelectedID({});
                          }
                    }
                    style={{
                      paddingHorizontal: 18,
                      paddingVertical: 8,
                      borderRadius: 50,
                      elevation: 3,
                      shadowColor: BaseColor.blackColor,
                      shadowOffset: {
                        width: 0,
                        height: 2,
                      },
                      shadowOpacity: 0.25,
                      shadowRadius: 3.84,
                      alignItems: 'center',
                      justifyContent: 'center',
                      borderWidth: StyleSheet.hairlineWidth,
                      borderColor: BaseColor.ThemeBlue,
                      backgroundColor: BaseColor.ThemeBlue,
                    }}>
                    <Text body2 whiteColor>
                      Cancel
                    </Text>
                  </TouchableOpacity>

                  <TouchableOpacity
                    activeOpacity={0.8}
                    onPress={
                      setAddCatLoad
                        ? null
                        : () => {
                            Keyboard.dismiss();
                            setAddCatLoader(true);
                            setTimeout(() => {
                              addProductAction();
                            }, 1000);
                          }
                    }
                    style={{
                      paddingHorizontal: 18,
                      paddingVertical: 8,
                      borderRadius: 50,
                      elevation: 3,
                      shadowColor: BaseColor.blackColor,
                      shadowOffset: {
                        width: 0,
                        height: 2,
                      },
                      shadowOpacity: 0.25,
                      shadowRadius: 3.84,
                      marginLeft: 10,
                      alignItems: 'center',
                      justifyContent: 'center',
                      borderWidth: StyleSheet.hairlineWidth,
                      borderColor: BaseColor.greyColor,
                      backgroundColor: BaseColor.whiteColor,
                    }}>
                    {setAddCatLoad ? (
                      <ActivityIndicator
                        size={'small'}
                        color={BaseColor.ThemeBlue}
                        animating
                      />
                    ) : (
                      <Text body2 greyColor>
                        Save
                      </Text>
                    )}
                  </TouchableOpacity>
                </View>
              )}
            </View>
          )}
        </View>
      </Modal>
    );
  }

  return (
    <View style={styles.main}>
      {renderHeader()}
      {pageLoader ? renderLoader() : null}
      {pageLoader ? null : renderMainScroll()}
      {pageLoader ? null : renderBottomButtons()}
      {renderModal()}
      <Toast
        ref={ToastRef}
        position="bottom"
        positionValue={150}
        fadeInDuration={750}
        fadeOutDuration={2000}
        opacity={0.8}
      />
    </View>
  );
};

export default ProductDetails;
