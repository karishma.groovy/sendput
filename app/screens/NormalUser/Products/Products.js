import React, {useCallback, useEffect, useRef, useState} from 'react';
import LinearGradient from 'react-native-linear-gradient';
import {useDispatch, useSelector} from 'react-redux';
import _ from 'lodash';
import {
  View,
  Image,
  FlatList,
  Dimensions,
  StyleSheet,
  RefreshControl,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import CAlert from '../../../components/CAlert';
import {BaseColor} from '../../../config/theme';
import Text from '../../../components/Text/index';
import {BaseSetting} from '../../../config/setting';
import {getApiData} from '../../../utils/apiHelper';
import {checkObject} from '../../../utils/commonFunction';
import TextInput from '../../../components/TextInput/index';
import SubCategory from '../../../components/CHome/SubCategory';
import GredientHeader from '../../../components/DriverApp/GredientHeader';
import {StaticAlertMsg, StaticHeader} from '../../../config/StaticAlertMsg';
import {ProductLoader} from '../../../components/ContentLoader/ContentLoader';

const styles = StyleSheet.create({
  mainCon: {
    flex: 1,
    backgroundColor: BaseColor.whiteColor,
  },
  searchBarCon: {
    height: 50,
    width: '100%',
    paddingHorizontal: 8,
  },
  searchInputCon: {
    width: '100%',
    height: 40,
    borderRadius: 5,
    backgroundColor: 'white',
    borderWidth: 0,
    marginTop: 0,
  },
  searchIconClr: {
    color: BaseColor.grayColor,
  },
  flexMainCon: {
    flexGrow: 1,
  },
  listFooterView: {
    width: '100%',
    height: 30,
  },
  listHeaderView: {
    width: '100%',
    height: 50,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 7,
    justifyContent: 'space-between',
  },
  emptyViewCon: {
    width: '100%',
    height: BaseSetting.nWidth / 3,
    alignItems: 'center',
    justifyContent: 'center',
  },
  loaderFooterView: {
    width: '100%',
    padding: 30,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: BaseColor.whiteColor,
  },
});

export default function Products({navigation}) {
  const {isConnected, userData} = useSelector((state) => state.auth);
  const [refreshLoader, setRefreshLoader] = useState(false);
  const [searchCatList, setSearchCatList] = useState([]);
  const [pageLoader, setPageLoader] = useState(true);
  const [moreLoad, setMoreLoad] = useState(false);
  const [searchTxt, setSearchTxt] = useState('');
  const [listData, setListData] = useState({});

  const dispatch = useDispatch();

  const mailListRef = useRef();
  const subListRef = useRef();

  const userToken = checkObject(userData, 'access_token');

  const header = {
    Authorization: `Bearer ${userToken}`,
  };

  React.useEffect(
    () =>
      navigation.addListener('focus', () => {
        setSearchTxt('');
        getList(true);
      }),
    [],
  );

  async function getList(bool) {
    const cPage =
      listData && listData.pagination && listData.pagination.currentPage
        ? _.toNumber(listData.pagination.currentPage)
        : 0;
    if (isConnected === true) {
      let PageNo = 0;
      if (bool === true) {
        PageNo = 1;
      } else {
        PageNo = cPage + 1;
      }

      const lData = {
        page: PageNo,
      };

      try {
        let endPoint = `${BaseSetting.endpoints.category_list}?page=${PageNo}`;
        const response = await getApiData(
          navigation,
          endPoint,
          'GET',
          null,
          header,
        );
        if (
          _.isObject(response) &&
          !_.isEmpty(response) &&
          response.success === true
        ) {
          const obj = bool ? {} : _.cloneDeep(listData);

          const newListData =
            response && response.data && response.data.category
              ? response.data.category
              : [];
          const paginationDatas =
            response && response.data && response.data.pagination
              ? response.data.pagination
              : {};

          if (_.isArray(newListData)) {
            if (_.isArray(obj.data) && obj.data.length > 0) {
              obj.data = _.flattenDeep([...obj.data, newListData]);
            } else {
              obj.data = newListData;
            }
            obj.pagination = paginationDatas;
          }
          setListData(obj);
          handleAllLoader();
        } else {
          const errorMessage = checkObject(response, 'message');
          CAlert(errorMessage, StaticHeader.Oops, () => {
            setListData(listData);
            handleAllLoader();
          });
        }
      } catch (err) {
        console.log('Catch Part', err);
        CAlert(StaticAlertMsg.someThing_Went_Wrong, StaticHeader.Oops, () => {
          handleAllLoader();
        });
      }
    } else {
      CAlert(StaticAlertMsg.Network_Problem, StaticHeader.Oops, () => {
        handleAllLoader();
      });
    }
  }

  function handleAllLoader() {
    setTimeout(() => {
      setPageLoader(false);
      setRefreshLoader(false);
      setMoreLoad(false);
    }, 200);
  }

  async function getMoreData() {
    const cPage =
      listData && listData.pagination && listData.pagination.currentPage
        ? _.toNumber(listData.pagination.currentPage)
        : 0;
    const tPage =
      listData && listData.pagination && listData.pagination.totalPage
        ? _.toNumber(listData.pagination.totalPage)
        : 0;

    if (listData.pagination.isMore === true && cPage < tPage) {
      setMoreLoad(true);
      getList(false);
    }
  }

  function onRefresh() {
    setRefreshLoader(true);
    setPageLoader(true);
    setTimeout(() => {
      getList(true);
    }, 500);
  }

  function renderMainCatHeader(str, bool, item) {
    const productData = {
      mainCat: item,
      subCat: null,
    };
    return (
      <View style={styles.listHeaderView}>
        <Text bold headline>
          {str}
        </Text>
        {bool ? (
          <TouchableOpacity
            activeOpacity={0.8}
            onPress={() => {
              navigation.navigate('OtherCategory', productData);
            }}>
            <Text subhead>{'Discover >'}</Text>
          </TouchableOpacity>
        ) : null}
      </View>
    );
  }

  const renderListEmptyText = (str) => {
    return (
      <View style={styles.emptyViewCon}>
        <Text headline>
          {`Sorry currently ${_.toLower(str)} data is empty.`}
        </Text>
      </View>
    );
  };

  const renderCatItem = (parentData, {item, index}) => {
    const productData = {
      mainCat: parentData,
      subCat: item,
    };
    return (
      <SubCategory
        key={index}
        data={item}
        onPress={() => {
          navigation.navigate('OtherCategory', productData);
        }}
      />
    );
  };

  function renderEmptyComponent() {
    return (
      <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
        <Text headline>{'Sorry, Currently products are not available.'}</Text>
      </View>
    );
  }

  function renderFooterComponent() {
    if (moreLoad) {
      return (
        <View style={styles.loaderFooterView}>
          <ActivityIndicator
            size={'small'}
            animating
            color={BaseColor.ThemeOrange}
          />
        </View>
      );
    } else {
      return <View style={styles.listFooterView} />;
    }
  }

  function renderList() {
    return (
      <FlatList
        ref={mailListRef}
        data={listData.data}
        scrollsToTop={false}
        horizontal={false}
        renderItem={({item, index}) => {
          const catName = checkObject(item, 'category_name');
          const listData = checkObject(item, 'sub_category');
          const listLength =
            _.isArray(listData) && !_.isEmpty(listData)
              ? listData.length
              : null;
          const isBool = listLength > 5;
          return (
            <View key={index} style={styles.mainCon}>
              {renderMainCatHeader(catName, true, item)}
              <FlatList
                ref={subListRef}
                horizontal={true}
                data={listData}
                showsHorizontalScrollIndicator={false}
                renderItem={renderCatItem.bind(this, item)}
                keyExtractor={(item) => item.id}
                ListEmptyComponent={() => {
                  return renderListEmptyText(catName);
                }}
                contentContainerStyle={styles.flexMainCon}
                showsVerticalScrollIndicator={false}
              />
            </View>
          );
        }}
        refreshing={refreshLoader}
        keyExtractor={(item) => item.id}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={styles.flexMainCon}
        ListFooterComponent={renderFooterComponent}
        ListEmptyComponent={renderEmptyComponent}
        refreshControl={
          <RefreshControl
            colors={[BaseColor.ThemeOrange]}
            tintColor={BaseColor.ThemeOrange}
            refreshing={refreshLoader}
            onRefresh={() => {
              onRefresh();
            }}
          />
        }
        onEndReached={getMoreData}
        onEndReachedThreshold={0.5}
      />
    );
  }

  function renderLoader() {
    return <ProductLoader />;
  }

  function renderSearchBar() {
    return (
      <LinearGradient
        start={{x: 0.3, y: 0.25}}
        end={{x: 0.8, y: 1.0}}
        colors={[BaseColor.ThemeOrange, BaseColor.ThemeYellow]}
        style={styles.searchBarCon}>
        <TextInput
          leftIconName={'search'}
          otherCon={styles.searchInputCon}
          otherIconCss={styles.searchIconClr}
          placeholder="Search..."
          onChangeText={(t) => {
            setSearchTxt(t);
          }}
          value={searchTxt}
        />
      </LinearGradient>
    );
  }

  function renderHeader() {
    return (
      <GredientHeader
        leftIcon
        title={'Products'}
        onLeftAction={() => {
          navigation.pop();
        }}
      />
    );
  }

  return (
    <View style={styles.mainCon}>
      {renderHeader()}
      {renderSearchBar()}
      {pageLoader ? renderLoader() : renderList()}
    </View>
  );
}
