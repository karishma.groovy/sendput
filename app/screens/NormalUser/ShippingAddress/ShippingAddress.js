import {View, FlatList, StyleSheet, RefreshControl} from 'react-native';
import React, {useRef, useState} from 'react';
import {useSelector} from 'react-redux';
import _ from 'lodash';
import CAlert from '../../../components/CAlert';
import {BaseColor} from '../../../config/theme';
import Button from '../../../components/Button';
import Toast from '../../../components/Toast/index';
import {BaseSetting} from '../../../config/setting';
import {checkObject} from '../../../utils/commonFunction';
import EmptyView from '../../../components/DriverApp/EmptyView';
import {getApiData, getApiDataProgress} from '../../../utils/apiHelper';
import GredientHeader from '../../../components/DriverApp/GredientHeader';
import {StaticAlertMsg, StaticHeader} from '../../../config/StaticAlertMsg';
import {ShippingAddLoader} from '../../../components/ContentLoader/ContentLoader';
import CShippingAddress from '../../../components/ShippingAddress/CShippingAddress';

const nWidth = BaseSetting.nWidth;

const styles = StyleSheet.create({
  main: {
    flex: 1,
    position: 'relative',
  },
  FlatMainCon: {
    flexGrow: 1,
  },
  loaderFooterView: {
    width: '100%',
    padding: 30,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: BaseColor.whiteColor,
  },
  listFooterView: {
    width: '100%',
    height: 100,
  },
  container: {
    width: nWidth,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  MR10: {
    marginRight: 10,
  },
  ItemSeparatorCss: {
    height: 10,
    width: '100%',
  },
  bottomFixedBtn: {
    height: 50,
    width: 50,
    borderRadius: 25,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 0,
  },
  otherCSS: {
    position: 'absolute',
    bottom: 10,
    right: 10,
    zIndex: 10,
  },
});

const ShippingAddress = ({navigation}) => {
  const {isConnected, userData} = useSelector((state) => state.auth);
  const [pageLoader, setPageLoader] = useState(true);
  const [refreshLoader, setRefreshLoader] = useState(false);
  const [addressData, setAddressData] = useState([]);
  const [shippingAddressID, setShippingAddressID] = useState(null);

  const mailListRef = useRef();
  const ToastRef = useRef();

  const userToken = checkObject(userData, 'access_token');

  const header = {
    Authorization: `Bearer ${userToken}`,
  };

  React.useEffect(
    () =>
      navigation.addListener('focus', () => {
        getAddressList();
      }),
    [],
  );

  function handleAllLoader() {
    setTimeout(() => {
      setPageLoader(false);
      setRefreshLoader(false);
    }, 100);
  }

  async function deleteAddress(data) {
    const deleteId = checkObject(data, 'id');
    const deleteData = {
      id: deleteId,
    };

    if (isConnected === true) {
      try {
        let endPoint = BaseSetting.endpoints.remove_address;
        const response = await getApiDataProgress(
          navigation,
          endPoint,
          'POST',
          deleteData,
          header,
        );
        if (
          _.isObject(response) &&
          !_.isEmpty(response) &&
          response.success === true
        ) {
          const successMessage = checkObject(response, 'message');
          getAddressList();
          ToastRef.current.show(successMessage, 2000);
        } else {
          const errorMessage = checkObject(response, 'message');
          CAlert(errorMessage, StaticHeader.Oops, () => {
            handleAllLoader();
          });
        }
      } catch (err) {
        console.log('Catch Part', err);
        CAlert(StaticAlertMsg.someThing_Went_Wrong, StaticHeader.Oops, () => {
          setShippingAddressID(null);
          handleAllLoader();
        });
      }
    } else {
      CAlert(StaticAlertMsg.Network_Problem, StaticHeader.Oops, () => {
        setShippingAddressID(null);
        handleAllLoader();
      });
    }
  }

  async function getAddressList() {
    if (isConnected === true) {
      try {
        let endPoint = BaseSetting.endpoints.address_list;
        const response = await getApiData(
          navigation,
          endPoint,
          'GET',
          null,
          header,
        );
        if (
          _.isObject(response) &&
          !_.isEmpty(response) &&
          response.success === true
        ) {
          const newListData =
            response && response.data && response.data.address
              ? response.data.address
              : [];

          if (_.isArray(newListData) && !_.isEmpty(newListData)) {
            const checkIndex = newListData.findIndex(
              (v) => _.toNumber(v.is_primary) === 1,
            );
            if (checkIndex >= 0) {
              const checkIdValue = checkObject(newListData[checkIndex], 'id');
              setShippingAddressID(checkIdValue);
            } else {
              setShippingAddressID(null);
            }
          }

          setAddressData(newListData);
          handleAllLoader();
        } else {
          const errorMessage = checkObject(response, 'message');
          CAlert(errorMessage, StaticHeader.Oops, () => {
            handleAllLoader();
          });
        }
      } catch (err) {
        console.log('Catch Part', err);
        CAlert(StaticAlertMsg.someThing_Went_Wrong, StaticHeader.Oops, () => {
          handleAllLoader();
        });
      }
    } else {
      CAlert(StaticAlertMsg.Network_Problem, StaticHeader.Oops, () => {
        handleAllLoader();
      });
    }
  }

  async function chooseShippingAddress(sID) {
    setPageLoader(true);
    const addressID = {
      id: sID,
    };
    if (isConnected === true) {
      try {
        let endPoint = BaseSetting.endpoints.choose_shipping_address;
        const response = await getApiDataProgress(
          navigation,
          endPoint,
          'POST',
          addressID,
          header,
        );
        if (
          _.isObject(response) &&
          !_.isEmpty(response) &&
          response.success === true
        ) {
          const successMessage = checkObject(response, 'message');
          ToastRef.current.show(successMessage, 2000);
          getAddressList();
        } else {
          const errorMessage = checkObject(response, 'message');
          CAlert(errorMessage, StaticHeader.Oops, () => {
            setShippingAddressID(null);
            handleAllLoader();
          });
        }
      } catch (err) {
        console.log('Catch Part', err);
        CAlert(StaticAlertMsg.someThing_Went_Wrong, StaticHeader.Oops, () => {
          setShippingAddressID(null);
          handleAllLoader();
        });
      }
    } else {
      CAlert(StaticAlertMsg.Network_Problem, StaticHeader.Oops, () => {
        setShippingAddressID(null);
        handleAllLoader();
      });
    }
  }

  function onRefresh() {
    setRefreshLoader(true);
    setPageLoader(true);
    setTimeout(() => {
      getAddressList();
    }, 100);
  }

  function renderFooterComponent() {
    return <View style={styles.listFooterView} />;
  }

  function renderEmptyComponent() {
    return (
      <EmptyView
        emptyText={
          'Sorry currently address list is empty. \n Add your first shipping Address.'
        }
        centerIconName={'add-sharp'}
        onPress={() => {
          navigation.navigate('AddAddress');
        }}
      />
    );
  }

  const renderItem = ({item, index}) => {
    const isCheck = _.isEqual(shippingAddressID, item.id);
    return (
      <CShippingAddress
        key={index}
        data={item}
        isCheck={isCheck}
        onCheck={() => {
          setShippingAddressID(item.id);
          chooseShippingAddress(item.id);
        }}
        onDelete={() => {
          CAlert(
            StaticAlertMsg.removeShippingAdd,
            StaticHeader.Confirm,
            () => {
              setPageLoader(true);
              deleteAddress(item);
            },
            () => {
              return null;
            },
            'CONFIRM',
            'CANCEL',
          );
        }}
        onUpdate={() => {
          const updateOrDeleteObj = item;
          navigation.navigate('AddAddress', updateOrDeleteObj);
        }}
      />
    );
  };

  function renderList() {
    return (
      <FlatList
        ref={mailListRef}
        data={addressData}
        scrollsToTop={false}
        onEndReachedThreshold={0.5}
        showsVerticalScrollIndicator={false}
        keyExtractor={(item) => item.id}
        renderItem={renderItem}
        contentContainerStyle={styles.FlatMainCon}
        ListFooterComponent={renderFooterComponent}
        ListEmptyComponent={renderEmptyComponent}
        ItemSeparatorComponent={() => {
          return <View style={styles.ItemSeparatorCss} />;
        }}
        refreshControl={
          <RefreshControl
            colors={[BaseColor.ThemeOrange]}
            tintColor={BaseColor.ThemeOrange}
            refreshing={refreshLoader}
            onRefresh={() => {
              onRefresh();
            }}
          />
        }
      />
    );
  }

  function BottomFixedButton() {
    return (
      <Button
        style={[styles.bottomFixedBtn, styles.otherCSS]}
        otherGredientCss={styles.bottomFixedBtn}
        otherIconSty={{
          fontSize: 35,
        }}
        displayIcon
        centerIconName={'add-sharp'}
        onPress={() => {
          navigation.navigate('AddAddress');
        }}
      />
    );
  }

  function renderLoader() {
    return <ShippingAddLoader />;
  }

  function renderHeader() {
    return (
      <GredientHeader
        leftIcon
        title={'SHIPPING ADDRESS'}
        onLeftAction={() => {
          navigation.pop();
        }}
      />
    );
  }

  return (
    <View style={styles.main}>
      {renderHeader()}
      {pageLoader ? renderLoader() : renderList()}
      {pageLoader || _.isEmpty(addressData) ? null : BottomFixedButton()}
      <Toast
        ref={ToastRef}
        position="bottom"
        positionValue={150}
        fadeInDuration={750}
        fadeOutDuration={2000}
        opacity={0.8}
      />
    </View>
  );
};

export default ShippingAddress;
