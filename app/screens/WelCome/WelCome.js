import React, {useEffect, useRef} from 'react';
import VersionNumber from 'react-native-version-number';
import NetInfo from '@react-native-community/netinfo';
import {useDispatch, useSelector} from 'react-redux';
import _ from 'lodash';
import {View, Image, StyleSheet, Animated, SafeAreaView} from 'react-native';
import {BaseColor} from '@config';
import {BaseSetting} from '../../config';
import Text from '../../components/Text';
import {Images} from '../../config/images';
import Button from '../../components/Button';
import {setStatusbar} from '../../config/statusbar';
import authActions from '../../redux/reducers/auth/actions';
import {enableAnimateInEaseOut} from '../../utils/commonFunction';

const {setNetworkStatus, setUserType} = authActions;

const styles = StyleSheet.create({
  main: {
    flex: 1,
    position: 'relative',
    backgroundColor: BaseColor.ThemeBlue,
  },
  imageCon: {
    width: BaseSetting.nWidth / 2,
    height: BaseSetting.nHeight / 2,
    alignSelf: 'center',
  },
  image: {
    width: '100%',
    height: '100%',
    resizeMode: 'contain',
  },
  txt: {
    paddingRight: BaseSetting.nWidth / 3,
    letterSpacing: 0.3,
  },
  btnContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: 15,
  },
  btnWidth: {
    width: '48%',
    // borderColor: BaseColor.whiteColor,
    // borderWidth: 1,
  },
  PV20: {
    paddingVertical: 20,
  },
  versionText: {
    position: 'absolute',
    bottom: 10,
    left: 0,
    right: 0,
  },
  bottomInfoCon: {
    flex: 1,
    paddingHorizontal: 20,
    justifyContent: 'flex-end',
    paddingBottom: 50,
  },
  W48: {
    width: '48%',
  },
});

export default function WelCome({navigation}) {
  const {isDisplayAgePermissionUI, locationsData, userType} = useSelector(
    (state) => state.auth,
  );

  const animation = useRef(new Animated.Value(0)).current;
  const slideUpValue = useRef(new Animated.Value(0)).current;

  useEffect(() => {
    Animated.parallel([
      Animated.timing(animation, {
        toValue: 1,
        duration: 800,
        useNativeDriver: true,
      }),
      Animated.timing(slideUpValue, {
        toValue: 1,
        duration: 800,
        useNativeDriver: true,
      }),
    ]).start();
  }, [slideUpValue, animation]);

  const dispatch = useDispatch();

  function checkNetWorkStatus() {
    NetInfo.addEventListener((state) => {
      dispatch(setNetworkStatus(state.isConnected));
    });
  }

  React.useEffect(
    () =>
      navigation.addListener('focus', () => {
        setStatusbar('blue');
        checkNetWorkStatus();
      }),
    [],
  );

  React.useEffect(
    () => navigation.addListener('blur', () => setStatusbar('light')),
    [],
  );

  function onUserClickAction() {
    dispatch(setUserType('user'));
    setTimeout(() => {
      if (isDisplayAgePermissionUI === true) {
        navigation.navigate('AgePermission');
      } else if (_.isEmpty(locationsData)) {
        navigation.navigate('LocationAccess');
      } else {
        navigation.navigate('Login');
      }
    }, 50);
  }

  function onDriverClickAction() {
    dispatch(setUserType('driver'));
    setTimeout(() => {
      navigation.navigate('Login');
    }, 50);
  }

  enableAnimateInEaseOut();
  return (
    <SafeAreaView style={styles.main}>
      <Animated.View
        style={[
          styles.imageCon,
          {
            transform: [
              {
                translateX: slideUpValue.interpolate({
                  inputRange: [0, 1],
                  outputRange: [-600, 0],
                }),
              },
            ],
          },
        ]}>
        <Image source={Images.logoWhiteImg} style={styles.image} />
      </Animated.View>

      <Animated.View
        style={[
          styles.bottomInfoCon,
          {
            transform: [
              {
                translateX: animation.interpolate({
                  inputRange: [0, 1],
                  outputRange: [BaseSetting.nWidth, 0],
                }),
              },
            ],
          },
        ]}>
        <Text title1 whiteColor semibold numberOfLines={3} style={styles.txt}>
          Please select your login area.
        </Text>

        <View style={styles.btnContainer}>
          <Button
            style={styles.W48}
            buttonText={'user'}
            onPress={() => {
              onUserClickAction();
            }}
          />
          <Button
            style={styles.btnWidth}
            otherGredientCss={{
              borderColor: BaseColor.whiteColor,
              borderWidth: 1,
            }}
            colorAry={[BaseColor.ThemeBlue, BaseColor.ThemeBlue]}
            buttonText={'driver'}
            onPress={() => {
              onDriverClickAction();
            }}
          />
        </View>

        <Text
          caption1
          grayColor
          medium
          textAlignCenter
          style={styles.versionText}>
          {`VERSION ${VersionNumber.appVersion}`}
        </Text>
      </Animated.View>
    </SafeAreaView>
  );
}
