import React, {useCallback, useEffect, useRef, useState} from 'react';
import {ActionSheetCustom as ActionSheet} from 'react-native-actionsheet';
import ImagePicker from 'react-native-image-crop-picker';
import IoIcon from 'react-native-vector-icons/Ionicons';
import Icon from 'react-native-vector-icons/AntDesign';
import FIcon from 'react-native-vector-icons/FontAwesome5';
import FIcons from 'react-native-vector-icons/FontAwesome';
import {useDispatch, useSelector} from 'react-redux';
import moment from 'moment';
import _ from 'lodash';
import {
  View,
  Image,
  Platform,
  Dimensions,
  StyleSheet,
  Keyboard,
  ScrollView,
  ActivityIndicator,
  TouchableOpacity,
} from 'react-native';
import {Images} from '../../config/images';
import {BaseColor} from '../../config/theme';
import Text from '../../components/Text/index';
import {BaseSetting} from '../../config/setting';
import {genderTypes} from '../../data/StaticData';
import Button from '../../components/Button/index';
import {checkObject} from '../../utils/commonFunction';
import TextInput from '../../components/TextInput/index';
import {getApiDataProgress} from '../../utils/apiHelper';
import authActions from '../../redux/reducers/auth/actions';
import GredientHeader from '../../components/DriverApp/GredientHeader';
import {StaticAlertMsg, StaticHeader} from '../../config/StaticAlertMsg';
import CAlert from '../../components/CAlert';

const nWidth = BaseSetting.nWidth;
const imgHW = nWidth / 3.5;
const IOS = Platform.OS === 'ios';
const {setUserData, setUserType} = authActions;

const styles = StyleSheet.create({
  main: {
    flex: 1,
  },
  scrollMainCon: {
    flexGrow: 1,
    paddingHorizontal: 10,
    paddingBottom: 50,
  },
  imgView: {
    width: imgHW,
    height: imgHW,
    alignSelf: 'center',
    marginTop: 30,
    position: 'relative',
    borderRadius: imgHW / 2,
  },
  img: {
    width: '100%',
    height: '100%',
    borderRadius: imgHW / 2,
  },
  imgIconView: {
    width: 30,
    height: 30,
    borderRadius: 15,
    borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    bottom: 5,
    right: 2,
    backgroundColor: BaseColor.whiteColor,
  },
  picUploadLoaderCon: {
    width: imgHW,
    height: imgHW,
    borderRadius: imgHW / 2,
    backgroundColor: 'rgba(0,0,0,0.5)',
    overflow: 'hidden',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
  MT5: {
    marginTop: 5,
  },
  PL20: {
    paddingLeft: 20,
  },
  MT10: {
    marginTop: 10,
  },
  radioMainCon: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingVertical: 10,
  },
  radioBtnView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingRight: 20,
  },
  radioIcon: {
    fontSize: 22,
    color: BaseColor.blackColor,
    paddingRight: 8,
  },
  container: {
    width: BaseSetting.nWidth,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  MR10: {
    marginRight: 10,
  },
  dateTimeOtherCon: {
    borderWidth: 1,
    borderColor: BaseColor.dimGreyColor,
    backgroundColor: BaseColor.whiteColor,
    borderBottomWidth: 1,
  },
});

const today = new Date();
const commanDateFormat = 'YYYY-MM-DD HH:mm';

const CANCEL_INDEX = 2;
const DESTRUCTIVE_INDEX = 0;
const options = [
  <View style={styles.container}>
    <FIcons
      name="picture-o"
      size={22}
      color={BaseColor.ThemeBlue}
      style={styles.MR10}
    />
    <Text
      headline
      bold
      style={{
        color: BaseColor.ThemeBlue,
      }}>
      {'Gallery'}
    </Text>
  </View>,
  <View style={styles.container}>
    <FIcon
      name="camera"
      size={22}
      color={BaseColor.ThemeBlue}
      style={styles.MR10}
    />
    <Text
      headline
      bold
      style={{
        color: BaseColor.ThemeBlue,
      }}>
      {'Camera'}
    </Text>
  </View>,
  <View style={styles.container}>
    <Text
      headline
      bold
      style={{
        color: BaseColor.ThemeRedColor,
      }}>
      {'Cancel'}
    </Text>
  </View>,
];

export default function EditProfile({navigation}) {
  const {isConnected, userData} = useSelector((state) => state.auth);
  const userImgPath = checkObject(userData, 'user_image');
  const userFirstName = checkObject(userData, 'firstname');
  const userLastName = checkObject(userData, 'lastname');
  const userEmailId = checkObject(userData, 'email');
  const userPhoneNo = checkObject(userData, 'phone');
  const userBirthDate = checkObject(userData, 'birthdate');
  const gType = checkObject(userData, 'gender');

  const gIndex = genderTypes.findIndex(
    (v) => _.toUpper(v.Type) === _.toUpper(gType),
  );
  const selectedGType = gIndex >= 0 ? genderTypes[gIndex] : genderTypes[0];

  const [userImg, setUserImg] = useState(userImgPath);
  const [firstName, setFirstName] = useState(userFirstName);
  const [lastName, setLastName] = useState(userLastName);
  const [mobileNo, setMobileNo] = useState(userPhoneNo);
  const [email, setEmail] = useState(userEmailId);

  const fDate =
    _.isEmpty(userBirthDate) || userBirthDate === '-'
      ? moment(today).format(commanDateFormat)
      : moment(userBirthDate).format(commanDateFormat);

  const [bDate, setBDate] = useState(fDate);

  const [gender, setGender] = useState(selectedGType);
  const [loader, setLoader] = useState(false);
  const [selectedData, setSelectedData] = useState(null);
  const [picLoader, setPicLoader] = useState(false);

  const dispatch = useDispatch();

  const fnmRef = useRef();
  const lnmRef = useRef();
  const emailRef = useRef();
  const mnoRef = useRef();
  const bdateRef = useRef();
  const ActionSheetRef = useRef();

  const userToken = checkObject(userData, 'access_token');

  const header = {
    Authorization: `Bearer ${userToken}`,
  };

  async function updateUserPic(imageData) {
    if (isConnected === true) {
      const imagePath = checkObject(imageData, 'path');
      const mimeType = checkObject(imageData, 'mime');
      const picData = {
        uri: imagePath,
        name: imagePath.substr(imagePath.lastIndexOf('/') + 1),
        type: mimeType,
      };

      const imgDataValue = {
        'ImageForm[photo]': picData,
      };

      try {
        let endPoint = BaseSetting.endpoints.photo;
        const response = await getApiDataProgress(
          navigation,
          endPoint,
          'POST',
          imgDataValue,
          header,
        );
        if (
          _.isObject(response) &&
          !_.isEmpty(response) &&
          response.success === true
        ) {
          const msg = checkObject(response, 'message');
          const userData = checkObject(response, 'data');
          const uType = (userData, 'user_type');
          dispatch(setUserType(uType));
          dispatch(setUserData(userData));
          CAlert(msg, StaticHeader.Success, () => {
            setPicLoader(false);
          });
        } else {
          const errorMessage = checkObject(response, 'message');
          CAlert(errorMessage, StaticHeader.Oops, () => {
            setPicLoader(false);
          });
        }
      } catch (err) {
        console.log('Catch Part', err);
        CAlert(StaticAlertMsg.someThing_Went_Wrong, StaticHeader.Oops, () => {
          setPicLoader(false);
        });
      }
    } else {
      CAlert(StaticAlertMsg.Network_Problem, StaticHeader.Oops, () => {
        setPicLoader(false);
      });
    }
  }

  async function updateUserData() {
    Keyboard.dismiss();
    setLoader(true);
    if (isConnected === true) {
      const updatedData = {
        'UserEditForm[firstname]': firstName,
        'UserEditForm[lastname]': lastName,
        'usereditform[email]': email,
        'UserEditForm[phone]': mobileNo,
        'UserEditForm[phone_code]': '+1',
        'UserEditForm[gender]': _.toLower(gender.Type),
        'UserEditForm[birth_date]': moment(bDate).format('YYYY-MM-DD'),
      };

      try {
        let endPoint = BaseSetting.endpoints.me_update;
        const response = await getApiDataProgress(
          navigation,
          endPoint,
          'POST',
          updatedData,
          header,
        );
        if (
          _.isObject(response) &&
          !_.isEmpty(response) &&
          response.success === true
        ) {
          const msg = checkObject(response, 'message');
          const userData = checkObject(response, 'data');
          const uType = (userData, 'user_type');
          dispatch(setUserType(uType));
          dispatch(setUserData(userData));
          CAlert(msg, StaticHeader.Success, () => {
            navigation.pop();
            setLoader(false);
          });
        } else {
          const errorMessage = checkObject(response, 'message');
          CAlert(errorMessage, StaticHeader.Oops, () => {
            setLoader(false);
          });
        }
      } catch (err) {
        console.log('Catch Part', err);
        CAlert(StaticAlertMsg.someThing_Went_Wrong, StaticHeader.Oops, () => {
          setLoader(false);
        });
      }
    } else {
      CAlert(StaticAlertMsg.Network_Problem, StaticHeader.Oops, () => {
        setLoader(false);
      });
    }
  }

  function validate() {
    let valid = true;
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (
      _.isEmpty(firstName) ||
      _.isEmpty(email) ||
      _.isEmpty(lastName) ||
      _.isEmpty(mobileNo) ||
      _.isEmpty(bDate)
    ) {
      valid = false;
      CAlert(StaticAlertMsg.fillAllData, StaticHeader.Alert);
      return false;
    }
    if (_.trim(firstName).length < 3) {
      valid = false;
      CAlert(StaticAlertMsg.firstName, StaticHeader.Alert);
      return false;
    }
    // if (reg.test(email) === false) {
    //   valid = false;
    //   CAlert('please enter a valid email address ')
    //   return false;

    // }
    if (_.trim(lastName.length) < 3) {
      valid = false;
      CAlert(StaticAlertMsg.lastName, StaticHeader.Alert);
      return false;
    }
    // if(_.trim(mobileNo.length) != 10){
    //   valid = false;
    //   CAlert('please enter a 10 digit mobile number ')
    //   return false;
    // }

    if (valid === true) {
      Keyboard.dismiss();
      setLoader(true);
      setTimeout(() => {
        updateUserData();
      }, 100);
    }
  }

  function showActionSheet() {
    ActionSheetRef.current.show();
  }

  function doAction(index) {
    if (index === 0) {
      OpenGallery();
    } else if (index === 1) {
      OpenCamera();
    }
  }

  function OpenCamera() {
    setTimeout(
      () => {
        ImagePicker.openCamera({
          width: 300,
          height: 400,
          cropping: true,
          mediaType: 'photo',
        })
          .then((image) => {
            setUserImg(image.path);
            setSelectedData(image);
            setPicLoader(true);
            updateUserPic(image);
          })
          .catch((error) => {
            console.log(error);
            setSelectedData(null);
          });
      },
      IOS ? 1000 : 0,
    );
  }

  function OpenGallery() {
    setTimeout(
      () => {
        ImagePicker.openPicker({
          mediaType: 'photo',
          cropping: true,
        })
          .then((image) => {
            setUserImg(image.path);
            setSelectedData(image);
            setPicLoader(true);
            updateUserPic(image);
          })
          .catch((error) => {
            console.log(error);
            setSelectedData(null);
          });
      },
      IOS ? 1000 : 0,
    );
  }

  function renderHeader() {
    return (
      <GredientHeader
        leftIcon
        title={'Account Setting'}
        onLeftAction={() => {
          navigation.pop();
        }}
      />
    );
  }

  function renderTopProfileComponent() {
    return (
      <TouchableOpacity
        activeOpacity={0.8}
        style={styles.imgView}
        onPress={
          picLoader
            ? null
            : () => {
                showActionSheet();
              }
        }>
        <Image
          source={userImg !== '-' ? {uri: userImg} : Images.userImage}
          style={styles.img}
        />
        {picLoader ? null : (
          <TouchableOpacity
            activeOpacity={0.8}
            onPress={() => {
              showActionSheet();
            }}
            style={styles.imgIconView}>
            <Icon name="edit" size={16} color={BaseColor.ThemeBlue} />
          </TouchableOpacity>
        )}
        {picLoader ? (
          <View style={styles.picUploadLoaderCon}>
            <ActivityIndicator
              color={BaseColor.whiteColor}
              size={'small'}
              animating
            />
          </View>
        ) : null}
      </TouchableOpacity>
    );
  }

  function renderInputTitle(str) {
    return (
      <Text subhead grayColor style={styles.MT10}>
        {str}
      </Text>
    );
  }

  function renderForm() {
    return (
      <ScrollView
        keyboardShouldPersistTaps="never"
        showsVerticalScrollIndicator={false}
        contentContainerStyle={styles.scrollMainCon}>
        {renderTopProfileComponent()}

        <View style={styles.MT10}>
          {/* {renderInputTitle('First Name')} */}
          <TextInput
            ref={fnmRef}
            value={firstName}
            returnKeyType="next"
            placeholder="Enter First Name"
            keyboardType={'default'}
            otherCon={styles.MT5}
            inputStyle={styles.PL20}
            onChangeText={(t) => {
              setFirstName(t);
            }}
            onSubmitEditing={() => lnmRef.current.focus()}
          />
        </View>

        <View style={styles.MT10}>
          {/* {renderInputTitle('Last Name')} */}
          <TextInput
            ref={lnmRef}
            value={lastName}
            returnKeyType="next"
            placeholder="Enter Last Name"
            keyboardType={'default'}
            otherCon={styles.MT5}
            inputStyle={styles.PL20}
            onChangeText={(t) => {
              setLastName(t);
            }}
            onSubmitEditing={() => Keyboard.dismiss()}
          />
        </View>

        <View style={styles.MT10}>
          {/* {renderInputTitle('Email')} */}
          <TextInput
            ref={emailRef}
            value={email}
            editable={false}
            returnKeyType="next"
            placeholder="Enter Email"
            keyboardType={'email-address'}
            otherCon={styles.MT5}
            inputStyle={styles.PL20}
            onChangeText={(t) => {
              setEmail(t);
            }}
            onSubmitEditing={() => Keyboard.dismiss()}
          />
        </View>

        <View style={styles.MT10}>
          {/* {renderInputTitle('Mobile Number')} */}
          <TextInput
            ref={mnoRef}
            phone
            editable={false}
            value={mobileNo}
            returnKeyType="done"
            keyboardType={'numeric'}
            otherCon={styles.MT5}
            inputStyle={styles.PL20}
            placeholder="Enter Mobile Number"
            onChangeText={(t) => {
              setMobileNo(t);
            }}
            onSubmitEditing={() => Keyboard.dismiss()}
          />
        </View>

        <View style={styles.MT10}>
          {/* {renderInputTitle('Birthday')} */}
          <TextInput
            ref={bdateRef}
            onChangeText={(t) => {
              setBDate(moment(t).format(commanDateFormat));
            }}
            inputStyle={{fontSize: 13}}
            value={bDate}
            datePicker
            mode="datetime"
            returnKeyType="go"
            format={commanDateFormat}
            placeholder="Choose date"
            placeholderColor={BaseColor.ThemeBlue}
            inputContainerStyle={styles.dateTimeOtherCon}
          />
        </View>

        <View style={styles.MT10}>
          {/* {renderInputTitle('Gender')} */}
          <View style={styles.radioMainCon}>
            {genderTypes.map((obj, index) => {
              const iconName = _.isEqual(gender, obj)
                ? 'checkmark-circle'
                : 'radio-button-off';
              return (
                <TouchableOpacity
                  key={index}
                  style={styles.radioBtnView}
                  onPress={() => {
                    setGender(obj);
                  }}>
                  <IoIcon name={iconName} style={styles.radioIcon} />
                  <Text subhead blackColor>
                    {obj.Type}
                  </Text>
                </TouchableOpacity>
              );
            })}
          </View>
        </View>

        <Button
          buttonText={'SUBMIT'}
          loading={loader}
          onPress={
            picLoader
              ? null
              : () => {
                  updateUserData();
                  // validate();
                }
          }
        />
      </ScrollView>
    );
  }

  function renderActionSheet() {
    return (
      <ActionSheet
        ref={ActionSheetRef}
        options={options}
        cancelButtonIndex={CANCEL_INDEX}
        destructiveButtonIndex={DESTRUCTIVE_INDEX}
        onPress={(index) => {
          doAction(index);
        }}
      />
    );
  }

  return (
    <View style={styles.main}>
      {renderHeader()}
      {renderForm()}
      {renderActionSheet()}
    </View>
  );
}
