import React, {useEffect, useRef, useState} from 'react';
import VersionNumber from 'react-native-version-number';
import {useDispatch, useSelector} from 'react-redux';
import _ from 'lodash';
import {
  View,
  Image,
  StyleSheet,
  Keyboard,
  ScrollView,
  TouchableOpacity,
  Platform,
  SafeAreaView,
} from 'react-native';
import Text from '../../components/Text/index';
import {Images} from '../../config/images';
import {BaseColor} from '../../config/theme';
import CAlert from '../../components/CAlert';
import Button from '../../components/Button';
import {BaseSetting} from '../../config/setting';
import TextInput from '../../components/TextInput';
import {setStatusbar} from '../../config/statusbar';
import TopBar from '../../components/TopBars/index';
import {checkObject} from '../../utils/commonFunction';
import {getApiDataProgress} from '../../utils/apiHelper';
import authActions from '../../redux/reducers/auth/actions';
import {StaticAlertMsg, StaticHeader} from '../../config/StaticAlertMsg';

const isIOS = Platform.OS === 'ios';
const {setUserData, setUserType, setCartBadgeCount} = authActions;

const styles = StyleSheet.create({
  main: {
    flex: 1,
  },
  img: {
    width: BaseSetting.nWidth / 2.5,
    height: BaseSetting.nWidth / 2.5,
    resizeMode: 'contain',
    alignSelf: 'center',
  },
  scrollMainCon: {
    flexGrow: 1,
    paddingHorizontal: 20,
  },
  loginTxt: {
    paddingVertical: 15,
  },
  forgotText: {
    paddingLeft: 20,
    paddingTop: 15,
  },
  orText: {
    color: BaseColor.ThemeBlue,
  },
  linkView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 20,
  },
  text: {
    lineHeight: 25,
  },
  signUpText: {
    paddingLeft: 5,
    lineHeight: 25,
    textDecorationLine: 'underline',
  },
  orLineView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 20,
  },
  emptyView: {
    width: '45%',
    height: StyleSheet.hairlineWidth,
    backgroundColor: BaseColor.grayColor,
  },
  orTextView: {
    width: '10%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  MT10: {
    marginTop: 10,
  },
  PV8: {
    paddingVertical: 8,
  },
});

export default function Login({navigation}) {
  const {isConnected, userType, uuid} = useSelector((state) => state.auth);
  const [loading, setLoading] = useState(false);

  const isDriver = _.toUpper(userType) === 'DRIVER';

  const staticPhoneNo = isDriver ? '7777222211' : '8855220011';
  const staticPassword = isDriver ? '123456' : '123456';
  
  // const staticPhoneNo = isDriver ? '9874561230' : '8754213690';
  // const staticPassword = isDriver ? '123456' : '123456';

  // const staticPhoneNo = isDriver ? '7894561230' : '9909155425';
  // const staticPassword = isDriver ? '123456' : '12345678';

  const [phoneNo, setPhoneNo] = useState(__DEV__ ? staticPhoneNo : '');
  const [pwd, setPwd] = useState(__DEV__ ? staticPassword : '');

  const dispatch = useDispatch();

  const phoneNoRef = useRef();
  const pwdRef = useRef();

  useEffect(() => {
    setStatusbar('light');
  }, []);

  function validateData() {
    let valid = true;

    if (_.isEmpty(phoneNo) || _.isEmpty(pwd)) {
      valid = false;
      CAlert(StaticAlertMsg.loginAlert, StaticHeader.Oops);
    }

    if (valid === true) {
      setLoading(true);
      loginAction();
    }
  }

  async function loginAction() {
    Keyboard.dismiss();
    if (isConnected === true) {
      const loginData = {
        'LoginForm[phone]': phoneNo,
        'LoginForm[password]': pwd,
        'LoginForm[uuid]': uuid,
        'LoginForm[user_type]': isDriver ? 'driver' : 'user',
        'LoginForm[platform]': isIOS ? 'Ios' : 'Android',
      };

      try {
        let endPoint = BaseSetting.endpoints.login;
        const response = await getApiDataProgress(
          navigation,
          endPoint,
          'POST',
          loginData,
        );
        if (
          _.isObject(response) &&
          !_.isEmpty(response) &&
          response.success === true
        ) {
          const userData = checkObject(response, 'data');
          if (
            userData &&
            userData.on_verification &&
            userData.on_verification === 1
          ) {
            const msg = checkObject(response, 'message');
            CAlert(msg, StaticHeader.Oops, () => {
              const verifyData = {
                phoneNo: phoneNo,
                from: 'Login',
              };
              navigation.navigate('VerifyOtp', verifyData);
              setLoading(false);
            });
          } else {
            const uType = checkObject(userData, 'user_type');
            const cartCount = checkObject(userData, 'cart_count');
            dispatch(setCartBadgeCount(_.toNumber(cartCount)))
            dispatch(setUserType(uType));
            dispatch(setUserData(userData));
            setTimeout(() => {
              setLoading(false);
              setPhoneNo('');
              setPwd('');
            }, 100);
          }
        } else {
          const errorMessage = checkObject(response, 'message');
          CAlert(errorMessage, StaticHeader.Oops, () => {
            setLoading(false);
          });
        }
      } catch (err) {
        console.log('Catch Part', err);
        CAlert(StaticAlertMsg.someThing_Went_Wrong, StaticHeader.Oops, () => {
          setLoading(false);
        });
      }
    } else {
      CAlert(StaticAlertMsg.Network_Problem, StaticHeader.Oops, () => {
        setLoading(false);
      });
    }
  }

  function renderSocialButtons() {
    return (
      <View>
        <View style={styles.orLineView}>
          <View style={styles.emptyView} />
          <View style={styles.orTextView}>
            <Text body1 medium style={styles.orText}>
              OR
            </Text>
          </View>
          <View style={styles.emptyView} />
        </View>

        <Button
          colorAry={[BaseColor.ThemeBlue, BaseColor.ThemeBlue]}
          displayLeftIcon
          iconName="facebook"
          buttonText={'LOGIN WITH FACEBOOK'}
          onPress={() => {}}
          iconSty={{color: BaseColor.whiteColor}}
        />

        <Button
          style={styles.MT10}
          displayLeftIcon
          iconName="google"
          colorAry={[BaseColor.ThemeBlue, BaseColor.ThemeBlue]}
          buttonText={'LOGIN WITH GOOGLE'}
          onPress={() => {}}
          iconSty={{color: BaseColor.whiteColor}}
        />
      </View>
    );
  }

  function renderBottomView() {
    return (
      <View>
        {isDriver ? null : (
          <View style={styles.linkView}>
            <Text subhead textAlignCenter blackColor medium style={styles.text}>
              Don't Have An Account?
            </Text>
            <TouchableOpacity
              onPress={() => {
                navigation.navigate('SignUp');
              }}>
              <Text subhead blackColor bold style={styles.signUpText}>
                SIGNUP
              </Text>
            </TouchableOpacity>
          </View>
        )}

        <Text caption1 grayColor medium textAlignCenter style={styles.PV8}>
          {isDriver
            ? `DRIVER APP ${VersionNumber.appVersion}`
            : `VERSION ${VersionNumber.appVersion}`}
        </Text>
      </View>
    );
  }

  function renderForm() {
    return (
      <ScrollView
        keyboardShouldPersistTaps="never"
        showsVerticalScrollIndicator={false}
        contentContainerStyle={styles.scrollMainCon}>
        <Image source={Images.logoBlackImg} style={styles.img} />
        <Text title2 textAlignCenter blackColor bold style={styles.loginTxt}>
          Login
        </Text>

        <TextInput
          ref={phoneNoRef}
          phone
          placeholder="Enter mobile"
          onChangeText={(t) => {
            setPhoneNo(t);
          }}
          value={phoneNo}
          onSubmitEditing={() => pwdRef.current.focus()}
          returnKeyType="next"
          keyboardType={'numeric'}
        />

        <TextInput
          ref={pwdRef}
          placeholder="Password"
          secureTextEntry
          leftIconName={'unlock-alt'}
          onChangeText={(t) => {
            setPwd(t);
          }}
          value={pwd}
          onSubmitEditing={() => {
            validateData();
          }}
          returnKeyType="done"
        />

        <Text
          subhead
          blackColor
          bold
          style={styles.forgotText}
          onPress={() => navigation.navigate('ForgotPwd')}>
          FORGOT PASSWORD?
        </Text>

        <Button
          buttonText={'LOGIN'}
          loading={loading}
          onPress={() => {
            validateData();
          }}
        />
        {/* {renderSocialButtons()} */}
      </ScrollView>
    );
  }

  function renderHeader() {
    return (
      <TopBar
        leftIcon
        title=""
        onPressLeft={() => {
          navigation.navigate('WelCome');
        }}
      />
    );
  }

  return (
    <SafeAreaView style={styles.main}>
      {renderHeader()}
      {renderForm()}
      {renderBottomView()}
    </SafeAreaView>
  );
}
