import {View, StyleSheet, ScrollView, RefreshControl} from 'react-native';
import React, {useState} from 'react';
import Accordion from 'react-native-collapsible/Accordion';
import Icon from 'react-native-vector-icons/AntDesign';
import * as Animatable from 'react-native-animatable';
import HTML from 'react-native-render-html';
import {useSelector} from 'react-redux';
import _ from 'lodash';
import CAlert from '../../components/CAlert';
import {BaseColor} from '../../config/theme';
import Text from '../../components/Text/index';
import {BaseSetting} from '../../config/setting';
import {getApiData} from '../../utils/apiHelper';
import {FontFamily} from '../../config/typography';
import {checkObject} from '../../utils/commonFunction';
import EmptyView from '../../components/DriverApp/EmptyView';
import {FAQLoader} from '../../components/ContentLoader/ContentLoader';
import GredientHeader from '../../components/DriverApp/GredientHeader';
import {StaticAlertMsg, StaticHeader} from '../../config/StaticAlertMsg';

const nWidth = BaseSetting.nWidth;

const styles = StyleSheet.create({
  main: {
    flex: 1,
  },
  scrollMainCon: {
    flexGrow: 1,
    paddingBottom: 50,
  },
  FAQHeaderSty: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 13,
    paddingHorizontal: 10,
    justifyContent: 'space-between',
  },
  FAQimage: {
    width: nWidth / 4,
    height: nWidth / 4,
  },
  sectionHeader: {
    flexDirection: 'row',
    paddingVertical: 20,
    alignItems: 'center',
    paddingHorizontal: 10,
    justifyContent: 'space-between',
    borderColor: BaseColor.grayColor,
  },
  infoCon: {
    paddingBottom: 15,
    paddingHorizontal: 10,
  },
});

export default function FAQ({navigation}) {
  const {isConnected, userData} = useSelector((state) => state.auth);
  const [activeSections, setActiveSections] = useState([]);
  const [pageLoader, setPageLoader] = useState(true);
  const [refreshLoader, setRefreshLoader] = useState(false);
  const [listData, setListData] = useState({
    data: [],
    pagination: {},
  });

  const userToken = checkObject(userData, 'access_token');
  const userType = checkObject(userData, 'user_type');

  const header = {
    Authorization: `Bearer ${userToken}`,
  };

  React.useEffect(
    () =>
      navigation.addListener('focus', () => {
        getFAQData();
      }),
    [],
  );

  function onRefresh() {
    setRefreshLoader(true);
    setPageLoader(true);
    setTimeout(() => {
      getFAQData();
    }, 500);
  }

  function handleAllLoader() {
    setTimeout(() => {
      setPageLoader(false);
      setRefreshLoader(false);
    }, 200);
  }

  async function getFAQData() {
    if (isConnected === true) {
      const uData = {
        type: userType === 'user' ? 'customer' : 'driver',
      };

      try {
        let endPoint = `${BaseSetting.endpoints.faq_list}?type=${
          userType === 'user' ? 'customer' : 'driver'
        }`;
        const response = await getApiData(
          navigation,
          endPoint,
          'GET',
          null,
          header,
        );

        if (
          _.isObject(response) &&
          !_.isEmpty(response) &&
          response.success === true
        ) {
          const obj = {};

          const newListData =
            response && response.data && response.data.rows
              ? response.data.rows
              : [];
          const paginationDatas =
            response && response.data && response.data.pagination
              ? response.data.pagination
              : {};

          if (_.isArray(newListData)) {
            obj.data = newListData;
            obj.pagination = paginationDatas;
          }
          setListData(obj);
          handleAllLoader();
        } else {
          const errorMessage = checkObject(response, 'message');
          CAlert(errorMessage, StaticHeader.Oops, () => {
            setListData(listData);
            handleAllLoader();
          });
        }
      } catch (err) {
        console.log('Catch Part', err);
        CAlert(StaticAlertMsg.someThing_Went_Wrong, StaticHeader.Oops, () => {
          handleAllLoader();
        });
      }
    } else {
      CAlert(StaticAlertMsg.Network_Problem, StaticHeader.Oops, () => {
        handleAllLoader();
      });
    }
  }

  const renderHeader = (content, index, isActive, sections) => {
    const bool = index === activeSections[0];
    const borderWidth = bool ? 0 : 0.5;
    const iconName = bool ? 'down' : 'right';
    return (
      <Animatable.View
        duration={500}
        easing="ease-out"
        animation={isActive ? 'zoomIn' : false}
        style={[styles.sectionHeader, {borderBottomWidth: borderWidth}]}>
        <Text bold body1 blackColor>
          {content.title}
        </Text>
        <Icon name={iconName} size={20} color={BaseColor.blackColor} />
      </Animatable.View>
    );
  };

  const renderContent = (content, index, isActive, sections) => {
    return (
      <Animatable.View
        duration={500}
        transition="backgroundColor"
        style={styles.infoCon}>
        <HTML
          tagsStyles={{
            p: {fontFamily: FontFamily.regular, color: BaseColor.lightBlack},
          }}
          source={{html: content.content}}
          contentWidth={nWidth}
        />
      </Animatable.View>
    );
  };

  const updateSections = (activeSections) => {
    setActiveSections(activeSections);
  };

  function renderMainHeader() {
    return (
      <GredientHeader
        leftIcon
        title={'FAQ'}
        onLeftAction={() => {
          navigation.pop();
        }}
      />
    );
  }

  function renderInfoComponent() {
    const sectionList = checkObject(listData, 'data');

    if (_.isArray(sectionList) && !_.isEmpty(sectionList)) {
      return (
        <ScrollView
          keyboardShouldPersistTaps="never"
          showsVerticalScrollIndicator={false}
          contentContainerStyle={styles.scrollMainCon}
          refreshControl={
            <RefreshControl
              colors={[BaseColor.ThemeOrange]}
              tintColor={BaseColor.ThemeOrange}
              refreshing={refreshLoader}
              onRefresh={() => {
                onRefresh();
              }}
            />
          }>
          <Accordion
            underlayColor={'transparent'}
            sections={sectionList}
            activeSections={activeSections}
            renderHeader={renderHeader}
            renderContent={renderContent}
            onChange={updateSections}
          />
        </ScrollView>
      );
    } else {
      return renderEmptyComponent();
    }
  }

  function renderLoader() {
    return <FAQLoader />;
  }

  function renderEmptyComponent() {
    return (
      <EmptyView
        emptyText={"Sorry we don't have any information yet."}
        btnLoader={pageLoader}
        onPress={() => {
          setPageLoader(true);
          setTimeout(() => {
            getFAQData();
          }, 100);
        }}
      />
    );
  }

  return (
    <View style={styles.main}>
      {renderMainHeader()}
      {pageLoader ? renderLoader() : renderInfoComponent()}
    </View>
  );
}
