import React, {useCallback, useEffect, useRef, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import _ from 'lodash';
import {
  View,
  StyleSheet,
  Keyboard,
  Alert,
  ScrollView,
} from 'react-native';
import {BaseColor} from '../../config/theme';
import {BaseSetting} from '../../config/setting';
import Button from '../../components/Button/index';
import {checkObject} from '../../utils/commonFunction';
import TextInput from '../../components/TextInput/index';
import {getApiDataProgress} from '../../utils/apiHelper';
import authActions from '../../redux/reducers/auth/actions';
import GredientHeader from '../../components/DriverApp/GredientHeader';
import {StaticAlertMsg, StaticHeader} from '../../config/StaticAlertMsg';
import CAlert from '../../components/CAlert';

const styles = StyleSheet.create({
  main: {
    flex: 1,
  },
  scrollMainCon: {
    flexGrow: 1,
    paddingHorizontal: 10,
    paddingBottom: 50,
  },
  MT15: {
    marginTop: 20,
  },
  MT30: {
    marginTop: 30,
  },
  PL20: {
    paddingLeft: 20,
  },
});

export default function ChangePassword({navigation}) {
  const {isConnected, userData} = useSelector((state) => state.auth);
  const [oldPWd, setOldPwd] = useState('');
  const [newPwd, setNewPwd] = useState('');
  const [confirmPwd, setConfirmPwd] = useState('');
  const [loader, setLoader] = useState(false);

  const oldPWdRef = useRef();
  const newPwdRef = useRef();
  const conPwdRef = useRef();

  const userToken = checkObject(userData, 'access_token');

  const header = {
    Authorization: `Bearer ${userToken}`,
  };

  function validatePassword() {
    Keyboard.dismiss();
    setLoader(true);
    changePwdAction();
    // Remove All Validation Valiidate all data from Backend (sir told me that's why)//
    // let valid = true;

    // if (_.isEmpty(oldPWd) || _.isEmpty(newPwd) || _.isEmpty(confirmPwd)) {
    //   valid = false;
    //   CAlert(StaticAlertMsg.pwdBlank, StaticHeader.Oops);
    //   return false;
    // }
    // if(!((_.trim(oldPWd.length) >=6) && (_.trim(oldPWd.length) <=8))){
    //   valid = false;
    //   CAlert(StaticAlertMsg.oldPWd, StaticHeader.Alert)
    //   return false;
    // }
    // if(!((_.trim(newPwd.length) >=6) && (_.trim(newPwd.length) <=8))){
    //   valid = false;
    //   CAlert(StaticAlertMsg.oldPWd, StaticHeader.Alert)
    //   return false;
    // }
    // if(!((_.trim(confirmPwd.length) >=6) && (_.trim(confirmPwd.length) <=8))){
    //   valid = false;
    //   CAlert(StaticAlertMsg.oldPWd, StaticHeader.Alert)
    //   return false;
    // }
    //  else if (!_.isEqual(newPwd, confirmPwd)) {
    //   valid = false;
    //   CAlert(StaticAlertMsg.conPwd, StaticHeader.Oops);
    //   return false;
    // }
    
    // if (valid === true) {
    //   setLoader(true);
    //   changePwdAction();
    // }
  }

  async function changePwdAction() {
    if (isConnected === true) {
      const passwordData = {
        'ChangePasswordForm[password]': newPwd,
        'ChangePasswordForm[confirm_password]': confirmPwd,
        'ChangePasswordForm[old_password]':oldPWd
      };

      try {
        let endPoint = BaseSetting.endpoints.change_password;
        const response = await getApiDataProgress(navigation, endPoint, 'POST', passwordData, header);
        if (_.isObject(response) && !_.isEmpty(response) && response.success === true) {
          CAlert(StaticAlertMsg.pwdChange, StaticHeader.Success, () => {
            navigation.pop();
            setLoader(false);
            setOldPwd('');
            setNewPwd('');
            setConfirmPwd('');
          });
        } else {
          const errorMessage = checkObject(response, 'message');
          CAlert(errorMessage, StaticHeader.Oops, () => {
            setLoader(false);
          });
        }
      } catch (err) {
        console.log('Catch Part', err);
        CAlert(StaticAlertMsg.someThing_Went_Wrong, StaticHeader.Oops, () => {
          setLoader(false);
        });
      }
    } else {
      CAlert(StaticAlertMsg.Network_Problem, StaticHeader.Oops, () => {
        setLoader(false);
      });
    }
  }

  function renderForm() {
    return (
      <ScrollView
        keyboardShouldPersistTaps="never"
        showsVerticalScrollIndicator={false}
        contentContainerStyle={styles.scrollMainCon}
      >
        <TextInput
          ref={oldPWdRef}
          value={oldPWd}
          secureTextEntry
          returnKeyType="next"
          placeholder="Current password"
          keyboardType={'default'}
          otherCon={styles.MT15}
          inputStyle={styles.PL20}
          onChangeText={(t) => {
            setOldPwd(t.trim());
          }}
          onSubmitEditing={() => newPwdRef.current.focus()}
        />

        <TextInput
          ref={newPwdRef}
          value={newPwd}
          secureTextEntry
          returnKeyType="next"
          placeholder="New Password"
          keyboardType={'default'}
          otherCon={styles.MT15}
          inputStyle={styles.PL20}
          onChangeText={(t) => {
            setNewPwd(t.trim());
          }}
          onSubmitEditing={() => conPwdRef.current.focus()}
        />

        <TextInput
          ref={conPwdRef}
          value={confirmPwd}
          secureTextEntry
          returnKeyType="done"
          placeholder="Repeat new password"
          keyboardType={'default'}
          otherCon={styles.MT15}
          inputStyle={styles.PL20}
          onChangeText={(t) => {
            setConfirmPwd(t.trim());
          }}
          onSubmitEditing={() => {
            validatePassword();
          }}
        />

        <Button
          buttonText={'Save Password'}
          loading={loader}
          style={styles.MT30}
          onPress={() => {
            validatePassword();
          }}
        />
      </ScrollView>
    );
  }

  function renderHeader() {
    return (
      <GredientHeader
        leftIcon
        title={'Change Password'}
        onLeftAction={() => {
          navigation.pop();
        }}
      />
    );
  }

  return (
    <View style={styles.main}>
      {renderHeader()}
      {renderForm()}
    </View>
  );
}
