import React, {useRef, useState} from 'react';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {useDispatch, useSelector} from 'react-redux';
import OTPTextView from 'react-native-otp-textinput';
import _ from 'lodash';
import {
  View,
  StyleSheet,
  Keyboard,
  ScrollView,
  TouchableOpacity,
  Platform,
  SafeAreaView,
  ActivityIndicator,
  KeyboardAvoidingView,
} from 'react-native';
import {BaseColor} from '../../config/theme';
import CAlert from '../../components/CAlert';
import Button from '../../components/Button';
import Text from '../../components/Text/index';
import {BaseSetting} from '../../config/setting';
import TopBar from '../../components/TopBars/index';
import {checkObject} from '../../utils/commonFunction';
import {getApiDataProgress} from '../../utils/apiHelper';
import authActions from '../../redux/reducers/auth/actions';
import {StaticAlertMsg, StaticHeader} from '../../config/StaticAlertMsg';

const isIOS = Platform.OS === 'ios';

const {setUserData, setUserType} = authActions;
const nWidth = BaseSetting.nWidth;

const styles = StyleSheet.create({
  main: {
    flex: 1,
  },
  scrollCon: {
    flexGrow: 1,
    paddingTop: nWidth / 6,
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  iconView: {
    width: 80,
    height: 80,
    borderRadius: 40,
    borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    marginBottom: 30,
  },
  txt: {
    letterSpacing: 0.5,
    paddingTop: 5,
  },
  emailTxt: {
    letterSpacing: 0.3,
    lineHeight: 22,
    paddingVertical: 15,
  },
  roundedTextInput: {
    borderRadius: 10,
    borderBottomWidth: 0.8,
    borderWidth: 0.8,
    width: nWidth / 9,
    height: nWidth / 9,
    padding: 0,
  },
  resendView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default function VerifyOtp({route, navigation}) {
  const verifyData = route.params;
  const {isConnected, uuid} = useSelector((state) => state.auth);

  const otpInputRef = useRef('');
  const [output, setOutput] = useState('');
  const [loader, setLoader] = useState(false);
  const [resendLoader, setResendLoader] = useState(false);

  const verifyMobileNo = checkObject(verifyData, 'phoneNo');

  const dispatch = useDispatch();

  const clearText = () => {
    otpInputRef.current.clear();
  };

  async function verifyOtpAction() {
    Keyboard.dismiss();
    setLoader(true);
    if (isConnected === true) {
      const verificationData = {
        'VerificationForm[code]': output,
        'VerificationForm[platform]': isIOS ? 'Ios' : 'Android',
        'VerificationForm[uuid]': uuid,
      };

      try {
        let endPoint = BaseSetting.endpoints.verification;
        const response = await getApiDataProgress(
          navigation,
          endPoint,
          'POST',
          verificationData,
        );
        if (
          _.isObject(response) &&
          !_.isEmpty(response) &&
          response.success === true
        ) {
          const userData = checkObject(response, 'data');
          setLoader(false);
          const uType = (userData, 'user_type');
          dispatch(setUserType(uType));
          dispatch(setUserData(userData));
          setTimeout(() => {
            // setCurrentPass('');
            // setConfirmPwd('');
          }, 100);
        } else {
          const errorMessage = checkObject(response, 'message');
          CAlert(errorMessage, StaticHeader.Oops, () => {
            setLoader(false);
          });
        }
      } catch (err) {
        console.log('Catch Part', err);
        CAlert(StaticAlertMsg.someThing_Went_Wrong, StaticHeader.Oops, () => {
          setLoader(false);
        });
      }
    } else {
      CAlert(StaticAlertMsg.Network_Problem, StaticHeader.Oops, () => {
        setLoader(false);
      });
    }
  }

  async function resendAction() {
    if (isConnected === true) {
      const resendData = {
        'ResendOTPForm[phone]': verifyMobileNo,
      };

      try {
        let endPoint = BaseSetting.endpoints.resend_otp;
        const response = await getApiDataProgress(
          navigation,
          endPoint,
          'POST',
          resendData,
        );
        if (
          _.isObject(response) &&
          !_.isEmpty(response) &&
          response.success === true
        ) {
          const successMessage = checkObject(response, 'message');
          CAlert(successMessage, StaticHeader.success, () => {
            setResendLoader(false);
          });
        } else {
          const errorMessage = checkObject(response, 'message');
          CAlert(errorMessage, StaticHeader.Oops, () => {
            setResendLoader(false);
          });
        }
      } catch (err) {
        console.log('Catch Part', err);
        CAlert(StaticAlertMsg.someThing_Went_Wrong, StaticHeader.Oops, () => {
          setResendLoader(false);
        });
      }
    } else {
      CAlert(StaticAlertMsg.Network_Problem, StaticHeader.Oops, () => {
        setResendLoader(false);
      });
    }
  }

  function renderHeader() {
    return (
      <TopBar
        leftIcon
        title="CONFIRM NUMBER"
        onPressLeft={() => {
          navigation.pop();
        }}
      />
    );
  }

  function renderForm() {
    return (
      <ScrollView
        keyboardShouldPersistTaps="never"
        showsVerticalScrollIndicator={false}
        contentContainerStyle={styles.scrollCon}>
        <KeyboardAvoidingView behavior="position">
          <View style={styles.iconView}>
            <Icon
              name="cellphone-iphone"
              size={35}
              color={BaseColor.ThemeBlue}
            />
          </View>

          <Text textAlignCenter title1 blackColor bold style={styles.txt}>
            We sent a code
          </Text>

          <Text
            textAlignCenter
            subhead
            blackColor
            medium
            style={styles.emailTxt}>
            {'Enter the Email address associated\nwith your account'}
          </Text>

          <OTPTextView
            ref={otpInputRef}
            inputCount={6}
            handleTextChange={(text) => {
              setOutput(text);
            }}
            textInputStyle={styles.roundedTextInput}
            tintColor={BaseColor.blackColor}
            offTintColor={BaseColor.blackColor}
          />

          <Button
            loading={loader}
            buttonText={'Verify'}
            onPress={
              resendLoader
                ? null
                : () => {
                    verifyOtpAction();
                  }
            }
            style={{marginBottom: 30}}
          />

          <TouchableOpacity
            activeOpacity={0.8}
            onPress={
              resendLoader
                ? null
                : () => {
                    Keyboard.dismiss();
                    clearText();
                    setResendLoader(true);
                    resendAction();
                  }
            }
            style={styles.resendView}>
            {resendLoader ? (
              <ActivityIndicator
                size="small"
                color={BaseColor.ThemeRedColor}
                animating
                style={{paddingTop: 30}}
              />
            ) : null}

            {resendLoader ? null : (
              <Icon name="refresh" size={25} color={BaseColor.blackColor} />
            )}
            {resendLoader ? null : (
              <Text body1 blackColor bold style={{paddingRight: 10}}>
                RESEND
              </Text>
            )}
          </TouchableOpacity>
        </KeyboardAvoidingView>
      </ScrollView>
    );
  }

  return (
    <SafeAreaView style={styles.main}>
      {renderHeader()}
      {renderForm()}
    </SafeAreaView>
  );
}
