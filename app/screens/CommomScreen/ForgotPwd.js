import React, {useRef, useState} from 'react';
import {useSelector} from 'react-redux';
import {
  View,
  Keyboard,
  ScrollView,
  KeyboardAvoidingView,
  SafeAreaView,
  StyleSheet,
  Platform,
  TouchableOpacity,
} from 'react-native';
import _ from 'lodash';
import Icon from 'react-native-vector-icons/Foundation';
import IconIonicons from 'react-native-vector-icons/Ionicons';
import {useTheme} from '@config';
import {BaseColor} from '../../config/theme';
import Button from '../../components/Button/index';
import TextInput from '../../components/TextInput/index';
import Text from '../../components/Text/index';
import CAlert from '../../components/CAlert';
import {BaseSetting} from '../../config/setting';
import {checkObject} from '../../utils/commonFunction';
import {getApiDataProgress} from '../../utils/apiHelper';
import TopBar from '../../components/TopBars/index';
import {StaticAlertMsg, StaticHeader} from '../../config/StaticAlertMsg';

const styles = StyleSheet.create({
  main: {
    flex: 1,
  },
  iconBack: {
    position: 'absolute',
    top: 10,
    left: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  keyIconView: {
    width: 80,
    height: 80,
    borderRadius: 40,
    borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default function ForgotPwd({navigation}) {
  const {isConnected, userData} = useSelector((state) => state.auth);

  const {colors} = useTheme();
  const text1Ref = useRef();
  const [loader, setLoader] = useState(false);
  const [phone, setphone] = useState('');

  function validate() {
    Keyboard.dismiss();
    setLoader(true);
    forgotPassAction();

    // let valid = true;

    // if (_.isEmpty(phone)) {
    //   valid = false;
    //   CAlert(StaticAlertMsg.phonelength, StaticHeader.Oops);
    //   return false;
    // }
    // if (!(_.trim(phone.length) !== 10)) {
    //   valid = false;
    //   CAlert(StaticAlertMsg.phonelength, StaticHeader.Alert);
    //   return false;
    // }
    // if (valid === true) {
    //   setLoader(true);
    //   forgotPassAction();
    // }
  }

  async function forgotPassAction() {
    if (isConnected === true) {
      const forgotPassData = {
        'SendOTPForm[phone]': phone,
      };

      try {
        let endPoint = BaseSetting.endpoints.forget_password;
        const response = await getApiDataProgress(
          navigation,
          endPoint,
          'POST',
          forgotPassData,
          null,
        );
        console.log(
          '🚀 ~ file: ForgotPwd.js ~ line 189 ~ forgotPassAction ~ response',
          response,
        );
        if (
          _.isObject(response) &&
          !_.isEmpty(response) &&
          response.success === true
        ) {
          const successMessage = checkObject(response, 'message');
          CAlert(successMessage, StaticHeader.Success, () => {
            navigation.pop();
            setLoader(false);
          });
        } else {
          const errorMessage = checkObject(response, 'message');
          CAlert(errorMessage, StaticHeader.Oops, () => {
            setLoader(false);
          });
        }
      } catch (err) {
        console.log('Catch Part', err);
        CAlert(StaticAlertMsg.someThing_Went_Wrong, StaticHeader.Oops, () => {
          setLoader(false);
        });
      }
    } else {
      CAlert(StaticAlertMsg.Network_Problem, StaticHeader.Oops, () => {
        setLoader(false);
      });
    }
  }

  return (
    <SafeAreaView style={styles.main}>
      <TopBar
        leftIcon
        title=""
        onPressLeft={() => {
          navigation.pop();
        }}
      />
      <KeyboardAvoidingView
        style={{
          flex: 1,
          backgroundColor: BaseColor.whiteColor,
        }}
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
        <ScrollView
          keyboardShouldPersistTaps="never"
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{
            flexGrow: 1,
            alignItems: 'center',
            justifyContent: 'center',
            paddingHorizontal: 30,
          }}
        >
          <View style={styles.keyIconView}>
            <Icon name="key" size={50} color={BaseColor.ThemeBlue} />
          </View>

          <Text
            title1
            blackColor
            bold
            textAlignCenter
            style={{
              paddingVertical: 20,
            }}>
            Forgot Password?
          </Text>

          <Text
            subhead
            blackColor
            medium
            textAlignCenter
            style={{
              lineHeight: 22,
            }}>
            {'Enter the Email address associated\nwith your account'}
          </Text>

          <TextInput
            phone
            ref={text1Ref}
            placeholder="Phone number"
            // leftIconName={'envelope'}
            returnKeyType="done"
            keyboardType={'numeric'}
            value={phone}
            otherCon={{
              marginTop: 35,
            }}
            onChangeText={(t) => {
              setphone(t);
            }}
            onSubmitEditing={() => Keyboard.dismiss()}
          />

          <Button
            loading={loader}
            buttonText={'RESET'}
            onPress={() => {
              // Alert.alert(
              //   'Done',
              //   'Please Check Your mail.',
              //   [{text: 'OK', onPress: () => navigation.pop()}],
              //   {cancelable: false},
              // );
              validate();
            }}
          />
        </ScrollView>
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
}
