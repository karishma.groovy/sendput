import {StyleSheet, View, ScrollView, RefreshControl} from 'react-native';
import React, {useState} from 'react';
import _ from 'lodash';
import {useSelector} from 'react-redux';
import HTML from 'react-native-render-html';
import CAlert from '../../components/CAlert';
import {BaseColor} from '../../config/theme';
import {getApiData} from '../../utils/apiHelper';
import {BaseSetting} from '../../config/setting';
import {checkObject} from '../../utils/commonFunction';
import EmptyView from '../../components/DriverApp/EmptyView';
import {CMSLoader} from '../../components/ContentLoader/ContentLoader';
import GredientHeader from '../../components/DriverApp/GredientHeader';
import {StaticAlertMsg, StaticHeader} from '../../config/StaticAlertMsg';

const contentWidth = BaseSetting.nWidth;

const styles = StyleSheet.create({
  mainCon: {
    flex: 1,
    backgroundColor: BaseColor.whiteColor,
  },
  scrollCon: {
    flexGrow: 1,
    padding: 20,
  },
  MB30: {
    marginBottom: 30,
  },
});

export default function TermsConditions({route, navigation}) {
  const screenName = route.params ? route.params : 'TERMS AND CONDITIONS';
  const {isConnected} = useSelector((state) => state.auth);
  const [refreshLoader, setRefreshLoader] = useState(false);
  const [pageLoader, setPageLoader] = useState(true);
  const [realData, setRealData] = useState('');

  React.useEffect(
    () =>
      navigation.addListener('focus', () => {
        getTnCData();
      }),
    [],
  );

  async function getTnCData() {
    setRefreshLoader(true);
    setPageLoader(true);
    if (isConnected === true) {
      const Data = {
        slug:
          screenName === 'About Us'
            ? 'about-us'
            : screenName === 'Privacy Policy'
            ? 'privacy-policy'
            : 'terms-and-condition',
      };

      try {
        let endPoint = `${BaseSetting.endpoints.get_page}?slug=
        ${
          screenName === 'About Us'
            ? 'about-us'
            : screenName === 'Privacy Policy'
            ? 'privacy-policy'
            : 'terms-and-condition'
        }`;
        const response = await getApiData(
          navigation,
          endPoint,
          'GET',
          null,
          null,
        );
        if (
          _.isObject(response) &&
          !_.isEmpty(response) &&
          response.success === true
        ) {
          const msg = checkObject(response, 'message');
          setRealData(response.data.app_body);
          setRefreshLoader(false);
          setPageLoader(false);
        } else {
          const successMessage = checkObject(response, 'message');
          CAlert(successMessage, StaticHeader.Oops, () => {
            setRefreshLoader(false);
            setPageLoader(true);
          });
        }
      } catch (err) {
        console.log('Catch Part', err);
        CAlert(StaticAlertMsg.someThing_Went_Wrong, StaticHeader.Oops, () => {
          setRefreshLoader(false);
          setPageLoader(true);
        });
      }
    } else {
      CAlert(StaticAlertMsg.Network_Problem, StaticHeader.Oops, () => {
        setRefreshLoader(false);
        setPageLoader(true);
      });
    }
  }

  function renderHeader() {
    return (
      <GredientHeader
        leftIcon
        title={screenName}
        onLeftAction={() => {
          navigation.pop();
        }}
      />
    );
  }

  function emptyCon() {
    return (
      <EmptyView
        emptyText={'Sorry, Currently data is not avilable'}
        onPress={() => {
          getTnCData();
        }}
      />
    );
  }

  function renderList() {
    return (
      <ScrollView
        bounces={false}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={styles.scrollCon}
        refreshControl={
          <RefreshControl
            colors={[BaseColor.ThemeOrange]}
            tintColor={BaseColor.ThemeOrange}
            refreshing={refreshLoader}
            onRefresh={() => {
              onRefresh();
            }}
          />
        }>
        <View style={styles.MB30}>
          <HTML source={{html: realData}} contentWidth={contentWidth} />
        </View>
      </ScrollView>
    );
  }

  function renderLoader() {
    return <CMSLoader />;
  }

  function onRefresh() {
    setRefreshLoader(true);
    setTimeout(() => {
      getTnCData();
    }, 1000);
  }

  return (
    <View style={styles.mainCon}>
      {renderHeader()}
      {pageLoader
        ? renderLoader()
        : _.isEmpty(realData) || realData === '-'
        ? emptyCon()
        : renderList()}
    </View>
  );
}
