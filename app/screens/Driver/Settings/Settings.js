import React, {useCallback, useEffect, useRef, useState} from 'react';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {useDispatch, useSelector} from 'react-redux';
import _ from 'lodash';
import {
  Switch,
  Platform,
  StyleSheet,
  ScrollView,
  View,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import CAlert from '../../../components/CAlert';
import {BaseColor} from '../../../config/theme';
import Text from '../../../components/Text/index';
import {BaseSetting} from '../../../config/setting';
import {getApiData} from '../../../utils/apiHelper';
import {checkObject} from '../../../utils/commonFunction';
import {settingScreenList} from '../../../data/StaticData';
import authActions from '../../../redux/reducers/auth/actions';
import GredientHeader from '../../../components/DriverApp/GredientHeader';
import {StaticAlertMsg, StaticHeader} from '../../../config/StaticAlertMsg';

const {clearLocalStorageData, setUserData, setLogoutLoad} = authActions;
const isIOS = Platform.OS === 'ios';

const styles = StyleSheet.create({
  mainCon: {
    flex: 1,
    backgroundColor: BaseColor.whiteColor,
  },
  scrollMainCon: {
    flexGrow: 1,
    backgroundColor: BaseColor.whiteColor,
  },
  mainListCon: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 10,
    paddingVertical: 20,
    borderBottomColor: BaseColor.lightGrey,
  },
  iconSty: {
    fontSize: 22,
    color: BaseColor.blackColor,
  },
});

export default function Settings({navigation}) {
  const {isConnected, logoutLoad, userData, uuid} = useSelector(
    (state) => state.auth,
  );
  const jobStatus = checkObject(userData, 'job_status');
  const [isJobEnabled, setIsJobEnabled] = useState(
    jobStatus === 1 ? true : false,
  );

  const dispatch = useDispatch();
  const listLength = settingScreenList.length - 1;

  const userToken = checkObject(userData, 'access_token');

  const header = {
    Authorization: `Bearer ${userToken}`,
  };

  useEffect(() => {
    changeJobStatus();
  }, [isJobEnabled]);

  async function changeJobStatus() {
    const statusValue = isJobEnabled === true ? 1 : 0;
    if (isConnected === true) {
      try {
        let endPoint = `${BaseSetting.endpoints.job_status}?status=${statusValue}`;
        const response = await getApiData(
          navigation,
          endPoint,
          'GET',
          null,
          header,
        );
        if (
          _.isObject(response) &&
          !_.isEmpty(response) &&
          response.success === true
        ) {
          const successMessage = checkObject(response, 'message');
          console.log('statusValue ===========>', statusValue);
          userData.job_status = statusValue;
          dispatch(setUserData(userData));
        } else {
          const errorMessage = checkObject(response, 'message');
          CAlert(errorMessage, StaticHeader.Oops);
        }
      } catch (err) {
        console.log('Catch Part', err);
        CAlert(StaticAlertMsg.someThing_Went_Wrong, StaticHeader.Oops);
      }
    } else {
      CAlert(StaticAlertMsg.Network_Problem, StaticHeader.Oops);
    }
  }

  const toggleSwitch = () => setIsJobEnabled((previousState) => !previousState);

  async function logoutAction() {
    if (isConnected === true) {
      const logoutData = {
        uuid: uuid,
      };

      try {
        let endPoint = `${BaseSetting.endpoints.logout}?uuid=${uuid}`;
        const response = await getApiData(
          navigation,
          endPoint,
          'GET',
          null,
          header,
        );
        if (
          _.isObject(response) &&
          !_.isEmpty(response) &&
          response.success === true
        ) {
          backToLogin();
        } else {
          const errorMessage = checkObject(response, 'message');
          CAlert(errorMessage, StaticHeader.Oops, () => {
            dispatch(setLogoutLoad(false));
          });
        }
      } catch (err) {
        console.log('Catch Part', err);
        CAlert(StaticAlertMsg.someThing_Went_Wrong, StaticHeader.Oops, () => {
          dispatch(setLogoutLoad(false));
        });
      }
    } else {
      CAlert(StaticAlertMsg.Network_Problem, StaticHeader.Oops, () => {
        dispatch(setLogoutLoad(false));
      });
    }
  }

  function backToLogin() {
    dispatch(clearLocalStorageData());
    setTimeout(() => {
      navigation.navigate('WelCome');
    }, 2000);
  }

  function routeScreen(obj) {
    const screenName = checkObject(obj, 'screen');

    if (screenName === 'Logout') {
      CAlert(
        StaticAlertMsg.logout,
        StaticHeader.Confirm,
        () => {
          dispatch(setLogoutLoad(true));
          logoutAction();
        },
        () => {
          return null;
        },
        'YES',
        'NO',
      );
    } else {
      navigation.navigate(screenName);
    }
  }

  return (
    <View style={styles.mainCon}>
      <GredientHeader title={'Setting'} />
      <ScrollView
        bounces={false}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={styles.scrollMainCon}>
        {settingScreenList.map((obj, index) => {
          const isLogoutView = listLength === index;
          const isSwitch = obj.screen === 'Switch';
          return (
            <TouchableOpacity
              key={index}
              activeOpacity={0.8}
              onPress={
                logoutLoad
                  ? null
                  : () => {
                      routeScreen(obj);
                    }
              }
              style={[
                styles.mainListCon,
                {
                  borderBottomWidth: isLogoutView
                    ? 0
                    : StyleSheet.hairlineWidth,
                },
              ]}>
              <Text body1 bold>
                {obj.title}
              </Text>
              {isLogoutView || isSwitch ? null : (
                <Icon name="keyboard-arrow-right" style={styles.iconSty} />
              )}
              {isLogoutView && logoutLoad ? (
                <ActivityIndicator
                  size={'small'}
                  color={BaseColor.ThemeOrange}
                  animating
                />
              ) : null}
              {isSwitch ? (
                <Switch
                  trackColor={{
                    false: 'rgba(192, 192, 192, 0.3)',
                    true: 'rgba(0,128,0, 0.3)',
                  }}
                  thumbColor={isJobEnabled ? '#008000' : BaseColor.lightGrey}
                  ios_backgroundColor="#3e3e3e"
                  onValueChange={toggleSwitch}
                  value={isJobEnabled}
                />
              ) : null}
            </TouchableOpacity>
          );
        })}
      </ScrollView>
    </View>
  );
}
