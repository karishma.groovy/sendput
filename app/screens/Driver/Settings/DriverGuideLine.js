import React, {useState} from 'react';
import {useSelector} from 'react-redux';
import _ from 'lodash';
import {
  View,
  StyleSheet,
  ScrollView,
  ActivityIndicator,
  RefreshControl,
} from 'react-native';
import HTML from 'react-native-render-html';
import {BaseColor} from '../../../config/theme';
import Text from '../../../components/Text/index';
import {checkObject} from '../../../utils/commonFunction';
import {BaseSetting} from '../../../config/setting';
import CAlert from '../../../components/CAlert';
import {getApiData} from '../../../utils/apiHelper';
import EmptyView from '../../../components/DriverApp/EmptyView';
import {CMSLoader} from '../../../components/ContentLoader/ContentLoader';
import GredientHeader from '../../../components/DriverApp/GredientHeader';
import {StaticAlertMsg, StaticHeader} from '../../../config/StaticAlertMsg';

const contentWidth = BaseSetting.nWidth;

const styles = StyleSheet.create({
  mainCon: {
    flex: 1,
    backgroundColor: BaseColor.whiteColor,
  },
  scrollMainCon: {
    flexGrow: 1,
    padding: 15,
    backgroundColor: BaseColor.whiteColor,
  },
  mainListCon: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 10,
    paddingVertical: 20,
    borderBottomColor: BaseColor.lightGrey,
  },
  iconSty: {
    fontSize: 22,
    color: BaseColor.blackColor,
  },
});

export default function DriverGuideLine({navigation}) {
  const {isConnected, userData} = useSelector((state) => state.auth);
  const [htmlData, setHTMLData] = useState('');
  const [pageLoader, setPageLoader] = useState(true);

  const realData = checkObject(htmlData, 'app_body');
  const userToken = checkObject(userData, 'access_token');

  const header = {
    Authorization: `Bearer ${userToken}`,
  };

  React.useEffect(
    () =>
      navigation.addListener('focus', () => {
        getData();
      }),
    [],
  );

  async function getData() {
    const slugData = {
      slug: 'driver_help',
    };

    if (isConnected === true) {
      try {
        let endPoint = `${BaseSetting.endpoints.get_page}?slug=driver_help`;
        const response = await getApiData(
          navigation,
          endPoint,
          'GET',
          null,
          header,
        );
        if (
          _.isObject(response) &&
          !_.isEmpty(response) &&
          response.success === true
        ) {
          const htmlData = checkObject(response, 'data');
          setHTMLData(htmlData);
          setPageLoader(false);
        } else {
          const errorMessage = checkObject(response, 'message');
          CAlert(errorMessage, StaticHeader.Oops, () => {
            setPageLoader(false);
          });
        }
      } catch (err) {
        console.log('Catch Part', err);
        CAlert(StaticAlertMsg.someThing_Went_Wrong, StaticHeader.Oops, () => {
          setPageLoader(false);
        });
      }
    } else {
      CAlert(StaticAlertMsg.Network_Problem, StaticHeader.Oops, () => {
        setPageLoader(false);
      });
    }
  }

  function renderMainHeader() {
    return (
      <GredientHeader
        leftIcon
        title={'Driver GuideLine'}
        onLeftAction={() => {
          navigation.pop();
        }}
      />
    );
  }

  function renderLoader() {
    return <CMSLoader />;
  }

  function renderEmptyView() {
    return (
      <EmptyView
        emptyText={'Sorry, Currently data is not avilable'}
        onPress={() => {
          getData();
        }}
      />
    );
  }

  function onRefresh() {
    setPageLoader(true);
    setTimeout(() => {
      getData();
    }, 500);
  }

  function renderHTMLContent() {
    return (
      <ScrollView
        bounces={false}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={styles.scrollMainCon}
        refreshControl={
          <RefreshControl
            colors={[BaseColor.ThemeOrange]}
            tintColor={BaseColor.ThemeOrange}
            refreshing={pageLoader}
            onRefresh={() => {
              onRefresh();
            }}
          />
        }>
        <HTML source={{html: realData}} contentWidth={contentWidth} />
      </ScrollView>
    );
  }

  return (
    <View style={styles.mainCon}>
      {renderMainHeader()}
      {pageLoader
        ? renderLoader()
        : _.isEmpty(realData) || realData === '-'
        ? renderEmptyView()
        : renderHTMLContent()}
    </View>
  );
}
