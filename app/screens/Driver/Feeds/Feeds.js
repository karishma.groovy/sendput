import React, {useCallback, useEffect, useRef, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import _ from 'lodash';
import {
  View,
  FlatList,
  StyleSheet,
  RefreshControl,
  ActivityIndicator,
} from 'react-native';
import CAlert from '../../../components/CAlert';
import {BaseColor} from '../../../config/theme';
import {BaseSetting} from '../../../config/setting';
import {checkObject} from '../../../utils/commonFunction';
import EmptyView from '../../../components/DriverApp/EmptyView';
import JobFeedCard from '../../../components/DriverApp/JobFeedCard';
import {getApiData, getApiDataProgress} from '../../../utils/apiHelper';
import GredientHeader from '../../../components/DriverApp/GredientHeader';
import {StaticAlertMsg, StaticHeader} from '../../../config/StaticAlertMsg';
import {JobFeedLoader} from '../../../components/ContentLoader/ContentLoader';

const styles = StyleSheet.create({
  mainCon: {
    flex: 1,
    backgroundColor: BaseColor.whiteColor,
  },
  FlatMainCon: {
    flexGrow: 1,
  },
  icon: {
    width: '10%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  centerView: {
    elevation: 5,
    backgroundColor: BaseColor.whiteColor,
    borderRadius: 10,
    marginTop: 15,
    marginHorizontal: 20,
    marginBottom: 10,
    shadowColor: BaseColor.blackColor,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
  },
  firstView: {
    flexDirection: 'row',
    paddingVertical: 10,
    paddingHorizontal: 20,
    justifyContent: 'space-between',
  },
  listFooterView: {
    width: '100%',
    height: 30,
  },
  loaderFooterView: {
    width: '100%',
    padding: 30,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: BaseColor.whiteColor,
  },
});

export default function Feeds({navigation}) {
  const {isConnected, NotiNumber, userData} = useSelector((state) => state.auth);
  const [listData, setListData] = useState({});
  const [pageLoader, setPageLoader] = useState(true);
  const [refreshLoader, setRefreshLoader] = useState(false);
  const [moreLoad, setMoreLoad] = useState(false);
  const [acceptLodaer, setAcceptLoader] = useState(false);
  const [rejectLoader, setRejectLoader] = useState(false);

  const mailListRef = useRef();

  const userToken = checkObject(userData, 'access_token');

  const header = {
    Authorization: `Bearer ${userToken}`,
  };

  useEffect(() => {
    onRefresh();
  }, [NotiNumber]);

  React.useEffect(
    () =>
      navigation.addListener('focus', () => {
        getList(true);
      }),
    [],
  );

  async function acceptOrRejectOrderAction(item, status) {
    const orderId = checkObject(item, 'order_id');
    if (isConnected === true) {
      const oData = {
        'CustomerOrderDriverRequestForm[status]': status,
        'CustomerOrderDriverRequestForm[order_id]': orderId,
      };

      try {
        let endPoint = BaseSetting.endpoints.job_handle;
        const response = await getApiDataProgress(
          navigation,
          endPoint,
          'POST',
          oData,
          header,
        );
        if (
          _.isObject(response) &&
          !_.isEmpty(response) &&
          response.success === true
        ) {
          getList(true);
        } else {
          const errorMessage = checkObject(response, 'message');
          CAlert(errorMessage, StaticHeader.Oops, () => {
            handleAllLoader(false);
          });
        }
      } catch (err) {
        console.log('Catch Part', err);
        CAlert(StaticAlertMsg.someThing_Went_Wrong, StaticHeader.Oops, () => {
          handleAllLoader(false);
        });
      }
    } else {
      CAlert(StaticAlertMsg.Network_Problem, StaticHeader.Oops, () => {
        handleAllLoader(false);
      });
    }
  }

  async function getList(bool) {
    const cPage =
      listData && listData.pagination && listData.pagination.currentPage
        ? _.toNumber(listData.pagination.currentPage)
        : 0;
    if (isConnected === true) {
      let PageNo = 0;
      if (bool === true) {
        PageNo = 1;
      } else {
        PageNo = cPage + 1;
      }

      const lData = {
        page: PageNo,
      };

      try {
        let endPoint = `${BaseSetting.endpoints.job_feed}?page=${PageNo}`;
        const response = await getApiData(
          navigation,
          endPoint,
          'GET',
          null,
          header,
        );
        if (
          _.isObject(response) &&
          !_.isEmpty(response) &&
          response.success === true
        ) {
          const obj = bool ? {} : _.cloneDeep(listData);

          const newListData =
            response && response.data && response.data.rows
              ? response.data.rows
              : [];
          const paginationDatas =
            response && response.data && response.data.pagination
              ? response.data.pagination
              : {};

          if (_.isArray(newListData)) {
            if (_.isArray(obj.data) && obj.data.length > 0) {
              obj.data = _.flattenDeep([...obj.data, newListData]);
            } else {
              obj.data = newListData;
            }
            obj.pagination = paginationDatas;
          }
          setListData(obj);
          handleAllLoader();
        } else {
          const errorMessage = checkObject(response, 'message');
          CAlert(errorMessage, StaticHeader.Oops, () => {
            setListData(listData);
            handleAllLoader();
          });
        }
      } catch (err) {
        console.log('Catch Part', err);
        CAlert(StaticAlertMsg.someThing_Went_Wrong, StaticHeader.Oops, () => {
          handleAllLoader();
        });
      }
    } else {
      CAlert(StaticAlertMsg.Network_Problem, StaticHeader.Oops, () => {
        handleAllLoader();
      });
    }
  }

  function handleAllLoader() {
    setTimeout(() => {
      setPageLoader(false);
      setRefreshLoader(false);
      setMoreLoad(false);
      setAcceptLoader(false);
      setRejectLoader(false);
    }, 200);
  }

  async function getMoreData() {
    const cPage =
      listData && listData.pagination && listData.pagination.currentPage
        ? _.toNumber(listData.pagination.currentPage)
        : 0;
    const tPage =
      listData && listData.pagination && listData.pagination.totalPage
        ? _.toNumber(listData.pagination.totalPage)
        : 0;

    if (listData.pagination.isMore === true && cPage < tPage) {
      setMoreLoad(true);
      getList(false);
    }
  }

  const renderItem = ({item, index}) => {
    return (
      <JobFeedCard
        data={item}
        key={index}
        onShowOrderDetail={() => {
          navigation.navigate('OrderDetails', item);
        }}
        acceptLodaer={acceptLodaer}
        onAcceptOrder={() => {
          setAcceptLoader(true);
          setTimeout(() => {
            acceptOrRejectOrderAction(item, 'accepted');
          }, 50);
        }}
        rejectLoader={rejectLoader}
        onRejectOrder={() => {
          setRejectLoader(true);
          setTimeout(() => {
            acceptOrRejectOrderAction(item, 'rejected');
          }, 50);
        }}
      />
    );
  };

  function onRefresh() {
    setRefreshLoader(true);
    setPageLoader(true);
    setTimeout(() => {
      getList(true);
    }, 500);
  }

  function renderFooterComponent() {
    if (moreLoad) {
      return (
        <View style={styles.loaderFooterView}>
          <ActivityIndicator
            size={'small'}
            animating
            color={BaseColor.ThemeOrange}
          />
        </View>
      );
    } else {
      return <View style={styles.listFooterView} />;
    }
  }

  function renderEmptyComponent() {
    return (
      <EmptyView
        emptyText={
          "Sorry we don't have any job yet.\nPlease make sure your job status on."
        }
        btnLoader={refreshLoader}
        onPress={() => {
          onRefresh();
        }}
      />
    );
  }

  function renderList() {
    return (
      <FlatList
        ref={mailListRef}
        data={listData.data}
        scrollsToTop={false}
        onEndReachedThreshold={0.5}
        showsVerticalScrollIndicator={false}
        keyExtractor={(item) => item.id}
        renderItem={renderItem}
        contentContainerStyle={styles.FlatMainCon}
        ListFooterComponent={renderFooterComponent}
        ListEmptyComponent={renderEmptyComponent}
        onEndReached={getMoreData}
        refreshControl={
          <RefreshControl
            colors={[BaseColor.ThemeOrange]}
            tintColor={BaseColor.ThemeOrange}
            refreshing={refreshLoader}
            onRefresh={() => {
              onRefresh();
            }}
          />
        }
      />
    );
  }

  function renderLoader() {
    return <JobFeedLoader />;
  }

  function renderHeader() {
    return (
      <GredientHeader
        title={'Job Feed'}
        // rightIcon={pageLoader ? false : true}
        // rightLoader={refreshLoader}
        // onRightAction={
        //   pageLoader
        //     ? null
        //     : () => {
        //         onRefresh();
        //       }
        // }
      />
    );
  }

  return (
    <View style={styles.mainCon}>
      {renderHeader()}
      {pageLoader ? renderLoader() : renderList()}
    </View>
  );
}
