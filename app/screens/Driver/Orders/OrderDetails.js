import React, {useState, useEffect} from 'react';
import LinearGradient from 'react-native-linear-gradient';
import {useSelector} from 'react-redux';
import Icon from 'react-native-vector-icons/Feather';
import _ from 'lodash';
import {View, StyleSheet, ScrollView, RefreshControl} from 'react-native';
import Text from '../../../components/Text';
import {BaseColor} from '../../../config/theme';
import CAlert from '../../../components/CAlert';
import {BaseSetting} from '../../../config/setting';
import Button from '../../../components/Button/index';
import {getApiData, getApiDataProgress} from '../../../utils/apiHelper';
import GredientHeader from '../../../components/DriverApp/GredientHeader';
import {StaticAlertMsg, StaticHeader} from '../../../config/StaticAlertMsg';
import {checkObject, getOrderStatusClr} from '../../../utils/commonFunction';
import {DriverOrderDetailLoader} from '../../../components/ContentLoader/ContentLoader';

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: BaseColor.whiteColor,
  },
  scrollCon: {
    flexGrow: 1,
    paddingBottom: 50,
  },
  commonFlexSty: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 15,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderColor: BaseColor.lightGrey,
  },
  width50: {
    width: '50%',
  },
  btnStyle: {
    width: '48%',
  },
  centerView: {
    elevation: 5,
    backgroundColor: BaseColor.whiteColor,
    borderRadius: 10,
    marginHorizontal: 15,
    marginBottom: 10,
    padding: 15,
    shadowColor: BaseColor.blackColor,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
  },
  flxjustCenter: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 15,
  },
  bottomView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 15,
  },
  btnView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  orderTrackMainCon: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 15,
    paddingTop: 15,
    paddingBottom: 5,
  },
  linearG: {
    width: 40,
    height: 40,
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  lineView: {
    flex: 1,
    width: '100%',
    height: 2,
  },
  emptyCircle: {
    width: 40,
    height: 40,
    borderRadius: 20,
    borderWidth: 2,
    borderColor: BaseColor.lightGrey,
  },
});

const orderDetails = ({route, navigation}) => {
  const item = route.params;
  const orderId = checkObject(item, 'order_id');

  const {isConnected, NotiNumber, userData} = useSelector((state) => state.auth);
  const [pageLoader, setPageLoader] = useState(true);
  const [orderData, setOrderData] = useState({});
  const [acceptLodaer, setAcceptLoader] = useState(false);
  const [rejectLoader, setRejectLoader] = useState(false);
  const [updateStatusLoader, setUpdateStatusLoader] = useState(false);
  const [refreshLoader, setRefreshLoader] = useState(false);

  const userToken = checkObject(userData, 'access_token');

  const header = {
    Authorization: `Bearer ${userToken}`,
  };

  React.useEffect(
    () =>
      navigation.addListener('focus', () => {
        getOrderDetail();
      }),
    [],
  );

  useEffect(() => {
    onRefresh();
  }, [NotiNumber]);

  function onRefresh() {
    setRefreshLoader(true);
    setPageLoader(true);
    setTimeout(() => {
      getOrderDetail();
    }, 500);
  }

  function handleAllLoader() {
    setTimeout(() => {
      setPageLoader(false);
      setAcceptLoader(false);
      setRejectLoader(false);
      setUpdateStatusLoader(false);
      setRefreshLoader(false);
    }, 200);
  }

  async function getOrderDetail() {
    const orderData = {
      order_id: orderId,
    };

    if (isConnected === true) {
      try {
        let endPoint = `${BaseSetting.endpoints.order_detail}?order_id=${orderId}`;
        const response = await getApiData(
          navigation,
          endPoint,
          'GET',
          null,
          header,
        );
        if (
          _.isObject(response) &&
          !_.isEmpty(response) &&
          response.success === true
        ) {
          const details = checkObject(response, 'data');
          setOrderData(details);
          handleAllLoader();
        } else {
          const errorMessage = checkObject(response, 'message');
          CAlert(errorMessage, StaticHeader.Oops, () => {
            handleAllLoader();
          });
        }
      } catch (err) {
        console.log('Catch Part', err);
        CAlert(StaticAlertMsg.someThing_Went_Wrong, StaticHeader.Oops, () => {
          handleAllLoader();
        });
      }
    } else {
      CAlert(StaticAlertMsg.Network_Problem, StaticHeader.Oops, () => {
        handleAllLoader();
      });
    }
  }

  async function acceptOrRejectOrderAction(status) {
    if (isConnected === true) {
      const orderData = {
        'CustomerOrderDriverRequestForm[status]': status,
        'CustomerOrderDriverRequestForm[order_id]': orderId,
      };

      try {
        let endPoint = BaseSetting.endpoints.job_handle;
        const response = await getApiDataProgress(
          navigation,
          endPoint,
          'POST',
          orderData,
          header,
        );
        if (
          _.isObject(response) &&
          !_.isEmpty(response) &&
          response.success === true
        ) {
          getOrderDetail();
        } else {
          const errorMessage = checkObject(response, 'message');
          CAlert(errorMessage, StaticHeader.Oops, () => {
            handleAllLoader(false);
          });
        }
      } catch (err) {
        console.log('Catch Part', err);
        CAlert(StaticAlertMsg.someThing_Went_Wrong, StaticHeader.Oops, () => {
          handleAllLoader(false);
        });
      }
    } else {
      CAlert(StaticAlertMsg.Network_Problem, StaticHeader.Oops, () => {
        handleAllLoader(false);
      });
    }
  }

  async function updateOrderStatus(status) {
    if (isConnected === true) {
      const orderData = {
        order_id: orderId,
        status: status,
      };

      try {
        let endPoint = `${BaseSetting.endpoints.update_order_status}?order_id=${orderId}&status=${status}`;
        const response = await getApiData(
          navigation,
          endPoint,
          'GET',
          null,
          header,
        );
        if (
          _.isObject(response) &&
          !_.isEmpty(response) &&
          response.success === true
        ) {
          getOrderDetail();
        } else {
          const errorMessage = checkObject(response, 'message');
          CAlert(errorMessage, StaticHeader.Oops, () => {
            handleAllLoader(false);
          });
        }
      } catch (err) {
        console.log('Catch Part', err);
        CAlert(StaticAlertMsg.someThing_Went_Wrong, StaticHeader.Oops, () => {
          handleAllLoader(false);
        });
      }
    } else {
      CAlert(StaticAlertMsg.Network_Problem, StaticHeader.Oops, () => {
        handleAllLoader(false);
      });
    }
  }

  const orderStatus = checkObject(orderData, 'order_status');
  const orderNo = checkObject(orderData, 'order_no');
  const orderUSDCode = checkObject(orderData, 'total_amount');
  const deliverAdd = checkObject(orderData, 'delivery_address');
  const dateTime = checkObject(orderData, 'deliver_date');
  const jobDate = checkObject(orderData, 'order_date');

  const customerName = checkObject(orderData, 'customer_name');
  const customerPhone = checkObject(orderData, 'customer_phone');
  const productList = checkObject(orderData, 'product');
  const totalProduct = productList.length;

  const orderAmount = checkObject(orderData, 'order_amount');
  const orderDiscount = checkObject(orderData, 'discount');
  const totalAmount = checkObject(orderData, 'total_amount');
  const paymentMode = checkObject(orderData, 'payment_mode');

  const isOrderPending = orderStatus === 'pending';
  const isOrderAssigned = orderStatus === 'assigned';
  const isOrderOnGoing = orderStatus === 'ongoing';

  const isOrderPicked = orderStatus === 'picked';
  const isOrderDelivered = orderStatus === 'delivered';
  const isOrderCancelled = orderStatus === 'cancelled';

  const isHideTrack = isOrderPending || isOrderCancelled || isOrderAssigned;
  const isDisplayAcceptedView =
    isOrderOnGoing || isOrderPicked || isOrderDelivered;
  const isDisplayPickedView = isOrderPicked || isOrderDelivered;
  const isDisplayDeliverView = isOrderDelivered;

  function renderGTrackView() {
    return (
      <LinearGradient
        start={{x: 0.3, y: 0.25}}
        end={{x: 0.8, y: 1.0}}
        colors={[BaseColor.ThemeOrange, BaseColor.ThemeYellow]}
        style={styles.linearG}>
        <Icon name="check-circle" size={25} color={BaseColor.whiteColor} />
      </LinearGradient>
    );
  }

  function renderEmptyCirle() {
    return <View style={styles.emptyCircle} />;
  }

  function renderLoader() {
    return <DriverOrderDetailLoader />;
  }

  function renderHeader() {
    return (
      <GredientHeader
        leftIcon
        title={pageLoader ? '-' : `ORDER #${orderNo}`}
        onLeftAction={() => {
          navigation.pop();
        }}
      />
    );
  }

  function renderOrderDetails() {
    return (
      <ScrollView
        bounces={false}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={styles.scrollCon}
        refreshControl={
          <RefreshControl
            colors={[BaseColor.ThemeOrange]}
            tintColor={BaseColor.ThemeOrange}
            refreshing={refreshLoader}
            onRefresh={() => {
              onRefresh();
            }}
          />
        }>
        {isHideTrack ? null : (
          <View style={styles.orderTrackMainCon}>
            {isDisplayAcceptedView ? renderGTrackView() : renderEmptyCirle()}
            <View
              style={[
                styles.lineView,
                {
                  backgroundColor: isDisplayAcceptedView
                    ? BaseColor.ThemeOrange
                    : BaseColor.lightGrey,
                },
              ]}
            />
            {isDisplayPickedView ? renderGTrackView() : renderEmptyCirle()}
            <View
              style={[
                styles.lineView,
                {
                  backgroundColor: isDisplayDeliverView
                    ? BaseColor.ThemeOrange
                    : BaseColor.lightGrey,
                },
              ]}
            />
            {isDisplayDeliverView ? renderGTrackView() : renderEmptyCirle()}
          </View>
        )}

        {isHideTrack ? null : (
          <View style={styles.flxjustCenter}>
            <Text caption1 grayColor textAlignLeft>
              Accepted
              {/* {isDisplayAcceptedView ? `Accepted\n10 Aug 2021\n06:00AM` : 'Accept'} */}
            </Text>
            <Text caption1 grayColor textAlignLeft style={{marginLeft: -10}}>
              Picked up
              {/* {isDisplayPickedView ? `Picked up\n10 Aug 2021\n06:00AM` : 'Pick up'} */}
            </Text>
            <Text caption1 grayColor textAlignRight>
              Delivered
              {/* {isDisplayDeliverView ? `Delivered\n10 Aug 2021\n06:00AM` : 'Deliver'} */}
            </Text>
          </View>
        )}

        <View style={styles.commonFlexSty}>
          <View style={styles.width50}>
            <Text numberOfLines={1} grayColor semibold body2 textAlignLeft>
              Order Amount
            </Text>
            <Text
              numberOfLines={1}
              blackColor
              bold
              body2
              textAlignLeft>{`${orderUSDCode}`}</Text>
          </View>

          <View style={styles.width50}>
            <Text numberOfLines={1} grayColor semibold body2 textAlignRight>
              Order Status
            </Text>
            <Text
              numberOfLines={1}
              blackColor
              bold
              body2
              textAlignRight
              style={{color: getOrderStatusClr(orderStatus)}}>
              {_.toUpper(orderStatus)}
            </Text>
          </View>
        </View>

        <View
          style={{
            padding: 15,
            borderBottomWidth: StyleSheet.hairlineWidth,
            borderColor: BaseColor.lightGrey,
          }}>
          <Text numberOfLines={1} grayColor semibold body2>
            Delivery Address
          </Text>
          <Text numberOfLines={1} blackColor bold body2>
            {deliverAdd}
          </Text>
        </View>

        <View style={styles.commonFlexSty}>
          <View style={styles.width50}>
            <Text numberOfLines={1} grayColor semibold body2 textAlignLeft>
              Delivery Date & Time
            </Text>
            <Text numberOfLines={1} blackColor bold body2 textAlignLeft>
              {dateTime}
            </Text>
          </View>

          <View style={styles.width50}>
            <Text numberOfLines={1} grayColor semibold body2 textAlignRight>
              Order Date
            </Text>
            <Text numberOfLines={1} blackColor bold body2 textAlignRight>
              {jobDate}
            </Text>
          </View>
        </View>

        <View style={styles.commonFlexSty}>
          <View style={styles.width50}>
            <Text numberOfLines={1} grayColor semibold body2 textAlignLeft>
              Customer Name
            </Text>
            <Text numberOfLines={1} blackColor bold body2 textAlignLeft>
              {customerName}
            </Text>
          </View>
          <View style={styles.width50}>
            <Text numberOfLines={1} grayColor semibold body2 textAlignRight>
              Contact No
            </Text>
            <Text numberOfLines={1} blackColor bold body2 textAlignRight>
              {customerPhone}
            </Text>
          </View>
        </View>

        <View style={[styles.commonFlexSty, {borderBottomWidth: 0}]}>
          <Text numberOfLines={1} grayColor semibold body2 textAlignLeft>
            Ordered items
          </Text>
          <Text numberOfLines={1} grayColor bold body2 textAlignRight>
            {totalProduct}
          </Text>
        </View>

        {_.isArray(productList) && !_.isEmpty(productList)
          ? productList.map((obj, index) => {
              const productName = checkObject(obj, 'product_name');
              const productPrice = checkObject(obj, 'unit_price');
              const productUnit = checkObject(obj, 'unit');
              return (
                <View key={index} style={styles.centerView}>
                  <Text numberOfLines={1} blackColor bold body2>
                    {productName}
                  </Text>
                  <View style={styles.flxjustCenter}>
                    <View style={styles.width50}>
                      <Text
                        numberOfLines={1}
                        semibold
                        caption
                        grayColor
                        textAlignLeft>
                        Units
                      </Text>
                      <Text numberOfLines={1} bold caption textAlignLeft>
                        {productUnit}
                      </Text>
                    </View>
                    <View style={styles.width50}>
                      <Text
                        numberOfLines={1}
                        semibold
                        caption
                        grayColor
                        textAlignRight>
                        Unit Price
                      </Text>
                      <Text numberOfLines={1} bold caption textAlignRight>
                        {productPrice}
                      </Text>
                    </View>
                  </View>
                </View>
              );
            })
          : null}

        <View style={styles.bottomView}>
          <View>
            <Text numberOfLines={1} grayColor semibold body2 textAlignLeft>
              Total Amount
            </Text>
            <Text
              numberOfLines={1}
              blackColor
              bold
              body2
              textAlignLeft>{`${orderAmount}`}</Text>
          </View>
          <View>
            <Text numberOfLines={1} grayColor semibold body2 textAlignCenter>
              Discount
            </Text>
            <Text
              numberOfLines={1}
              blackColor
              bold
              body2
              textAlignCenter>{`${orderDiscount}`}</Text>
          </View>
          <View>
            <Text numberOfLines={1} grayColor semibold body2 textAlignRight>
              Net Amount
            </Text>
            <Text
              numberOfLines={1}
              blackColor
              bold
              body2
              textAlignRight>{`${totalAmount}`}</Text>
          </View>
        </View>

        <View style={{paddingHorizontal: 15}}>
          <Text numberOfLines={1} grayColor semibold body2>
            Payment mode
          </Text>
          <Text numberOfLines={1} blackColor bold body2>
            {_.toUpper(paymentMode)}
          </Text>
        </View>

        <View style={{paddingHorizontal: 15}}>
          {isOrderAssigned ? (
            <View style={styles.btnView}>
              <Button
                style={styles.btnStyle}
                loading={acceptLodaer}
                colorAry={[BaseColor.ThemeOrange, BaseColor.ThemeYellow]}
                buttonText={'ACCEPT'}
                onPress={() => {
                  acceptOrRejectOrderAction('accepted');
                }}
              />
              <Button
                style={styles.btnStyle}
                loading={rejectLoader}
                colorAry={[BaseColor.ThemeBlue, BaseColor.ThemeBlue]}
                buttonText={'REJECT'}
                onPress={() => {
                  acceptOrRejectOrderAction('rejected');
                }}
              />
            </View>
          ) : null}

          {isOrderOnGoing ? (
            <Button
              loading={updateStatusLoader}
              colorAry={[BaseColor.ThemeOrange, BaseColor.ThemeYellow]}
              buttonText={'PICKED UP'}
              onPress={() => {
                setUpdateStatusLoader(true);
                updateOrderStatus('picked');
              }}
            />
          ) : null}

          {isOrderPicked ? (
            <Button
              loading={updateStatusLoader}
              colorAry={[BaseColor.ThemeOrange, BaseColor.ThemeYellow]}
              buttonText={'DELIVERED'}
              onPress={() => {
                setUpdateStatusLoader(true);
                updateOrderStatus('delivered');
              }}
            />
          ) : null}
        </View>
      </ScrollView>
    );
  }

  return (
    <View style={styles.main}>
      {renderHeader()}
      {pageLoader ? renderLoader() : renderOrderDetails()}
    </View>
  );
};

export default orderDetails;
