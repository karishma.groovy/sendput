import {SafeAreaProvider} from 'react-native-safe-area-context';
import {PersistGate} from 'redux-persist/integration/react';
import Orientation from 'react-native-orientation-locker';
import CodePush from 'react-native-code-push';
import React, {Component} from 'react';
import {Provider} from 'react-redux';
import {LogBox, Platform} from 'react-native';
import App from './navigation';
import Toast from './components/Toast/index';
import CTopNotify from './components/CTopNotify/index';
import {store, persistor} from './redux/store/configureStore';
import {InAppNotificationProvider} from './libs/react-native-in-app-notification';

const IOS = Platform.OS === 'ios';
LogBox.ignoreAllLogs();

const codePushOptions = {
  installMode: CodePush.InstallMode.IMMEDIATE,
  checkFrequency: CodePush.CheckFrequency.ON_APP_START,

  updateDialog: {
    appendReleaseDescription: true,
    descriptionPrefix: "\n\nWhat's New:",
    mandatoryContinueButtonLabel: 'Install',
  },
};
class index extends Component {
  constructor(props) {
    super(props);

    this.state = {
      processing: false,
      syncMessage: '',
    };
  }

  componentDidMount() {
    Orientation.lockToPortrait();
  }

  codePushStatusDidChange(syncStatus) {
    this.codepushStatus = syncStatus;

    switch (syncStatus) {
      case CodePush.SyncStatus.CHECKING_FOR_UPDATE:
        this.setState({syncMessage: 'Checking for update.'});
        break;
      case CodePush.SyncStatus.DOWNLOADING_PACKAGE:
        this.setState(
          {
            processing: true,
            syncMessage: 'Downloading package.',
          },
          () => {
            this.showToast('New app update is available and being downloaded.');
          },
        );
        break;
      case CodePush.SyncStatus.INSTALLING_UPDATE:
        this.setState({syncMessage: 'Installing update.'}, () => {
          this.showToast('New app update is available and being installed.');
        });
        break;
      case CodePush.SyncStatus.UP_TO_DATE:
        this.setState({
          syncMessage: 'App up to date.',
          processing: false,
        });
        break;
      case CodePush.SyncStatus.UPDATE_INSTALLED:
        this.setState({
          syncMessage: 'Update installed and will be applied on restart.',
          processing: false,
        });
        break;
        break;
    }
  }

  showToast = (message) => {
    if (this.refs.notifyToast) {
      this.refs.notifyToast.show(message, 2000);
    }
  };

  render() {
    const {processing} = this.state;
    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <InAppNotificationProvider>
            <SafeAreaProvider>
              <App />
            </SafeAreaProvider>
          </InAppNotificationProvider>
        </PersistGate>

        {processing && <CTopNotify title="Installing Updates..." />}
        <Toast
          ref="notifyToast"
          position="bottom"
          positionValue={150}
          fadeInDuration={750}
          fadeOutDuration={2000}
          opacity={0.8}
        />
      </Provider>
    );
  }
}

let indexExport = index;
if (!__DEV__) {
  indexExport = CodePush(codePushOptions)(index);
}

export default indexExport;
