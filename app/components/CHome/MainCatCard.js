import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import {
  View,
  Image,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import Text from '../../components/Text/index';
import {checkObject} from '../../utils/commonFunction';
import {BaseSetting} from '../../config';

const styles = StyleSheet.create({
  mainCon: {
    width: BaseSetting.nWidth / 3,
    height: BaseSetting.nWidth / 3,
    paddingHorizontal: 7,
  },
  imgSty: {
    width: '100%',
    height: '80%',
    borderRadius: 3,
    resizeMode: 'cover',
  },
  catTitleView: {
    height: '20%',
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
});

export default function MainCatCard(props) {
  const {data, onPress} = props;

  const imgUrl = checkObject(data, 'Images');
  const title = checkObject(data, 'text');

  return (
    <TouchableOpacity
      activeOpacity={0.5}
      style={styles.mainCon}
      onPress={() => {
        onPress();
      }}>
      <Image style={styles.imgSty} source={{uri: imgUrl}} />
      <View style={styles.catTitleView}>
        <Text caption1>{title}</Text>
      </View>
    </TouchableOpacity>
  );
}

MainCatCard.propTypes = {
  data: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  onPress: PropTypes.func,
};

MainCatCard.defaultProps = {
  data: {},
  onPress: () => {},
};
