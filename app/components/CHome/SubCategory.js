import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import {Image, StyleSheet, TouchableOpacity} from 'react-native';
import Text from '../../components/Text/index';
import {checkObject} from '../../utils/commonFunction';
import {BaseSetting} from '../../config';

const styles = StyleSheet.create({
  mainCon: {
    borderWidth: StyleSheet.hairlineWidth,
    borderRadius: 5,
    padding: 12,
    marginHorizontal: 7,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
    borderColor: '#CCC',
    width: BaseSetting.nWidth / 3,
    height: BaseSetting.nWidth / 2.5,
  },
  imgSty: {
    width: '100%',
    height: '80%',
    borderRadius: 3,
    resizeMode: 'cover',
  },
  PT10: {
    paddingTop: 10,
  },
});

export default function SubCategory(props) {
  const {data, onPress} = props;

  const imgUrl = checkObject(data, 'category_photo');
  const title = checkObject(data, 'category_name');

  return (
    <TouchableOpacity
      activeOpacity={0.8}
      style={styles.mainCon}
      onPress={() => {
        onPress();
      }}>
      <Image style={styles.imgSty} source={{uri: imgUrl}} />
      <Text numberOfLines={1} style={styles.PT10}>
        {title}
      </Text>
    </TouchableOpacity>
  );
}

SubCategory.propTypes = {
  data: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  onPress: PropTypes.func,
};

SubCategory.defaultProps = {
  data: {},
  onPress: () => {},
};
