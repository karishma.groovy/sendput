import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import {View, Image, StyleSheet, TouchableOpacity} from 'react-native';
import {BaseColor} from '../../config/theme';
import Text from '../../components/Text/index';
import {Images} from '../../config/images';
import {checkObject} from '../../utils/commonFunction';
import {BaseSetting} from '../../config';

const styles = StyleSheet.create({
  mainCon: {
    flex: 1,
    elevation: 3,
    marginHorizontal: 10,
    marginTop: 10,
    paddingBottom: 15,
    position: 'relative',
    borderRadius: 5,
    backgroundColor: BaseColor.whiteColor,
    width: BaseSetting.nWidth / 2,
    shadowColor: BaseColor.blackColor,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
  },
  dispensariesImage: {
    width: '100%',
    height: 100,
    resizeMode: 'cover',
  },
  DispenLogoImageCon: {
    width: 50,
    height: 50,
    padding: 2,
    borderRadius: 5,
    position: 'absolute',
    left: 15,
    top: 75,
    overflow: 'hidden',
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: BaseColor.grayColor,
    backgroundColor: BaseColor.whiteColor,
  },
  DispenLogoImage: {
    width: '100%',
    height: '100%',
    resizeMode: 'contain',
    borderRadius: 5,
  },
  bottomInfoView: {
    paddingHorizontal: 15,
    paddingTop: 35,
    paddingBottom: 10,
  },
  PB5: {
    paddingBottom: 5,
  },
});

export default function DispensariesCard(props) {
  const {data, otherCss, onPress} = props;

  const mapImgUrl = checkObject(data, 'store_image');
  const storeImg = checkObject(data, 'store_image');
  const storeName = checkObject(data, 'store_name');
  const storeAddress = checkObject(data, 'store_address');
  const kmValue = checkObject(data, 'distance');

  return (
    <TouchableOpacity
      activeOpacity={0.8}
      style={[styles.mainCon, otherCss]}
      onPress={() => {
        onPress();
      }}>
      <Image source={Images.staticMap} style={styles.dispensariesImage} />
      <View style={styles.DispenLogoImageCon}>
        <Image source={{uri: storeImg}} style={styles.DispenLogoImage} />
      </View>
      

      <View style={styles.bottomInfoView}>
        <Text numberOfLines={1} overline bold style={styles.PB5}>
          {storeName}
        </Text>
        <Text numberOfLines={2} overline semibold>
          {storeAddress}
        </Text>
        <Text
          numberOfLines={1}
          overline
          bold
          style={{paddingTop: 2, color: BaseColor.ThemeOrange}}>
          {kmValue}
        </Text>
      </View>
    </TouchableOpacity>
  );
}

DispensariesCard.propTypes = {
  data: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  onPress: PropTypes.func,
};

DispensariesCard.defaultProps = {
  data: {},
  onPress: () => {},
};
