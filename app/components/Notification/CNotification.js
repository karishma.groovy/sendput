import {View, Image, StyleSheet, TouchableOpacity} from 'react-native';
import IoIcon from 'react-native-vector-icons/Ionicons';
import Swipeable from 'react-native-swipeable';
import React, {useState} from 'react';
import PropTypes from 'prop-types';
import Moment from 'moment';
import _ from 'lodash';
import Text from '../Text';
import {BaseSetting} from '../../config';
import {BaseColor} from '../../config/theme';
import {checkObject} from '../../utils/commonFunction';

const nWidth = BaseSetting.nWidth;

const styles = StyleSheet.create({
  mainCon: {
    flex: 1,
    padding: 10,
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'space-between',
    borderBottomColor: BaseColor.grayColor,
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
  notificationImg: {
    width: nWidth / 6,
    height: nWidth / 6,
    borderRadius: 5,
    resizeMode: 'cover',
  },
  infoCon: {
    flex: 1,
    paddingLeft: 10,
    justifyContent: 'flex-start',
  },
  swipeCon: {
    width: '20%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: BaseColor.ThemeRedColor,
  },
  selfCon: {
    alignSelf: 'center',
  },
});

export default function CNotification(props) {
  const [currentlyOpenSwipeable, setCurrentlyOpenSwipeable] = useState(null);
  const {data, onPress} = props;
  const id = checkObject(data, 'id');
  const title = checkObject(data, 'title');
  const message = checkObject(data, 'message');
  const type = checkObject(data, 'type');
  const userId = checkObject(data, 'user_id');
  const imgURL = checkObject(data, 'image');
  const createdAt = checkObject(data, 'created_at');
  const orderDateValue = Moment.unix(createdAt).format('MMMM DD YYYY');

  function onOpen(event, gestureState, swipeable) {
    if (currentlyOpenSwipeable && currentlyOpenSwipeable !== swipeable) {
      currentlyOpenSwipeable.recenter();
    }
    setCurrentlyOpenSwipeable(swipeable);
  }

  function onClose() {
    setCurrentlyOpenSwipeable(null);
  }

  return (
    <Swipeable
      rightButtons={[
        <TouchableOpacity
          activeOpacity={0.8}
          onPress={() => {
            onPress();
          }}
          style={styles.swipeCon}>
          <IoIcon
            style={styles.selfCon}
            name="trash-sharp"
            size={20}
            color={BaseColor.whiteColor}
          />
        </TouchableOpacity>,
      ]}
      onRightButtonsOpenRelease={onOpen}
      onRightButtonsCloseRelease={onClose}>
      <View style={styles.mainCon}>
        <Image source={{uri: imgURL}} style={styles.notificationImg} />
        <View style={styles.infoCon}>
          <Text bold body2>
            {title}
          </Text>
          <Text body2>{message}</Text>
          <Text grayColor caption1>
            {orderDateValue}
          </Text>
        </View>
      </View>
    </Swipeable>
  );
}

CNotification.propTypes = {
  data: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  onPress: PropTypes.func,
};

CNotification.defaultProps = {
  data: {},
  onPress: () => {},
};
