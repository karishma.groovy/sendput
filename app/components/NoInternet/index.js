import NetInfo from '@react-native-community/netinfo';
import LottieView from 'lottie-react-native';
import {bindActionCreators} from 'redux';
import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import _ from 'lodash';
import {View, Modal, StyleSheet} from 'react-native';
import Text from '../Text/index';
import {BaseColor} from '../../config/theme';
import authActions from '../../redux/reducers/auth/actions';
import {BaseSetting} from '../../config';

const nWidth = BaseSetting.nWidth;

const styles = StyleSheet.create({
  mainCon: {
    flex: 1,
    padding: 30,
    backgroundColor: 'rgba(0,0,0,0.6)',
  },
  animationWrap: {
    flex: 1,
    overflow: 'hidden',
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: BaseColor.whiteColor,
  },
  animation: {
    height: nWidth,
    width: nWidth,
  },
  MT10: {
    marginVertical: 10,
  },
});

class NoInternet extends Component {
  constructor(props) {
    super(props);
    this.state = {
      offlineModalOpen: false,
    };
  }

  componentDidMount() {
    const {
      authActions: {setNetworkStatus},
    } = this.props;
    NetInfo.addEventListener((state) => {
      setNetworkStatus(state.isConnected);
    });
  }

  componentWillReceiveProps(nextProps) {
    const {auth} = this.props;
    if (!_.isEqual(auth.isConnected, nextProps.auth.isConnected)) {
      this.handleConnectionChange(nextProps.auth.isConnected);
    }
  }

  handleConnectionChange = (isConnected) => {
    if (!isConnected) {
      this.setState({offlineModalOpen: true});
    } else if (isConnected) {
      this.setState({offlineModalOpen: false});
    }
  };

  render() {
    const {offlineModalOpen} = this.state;
    return (
      <Modal animationType="fade" transparent visible={offlineModalOpen}>
        <View style={styles.mainCon}>
          <View style={styles.animationWrap}>
            <Text title3 medium style={styles.MT10}>
              No internet connection
            </Text>
            <LottieView
              ref={(animation) => {
                this.animation1 = animation;
              }}
              autoSize={false}
              style={styles.animation}
              source={require('../../assets/lottie/noInternet.json')}
              autoPlay={true}
              loop={true}
            />
            <Text textAlignCenter caption1 grayTextColor style={styles.MT10}>
              {
                'Please check your internet connection \n again, or connect to WI-Fi'
              }
            </Text>
          </View>
        </View>
      </Modal>
    );
  }
}

NoInternet.defaultProps = {
  auth: {},
};

NoInternet.propTypes = {
  auth: PropTypes.objectOf(PropTypes.any),
};

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    authActions: bindActionCreators(authActions, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(NoInternet);
