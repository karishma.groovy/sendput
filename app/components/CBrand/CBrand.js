import React, {useCallback, useEffect, useRef, useState} from 'react';
import PropTypes from 'prop-types';
import Moment from 'moment';
import _ from 'lodash';
import {View, Image, StyleSheet, TouchableOpacity} from 'react-native';
import {BaseColor} from '../../config/theme';
import Text from '../../components/Text/index';
import {checkObject} from '../../utils/commonFunction';
import {BaseSetting} from '../../config';

const nWidth = BaseSetting.nWidth;

const styles = StyleSheet.create({
  mainCon: {
    flex: 1,
    padding: 10,
    elevation: 3,
    marginTop: 10,
    borderRadius: 5,
    width: nWidth / 2,
    marginHorizontal: 10,
    shadowColor: BaseColor.blackColor,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    backgroundColor: BaseColor.whiteColor,
  },
  brandImgSty: {
    width: '100%',
    height: nWidth / 2,
    resizeMode: 'cover',
    borderRadius: 5,
    marginBottom: 5,
  },
  cmnFlex: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingTop: 3,
  },
});

export default function CBrand(props) {
  const {data, onPress} = props;

  const brandStatus = checkObject(data, 'status');
  const brandName = checkObject(data, 'brand_name');
  const createdAt = checkObject(data, 'created_at');
  const brandPhoto = checkObject(data, 'brand_photo');
  const dateValue = Moment.unix(createdAt).format('DD MMM YYYY');

  if (brandStatus === 'Unpublished') {
    return null;
  }
  return (
    <TouchableOpacity
      activeOpacity={0.8}
      style={[styles.mainCon]}
      onPress={() => {
        onPress();
      }}>
      <Image source={{uri: brandPhoto}} style={styles.brandImgSty} />
      <Text body1 bold>
        {brandName}
      </Text>
      <View style={styles.cmnFlex}>
        <Text caption1 semibold>
          {brandStatus}
        </Text>
        <Text caption2 semibold>
          {dateValue}
        </Text>
      </View>
    </TouchableOpacity>
  );
}

CBrand.propTypes = {
  data: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  onPress: PropTypes.func,
};

CBrand.defaultProps = {
  data: {},
  onPress: () => {},
};
