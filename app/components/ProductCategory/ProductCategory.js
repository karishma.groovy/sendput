import React from 'react';
import Icon from 'react-native-vector-icons/Ionicons';
import PropTypes from 'prop-types';
import _ from 'lodash';
import {View, Image, StyleSheet, TouchableOpacity} from 'react-native';
import Text from '../Text/index';
import {BaseSetting} from '../../config';
import {BaseColor} from '../../config/theme';
import {checkObject} from '../../utils/commonFunction';

const nWidth = BaseSetting.nWidth;

const styles = StyleSheet.create({
  mainCon: {
    flex: 1,
    padding: 10,
    width: nWidth / 2,
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: BaseColor.grayColor,
  },
  imgSty: {
    width: '100%',
    height: nWidth / 2,
    resizeMode: 'contain',
  },
  cmnFlex: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  rateView: {
    flexDirection: 'row',
    paddingVertical: 5,
  },
});

export default function ProductCategory(props) {
  const starData = [1, 2, 3, 4, 5];
  const {data, onPress} = props;

  const imgUrl = checkObject(data, 'product_image');
  const title = checkObject(data, 'product_name');
  const price = checkObject(data, 'price');
  const starRate = checkObject(data, 'average_star');
  const brandName = checkObject(data, 'brand_name');

  return (
    <TouchableOpacity
      activeOpacity={0.8}
      onPress={() => {
        onPress();
      }}
      style={styles.mainCon}>
      <Image source={{uri: imgUrl}} style={styles.imgSty} />

      <Text bold body1 style={{paddingVertical: 5}}>
        {title}
      </Text>

      <View style={styles.cmnFlex}>
        <Icon name="checkmark-circle" size={20} color={BaseColor.blueColor} />
        <Text caption1>{brandName}</Text>
      </View>

      <View style={styles.rateView}>
        {starData.map((obj) => {
          const isYellow = obj <= _.toNumber(starRate);
          return (
            <Icon
              name={isYellow ? 'star' : 'star-outline'}
              size={13}
              color={isYellow ? BaseColor.ThemeYellow : BaseColor.grayColor}
            />
          );
        })}
      </View>
      <Text bold body2>
        {price}
      </Text>
    </TouchableOpacity>
  );
}

ProductCategory.propTypes = {
  data: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  onPress: PropTypes.func,
};

ProductCategory.defaultProps = {
  data: {},
  onPress: () => {},
};
