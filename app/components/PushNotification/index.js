import {CommonActions, StackActions} from '@react-navigation/native';
import messaging from '@react-native-firebase/messaging';
import {useDispatch, useSelector} from 'react-redux';
import React, {useEffect} from 'react';
import {Platform} from 'react-native';
import _ from 'lodash';
import CAlert from '../CAlert';
import {BaseSetting} from '../../config/theme';
import {checkObject} from '../../utils/commonFunction';
import {getApiDataProgress} from '../../utils/apiHelper';
import AuthActions from '../../redux/reducers/auth/actions';
import {withInAppNotification} from '../../libs/react-native-in-app-notification';

const {setUUID, getNewNotification, setNotificationBadge} = AuthActions;

const PushNotification = (props) => {
  const {userData, isConnected} = useSelector((state) => state.auth);
  const UserToken = checkObject(userData, 'access_token');
  const activeScreen = useSelector((auth) => auth.auth.activeScreen);
  const dispatch = useDispatch();

  const UserType = checkObject(userData, 'user_type');

  useEffect(() => {
    checkNotificationPermission();
  }, [props, userData]);

  useEffect(() => {
    const openNotification = messaging().onNotificationOpenedApp(
      async (remoteMessage) => {
        console.log(
          'Notification caused app to open from background state 1:',
          remoteMessage,
        );
        onNotificationClick(remoteMessage);
      },
    );

    return openNotification;
  }, [activeScreen]);

  async function checkNotificationPermission() {
    console.log(
      'checkNotificationPermission =================================>',
    );
    const hasPermission = await messaging().hasPermission();
    try {
      const enabled =
        hasPermission === messaging.AuthorizationStatus.AUTHORIZED ||
        hasPermission === messaging.AuthorizationStatus.PROVISIONAL;
      if (!enabled) {
        const authorizationStatus = await messaging().requestPermission();
        if (authorizationStatus === messaging.AuthorizationStatus.AUTHORIZED) {
        } else if (
          authorizationStatus === messaging.AuthorizationStatus.PROVISIONAL
        ) {
        } else {
        }
      }

      getFcmToken();
    } catch (error) {}
  }

  async function getFcmToken() {
    const fcmToken = await messaging().getToken();
    console.log(
      '<=================================== fcmToken ===================================>',
    );
    console.log(fcmToken);
    console.log(
      '<=================================== fcmToken ===================================>',
    );
    dispatch(setUUID(fcmToken));
    setFCMListeners();
  }

  useEffect(() => {
    const unsubscribe = messaging().onMessage(async (remoteMessage) => {
      handleNotification(remoteMessage, true);
    });

    return unsubscribe;
  }, []);

  async function setFCMListeners() {
    try {
      const onTokenRefreshListener = messaging().onTokenRefresh((fcmToken) => {
        if (fcmToken) {
          console.log('fcmToken ===================================>');
          console.log('setFCMListeners -> fcmToken', fcmToken);
          console.log('fcmToken ===================================>');
          dispatch(setUUID(fcmToken));
        }
      });

      console.log('onTokenRefreshListener ==', onTokenRefreshListener);

      try {
        messaging().onMessage(async (remoteMessage) => {
          console.log(
            'Notification caused app to open from foreground state:',
            remoteMessage,
            'A new FCM message arrived!',
          );
          handleNotification(remoteMessage);
        });
      } catch (error) {
        console.log('setFCMListeners -> error', error);
      }

      messaging().onNotificationOpenedApp(async (remoteMessage) => {
        console.log(
          'Notification caused app to open from background state 2:',
          remoteMessage,
        );
        onNotificationClick(remoteMessage);
      });

      messaging()
        .getInitialNotification()
        .then(async (remoteMessage) => {
          if (remoteMessage) {
            console.log(
              'Notification caused app to open from quit state:',
              remoteMessage,
            );
            handleNotification(remoteMessage);
          }
        });
    } catch (error) {
      console.log('setFCMListeners -> error', error);
    }
  }

  const onNotificationClick = (notificationData) => {
    console.log('On Notification CLick =====================>');
    console.log(notificationData);
  };

  const routeScreen = () => {
    if (
      props &&
      props.navigation &&
      props.navigation.current &&
      props.navigation.current.navigate
    ) {
      if (_.toUpper(UserType) === 'DRIVER') {
        props.navigation.current.navigate('Job Feed');
      } else {
        props.navigation.current.navigate('Orders');
      }
    }
  };

  async function handleNotification(notificationData) {
    console.log('handleNotification =======================================>');
    console.log(notificationData);

    const notiData = _.has(notificationData, 'data')
      ? notificationData.data
      : {};
    const notiTitle = _.has(notiData, 'title') ? notiData.title : '-';
    const notiDesc = _.has(notiData, 'message') ? notiData.message : 'message';
    const notiBadge = _.has(notiData, 'badge') ? _.toNumber(notiData.badge) : 0;

    const currentDateTime = new Date().getTime();

    dispatch(setNotificationBadge(notiBadge));
    dispatch(getNewNotification(currentDateTime));
    props.showNotification({
      title: notiTitle,
      message: notiDesc,
      data: notiDesc,
      onPress: () => {
        routeScreen();
      },
    });
  }

  return null;
};

PushNotification.propTypes = {};

PushNotification.defaultProps = {};

export default withInAppNotification(PushNotification);
