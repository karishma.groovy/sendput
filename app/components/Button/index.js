/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/FontAwesome';
import IoIcon from 'react-native-vector-icons/Ionicons';
import {
  TouchableOpacity,
  View,
  StyleSheet,
  ActivityIndicator,
} from 'react-native';
import {BaseColor} from '../../config/theme';
import Text from '../Text/index';

const styles = StyleSheet.create({
  default: {
    height: 50,
    width: '100%',
    zIndex: 10,
    borderRadius: 25,
    backgroundColor: '#0000',
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 5,
    marginTop: 20,
    shadowColor: BaseColor.blackColor,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
  },
  leftIconSty: {
    fontSize: 25,
    color: BaseColor.blackColor,
  },
});

export default class Button extends Component {
  render() {
    const {
      onPress,
      style,
      styleText,
      loading,
      buttonText,
      colorAry,
      iconSty,
      displayLeftIcon,
      iconName,
      otherGredientCss,
      displayIcon,
      centerIconName,
      loaderColor,
      otherIconSty,
    } = this.props;

    return (
      <TouchableOpacity
        activeOpacity={0.9}
        style={StyleSheet.flatten([styles.default, style])}
        onPress={
          loading
            ? null
            : () => {
                onPress();
              }
        }>
        <LinearGradient
          start={{x: 0.3, y: 0.25}}
          end={{x: 0.8, y: 1.0}}
          colors={
            colorAry ? colorAry : [BaseColor.ThemeOrange, BaseColor.ThemeYellow]
          }
          style={[
            {
              height: 50,
              width: '100%',
              borderRadius: 25,
              backgroundColor: '#0000',
              alignItems: 'center',
              justifyContent: 'center',
            },
            otherGredientCss,
          ]}>
          {loading ? (
            <ActivityIndicator size="small" color={loaderColor} animating />
          ) : null}

          {loading ? null : displayLeftIcon ? (
            <Icon name={iconName} style={[styles.leftIconSty, iconSty]} />
          ) : null}

          {loading ? null : (
            <View>
              {displayIcon ? (
                <IoIcon
                  name={centerIconName}
                  style={[
                    {fontSize: 40, color: BaseColor.whiteColor},
                    otherIconSty,
                  ]}
                />
              ) : (
                <Text
                  subhead
                  whiteColor
                  bold
                  style={[
                    {
                      paddingLeft: displayLeftIcon ? 10 : 0,
                    },
                    styleText,
                  ]}
                  numberOfLines={1}>
                  {_.toUpper(buttonText)}
                </Text>
              )}
            </View>
          )}
        </LinearGradient>
      </TouchableOpacity>
    );
  }
}

Button.propTypes = {
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  otherGredientCss: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  icon: PropTypes.node,
  iconLeft: PropTypes.node,
  outline: PropTypes.bool,
  full: PropTypes.bool,
  round: PropTypes.bool,
  loading: PropTypes.bool,
  disable: PropTypes.bool,
  buttonText: PropTypes.string,
  displayIcon: PropTypes.bool,
  centerIconName: PropTypes.string,
  loaderColor: PropTypes.string,
  otherIconSty: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
};

Button.defaultProps = {
  style: {},
  otherGredientCss: {},
  icon: null,
  displayIcon: false,
  iconLeft: null,
  outline: false,
  full: false,
  round: false,
  loading: false,
  disable: false,
  centerIconName: '',
  buttonText: 'Button Name',
  loaderColor: BaseColor.whiteColor,
  otherIconSty: {},
};
