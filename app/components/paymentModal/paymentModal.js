/* eslint-disable react-native/no-inline-styles */
import React, {useCallback, useEffect, useRef, useState} from 'react';
import {CreditCardInput} from 'react-native-input-credit-card';
import {useDispatch, useSelector} from 'react-redux';
import PropTypes from 'prop-types';
import _ from 'lodash';
import {
  View,
  Modal,
  Platform,
  Keyboard,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  KeyboardAvoidingView,
} from 'react-native';
import {BaseColor} from '../../config/theme';
import CAlert from '../../components/CAlert';
import {BaseSetting} from '../../config/setting';
import Button from '../../components/Button/index';
import {checkObject} from '../../utils/commonFunction';
import {getApiDataProgress} from '../../utils/apiHelper';
import {StaticAlertMsg, StaticHeader} from '../../config/StaticAlertMsg';
import {FORTAB} from '../../config/MQ';

const styles = StyleSheet.create({
  mainCon: {
    flex: 1,
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  modalView: {
    paddingHorizontal: 20,
    paddingVertical: 30,
    borderTopLeftRadius: 40,
    backgroundColor: BaseColor.whiteColor,
    width: BaseSetting.nWidth,
  },
});
export default function PaymentModal(props) {
  const {dismiss, navigation, onRefreshList, ismodalVisible} = props;

  const [creditCardData, setCreditCardData] = useState({});
  const [loader, setLoader] = useState(false);
  const {isConnected, userData} = useSelector((state) => state.auth);

  let reg = /^(0[1-9]|1[0-2])\/?([0-9]{4}|[0-9]{2})$/;

  const userToken = checkObject(userData, 'access_token');
  const header = {
    Authorization: `Bearer ${userToken}`,
  };

  const handleChange = (form) => {
    setCreditCardData(form);
  };

  const creditCardNumber = _.has(creditCardData, 'values.number')
    ? creditCardData.values.number.replace(/\s+/g, '')
    : '';

  const expiryDate = _.has(creditCardData, 'values.expiry')
    ? creditCardData.values.expiry
    : '';

  const cvcNumber = _.has(creditCardData, 'values.cvc')
    ? creditCardData.values.cvc
    : '';

  function Validate() {
    Keyboard.dismiss();
    let valid = true;
    // if (
    //   _.isEmpty(creditCardNumber) ||
    //   _.isEmpty(expiryDate) ||
    //   _.isEmpty(cvcNumber)
    // ) {
    //   valid = false;
    //   CAlert(StaticAlertMsg.fillAllData, StaticHeader.Alert);
    //   return false;
    // }
    // if (creditCardNumber.length !== 16) {
    //   valid = false;
    //   CAlert(StaticAlertMsg.cardNumberLength, StaticHeader.Alert);
    //   return false;
    // }
    // if (reg.test(expiryDate) === false) {
    //   valid = false;
    //   CAlert(StaticAlertMsg.validExpiryDate, StaticHeader.Alert);
    //   return false;
    // }
    // if (cvcNumber.length !== 3) {
    //   valid = false;
    //   CAlert(StaticAlertMsg.cvcNumber, StaticHeader.Alert);
    //   return false;
    // }
    // if (valid === true) {
    setLoader(true);
    setTimeout(() => {
      addCardAction();
    }, 100);
    // }
  }

  async function addCardAction() {
    setLoader(true);
    if (isConnected === true) {
      const cardDetails = {
        'Card[creditCard_number]': creditCardNumber,
        'Card[creditCard_expirationDate]': expiryDate,
        'Card[creditCard_cvv]': cvcNumber,
      };

      try {
        let endPoint = BaseSetting.endpoints.add_card;
        const response = await getApiDataProgress(
          navigation,
          endPoint,
          'POST',
          cardDetails,
          header,
        );
        if (
          _.isObject(response) &&
          !_.isEmpty(response) &&
          response.success === true
        ) {
          setLoader(false);
          onRefreshList();
        } else {
          const errorMessage = checkObject(response, 'message');
          CAlert(errorMessage, StaticHeader.Oops, () => {
            setLoader(false);
          });
        }
      } catch (err) {
        console.log('Catch Part', err);
        CAlert(StaticAlertMsg.someThing_Went_Wrong, StaticHeader.Oops, () => {
          setLoader(false);
        });
      }
    } else {
      CAlert(StaticAlertMsg.Network_Problem, StaticHeader.Oops, () => {
        setLoader(false);
      });
    }
  }

  return (
    <Modal
      animationType="slide"
      visible={ismodalVisible}
      transparent={true}
      onRequestClose={() => {
        dismiss();
      }}>
      <View style={styles.mainCon}>
        <TouchableOpacity
          style={{flex: 0.8}}
          onPress={() => {
            dismiss();
          }}
        />
        <KeyboardAvoidingView
          style={{
            flex: 1,
            paddingTop: 30,
            paddingHorizontal: 30,
            alignItems: 'center',
            borderTopLeftRadius: 20,
            borderTopRightRadius: 20,
            justifyContent: 'center',
            backgroundColor: BaseColor.whiteColor,
          }}
          behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
          <ScrollView
            keyboardShouldPersistTaps="never"
            showsVerticalScrollIndicator={false}
            contentContainerStyle={{flexGrow: 1}}>
            <CreditCardInput onChange={(form) => handleChange(form)} />
            <Button
              loading={loader}
              style={{
                marginTop: 0,
                marginBottom: 30,
              }}
              buttonText={'SUBMIT'}
              onPress={() => {
                Validate();
              }}
            />
          </ScrollView>
        </KeyboardAvoidingView>
      </View>
    </Modal>
  );
}

PaymentModal.propTypes = {
  data: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  onPress: PropTypes.func,
};

PaymentModal.defaultProps = {
  data: {},
  onPress: () => {},
};
