/* eslint-disable prettier/prettier */
import ContentLoader, {Rect, Circle} from 'react-content-loader/native';
import {View} from 'react-native';
import React from 'react';
import _ from 'lodash';
import {BaseSetting} from '../../config';
import {FORTAB} from '../../config/MQ';

const arrayList = [{}, {}, {}, {}, {}, {}, {}, {}, {}];
const Dw = BaseSetting.nWidth;
const Dh = BaseSetting.nHeight;

const cmnCSS = {flex: 1};
const loaderBGColor = '#dcdcdc';
const loaderFGColor = '#f7f7f7';

export const NotificationLoader = () => {
  const conWidth = Dw / 7;
  const conXValue = conWidth + 18;
  const TitleW = Dw / 1.3;
  const MsgOW = Dw / 1.5;
  const MsgTW = Dw / 1.7;
  const MsgTHW = Dw / 2;
  const DateW = Dw / 5;
  return (
    <View style={cmnCSS}>
      <ContentLoader
        speed={2}
        width={Dw}
        height={Dh}
        backgroundColor={loaderBGColor}
        foregroundColor={loaderFGColor}>
          {arrayList.map((obj, index) => {
            const CY = index === 0 ? 10 : (conWidth * index) + (20 * index) + 10;
            const TY = index === 0 ? 15 : CY + 5;
            const M1Y = index === 0 ? 30 : TY + 15;
            const M2Y = index === 0 ? 38 : M1Y + 8;
            const M3Y = index === 0 ? 46 : M2Y + 8;
            const DY = index === 0 ? 55 : M3Y + 9;
            const BL = (conWidth * index) + (20 * index);
            return (
              <>
                <Rect x="10" y={CY} rx="5" ry="5" width={conWidth} height={conWidth} />
                <Rect x={conXValue} y={TY} rx="0" ry="0" width={TitleW} height="4" />
                <Rect x={conXValue} y={M1Y} rx="0" ry="0" width={MsgOW} height="2" />
                <Rect x={conXValue} y={M2Y} rx="0" ry="0" width={MsgTW} height="2" />
                <Rect x={conXValue} y={M3Y} rx="0" ry="0" width={MsgTHW} height="2" />
                <Rect x={conXValue} y={DY} rx="0" ry="0" width={DateW} height="4" />
                <Rect x="0" y={BL} rx="0" ry="0" width={Dw} height="1" />
              </>
            );
          })}
      </ContentLoader>
    </View>
  );
};

export const ShippingAddLoader = () => {
  const HLine = Dw - 20;
  const VLine = Dw / 6;
  const cmnWH = 3;
  const BHLine = VLine + cmnWH;
  const RXV = HLine + 7;
  const xPD = 25;
  const Title = Dw / 1.2;
  const Desc1 = Dw / 1.5;
  const Desc2 = Dw / 1.7;
  const Desc3 = Dw / 2;
  const CWH = Dw / 40;
  const cXV = CWH + xPD;
  const cYX = VLine - 15;
  const BLX = cXV + 20;
  const BLY = cYX - 3;

  return (
    <View style={cmnCSS}>
      <ContentLoader
        speed={2}
        width={Dw}
        height={Dh}
        backgroundColor={loaderBGColor}
        foregroundColor={loaderFGColor}>
        {arrayList.map((obj, index) => {
          const cmnCal = (BHLine * index) + (15 * index);
          const topY = index === 0 ? 0 : cmnCal;
          const lrXY = index === 0 ? cmnWH : cmnWH + cmnCal;
          const btmY = index === 0 ? BHLine : (BHLine * (index + 1)) + (15 * index);
          const TY = index === 0 ? 10 : 10 + cmnCal;
          const D1Y = index === 0 ? 25 : 15 + TY;
          const D2Y = index === 0 ? 33 : 8 + D1Y;
          const CRY = index === 0 ? cYX : cYX + cmnCal;
          const LLY = index === 0 ? BLY : BLY + cmnCal;
          return (
            <>
              <Rect x="10" y={topY} rx="0" ry="0" width={HLine} height={cmnWH} />
              <Rect x="10" y={lrXY} rx="0" ry="0" width={cmnWH} height={VLine} />
              <Rect x={RXV} y={lrXY} rx="0" ry="0" width={cmnWH} height={VLine} />
              <Rect x="10" y={btmY} rx="0" ry="0" width={HLine} height={cmnWH} />
              <Rect x={xPD} y={TY} rx="0" ry="0" width={Title} height={cmnWH} />
              <Rect x={xPD} y={D1Y} rx="0" ry="0" width={Desc1} height="2" />
              <Rect x={xPD} y={D2Y} rx="0" ry="0" width={Desc2} height="2" />
              <Circle cx={cXV} cy={CRY} r={CWH} />
              <Rect x={BLX} y={LLY} rx="0" ry="0" width={Desc3} height={cmnWH} />
            </>
          );
        })}
      </ContentLoader>
    </View>
  );
};

export const PaymentMethodLoader = () => {
  const HLine = Dw - 20;
  const VLine = Dw / 7;
  const cmnWH = 3;
  const BHLine = VLine + cmnWH;
  const RXV = HLine + 7;
  const CWH = Dw / 100;
  const SqW = Dw / 7;
  const SqH = Dw / 11;
  const TW = Dw / 1.5;
  const STW = Dw / 2;
  const TMY = CWH * 2 + 25;
  const SMY = CWH * 2 + 32;

  return (
    <View style={cmnCSS}>
      <ContentLoader
        speed={2}
        width={Dw}
        height={Dh}
        backgroundColor={loaderBGColor}
        foregroundColor={loaderFGColor}>

        {arrayList.map((obj, index) => {
          const cmnCal = (BHLine * index) + (10 * index);
          const THLY = index === 0 ? 0 : cmnCal;
          const lrXV = index === 0 ? cmnWH : cmnWH + cmnCal;
          const BHLY = index === 0 ? BHLine : (BHLine * (index + 1)) + (10 * index);
          const DY = index === 0 ? 15 : 15 + cmnCal;
          const SQY = index === 0 ? 15 : 15 + cmnCal;
          const M1Y = index === 0 ? TMY : TMY + cmnCal;
          const M2Y = index === 0 ? SMY : 7 + M1Y;

          return (
            <>
              <Rect x="10" y={THLY} rx="0" ry="0" width={HLine} height={cmnWH} />
              <Rect x="10" y={lrXV} rx="0" ry="0" width={cmnWH} height={VLine} />
              <Rect x={RXV} y={lrXV} rx="0" ry="0" width={cmnWH} height={VLine} />
              <Rect x="10" y={BHLY} rx="0" ry="0" width={HLine} height={cmnWH} />

              <Circle cx={25} cy={DY} r={CWH} />
              <Circle cx={31 + CWH} cy={DY} r={CWH} />
              <Circle cx={36 + (CWH * 2)} cy={DY} r={CWH} />
              <Circle cx={42 + (CWH * 3)} cy={DY} r={CWH} />
              <Circle cx={62 + (CWH * 4)} cy={DY} r={CWH} />
              <Circle cx={68 + (CWH * 5)} cy={DY} r={CWH} />
              <Circle cx={74 + (CWH * 6)} cy={DY} r={CWH} />
              <Circle cx={80 + (CWH * 7)} cy={DY} r={CWH} />
              <Circle cx={100 + (CWH * 8)} cy={DY} r={CWH} />
              <Circle cx={106 + (CWH * 9)} cy={DY} r={CWH} />
              <Circle cx={112 + (CWH * 10)} cy={DY} r={CWH} />
              <Circle cx={118 + (CWH * 11)} cy={DY} r={CWH} />
              <Circle cx={138 + (CWH * 12)} cy={DY} r={CWH} />
              <Circle cx={146 + (CWH * 13)} cy={DY} r={CWH} />
              <Circle cx={152 + (CWH * 14)} cy={DY} r={CWH} />
              <Circle cx={158 + (CWH * 15)} cy={DY} r={CWH} />

              <Rect x={HLine - SqW} y={SQY} rx="5" ry="5" width={SqW} height={SqH} />
              <Rect x="20" y={M1Y} rx="0" ry="0" width={TW} height={3} />
              <Rect x="20" y={M2Y} rx="0" ry="0" width={STW} height={3} />
            </>
          );
        })}
      </ContentLoader>
    </View>
  );
};

export const CMSLoader = () => {
  const TW = Dw - 30;
  const M1W = Dw - 30;
  const XAV = 15;
  return (
    <View style={cmnCSS}>
      <ContentLoader
        speed={2}
        width={Dw}
        height={Dh}
        backgroundColor={loaderBGColor}
        foregroundColor={loaderFGColor}>
          {arrayList.map((obj, index) => {
            const T1 = index === 0 ? 20 : (117 * index) + 20;
            const T2 = index === 0 ? 40 : 20 + T1;
            const T3 = index === 0 ? 47 : 7 + T2;
            const T4 = index === 0 ? 54 : 7 + T3;
            const T5 = index === 0 ? 61 : 7 + T4;
            const T6 = index === 0 ? 68 : 7 + T5;
            const T7 = index === 0 ? 75 : 7 + T6;
            const T8 = index === 0 ? 82 : 7 + T7;
            const T9 = index === 0 ? 89 : 7 + T8;
            const T10 = index === 0 ? 96 : 7 + T9;
            const T11 = index === 0 ? 103 : 7 + T10;
            const T12 = index === 0 ? 110 : 7 + T11;
            const T13 = index === 0 ? 117 : 7 + T12;
            return (
              <>
                <Rect x={XAV} y={T1} rx="0" ry="0" width={TW} height="4" />
                <Rect x={XAV} y={T2} rx="0" ry="0" width={M1W} height="2" />
                <Rect x={XAV} y={T3} rx="0" ry="0" width={M1W} height="2" />
                <Rect x={XAV} y={T4} rx="0" ry="0" width={M1W} height="2" />
                <Rect x={XAV} y={T5} rx="0" ry="0" width={M1W} height="2" />
                <Rect x={XAV} y={T6} rx="0" ry="0" width={M1W} height="2" />
                <Rect x={XAV} y={T7} rx="0" ry="0" width={M1W} height="2" />
                <Rect x={XAV} y={T8} rx="0" ry="0" width={M1W} height="2" />
                <Rect x={XAV} y={T9} rx="0" ry="0" width={M1W} height="2" />
                <Rect x={XAV} y={T10} rx="0" ry="0" width={M1W} height="2" />
                <Rect x={XAV} y={T11} rx="0" ry="0" width={M1W} height="2" />
                <Rect x={XAV} y={T12} rx="0" ry="0" width={M1W} height="2" />
                <Rect x={XAV} y={T13} rx="0" ry="0" width={M1W} height="2" />
              </>
            );
          })}
      </ContentLoader>
    </View>
  );
};

export const FAQLoader = () => {
  const TW = Dw / 1.7;
  const BL = Dw;
  const XAV = 10;
  const CW = Dw / 40;
  return (
    <View style={cmnCSS}>
      <ContentLoader
        speed={2}
        width={Dw}
        height={Dh}
        backgroundColor={loaderBGColor}
        foregroundColor={loaderFGColor}>
          {arrayList.map((obj, index) => {
            const T1Y = index === 0 ? 30 : (60 * index) + 30;
            const T2Y = index === 0 ? 60 : 30 + T1Y;
            return (
              <>
                <Rect x={XAV} y={T1Y} rx="0" ry="0" width={TW} height="4" />
                <Rect x={Dw - (CW + XAV)} y={T1Y} rx="0" ry="0" width={CW} height={CW} />
                <Rect x={0} y={T2Y} rx="0" ry="0" width={BL} height="2" />
              </>
            );
          })}
      </ContentLoader>
    </View>
  );
};

export const CartLoader = () => {
  const HLine = Dw - 20;
  const VLine = Dh;
  const cmnWH = 2;
  const TCW = Dw / 45;

  const STW = Dw / 5;
  const CDV = (Dw / 6) / 3;
  const CCal = HLine - (Dw / 6) - 40;
  return (
    <View style={cmnCSS}>
      <ContentLoader
        speed={2}
        width={Dw}
        height={Dh}
        backgroundColor={loaderBGColor}
        foregroundColor={loaderFGColor}>
        <Rect x="10" y={10} rx="0" ry="0" width={HLine - (TCW + 4)} height={cmnWH} />
        <Rect x="10" y={12} rx="0" ry="0" width={cmnWH} height={VLine} />
        <Rect x={HLine + 8} y={14 + TCW} rx="0" ry="0" width={cmnWH} height={VLine} />
        <Circle cx={HLine + (TCW / 2)} cy={15} r={TCW} />

        <Rect x="20" y={20} rx="0" ry="0" width={STW} height={STW} />
        <Rect x={STW + 30} y={25} rx="2" ry="2" width={Dw / 1.7} height={7} />
        <Rect x={STW + 30} y={45} rx="0" ry="0" width={Dw / 1.7} height={2} />
        <Rect x={STW + 30} y={52} rx="0" ry="0" width={Dw / 1.9} height={2} />
        <Rect x={STW + 30} y={59} rx="0" ry="0" width={Dw / 2} height={2} />

        <Rect x={20} y={STW + 35} rx="3" ry="3" width={Dw / 2.5} height={6} />

        <Rect x={20} y={STW + 55} rx="0" ry="0" width={Dw - 40} height={cmnWH} />
        <Rect x={30} y={STW + 65} rx="0" ry="0" width={Dw / 2.5} height={3} />
        <Rect x={30} y={STW + 73} rx="0" ry="0" width={Dw / 3.2} height={2} />
        <Rect x={30} y={STW + 81} rx="0" ry="0" width={Dw / 6} height={2} />

        <Rect x={CCal} y={STW + 73} rx="0" ry="0" width={Dw / 6} height={1} />
        <Rect x={CCal} y={STW + 75} rx="0" ry="0" width={1} height={20} />
        <Rect x={CCal + CDV} y={STW + 75} rx="0" ry="0" width={1} height={20} />
        <Rect x={CCal + (CDV * 2)} y={STW + 75} rx="0" ry="0" width={1} height={20} />
        <Rect x={CCal + (CDV * 3)} y={STW + 75} rx="0" ry="0" width={1} height={20} />

        <Rect x={CCal + ((CDV / 2) - 1)} y={STW + 84} rx="0" ry="0" width={3} height={2} />
        <Rect x={(CCal + CDV) + ((CDV / 2) - 1)} y={STW + 84} rx="0" ry="0" width={3} height={2} />
        <Rect x={(CCal + (CDV * 2)) + ((CDV / 2) - 1)} y={STW + 84} rx="0" ry="0" width={3} height={2} />
        <Rect x={(CCal + (CDV * 3)) + 10} y={STW + 80} rx="2" ry="2" width={10} height={10} />
        <Rect x={HLine - (Dw / 6) - 40} y={STW + 95} rx="0" ry="0" width={Dw / 6} height={1} />

        <Rect x={20} y={STW + 57} rx="0" ry="0" width={cmnWH} height={50} />
        <Rect x={(Dw - 40) + 18} y={STW + 57} rx="0" ry="0" width={cmnWH} height={50} />
        <Rect x={20} y={STW + 107} rx="0" ry="0" width={Dw - 40} height={cmnWH} />

        <Rect x={20} y={STW + 127} rx="0" ry="0" width={Dw - 40} height={cmnWH} />
        <Rect x={30} y={STW + 137} rx="0" ry="0" width={Dw / 2.5} height={3} />
        <Rect x={30} y={STW + 145} rx="0" ry="0" width={Dw / 3.2} height={2} />
        <Rect x={30} y={STW + 153} rx="0" ry="0" width={Dw / 6} height={2} />

        <Rect x={CCal} y={STW + 145} rx="0" ry="0" width={Dw / 6} height={1} />
        <Rect x={CCal} y={STW + 147} rx="0" ry="0" width={1} height={20} />
        <Rect x={CCal + CDV} y={STW + 147} rx="0" ry="0" width={1} height={20} />
        <Rect x={CCal + (CDV * 2)} y={STW + 147} rx="0" ry="0" width={1} height={20} />
        <Rect x={CCal + (CDV * 3)} y={STW + 147} rx="0" ry="0" width={1} height={20} />

        <Rect x={CCal + ((CDV / 2) - 1)} y={STW + 156} rx="0" ry="0" width={3} height={2} />
        <Rect x={(CCal + CDV) + ((CDV / 2) - 1)} y={STW + 156} rx="0" ry="0" width={3} height={2} />
        <Rect x={(CCal + (CDV * 2)) + ((CDV / 2) - 1)} y={STW + 156} rx="0" ry="0" width={3} height={2} />
        <Rect x={(CCal + (CDV * 3)) + 10} y={STW + 152} rx="2" ry="2" width={10} height={10} />
        <Rect x={HLine - (Dw / 6) - 40} y={STW + 167} rx="0" ry="0" width={Dw / 6} height={1} />

        <Rect x={20} y={STW + (57 * 2) + 15} rx="0" ry="0" width={cmnWH} height={50} />
        <Rect x={(Dw - 40) + 18} y={STW + (57 * 2) + 15} rx="0" ry="0" width={cmnWH} height={50} />
        <Rect x={20} y={STW + (57 * 3) + 8} rx="0" ry="0" width={Dw - 40} height={cmnWH} />

        <Rect x={20} y={(STW + (57 * 3) + 8) + 18} rx={(Dw / 9) /2} ry={(Dw / 9) /2} width={Dw - 40} height={Dw / 9} />

        <Rect x={20} y={((STW + (57 * 3) + 8) + (Dw / 9)) + 45} rx="2" ry="2" width={Dw / 4} height={5} />
        <Rect x={HLine - 60} y={((STW + (57 * 3) + 8) + (Dw / 9)) + 45} rx="2" ry="2" width={Dw / 7} height={5} />

        <Rect x="12" y={((STW + (57 * 3) + 8) + (Dw / 9)) + 63} rx="0" ry="0" width={HLine - 2.5} height={1} />

        <Rect x={20} y={((STW + (57 * 3) + 8) + (Dw / 9)) + 75} rx="2" ry="2" width={Dw / 3} height={3} />

        <Rect x={20} y={((STW + (57 * 3) + 8) + (Dw / 9)) + 87} rx="2" ry="2" width={Dw / 6} height={2} />
        <Rect x={Dw / 3.1} y={((STW + (57 * 3) + 8) + (Dw / 9)) + 87} rx="2" ry="2" width={Dw / 3} height={2} />

        <Rect x={20} y={((STW + (57 * 3) + 8) + (Dw / 9)) + 97} rx="2" ry="2" width={Dw / 4} height={2} />
        <Rect x={20} y={((STW + (57 * 3) + 8) + (Dw / 9)) + 99} rx="2" ry="2" width={2} height={35} />
        <Rect x={Dw / 4 + 18} y={((STW + (57 * 3) + 8) + (Dw / 9)) + 99} rx="2" ry="2" width={2} height={35} />
        <Rect x={20} y={((STW + (57 * 3) + 8) + (Dw / 9)) + 134} rx="2" ry="2" width={Dw / 4} height={2} />

        <Rect x={Dw / 3.1} y={((STW + (57 * 3) + 8) + (Dw / 9)) + 97} rx="2" ry="2" width={Dw / 1.6} height={2} />
        <Rect x={Dw / 3.1} y={((STW + (57 * 3) + 8) + (Dw / 9)) + 99} rx="2" ry="2" width={2} height={35} />
        <Rect x={(Dw - 40) + 16} y={((STW + (57 * 3) + 8) + (Dw / 9)) + 99} rx="2" ry="2" width={2} height={35} />
        <Rect x={Dw / 3.1} y={((STW + (57 * 3) + 8) + (Dw / 9)) + 134} rx="2" ry="2" width={Dw / 1.6} height={2} />

        <Rect x={20} y={((STW + (57 * 3) + 8) + (Dw / 9)) + 146} rx="2" ry="2" width={Dw / 2} height={2} />
        <Rect x={20} y={((STW + (57 * 3) + 8) + (Dw / 9)) + 156} rx="2" ry="2" width={Dw - 40} height={2} />
        <Rect x={20} y={((STW + (57 * 3) + 8) + (Dw / 9)) + 158} rx="2" ry="2" width={2} height={35} />
        <Rect x={(Dw - 40) + 18} y={((STW + (57 * 3) + 8) + (Dw / 9)) + 158} rx="2" ry="2" width={2} height={35} />
        <Rect x={20} y={((STW + (57 * 3) + 8) + (Dw / 9)) + 193} rx="2" ry="2" width={Dw - 40} height={2} />

        <Rect x="12" y={((STW + (57 * 3) + 8) + (Dw / 9)) + 208} rx="0" ry="0" width={HLine - 2.5} height={1} />

        <Rect x={20} y={((STW + (57 * 3) + 8) + (Dw / 9)) + 218} rx="2" ry="2" width={Dw / 3} height={3} />

        <Rect x={20} y={((STW + (57 * 3) + 8) + (Dw / 9)) + 230} rx="2" ry="2" width={Dw - 40} height={2} />
        <Rect x={20} y={((STW + (57 * 3) + 8) + (Dw / 9)) + 232} rx="2" ry="2" width={2} height={35} />
        <Rect x={(Dw - 40) + 18} y={((STW + (57 * 3) + 8) + (Dw / 9)) + 232} rx="2" ry="2" width={2} height={35} />
        <Rect x={20} y={((STW + (57 * 3) + 8) + (Dw / 9)) + 267} rx="2" ry="2" width={Dw - 40} height={2} />

        <Rect x="12" y={((STW + (57 * 3) + 8) + (Dw / 9)) + 285} rx="0" ry="0" width={HLine - 2.5} height={1} />

        <Rect x={HLine - 105} y={((STW + (57 * 3) + 8) + (Dw / 9)) + 300} rx="1" ry="1" width={Dw / 4} height={3} />

        <Rect x={20} y={((STW + (57 * 3) + 8) + (Dw / 9)) + 315} rx={(Dw / 9) /2} ry={(Dw / 9) /2} width={Dw - 40} height={Dw / 9} />
      </ContentLoader>
    </View>
  );
};

export const DispensariesLoader = () => {
  const CW = (Dw / 2) - 15;
  return (
    <View style={cmnCSS}>
      <ContentLoader
        speed={2}
        width={Dw}
        height={Dh}
        backgroundColor={loaderBGColor}
        foregroundColor={loaderFGColor}>
          {arrayList.map((obj, index) => {
            const T1Y = index === 0 ? 10 : (CW * index) + (10 * (index + 1));
            return (
              <>
                <Rect x="10" y={T1Y} rx="5" ry="5" width={CW} height={CW} />
                <Rect x={CW + 20} y={T1Y} rx="5" ry="5" width={CW} height={CW} />
              </>
            );
          })}
      </ContentLoader>
    </View>
  );
};

export const DispensariesDetailLoader = (props) => {
  const {selectedTab} = props;
  const SIW = Dw / 5;
  const CWH = Dw / 100;
  const CW = (Dw / 2) - 15;
  const CH = Dw / 11;
  const TTW = (Dw / 4) - 22;
  const STW = Dw / 4;
  const LRPD = ((Dw / 2) - (Dw / 4)) / 2;

  return (
    <View style={cmnCSS}>
      <ContentLoader
        speed={2}
        width={Dw}
        height={Dh}
        backgroundColor={loaderBGColor}
        foregroundColor={loaderFGColor}>

        <Rect x="10" y="10" rx="5" ry="5" width={SIW} height={SIW} />
        <Rect x={SIW + 20} y="17" rx="3" ry="3" width={Dw / 1.5} height="6" />

        <Circle x={SIW + 27} cy="32" r={CWH} />
        <Circle x={SIW + 40} cy="32" r={CWH} />
        <Circle x={SIW + 53} cy="32" r={CWH} />
        <Circle x={SIW + 66} cy="32" r={CWH} />
        <Circle x={SIW + 79} cy="32" r={CWH} />
        <Rect x={SIW + 90} y="28" rx="2" ry="2" width={CWH * 4} height={CWH * 2}/>

        <Rect x={SIW + 20} y="43" rx="5" ry="5" width={CWH * 3} height={CWH * 3} />

        <Rect x={5 + (CWH * 3) + (SIW + 20)} y="48" rx="0" ry="0" width={Dw / 3} height={4} />

        <Rect x={SIW + 20} y={(CWH * 3) + 51} rx="2" ry="2" width={Dw / 2} height="4" />
        <Rect x={SIW + 20} y={(CWH * 3) + 61} rx="2" ry="2" width={Dw / 3} height="3" />

        <Rect x={10} y={SIW + 25} rx="3" ry="3" width={CW} height={CH} />
        <Rect x={CW + 20} y={SIW + 25} rx="3" ry="3" width={CW} height={CH} />

        <Rect x="0" y={SIW + CH + 40} rx="0" ry="0" width={Dw} height="1" />
        <Rect x="10" y={SIW + CH + 60} rx="2" ry="2" width={TTW} height={4} />
        <Rect x={TTW + 32} y={SIW + CH + 60} rx="2" ry="2" width={TTW} height={4} />
        <Rect x={TTW * 2 + 54} y={SIW + CH + 60} rx="2" ry="2" width={TTW} height={4} />
        <Rect x={TTW * 3 + 76} y={SIW + CH + 60} rx="2" ry="2" width={TTW} height={4} />

        {selectedTab === 0 ?
          <>
            <Rect x="0" y={SIW + CH + 84} rx="0" ry="0" width={STW} height={4} />
            <Rect x={STW} y={SIW + CH + 87} rx="0" ry="0" width={Dw - STW} height="1" />

            <Rect x={LRPD} y={SIW + CH + 87 + LRPD} rx="3" ry="3" width={Dw / 4} height={Dw / 3} />
            <Rect x="15" y={SIW + CH + 87 + (LRPD * 2) + (Dw / 3)} rx="2" ry="2" width={Dw / 2.7} height={4} />
            <Rect x="15" y={SIW + CH + 95 + (LRPD * 2) + (Dw / 3)} rx="2" ry="2" width={Dw / 3.3} height={3} />
            <Rect x="0" y={SIW + CH + 112 + (LRPD * 2) + (Dw / 3)} rx="2" ry="2" width={Dw / 2} height={1} />
            <Rect x={Dw / 2} y={SIW + CH + 87} rx="0" ry="0" width={1} height={(Dw / 3) + (LRPD * 2) + 25} />
            <Rect x={(Dw / 2) + LRPD} y={SIW + CH + 87 + LRPD} rx="3" ry="3" width={Dw / 4} height={Dw / 3} />
            <Rect x={(Dw / 2) + 15} y={SIW + CH + 87 + (LRPD * 2) + (Dw / 3)} rx="2" ry="2" width={Dw / 2.7} height={4} />
            <Rect x={(Dw / 2) + 15} y={SIW + CH + 95 + (LRPD * 2) + (Dw / 3)} rx="2" ry="2" width={Dw / 3.3} height={3} />
            <Rect x={Dw / 2} y={SIW + CH + 112 + (LRPD * 2) + (Dw / 3)} rx="2" ry="2" width={Dw / 2} height={1} />


            <Rect x={LRPD} y={((Dw / 3) + (LRPD * 2) + 25) + SIW + CH + 87 + LRPD} rx="3" ry="3" width={Dw / 4} height={Dw / 3} />
            <Rect x="15" y={((Dw / 3) + (LRPD * 2) + 25) + SIW + CH + 87 + (LRPD * 2) + (Dw / 3)} rx="2" ry="2" width={Dw / 2.7} height={4} />
            <Rect x="15" y={((Dw / 3) + (LRPD * 2) + 25) + SIW + CH + 95 + (LRPD * 2) + (Dw / 3)} rx="2" ry="2" width={Dw / 3.3} height={3} />
            <Rect x="0" y={((Dw / 3) + (LRPD * 2) + 25) + SIW + CH + 112 + (LRPD * 2) + (Dw / 3)} rx="2" ry="2" width={Dw / 2} height={1} />
            <Rect x={Dw / 2} y={(Dw / 3) + (LRPD * 2) + 25 + SIW + CH + 87} rx="0" ry="0" width={1} height={(Dw / 3) + (LRPD * 2) + 25} />
            <Rect x={(Dw / 2) + LRPD} y={((Dw / 3) + (LRPD * 2) + 25) + SIW + CH + 87 + LRPD} rx="3" ry="3" width={Dw / 4} height={Dw / 3} />
            <Rect x={(Dw / 2) + 15} y={((Dw / 3) + (LRPD * 2) + 25) + SIW + CH + 87 + (LRPD * 2) + (Dw / 3)} rx="2" ry="2" width={Dw / 2.7} height={4} />
            <Rect x={(Dw / 2) + 15} y={((Dw / 3) + (LRPD * 2) + 25) + SIW + CH + 95 + (LRPD * 2) + (Dw / 3)} rx="2" ry="2" width={Dw / 3.3} height={3} />
            <Rect x={Dw / 2} y={((Dw / 3) + (LRPD * 2) + 25) + SIW + CH + 112 + (LRPD * 2) + (Dw / 3)} rx="2" ry="2" width={Dw / 2} height={1} />
          </>
        : null}

        {selectedTab === 1 ?
          <>
            <Rect x="0" y={SIW + CH + 87} rx="0" ry="0" width={STW} height={1} />
            <Rect x={STW} y={SIW + CH + 84} rx="0" ry="0" width={STW} height={4} />
            <Rect x={STW * 2} y={SIW + CH + 87} rx="0" ry="0" width={Dw - (STW * 2)} height={1} />

            <Rect x="15" y={SIW + CH + 117} rx="0" ry="0" width={Dw - 40} height={1} />
            <Rect x="15" y={SIW + CH + 122} rx="0" ry="0" width={Dw - 50} height={1} />
            <Rect x="15" y={SIW + CH + 127} rx="0" ry="0" width={Dw - 45} height={1} />
            <Rect x="15" y={SIW + CH + 132} rx="0" ry="0" width={Dw - 55} height={1} />
            <Rect x="15" y={SIW + CH + 137} rx="0" ry="0" width={Dw - 40} height={1} />
            <Rect x="15" y={SIW + CH + 142} rx="0" ry="0" width={Dw - 45} height={1} />
            <Rect x="15" y={SIW + CH + 147} rx="0" ry="0" width={Dw - 55} height={1} />
            <Rect x="15" y={SIW + CH + 152} rx="0" ry="0" width={Dw - 55} height={1} />
            <Rect x="15" y={SIW + CH + 157} rx="0" ry="0" width={Dw - 45} height={1} />
            <Rect x="15" y={SIW + CH + 162} rx="0" ry="0" width={Dw - 43} height={1} />
            <Rect x="15" y={SIW + CH + 167} rx="0" ry="0" width={Dw - 42} height={1} />
          </>
        : null}

        {selectedTab === 2 ?
          <>
            <Rect x="0" y={SIW + CH + 87} rx="0" ry="0" width={STW * 2} height={1} />
            <Rect x={STW * 2} y={SIW + CH + 84} rx="0" ry="0" width={STW} height={4} />
            <Rect x={STW * 3} y={SIW + CH + 87} rx="0" ry="0" width={Dw - (STW * 3)} height={1} />

            <Rect x="10" y={SIW + CH + 107} rx="0" ry="0" width={Dw - 20} height={1} />
            <Rect x="20" y={SIW + CH + 117} rx="2" ry="2" width={Dw - 40} height={Dw / 3} />
            <Rect x="20" y={(SIW + CH) + 133 + (Dw / 3)} rx="2" ry="2" width={Dw / 1.3} height={2} />
            <Rect x="20" y={(SIW + CH) + 143 + (Dw / 3)} rx="2" ry="2" width={Dw / 2} height={2} />
            <Rect x="20" y={(SIW + CH) + 153 + (Dw / 3)} rx="2" ry="2" width={Dw / 1.7} height={2} />
            <Rect x="10" y={SIW + CH + 108} rx="0" ry="0" width={1} height={(Dw / 3) + 66} />
            <Rect x={(Dw - 40) + 29} y={SIW + CH + 108} rx="0" ry="0" width={1} height={(Dw / 3) + 66} />
            <Rect x={10} y={(SIW + CH ) + (Dw / 3) + 175} rx="0" ry="0" width={Dw - 20} height={1} />


            <Rect x="10" y={(SIW + CH) + (Dw / 3) + 190} rx="0" ry="0" width={Dw - 20} height={1} />
            <Rect x="20" y={(SIW + CH) + (Dw / 3) + 200} rx="2" ry="2" width={Dw - 40} height={Dw / 3} />
            <Rect x="20" y={(SIW + CH) + 216 + ((Dw / 3) * 2)} rx="2" ry="2" width={Dw / 1.3} height={2} />
            <Rect x="20" y={(SIW + CH) + 226 + ((Dw / 3) * 2)} rx="2" ry="2" width={Dw / 2} height={2} />
            <Rect x="20" y={(SIW + CH) + 236 + ((Dw / 3) * 2)} rx="2" ry="2" width={Dw / 1.7} height={2} />
            <Rect x="10" y={(SIW + CH ) + (Dw / 3) + 191} rx="0" ry="0" width={1} height={(Dw / 3) + 66} />
            <Rect x={(Dw - 40) + 29} y={(SIW + CH ) + (Dw / 3) + 191} rx="0" ry="0" width={1} height={(Dw / 3) + 66} />
            <Rect x={10} y={(SIW + CH) + ((Dw / 3) * 2) + 258} rx="0" ry="0" width={Dw - 20} height={1} />

            <Rect x="10" y={(SIW + CH) + ((Dw / 3) * 2) + 273} rx="0" ry="0" width={Dw - 20} height={1} />
            <Rect x="20" y={(SIW + CH) + ((Dw / 3) * 2) + 283} rx="2" ry="2" width={Dw - 40} height={Dw / 3} />
            <Rect x="20" y={(SIW + CH) + 299 + ((Dw / 3) * 3)} rx="2" ry="2" width={Dw / 1.3} height={2} />
            <Rect x="20" y={(SIW + CH) + 309 + ((Dw / 3) * 3)} rx="2" ry="2" width={Dw / 2} height={2} />
            <Rect x="20" y={(SIW + CH) + 319 + ((Dw / 3) * 3)} rx="2" ry="2" width={Dw / 1.7} height={2} />
            <Rect x="10" y={(SIW + CH ) + ((Dw / 3) * 2) + 274} rx="0" ry="0" width={1} height={(Dw / 3) + 66} />
            <Rect x={(Dw - 40) + 29} y={(SIW + CH ) + ((Dw / 3) * 2) + 274} rx="0" ry="0" width={1} height={(Dw / 3) + 66} />
            <Rect x={10} y={(SIW + CH ) + ((Dw / 3) * 4) + 258} rx="0" ry="0" width={Dw - 20} height={1} />

          </>
        : null}

        {selectedTab === 3 ?
          <>
            <Rect x="0" y={SIW + CH + 87} rx="0" ry="0" width={STW * 3} height={1} />
            <Rect x={STW * 3} y={SIW + CH + 84} rx="0" ry="0" width={STW} height={4} />

            <Rect x="10" y={SIW + CH + 107} rx="0" ry="0" width={Dw - 20} height={1} />
            <Rect x="22" y={SIW + CH + 117} rx="2" ry="2" width={Dw / 5} height={Dw / 5} />
            <Rect x={(Dw / 5) + 32} y={SIW + CH + 125} rx="2" ry="2" width={Dw / 1.7} height={4} />
            <Rect x={(Dw / 5) + 32} y={SIW + CH + 140} rx="2" ry="2" width={Dw / 2.3} height={2} />
            <Rect x={(Dw / 5) + 32} y={SIW + CH + 148} rx="2" ry="2" width={Dw / 2.5} height={2} />
            <Rect x={(Dw / 5) + 32} y={SIW + CH + 168} rx="2" ry="2" width={Dw - (Dw / 5) - 53} height={2} />
            <Circle x={(Dw / 5) + 35} cy={SIW + CH + 179} r={CWH} />
            <Circle x={(Dw / 5) + CWH + 43} cy={SIW + CH + 179} r={CWH} />
            <Circle x={(Dw / 5) + (CWH * 2) + 51} cy={SIW + CH + 179} r={CWH} />
            <Circle x={(Dw / 5) + (CWH * 3) + 58} cy={SIW + CH + 179} r={CWH} />
            <Circle x={(Dw / 5) + (CWH * 4) + 66} cy={SIW + CH + 179} r={CWH} />
            <Rect x="10" y={SIW + CH + 108} rx="0" ry="0" width={1} height={(Dw / 5) + 20} />
            <Rect x={(Dw - 40) + 29} y={SIW + CH + 108} rx="0" ry="0" width={1} height={(Dw / 5) + 20} />
            <Rect x={10} y={(SIW + CH ) + (Dw / 5) + 127} rx="0" ry="0" width={Dw - 20} height={1} />


            <Rect x="10" y={(SIW + CH ) + (Dw / 5) + 138} rx="0" ry="0" width={Dw - 20} height={1} />
            <Rect x="22" y={(SIW + CH ) + (Dw / 5) + 148} rx="2" ry="2" width={Dw / 5} height={Dw / 5} />
            <Rect x={(Dw / 5) + 32} y={(SIW + CH ) + (Dw / 5) + 156} rx="2" ry="2" width={Dw / 1.7} height={4} />
            <Rect x={(Dw / 5) + 32} y={(SIW + CH ) + (Dw / 5) + 171} rx="2" ry="2" width={Dw / 2.3} height={2} />
            <Rect x={(Dw / 5) + 32} y={(SIW + CH ) + (Dw / 5) + 179} rx="2" ry="2" width={Dw / 2.5} height={2} />
            <Rect x={(Dw / 5) + 32} y={(SIW + CH ) + (Dw / 5) + 199} rx="2" ry="2" width={Dw - (Dw / 5) - 53} height={2} />
            <Circle x={(Dw / 5) + 35} y={(SIW + CH ) + (Dw / 5) + 210} r={CWH} />
            <Circle x={(Dw / 5) + CWH + 43} y={(SIW + CH ) + (Dw / 5) + 210} r={CWH} />
            <Circle x={(Dw / 5) + (CWH * 2) + 51} y={(SIW + CH ) + (Dw / 5) + 210} r={CWH} />
            <Circle x={(Dw / 5) + (CWH * 3) + 58} y={(SIW + CH ) + (Dw / 5) + 210} r={CWH} />
            <Circle x={(Dw / 5) + (CWH * 4) + 66} y={(SIW + CH ) + (Dw / 5) + 210} r={CWH} />
            <Rect x="10" y={(SIW + CH ) + (Dw / 5) + 139} rx="0" ry="0" width={1} height={(Dw / 5) + 20} />
            <Rect x={(Dw - 40) + 29} y={(SIW + CH ) + (Dw / 5) + 139} rx="0" ry="0" width={1} height={(Dw / 5) + 20} />
            <Rect x={10} y={(SIW + CH ) + ((Dw / 5) * 2) + 158} rx="0" ry="0" width={Dw - 20} height={1} />

            <Rect x="10" y={(SIW + CH ) + ((Dw / 5) * 2) + 169} rx="0" ry="0" width={Dw - 20} height={1} />
            <Rect x="22" y={(SIW + CH ) + ((Dw / 5) * 2) + 179} rx="2" ry="2" width={Dw / 5} height={Dw / 5} />
            <Rect x={(Dw / 5) + 32} y={(SIW + CH ) + ((Dw / 5) * 2) + 187} rx="2" ry="2" width={Dw / 1.7} height={4} />
            <Rect x={(Dw / 5) + 32} y={(SIW + CH ) + ((Dw / 5) * 2) + 202} rx="2" ry="2" width={Dw / 2.3} height={2} />
            <Rect x={(Dw / 5) + 32} y={(SIW + CH ) + ((Dw / 5) * 2) + 210} rx="2" ry="2" width={Dw / 2.5} height={2} />
            <Rect x={(Dw / 5) + 32} y={(SIW + CH ) + ((Dw / 5) * 2) + 230} rx="2" ry="2" width={Dw - ((Dw / 5) * 2) - 53} height={2} />
            <Circle x={(Dw / 5) + 35} y={(SIW + CH ) + ((Dw / 5) * 2) + 241} r={CWH} />
            <Circle x={(Dw / 5) + CWH + 43} y={(SIW + CH ) + ((Dw / 5) * 2) + 241} r={CWH} />
            <Circle x={(Dw / 5) + (CWH * 2) + 51} y={(SIW + CH ) + ((Dw / 5) * 2) + 241} r={CWH} />
            <Circle x={(Dw / 5) + (CWH * 3) + 58} y={(SIW + CH ) + ((Dw / 5) * 2) + 241} r={CWH} />
            <Circle x={(Dw / 5) + (CWH * 4) + 66} y={(SIW + CH ) + ((Dw / 5) * 2) + 241} r={CWH} />
            <Rect x="10" y={(SIW + CH ) + ((Dw / 5) * 2) + 170} rx="0" ry="0" width={1} height={(Dw / 5) + 20} />
            <Rect x={(Dw - 40) + 29} y={(SIW + CH ) + ((Dw / 5) * 2) + 170} rx="0" ry="0" width={1} height={(Dw / 5) + 20} />
            <Rect x={10} y={(SIW + CH ) + ((Dw / 5) * 3) + 189} rx="0" ry="0" width={Dw - 20} height={1} />

            <Rect x="10" y={(SIW + CH ) + ((Dw / 5) * 3) + 200} rx="0" ry="0" width={Dw - 20} height={1} />
            <Rect x="22" y={(SIW + CH ) + ((Dw / 5) * 3) + 210} rx="2" ry="2" width={Dw / 5} height={Dw / 5} />
            <Rect x={(Dw / 5) + 32} y={(SIW + CH ) + ((Dw / 5) * 3) + 218} rx="2" ry="2" width={Dw / 1.7} height={4} />
            <Rect x={(Dw / 5) + 32} y={(SIW + CH ) + ((Dw / 5) * 3) + 233} rx="2" ry="2" width={Dw / 2.3} height={2} />
            <Rect x={(Dw / 5) + 32} y={(SIW + CH ) + ((Dw / 5) * 3) + 241} rx="2" ry="2" width={Dw / 2.5} height={2} />
            <Rect x={(Dw / 5) + 32} y={(SIW + CH ) + ((Dw / 5) * 3) + 261} rx="2" ry="2" width={Dw - ((Dw / 5) * 2) - 53} height={2} />
            <Circle x={(Dw / 5) + 35} y={(SIW + CH ) + ((Dw / 5) * 3) + 272} r={CWH} />
            <Circle x={(Dw / 5) + CWH + 43} y={(SIW + CH ) + ((Dw / 5) * 3) + 272} r={CWH} />
            <Circle x={(Dw / 5) + (CWH * 2) + 51} y={(SIW + CH ) + ((Dw / 5) * 3) + 272} r={CWH} />
            <Circle x={(Dw / 5) + (CWH * 3) + 58} y={(SIW + CH ) + ((Dw / 5) * 3) + 272} r={CWH} />
            <Circle x={(Dw / 5) + (CWH * 4) + 66} y={(SIW + CH ) + ((Dw / 5) * 3) + 272} r={CWH} />
            <Rect x="10" y={(SIW + CH ) + ((Dw / 5) * 3) + 201} rx="0" ry="0" width={1} height={(Dw / 5) + 20} />
            <Rect x={(Dw - 40) + 29} y={(SIW + CH ) + ((Dw / 5) * 3) + 201} rx="0" ry="0" width={1} height={(Dw / 5) + 20} />
            <Rect x={10} y={(SIW + CH ) + ((Dw / 5) * 4) + 219} rx="0" ry="0" width={Dw - 20} height={1} />

            <Rect x="10" y={(SIW + CH ) + ((Dw / 5) * 4) + 230} rx="0" ry="0" width={Dw - 20} height={1} />
            <Rect x="22" y={(SIW + CH ) + ((Dw / 5) * 4) + 240} rx="2" ry="2" width={Dw / 5} height={Dw / 5} />
            <Rect x={(Dw / 5) + 32} y={(SIW + CH ) + ((Dw / 5) * 4) + 248} rx="2" ry="2" width={Dw / 1.7} height={4} />
            <Rect x={(Dw / 5) + 32} y={(SIW + CH ) + ((Dw / 5) * 4) + 263} rx="2" ry="2" width={Dw / 2.3} height={2} />
            <Rect x={(Dw / 5) + 32} y={(SIW + CH ) + ((Dw / 5) * 4) + 271} rx="2" ry="2" width={Dw / 2.5} height={2} />
            <Rect x={(Dw / 5) + 32} y={(SIW + CH ) + ((Dw / 5) * 4) + 291} rx="2" ry="2" width={Dw - ((Dw / 5) * 2) - 53} height={2} />
            <Circle x={(Dw / 5) + 35} y={(SIW + CH ) + ((Dw / 5) * 4) + 302} r={CWH} />
            <Circle x={(Dw / 5) + CWH + 43} y={(SIW + CH ) + ((Dw / 5) * 4) + 302} r={CWH} />
            <Circle x={(Dw / 5) + (CWH * 2) + 51} y={(SIW + CH ) + ((Dw / 5) * 4) + 302} r={CWH} />
            <Circle x={(Dw / 5) + (CWH * 3) + 58} y={(SIW + CH ) + ((Dw / 5) * 4) + 302} r={CWH} />
            <Circle x={(Dw / 5) + (CWH * 4) + 66} y={(SIW + CH ) + ((Dw / 5) * 4) + 302} r={CWH} />
            <Rect x="10" y={(SIW + CH ) + ((Dw / 5) * 4) + 231} rx="0" ry="0" width={1} height={(Dw / 5) + 20} />
            <Rect x={(Dw - 40) + 29} y={(SIW + CH ) + ((Dw / 5) * 4) + 231} rx="0" ry="0" width={1} height={(Dw / 5) + 20} />
            <Rect x={10} y={(SIW + CH ) + ((Dw / 5) * 5) + 250} rx="0" ry="0" width={Dw - 20} height={1} />

          </>
        : null}

      </ContentLoader>
    </View>
  );
};

export const ProductDetailLoader = () => {
  const data = [{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}];
  const IMW = Dw - 20;
  const IMH = Dw / 2;
  const CWH = Dw / 90;
  return (
    <View style={cmnCSS}>
      <ContentLoader
        speed={2}
        width={Dw}
        height={Dh}
        backgroundColor={loaderBGColor}
        foregroundColor={loaderFGColor}>
        <Rect x="10" y="10" rx="5" ry="5" width={IMW} height={IMH} />

        <Rect x="10" y={IMH + 30} rx="2" ry="2" width={Dw / 1.3} height={4} />
        <Rect x="10" y={IMH + 40} rx="2" ry="2" width={Dw / 1.7} height={4} />

        <Rect x="10" y={IMH + 60} rx="0" ry="0" width={Dw / 3} height={4} />
        <Circle x={15} cy={IMH + 74} r={CWH} />
        <Circle x={CWH + 23} cy={IMH + 74} r={CWH} />
        <Circle x={(CWH * 2) + 31} cy={IMH + 74} r={CWH} />
        <Circle x={(CWH * 3) + 39} cy={IMH + 74} r={CWH} />
        <Circle x={(CWH * 4) + 47} cy={IMH + 74} r={CWH} />

        <Rect x={IMW - (Dw / 4) + 10} y={IMH + 74} rx="2" ry="2" width={Dw / 4} height={4} />

        <Rect x={10} y={IMH + 100} rx="2" ry="2" width={Dw / 3} height={3} />

        {data.map((obj, index) => {
          const XV = index === 0 ? IMH + 125 : IMH + (125 + (6 * index + 1));
          return (
            <Rect x={10} y={XV} rx="2" ry="2" width={Dw - 20} height={2} />
          );
        })}
      </ContentLoader>
    </View>
  );
};

export const MyOrderLoader = () => {
  const TW = Dw - 20;
  const TTW = Dw / 2.5;
  const STW = Dw / 4;
  const BTNW = Dw / 4;
  const BTNH = Dw / 11;

  const VLine = 110 + BTNH;
  const BHLine = VLine + 2;
  return (
    <View style={cmnCSS}>
      <ContentLoader
        speed={2}
        width={Dw}
        height={Dh}
        backgroundColor={loaderBGColor}
        foregroundColor={loaderFGColor}>

        {arrayList.map((obj, index) => {
          const cmnCal = (BHLine * index) + (10 * index);
          const MW = index === 0 ? 0 : cmnCal;
          const LRXV = index === 0 ? 2 : 2 + MW;
          const T1 = index === 0 ? 10 : 10 + MW;
          const T2 = index === 0 ? 30 : 20 + T1;
          const T3 = index === 0 ? 40 : 10 + T2;
          const T4 = index === 0 ? 60 : 20 + T3;
          const T5 = index === 0 ? 70 : 10 + T4;
          const T6 = index === 0 ? 95 : 25 + T5;
          const LW = index === 0 ? 112 + (Dw / 11) : 112 + (Dw / 11) + cmnCal;

          return (
            <>
              <Rect x="10" y={MW} rx="0" ry="0" width={TW} height={2} />
              <Rect x="20" y={T1} rx="2" ry="2" width={TTW} height={4} />
              <Rect x={TW - STW} y={T1} rx="2" ry="2" width={STW} height={4} />
              <Rect x="20" y={T2} rx="0" ry="0" width={STW} height={1} />
              <Rect x="20" y={T3} rx="2" ry="2" width={TTW} height={4} />
              <Rect x="20" y={T4} rx="0" ry="0" width={STW} height={1} />
              <Rect x="20" y={T5} rx="2" ry="2" width={TTW} height={4} />
              <Rect x={TW - (STW)} y={T4} rx="0" ry="0" width={STW} height={1} />
              <Rect x={TW - TTW} y={T5} rx="2" ry="2" width={TTW} height={4} />
              <Rect x="20" y={T6} rx={(BTNH) / 2} ry={(BTNH) / 2} width={BTNW} height={BTNH} />
              <Rect x={TW - (BTNW)} y={T6 + ((BTNH) / 2)} rx={2} ry={2} width={BTNW} height={4} />
              <Rect x="10" y={LRXV} rx="0" ry="0" width={2} height={VLine} />
              <Rect x={TW + 8} y={LRXV} rx="0" ry="0" width={2} height={VLine} />
              <Rect x="10" y={LW} rx="0" ry="0" width={TW} height={2} />
            </>
          );
        })}
      </ContentLoader>
    </View>
  );
};

export const OrderDetailLoader = () => {
  const MW = Dw - 10;
  const SIW = Dw / 6;
  const ICW = Dw - 30;
  return (
    <View style={cmnCSS}>
      <ContentLoader
        speed={2}
        width={Dw}
        height={Dh}
        backgroundColor={loaderBGColor}
        foregroundColor={loaderFGColor}>
          <Rect x="10" y="10" rx="5" ry="5" width={SIW} height={SIW} />
          <Rect x={SIW + 20} y="15" rx="2" ry="2" width={Dw / 1.5} height={4} />
          <Rect x={SIW + 20} y="27" rx="" ry="0" width={Dw / 1.5} height={2} />
          <Rect x={SIW + 20} y="35" rx="" ry="0" width={Dw / 1.7} height={2} />
          <Rect x={SIW + 20} y="42" rx="" ry="0" width={Dw / 2} height={2} />
          <Rect x="10" y={SIW + 30} rx="2" ry="2" width={Dw / 3} height={3} />
          <Rect x="10" y={SIW + 40} rx="2" ry="2" width={Dw / 2.5} height={4} />
          <Rect x={MW - (Dw / 3)} y={SIW + 30} rx="2" ry="2" width={Dw / 3} height={3} />
          <Rect x={MW - (Dw / 2.5)} y={SIW + 40} rx="2" ry="2" width={Dw / 2.5} height={4} />
          <Rect x="0" y={SIW + 55} rx="0" ry="0" width={Dw} height={1} />
          <Rect x="10" y={SIW + 75} rx="2" ry="2" width={Dw / 2} height={4} />
          <Rect x="10" y={SIW + 90} rx="0" ry="0" width={Dw - 30} height={2} />
          <Rect x="10" y={SIW + 95} rx="0" ry="0" width={Dw - 50} height={2} />
          <Rect x="10" y={SIW + 100} rx="0" ry="0" width={Dw - 60} height={2} />
          <Rect x="0" y={SIW + 120} rx="0" ry="0" width={Dw} height={1} />
          <Rect x="10" y={SIW + 140} rx="2" ry="2" width={Dw / 3} height={3} />
          <Rect x="10" y={SIW + 150} rx="2" ry="2" width={Dw / 2.5} height={4} />
          <Rect x={MW - (Dw / 3)} y={SIW + 140} rx="2" ry="2" width={Dw / 3} height={3} />
          <Rect  x={MW - (Dw / 2.5)} y={SIW + 150} rx="2" ry="2" width={Dw / 2.5} height={4} />
          <Rect x="0" y={SIW + 170} rx="0" ry="0" width={Dw} height={1} />
          <Rect x="10" y={SIW + 190} rx="2" ry="2" width={Dw / 5} height={4} />
          <Rect x="10" y={SIW + 200} rx="0" ry="0" width={Dw / 7} height={3} />
          <Rect x="0" y={SIW + 225} rx="0" ry="0" width={Dw} height={1} />
          <Rect x="10" y={SIW + 245} rx="1" ry="1" width={Dw / 3} height={3} />
          <Rect x="15" y={SIW + 260} rx="0" ry="0" width={ICW} height={1} />
          <Rect x="25" y={SIW + 270} rx="0" ry="0" width={Dw / 2} height={3} />
          <Rect x="25" y={SIW + 290} rx="0" ry="0" width={Dw / 4} height={2} />
          <Rect x="25" y={SIW + 295} rx="0" ry="0" width={Dw / 5} height={2} />
          <Rect x="15" y={SIW + 310} rx="0" ry="0" width={ICW} height={1} />
          <Rect x="15" y={SIW + 261} rx="0" ry="0" width={1} height={50} />
          <Rect x={ICW + 15} y={SIW + 261} rx="0" ry="0" width={1} height={50} />
      </ContentLoader>
    </View>
  );
};

export const HomeLoader = () => {
  const CRW = (Dw / 7) / 2;
  const SIW = Dw / 4;
  const SCW = Dw / 95;
  const CATW = Dw - 20;
  const CATVL = SIW + 20;
  const MSGXV = SIW + 30;
  return (
    <View style={cmnCSS}>
      <ContentLoader
        speed={2}
        width={'100%'}
        height={'100%'}
        backgroundColor={loaderBGColor}
        foregroundColor={loaderFGColor}>

        {arrayList.map((obj, index) => {
          const TW = CRW * 2;
          const WV = index === 0 ? CRW + 10 : (CRW * index) + (10 * (index + 1)) + (CRW * (index + 1));
          const OW = index === 0 ? 10 : (TW * index) + (10 * (index + 1));
          return (
            <>
              <Circle cx={WV} cy={CRW + 10} r={CRW} />
              <Rect x={OW} y={(CRW * 2) + 20} rx="2" ry="2" width={CRW * 2} height="4" />
            </>
          );
        })}

        {arrayList.map((obj, index) => {
          const cmnCal = ((CATVL + 1) * index) + (10 * index); 
          const TW = index === 0 ? (CRW * 2) + 40 : (CRW * 2) + 40 + cmnCal;
          const LRXV = index === 0 ? (CRW * 2) + 41 : 1 + TW;
          const IMGY = index === 0 ? (CRW * 2) + 50 : 10 + TW;
          const L1Y = index === 0 ? (CRW * 2) + 60 : 10 + IMGY;
          const L2Y = index === 0 ? (CRW * 2) + 75 : 15 + L1Y;
          const L3Y = index === 0 ? (CRW * 2) + 80 : 5 + L2Y;
          const L4Y = index === 0 ? (CRW * 2) + 85 : 5 + L3Y;
          const L5Y = index === 0 ? (CRW * 2) + 85 + (SIW / 3) : L4Y + (SIW / 3);
          const CRY = L5Y + 12;
          const SQW = L5Y + 8;
          const LLW = index === 0 ? (CRW * 2) + 60 + SIW : (CRW * 2) + 60 + SIW + cmnCal;
          return (
            <>
              <Rect x="10" y={TW} rx="0" ry="0" width={CATW} height={1} />
              <Rect x="10" y={LRXV} rx="0" ry="0" width={'1'} height={CATVL} />
              <Rect x={CATW + 9} y={LRXV} rx="0" ry="0" width={'1'} height={CATVL} />
              <Rect x="20" y={IMGY} rx="0" ry="0" width={SIW} height={SIW} />

              <Rect x={MSGXV} y={L1Y} rx="2" ry="2" width={Dw / 1.7} height="4" />
              <Rect x={MSGXV} y={L2Y} rx="0" ry="0" width={Dw / 1.8} height="2" />
              <Rect x={MSGXV} y={L3Y} rx="0" ry="0" width={Dw / 2} height="2" />
              <Rect x={MSGXV} y={L4Y} rx="0" ry="0" width={Dw / 2.5} height="2" />
              <Rect x={MSGXV} y={L5Y} rx="0" ry="0" width={Dw - (Dw / 4) - 50} height="1" />

              <Circle cx={SIW + 35} cy={CRY} r={SCW} />
              <Rect x={SIW + 35 + (Dw / 40)} y={SQW} rx="0" ry="0" width={Dw / 40} height={Dw / 40} />
              <Rect x={CATW - (Dw / 4)} y={SQW} rx="2" ry="2" width={Dw / 4} height={4} />
              <Rect x="10" y={LLW} rx="0" ry="0" width={CATW} height={1} />
            </>
          );
        })}
      </ContentLoader>
    </View>
  );
};

export const JobFeedLoader = () => {
  const TW = Dw - 20;
  const TTW = Dw / 2.5;
  const STW = Dw / 4;
  const BTNW = Dw / 4;
  const BTNH = Dw / 11;

  const VLine = 110 + BTNH;
  const BHLine = VLine + 2;
  return (
    <View style={cmnCSS}>
      <ContentLoader
        speed={2}
        width={Dw}
        height={Dh}
        backgroundColor={loaderBGColor}
        foregroundColor={loaderFGColor}>

        {arrayList.map((obj, index) => {
          const cmnCal = (BHLine * index) + (10 * index);
          const MW = index === 0 ? 0 : cmnCal;
          const LRXV = index === 0 ? 2 : 2 + MW;
          const T1 = index === 0 ? 10 : 10 + MW;
          const T2 = index === 0 ? 30 : 20 + T1;
          const T3 = index === 0 ? 40 : 10 + T2;
          const T4 = index === 0 ? 60 : 20 + T3;
          const T5 = index === 0 ? 70 : 10 + T4;
          const T6 = index === 0 ? 95 : 25 + T5;
          const LW = index === 0 ? 112 + (Dw / 11) : 112 + (Dw / 11) + cmnCal;

          return (
            <>
              <Rect x="10" y={MW} rx="0" ry="0" width={TW} height={2} />
              <Rect x="20" y={T1} rx="2" ry="2" width={TTW} height={4} />
              <Rect x={TW - STW} y={T1} rx="2" ry="2" width={STW} height={4} />
              <Rect x="20" y={T2} rx="0" ry="0" width={STW} height={1} />
              <Rect x="20" y={T3} rx="2" ry="2" width={TTW} height={4} />
              <Rect x="20" y={T4} rx="0" ry="0" width={STW} height={1} />
              <Rect x="20" y={T5} rx="2" ry="2" width={TTW} height={4} />
              <Rect x={TW - (STW)} y={T4} rx="0" ry="0" width={STW} height={1} />
              <Rect x={TW - TTW} y={T5} rx="2" ry="2" width={TTW} height={4} />
              <Rect x="20" y={T6} rx={(BTNH) / 2} ry={(BTNH) / 2} width={BTNW} height={BTNH} />
              <Rect x={TW - (BTNW + 20)} y={T6 + ((BTNH) / 2.5)} rx={0} ry={0} width={BTNW + 20} height={2} />
              <Rect x={TW - (BTNW - 10)} y={T6 + ((BTNH / 1.5))} rx={2} ry={2} width={BTNW - 10} height={4} />
              <Rect x="10" y={LRXV} rx="0" ry="0" width={2} height={VLine} />
              <Rect x={TW + 8} y={LRXV} rx="0" ry="0" width={2} height={VLine} />
              <Rect x="10" y={LW} rx="0" ry="0" width={TW} height={2} />
            </>
          );
        })}
      </ContentLoader>
    </View>
  );
};

export const DriverOrderDetailLoader = () => {
  const MW = Dw - 10;
  const SIW = Dw / 6;
  const ICW = Dw - 30;

  const CRW = Dw / 22;
  const HLW = (Dw - ((CRW * 2) * 3) - 20) / 2;
  return (
    <View style={cmnCSS}>
      <ContentLoader
        speed={2}
        width={Dw}
        height={Dh}
        backgroundColor={loaderBGColor}
        foregroundColor={loaderFGColor}>
          <Circle x={CRW + 10} cy={CRW + 15} r={CRW} />
          <Rect x={(CRW * 2) + 10} y={((CRW * 2) / 2) + 11} rx="0" ry="0" width={HLW} height={3} />
          <Circle x={(CRW * 3) + 10 + HLW} cy={CRW + 15} r={CRW} />
          <Rect x={(CRW * 4) + 10 + HLW} y={((CRW * 2) / 2) + 11} rx="0" ry="0" width={HLW} height={3} />
          <Circle x={(CRW * 5) + 10 + (HLW * 2)} cy={CRW + 15} r={CRW} />
          <Rect x={10} y={(CRW * 2) + 28} rx="0" ry="0" width={Dw / 6} height={3} />
          <Rect x={(CRW * 2) - 5 + HLW} y={(CRW * 2) + 28} rx="0" ry="0" width={Dw / 6} height={3} />
          <Rect x={(Dw - 10) - (Dw / 6)} y={(CRW * 2) + 28} rx="0" ry="0" width={Dw / 6} height={3} />
          <Rect x="10" y={SIW + 30} rx="2" ry="2" width={Dw / 3} height={3} />
          <Rect x="10" y={SIW + 40} rx="2" ry="2" width={Dw / 2.5} height={4} />
          <Rect x={MW - (Dw / 3)} y={SIW + 30} rx="2" ry="2" width={Dw / 3} height={3} />
          <Rect x={MW - (Dw / 2.5)} y={SIW + 40} rx="2" ry="2" width={Dw / 2.5} height={4} />
          <Rect x="0" y={SIW + 55} rx="0" ry="0" width={Dw} height={1} />
          <Rect x="10" y={SIW + 75} rx="2" ry="2" width={Dw / 2} height={4} />
          <Rect x="10" y={SIW + 90} rx="0" ry="0" width={Dw - 30} height={2} />
          <Rect x="10" y={SIW + 95} rx="0" ry="0" width={Dw - 50} height={2} />
          <Rect x="10" y={SIW + 100} rx="0" ry="0" width={Dw - 60} height={2} />
          <Rect x="0" y={SIW + 120} rx="0" ry="0" width={Dw} height={1} />
          <Rect x="10" y={SIW + 140} rx="2" ry="2" width={Dw / 3} height={3} />
          <Rect x="10" y={SIW + 150} rx="2" ry="2" width={Dw / 2.5} height={4} />
          <Rect x={MW - (Dw / 3)} y={SIW + 140} rx="2" ry="2" width={Dw / 3} height={3} />
          <Rect  x={MW - (Dw / 2.5)} y={SIW + 150} rx="2" ry="2" width={Dw / 2.5} height={4} />
          <Rect x="0" y={SIW + 170} rx="0" ry="0" width={Dw} height={1} />
          <Rect x="10" y={SIW + 190} rx="2" ry="2" width={Dw / 5} height={4} />
          <Rect x="10" y={SIW + 200} rx="0" ry="0" width={Dw / 7} height={3} />
          <Rect x="0" y={SIW + 225} rx="0" ry="0" width={Dw} height={1} />
          <Rect x="10" y={SIW + 245} rx="1" ry="1" width={Dw / 3} height={3} />
          <Rect x="15" y={SIW + 260} rx="0" ry="0" width={ICW} height={1} />
          <Rect x="25" y={SIW + 270} rx="0" ry="0" width={Dw / 2} height={3} />
          <Rect x="25" y={SIW + 290} rx="0" ry="0" width={Dw / 4} height={2} />
          <Rect x="25" y={SIW + 295} rx="0" ry="0" width={Dw / 5} height={2} />
          <Rect x="15" y={SIW + 310} rx="0" ry="0" width={ICW} height={1} />
          <Rect x="15" y={SIW + 261} rx="0" ry="0" width={1} height={50} />
          <Rect x={ICW + 15} y={SIW + 261} rx="0" ry="0" width={1} height={50} />
          <Rect x={15} y={SIW + 325} rx="0" ry="0" width={Dw / 3} height={3} />
          <Rect x={15} y={SIW + 333} rx="0" ry="0" width={Dw / 5} height={3} />
      </ContentLoader>
    </View>
  );
};

export const DriverOrderLoader = () => {
  const TABW = Dw / 5;
  const TABH = Dw / 11;

  const TW = Dw - 20;
  const TTW = Dw / 2.5;
  const STW = Dw / 4;
  const BTNW = Dw / 4;
  const BTNH = Dw / 11;

  const VLine = 110 + BTNH;
  const BHLine = VLine + 2;

  return (
    <View style={cmnCSS}>
      <ContentLoader
        speed={2}
        width={Dw}
        height={Dh}
        backgroundColor={loaderBGColor}
        foregroundColor={loaderFGColor}>

        {arrayList.map((obj, index) => {
          const XV = index === 0 ? 10 : (TABW * index) + (10 * (index + 1));
          return (
            <Rect x={XV} y="10" rx={TABH / 2} ry={TABH / 2} width={TABW} height={TABH} />
          );
        })}

        {arrayList.map((obj, index) => {
          const cmnCal = (BHLine * index) + (15 * (index + 1)) + TABH;
          const MW = index === 0 ? TABH + 20 : cmnCal;
          const LRXV = index === 0 ? TABH + 22 : 2 + MW;
          const T1 = index === 0 ? TABH + 30 : 10 + MW;
          const T2 = index === 0 ? TABH + 50 : 20 + T1;
          const T3 = index === 0 ? TABH + 60 : 10 + T2;
          const T4 = index === 0 ? TABH + 80 : 20 + T3;
          const T5 = index === 0 ? TABH + 90 : 10 + T4;
          const T6 = index === 0 ? TABH + 115 : 25 + T5;
          const LW = (T6 + 17) + (Dw / 11);

          return (
            <>
              <Rect x="10" y={MW} rx="0" ry="0" width={TW} height={2} />
              <Rect x="20" y={T1} rx="2" ry="2" width={TTW} height={4} />
              <Rect x={TW - STW} y={T1} rx="2" ry="2" width={STW} height={4} />
              <Rect x="20" y={T2} rx="0" ry="0" width={STW} height={1} />
              <Rect x="20" y={T3} rx="2" ry="2" width={TTW} height={4} />
              <Rect x="20" y={T4} rx="0" ry="0" width={STW} height={1} />
              <Rect x="20" y={T5} rx="2" ry="2" width={TTW} height={4} />
              <Rect x={TW - (STW)} y={T4} rx="0" ry="0" width={STW} height={1} />
              <Rect x={TW - TTW} y={T5} rx="2" ry="2" width={TTW} height={4} />
              <Rect x="20" y={T6} rx={(BTNH) / 2} ry={(BTNH) / 2} width={BTNW} height={BTNH} />
              <Rect x={TW - (BTNW)} y={T6 + ((BTNH) / 2)} rx={2} ry={2} width={BTNW} height={4} />
              <Rect x="10" y={LRXV} rx="0" ry="0" width={2} height={VLine} />
              <Rect x={TW + 8} y={LRXV} rx="0" ry="0" width={2} height={VLine} />
              <Rect x="10" y={LW} rx="0" ry="0" width={TW} height={2} />
            </>
          );
        })}
      </ContentLoader>
    </View>
  );
};


export const ReviewsLoader = (props) => (
  <View style={{flex: 1}}>
    <ContentLoader
      speed={2}
      width={Dw}
      height={Dh}
      viewBox="0 65 400 160"
      backgroundColor="#dcdcdc"
      foregroundColor="#f7f7f7"
      {...props}>
      <Rect x="10" y="30" rx="0" ry="0" width={'93%'} height="8" />
      <Rect x="10" y="30" rx="0" ry="0" width={'2%'} height="175" />
      <Rect x="10" y="200" rx="0" ry="0" width={'93%'} height="8" />
      <Rect x="375" y="30" rx="0" ry="0" width={'2%'} height="175" />

      <Circle cx="60" cy="80" r="20" />
      <Rect x="90" y="75" rx="0" ry="0" width={'50%'} height="8" />
      <Circle cx="50" cy="115" r="5" />
      <Circle cx="65" cy="115" r="5" />
      <Circle cx="80" cy="115" r="5" />
      <Circle cx="95" cy="115" r="5" />
      <Circle cx="110" cy="115" r="5" />
      <Rect x="120" y="113" rx="0" ry="0" width={'7%'} height="4" />
      <Rect x="40" y="130" rx="0" ry="0" width={'80%'} height="4" />
      <Rect x="40" y="140" rx="0" ry="0" width={'80%'} height="4" />
      <Rect x="40" y="150" rx="0" ry="0" width={'80%'} height="4" />
      <Rect x="315" y="180" rx="0" ry="0" width={'11%'} height="4" />

      <Rect x="10" y="220" rx="0" ry="0" width={'93%'} height="8" />
      <Rect x="10" y="220" rx="0" ry="0" width={'2%'} height="175" />
      <Rect x="10" y="390" rx="0" ry="0" width={'93%'} height="8" />
      <Rect x="375" y="220" rx="0" ry="0" width={'2%'} height="175" />

      <Circle cx="60" cy="270" r="20" />
      <Rect x="90" y="265" rx="0" ry="0" width={'50%'} height="8" />
      <Circle cx="50" cy="305" r="5" />
      <Circle cx="65" cy="305" r="5" />
      <Circle cx="80" cy="305" r="5" />
      <Circle cx="95" cy="305" r="5" />
      <Circle cx="110" cy="305" r="5" />
      <Rect x="120" y="303" rx="0" ry="0" width={'7%'} height="4" />
      <Rect x="40" y="325" rx="0" ry="0" width={'80%'} height="4" />
      <Rect x="40" y="335" rx="0" ry="0" width={'80%'} height="4" />
      <Rect x="40" y="345" rx="0" ry="0" width={'80%'} height="4" />
      <Rect x="315" y="375" rx="0" ry="0" width={'11%'} height="4" />

      <Rect x="10" y="410" rx="0" ry="0" width={'93%'} height="8" />
      <Rect x="10" y="410" rx="0" ry="0" width={'2%'} height="175" />
      <Rect x="10" y="580" rx="0" ry="0" width={'93%'} height="8" />
      <Rect x="375" y="410" rx="0" ry="0" width={'2%'} height="175" />

      <Circle cx="60" cy="455" r="20" />
      <Rect x="90" y="450" rx="0" ry="0" width={'50%'} height="8" />
      <Circle cx="50" cy="490" r="5" />
      <Circle cx="65" cy="490" r="5" />
      <Circle cx="80" cy="490" r="5" />
      <Circle cx="95" cy="490" r="5" />
      <Circle cx="110" cy="490" r="5" />
      <Rect x="120" y="488" rx="0" ry="0" width={'7%'} height="4" />
      <Rect x="40" y="505" rx="0" ry="0" width={'80%'} height="4" />
      <Rect x="40" y="515" rx="0" ry="0" width={'80%'} height="4" />
      <Rect x="40" y="525" rx="0" ry="0" width={'80%'} height="4" />
      <Rect x="315" y="555" rx="0" ry="0" width={'11%'} height="4" />
      <Rect x="200" y="650" rx="25" ry="25" width={'45%'} height="48" />
    </ContentLoader>
  </View>
);

export const OtherCategorLoader = (props) => (
  <View style={{flex: 1}}>
    <ContentLoader
      speed={2}
      width={Dw}
      height={Dh}
      viewBox="0 60 400 160"
      backgroundColor="#dcdcdc"
      foregroundColor="#f7f7f7"
      {...props}>
      <Rect x="10" y="0" rx="20" ry="20" width={Dw / 8} height="40" />
      <Rect x="70" y="0" rx="20" ry="20" width={Dw / 3.5} height="40" />
      <Rect x="200" y="0" rx="20" ry="20" width={Dw / 5.5} height="40" />
      <Rect x="280" y="0" rx="20" ry="20" width={Dw / 5} height="40" />

      <Rect x="0" y="50" rx="0" ry="0" width={'100%'} height="2" />
      <Rect x="200" y="50" rx="0" ry="0" width="2" height={Dh} />
      <Rect x="0" y="350" rx="0" ry="0" width={'100%'} height="2" />

      <Rect x="50" y="80" rx="0" ry="0" width={Dw / 5} height={Dh / 8} />
      <Rect x="10" y="250" rx="0" ry="0" width={Dw / 3} height="2" />
      <Rect x="10" y="260" rx="0" ry="0" width={Dw / 7} height="2" />
      <Circle cx="20" cy="280" r="8" />
      <Rect x="40" y="280" rx="0" ry="0" width={Dw / 7} height="2" />
      <Circle cx="20" cy="310" r="5" />
      <Circle cx="35" cy="310" r="5" />
      <Circle cx="50" cy="310" r="5" />
      <Circle cx="65" cy="310" r="5" />
      <Circle cx="80" cy="310" r="5" />
      <Rect x="10" y="330" rx="0" ry="0" width={Dw / 7} height="5" />

      <Rect x="270" y="80" rx="0" ry="0" width={Dw / 5} height={Dh / 8} />
      <Rect x="220" y="250" rx="0" ry="0" width={Dw / 3} height="2" />
      <Rect x="220" y="260" rx="0" ry="0" width={Dw / 7} height="2" />
      <Circle cx="240" cy="280" r="8" />
      <Rect x="260" y="280" rx="0" ry="0" width={Dw / 7} height="2" />
      <Circle cx="240" cy="310" r="5" />
      <Circle cx="255" cy="310" r="5" />
      <Circle cx="270" cy="310" r="5" />
      <Circle cx="285" cy="310" r="5" />
      <Circle cx="300" cy="310" r="5" />
      <Rect x="220" y="330" rx="0" ry="0" width={Dw / 7} height="5" />

      <Rect x="50" y="390" rx="0" ry="0" width={Dw / 5} height={Dh / 8} />
      <Rect x="10" y="570" rx="0" ry="0" width={Dw / 3} height="2" />
      <Rect x="10" y="580" rx="0" ry="0" width={Dw / 7} height="2" />
      <Circle cx="20" cy="600" r="8" />
      <Rect x="40" y="600" rx="0" ry="0" width={Dw / 7} height="2" />
      <Circle cx="20" cy="620" r="5" />
      <Circle cx="35" cy="620" r="5" />
      <Circle cx="50" cy="620" r="5" />
      <Circle cx="65" cy="620" r="5" />
      <Circle cx="80" cy="620" r="5" />
      <Rect x="10" y="640" rx="0" ry="0" width={Dw / 7} height="5" />

      <Rect x="270" y="390" rx="0" ry="0" width={Dw / 5} height={Dh / 8} />
      <Rect x="220" y="570" rx="0" ry="0" width={Dw / 3} height="2" />
      <Rect x="220" y="580" rx="0" ry="0" width={Dw / 7} height="2" />
      <Circle cx="240" cy="600" r="8" />
      <Rect x="260" y="600" rx="0" ry="0" width={Dw / 7} height="2" />
      <Circle cx="240" cy="620" r="5" />
      <Circle cx="255" cy="620" r="5" />
      <Circle cx="270" cy="620" r="5" />
      <Circle cx="285" cy="620" r="5" />
      <Circle cx="300" cy="620" r="5" />
      <Rect x="220" y="640" rx="0" ry="0" width={Dw / 7} height="5" />
    </ContentLoader>
  </View>
);

export const FavoriteLoader = (props) => (
  <View style={{flex: 1}}>
    <ContentLoader
      speed={2}
      width={Dw}
      height={Dh}
      viewBox="0 55 400 160"
      backgroundColor="#dcdcdc"
      foregroundColor="#f7f7f7"
      {...props}>
      <Rect x="200" y="0" rx="0" ry="0" width="1" height="100%" />
      <Rect x="0" y="300" rx="0" ry="0" width={'100%'} height="1" />
      <Rect x="10" y="10" rx="0" ry="0" width={Dw / 2.4} height="150" />
      {/* <Rect x="10" y="200" rx="0" ry="0" width={Dw/10} height="8" />
      <Rect x="140" y="200" rx="0" ry="0" width={Dw/10} height="8" /> */}
      <Rect x="10" y="230" rx="0" ry="0" width={Dw / 4} height="5" />
      <Rect x="10" y="245" rx="0" ry="0" width={Dw / 3} height="5" />
      <Rect x="10" y="260" rx="0" ry="0" width={Dw / 3} height="5" />

      {/* <Circle cx="20" cy="270" r="9" />
      <Rect x="40" y="265" rx="0" ry="0" width={Dw/5} height="5" /> */}

      <Rect x="220" y="10" rx="0" ry="0" width={Dw / 2.4} height="150" />
      {/* <Rect x="220" y="200" rx="0" ry="0" width={Dw/10} height="8" />
      <Rect x="350" y="200" rx="0" ry="0" width={Dw/10} height="8" /> */}
      <Rect x="220" y="230" rx="0" ry="0" width={Dw / 4} height="5" />
      <Rect x="220" y="245" rx="0" ry="0" width={Dw / 3} height="5" />
      <Rect x="220" y="260" rx="0" ry="0" width={Dw / 3} height="5" />

      {/* <Circle cx="230" cy="270" r="9" />
      <Rect x="250" y="265" rx="0" ry="0"width={Dw/7}  height="5" /> */}

      <Rect x="10" y="320" rx="0" ry="0" width={Dw / 2.4} height="150" />
      {/* <Rect x="10" y="510" rx="0" ry="0" width={Dw/10} height="8" />
      <Rect x="140" y="510" rx="0" ry="0" width={Dw/10} height="8" /> */}
      <Rect x="10" y="545" rx="0" ry="0" width={Dw / 4} height="5" />
      <Rect x="10" y="560" rx="0" ry="0" width={Dw / 3} height="5" />
      <Rect x="10" y="575" rx="0" ry="0" width={Dw / 3} height="5" />

      {/* <Circle cx="20" cy="570" r="9" />
      <Rect x="40" y="565" rx="0" ry="0"width={Dw/7}  height="5" /> */}

      <Rect x="220" y="320" rx="0" ry="0" width={Dw / 2.4} height="150" />
      {/* <Rect x="220" y="510" rx="0" ry="0" width={Dw/10} height="8" />
      <Rect x="350" y="510" rx="0" ry="0" width={Dw/10} height="8" /> */}
      <Rect x="220" y="545" rx="0" ry="0" width={Dw / 4} height="5" />
      <Rect x="220" y="560" rx="0" ry="0" width={Dw / 3} height="5" />
      <Rect x="220" y="575" rx="0" ry="0" width={Dw / 3} height="5" />

      {/* <Circle cx="230" cy="570" r="9" />
      <Rect x="250" y="565" rx="0" ry="0"width={Dw/7}  height="5" /> */}

      <Rect x="0" y="600" rx="0" ry="0" width={'100%'} height="1" />

      <Rect x="10" y="620" rx="0" ry="0" width={Dw / 2.4} height="150" />

      <Rect x="220" y="620" rx="0" ry="0" width={Dw / 2.4} height="150" />
    </ContentLoader>
  </View>
);

export const ProductLoader = (props) => (
  <View style={{flex: 1}}>
    <ContentLoader
      speed={2}
      width={Dw}
      height={Dh}
      viewBox="0 60 400 160"
      backgroundColor="#dcdcdc"
      foregroundColor="#f7f7f7"
      {...props}>
      <Rect x="10" y="10" rx="0" ry="0" width={Dw / 5} height="5" />
      <Rect x="330" y="10" rx="0" ry="0" width={Dw / 6.5} height="5" />

      <Rect x="10" y="30" rx="0" ry="0" width={'30%'} height="2" />
      <Rect x="10" y="30" rx="0" ry="0" width="3" height="150" />
      <Rect x="130" y="30" rx="0" ry="0" width="3" height="152" />
      <Rect x="10" y="180" rx="0" ry="0" width={'30%'} height="2" />
      <Rect x="35" y="50" rx="0" ry="0" width={Dw / 6} height="70" />
      <Rect x="30" y="160" rx="0" ry="0" width={Dw / 5} height="5" />

      <Rect x="10" y="200" rx="0" ry="0" width={Dw / 5} height="4" />

      <Rect x="160" y="30" rx="0" ry="0" width={'30%'} height="2" />
      <Rect x="160" y="30" rx="0" ry="0" width="3" height="150" />
      <Rect x="280" y="30" rx="0" ry="0" width="3" height="152" />
      <Rect x="160" y="180" rx="0" ry="0" width={'30%'} height="2" />
      <Rect x="185" y="50" rx="0" ry="0" width={Dw / 6} height="70" />
      <Rect x="180" y="160" rx="0" ry="0" width={Dw / 5} height="5" />

      <Rect x="310" y="30" rx="0" ry="0" width={'30%'} height="2" />
      <Rect x="310" y="30" rx="0" ry="0" width="3" height="150" />
      <Rect x="430" y="30" rx="0" ry="0" width="3" height="152" />
      <Rect x="310" y="180" rx="0" ry="0" width={'30%'} height="2" />
      <Rect x="335" y="50" rx="0" ry="0" width={Dw / 6} height="70" />
      <Rect x="330" y="160" rx="0" ry="0" width={Dw / 5} height="5" />

      <Rect x="330" y="200" rx="0" ry="0" width={Dw / 6.5} height="4" />

      <Rect x="10" y="230" rx="0" ry="0" width={'30%'} height="2" />
      <Rect x="10" y="230" rx="0" ry="0" width="3" height="150" />
      <Rect x="130" y="230" rx="0" ry="0" width="3" height="154" />
      <Rect x="10" y="180" rx="0" ry="0" width={'30%'} height="2" />
      <Rect x="35" y="250" rx="0" ry="0" width={Dw / 6} height="70" />
      <Rect x="30" y="360" rx="0" ry="0" width={Dw / 5} height="5" />

      <Rect x="10" y="380" rx="0" ry="0" width={'30%'} height="4" />
      <Rect x="10" y="410" rx="0" ry="0" width={Dw / 6.5} height="4" />

      <Rect x="160" y="230" rx="0" ry="0" width={'30%'} height="2" />
      <Rect x="160" y="230" rx="0" ry="0" width="3" height="150" />
      <Rect x="280" y="230" rx="0" ry="0" width="3" height="154" />
      <Rect x="185" y="250" rx="0" ry="0" width={Dw / 6} height="70" />
      <Rect x="180" y="360" rx="0" ry="0" width={Dw / 5} height="5" />

      <Rect x="160" y="380" rx="0" ry="0" width={'30%'} height="4" />

      <Rect x="310" y="230" rx="0" ry="0" width={'30%'} height="2" />
      <Rect x="310" y="230" rx="0" ry="0" width="3" height="150" />
      <Rect x="430" y="230" rx="0" ry="0" width="3" height="154" />
      <Rect x="335" y="250" rx="0" ry="0" width={Dw / 6} height="70" />
      <Rect x="330" y="360" rx="0" ry="0" width={Dw / 5} height="5" />

      <Rect x="310" y="380" rx="0" ry="0" width={'30%'} height="4" />
      <Rect x="330" y="410" rx="0" ry="0" width={Dw / 6.5} height="4" />

      <Rect x="10" y="430" rx="0" ry="0" width={'30%'} height="2" />
      <Rect x="10" y="430" rx="0" ry="0" width="3" height="150" />
      <Rect x="130" y="430" rx="0" ry="0" width="3" height="154" />
      <Rect x="10" y="180" rx="0" ry="0" width={'30%'} height="2" />
      <Rect x="35" y="450" rx="0" ry="0" width={Dw / 6} height="70" />
      <Rect x="30" y="560" rx="0" ry="0" width={Dw / 5} height="5" />

      <Rect x="10" y="580" rx="0" ry="0" width={'30%'} height="4" />

      <Rect x="160" y="430" rx="0" ry="0" width={'30%'} height="2" />
      <Rect x="160" y="430" rx="0" ry="0" width="3" height="150" />
      <Rect x="280" y="430" rx="0" ry="0" width="3" height="154" />
      <Rect x="185" y="450" rx="0" ry="0" width={Dw / 6} height="70" />
      <Rect x="180" y="560" rx="0" ry="0" width={Dw / 5} height="5" />

      <Rect x="160" y="580" rx="0" ry="0" width={'30%'} height="4" />

      <Rect x="310" y="430" rx="0" ry="0" width={'30%'} height="2" />
      <Rect x="310" y="430" rx="0" ry="0" width="3" height="150" />
      <Rect x="430" y="430" rx="0" ry="0" width="3" height="154" />
      <Rect x="335" y="450" rx="0" ry="0" width={Dw / 5} height="70" />
      <Rect x="330" y="560" rx="0" ry="0" width={Dw / 5} height="5" />

      <Rect x="310" y="580" rx="0" ry="0" width={'30%'} height="4" />

      <Rect x="10" y="600" rx="0" ry="0" width={Dw / 6} height="4" />
      <Rect x="325" y="600" rx="0" ry="0" width={Dw / 6} height="4" />
    </ContentLoader>
  </View>
);

export const DealsLoader = (props) => (
  <View style={{flex: 1}}>
    <ContentLoader
      speed={2}
      width={Dw}
      height={Dh}
      viewBox="0 60 400 160"
      backgroundColor="#dcdcdc"
      foregroundColor="#f7f7f7"
      {...props}>
      <Rect x="10" y="0" rx="5" ry="5" width={'45%'} height={'180'} />
      <Rect x="210" y="0" rx="5" ry="5" width={'45%'} height={'180'} />
      <Rect x="10" y="200" rx="5" ry="5" width={'45%'} height={'180'} />
      <Rect x="210" y="200" rx="5" ry="5" width={'45%'} height={'180'} />
      <Rect x="10" y="410" rx="5" ry="5" width={'95%'} height={'180'} />
    </ContentLoader>
  </View>
);

export const DealsDetailsLoader = (props) => (
  <View style={{flex: 1}}>
    <ContentLoader
      speed={2}
      width={Dw}
      height={Dh}
      viewBox="0 65 400 160"
      backgroundColor="#dcdcdc"
      foregroundColor="#f7f7f7"
      {...props}>
      <Rect x="0" y="0" rx="0" ry="0" width={'100%'} height="250" />
      <Rect x="140" y="280" rx="0" ry="0" width={Dw / 3.5} height="8" />
      <Rect x="90" y="320" rx="30" ry="30" width={'50%'} height="50" />
      <Rect x="10" y="400" rx="0" ry="0" width={Dw / 2.5} height="4" />
      <Rect x="10" y="420" rx="0" ry="0" width={Dw / 2.5} height="4" />
      <Rect x="225" y="420" rx="0" ry="0" width={Dw / 2.5} height="4" />
      <Rect x="10" y="440" rx="0" ry="0" width={'96%'} height="1" />
      <Rect x="10" y="460" rx="0" ry="0" width={'96%'} height={'140'} />
      <Rect x="10" y="610" rx="0" ry="0" width={'96%'} height={'140'} />
    </ContentLoader>
  </View>
);

export const BrandsLoader = (props) => (
  <View style={{flex: 1}}>
    <ContentLoader
      speed={2}
      width={Dw}
      height={Dh}
      viewBox="0 60 400 160"
      backgroundColor="#dcdcdc"
      foregroundColor="#f7f7f7"
      {...props}>
      <Rect x="10" y="0" rx="0" ry="0" width={'46%'} height="1" />
      <Rect x="10" y="0" rx="0" ry="0" width="1" height={'260'} />
      <Rect x="10" y="260" rx="0" ry="0" width={'46%'} height="1" />
      <Rect x="192" y="0" rx="0" ry="0" width="1" height={'260'} />
      <Rect x="50" y="30" rx="0" ry="0" width={Dw / 4} height={'120'} />
      <Rect x="20" y="225" rx="0" ry="0" width={Dw / 7} height="5" />
      <Rect x="20" y="240" rx="0" ry="0" width={Dw / 4.5} height="5" />
      <Rect x="130" y="240" rx="0" ry="0" width={Dw / 7.5} height="5" />

      <Rect x="212" y="0" rx="0" ry="0" width={'46%'} height="1" />
      <Rect x="212" y="0" rx="0" ry="0" width="1" height={'260'} />
      <Rect x="212" y="260" rx="0" ry="0" width={'46%'} height="1" />
      <Rect x="395" y="0" rx="0" ry="0" width="1" height={'260'} />
      <Rect x="260" y="30" rx="0" ry="0" width={Dw / 4} height={'120'} />
      <Rect x="222" y="225" rx="0" ry="0" width={Dw / 7} height="5" />
      <Rect x="222" y="240" rx="0" ry="0" width={Dw / 4.5} height="5" />
      <Rect x="330" y="240" rx="0" ry="0" width={Dw / 7.5} height="5" />

      <Rect x="10" y="280" rx="0" ry="0" width={'46%'} height="1" />
      <Rect x="10" y="280" rx="0" ry="0" width="1" height={'260'} />
      <Rect x="10" y="540" rx="0" ry="0" width={'46%'} height="1" />
      <Rect x="192" y="280" rx="0" ry="0" width="1" height={'260'} />
      <Rect x="50" y="310" rx="0" ry="0" width={Dw / 4} height={'120'} />
      <Rect x="20" y="505" rx="0" ry="0" width={Dw / 7} height="5" />
      <Rect x="20" y="520" rx="0" ry="0" width={Dw / 4.5} height="5" />
      <Rect x="130" y="520" rx="0" ry="0" width={Dw / 7.5} height="5" />

      <Rect x="212" y="280" rx="0" ry="0" width={'46%'} height="1" />
      <Rect x="212" y="280" rx="0" ry="0" width="1" height={'260'} />
      <Rect x="212" y="540" rx="0" ry="0" width={'46%'} height="1" />
      <Rect x="395" y="280" rx="0" ry="0" width="1" height={'260'} />
      <Rect x="260" y="310" rx="0" ry="0" width={Dw / 4} height={'120'} />
      <Rect x="222" y="505" rx="0" ry="0" width={Dw / 7} height="5" />
      <Rect x="222" y="520" rx="0" ry="0" width={Dw / 4.5} height="5" />
      <Rect x="330" y="520" rx="0" ry="0" width={Dw / 7.5} height="5" />

      <Rect x="10" y="560" rx="0" ry="0" width={'46%'} height="1" />
      <Rect x="10" y="560" rx="0" ry="0" width="1" height={'260'} />
      <Rect x="10" y="800" rx="0" ry="0" width={'46%'} height="1" />
      <Rect x="192" y="560" rx="0" ry="0" width="1" height={'260'} />
      <Rect x="50" y="580" rx="0" ry="0" width={Dw / 4} height={'120'} />

      <Rect x="212" y="560" rx="0" ry="0" width={'46%'} height="1" />
      <Rect x="212" y="560" rx="0" ry="0" width="1" height={'260'} />
      <Rect x="212" y="800" rx="0" ry="0" width={'46%'} height="1" />
      <Rect x="395" y="560" rx="0" ry="0" width="1" height={'260'} />
      <Rect x="260" y="580" rx="0" ry="0" width={Dw / 4} height={'120'} />
    </ContentLoader>
  </View>
);




