import {View, StyleSheet, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import AIcon from 'react-native-vector-icons/AntDesign';
import Swipeable from 'react-native-swipeable';
import React, {useState} from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import Text from '../Text';
import {BaseColor} from '../../config/theme';
import {checkObject} from '../../utils/commonFunction';

const styles = StyleSheet.create({
  mainCardView: {
    flex: 1,
    padding: 10,
    elevation: 5,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    borderRadius: 5,
    shadowRadius: 3.84,
    shadowOpacity: 0.25,
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 10,
    justifyContent: 'space-between',
    shadowColor: BaseColor.blackColor,
    backgroundColor: BaseColor.whiteColor,
  },
  P10: {
    flex: 1,
    padding: 10,
  },
  cmnFlex: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  swipeCon: {
    width: '20%',
    height: '100%',
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: BaseColor.ThemeRedColor,
  },
  selfCon: {
    alignSelf: 'center',
  },
  cardType: {
    width: '20%',
    backgroundColor: '#f9f9f9',
    padding: 10,
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: BaseColor.grayColor,
    borderRadius: 5,
  },
});

export default function CPaymentCard(props) {
  const [currentlyOpenSwipeable, setCurrentlyOpenSwipeable] = useState(null);
  const {data, onDelete} = props;
  const customerName = checkObject(data, 'customer');
  const expYear = checkObject(data, 'exp_year');
  const expMonth = checkObject(data, 'exp_month');
  const last4 = checkObject(data, 'last4');
  const brand = checkObject(data, 'brand');

  function onOpen(event, gestureState, swipeable) {
    if (currentlyOpenSwipeable && currentlyOpenSwipeable !== swipeable) {
      currentlyOpenSwipeable.recenter();
    }
    setCurrentlyOpenSwipeable(swipeable);
  }

  function onClose() {
    setCurrentlyOpenSwipeable(null);
  }

  return (
    <Swipeable
      rightButtons={[
        <TouchableOpacity
          activeOpacity={0.8}
          onPress={() => {
            onDelete();
          }}
          style={styles.swipeCon}>
          <Icon
            style={styles.selfCon}
            name="trash-sharp"
            size={20}
            color={BaseColor.whiteColor}
          />
        </TouchableOpacity>,
      ]}
      onRightButtonsOpenRelease={onOpen}
      onRightButtonsCloseRelease={onClose}>
      <View style={styles.mainCardView}>
        <View style={styles.P10}>
          <Text>{`**** **** **** ${last4}`}</Text>
          <View style={styles.cmnFlex}>
            <Text caption1 textAlignCenter>
              {'VALID THRU '}
            </Text>
            <Text bold>{`  ${expMonth} / ${expYear}`}</Text>
          </View>
        </View>
        <View style={styles.cardType}>
          <Text numberOfLines={1} textAlignCenter bold body2>
            {_.toUpper(brand)}
          </Text>
        </View>
      </View>
    </Swipeable>
  );
}

CPaymentCard.propTypes = {
  data: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  onDelete: PropTypes.func,
};

CPaymentCard.defaultProps = {
  data: {},
  onDelete: () => {},
};
