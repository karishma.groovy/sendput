import React, {useState} from 'react';
import Icon from 'react-native-vector-icons/AntDesign';
import {
  View,
  Modal,
  View,
  KeyboardAvoidingView,
  StyleSheet,
  Keyboard,
} from 'react-native';
import {useSelector} from 'react-redux';
import Button from '../../components/Button/index';
import Text from '../../components/Text/index';
import TextInput from '../../components/TextInput/index';
import _ from 'lodash';
import CAlert from '../../components/CAlert';
import {BaseSetting} from '../../config/setting';
import {BaseColor} from '../../config/theme';
import {getApiDataProgress} from '../../utils/apiHelper';
import {checkObject} from '../../utils/commonFunction';
import {StaticAlertMsg, StaticHeader} from '../../config/StaticAlertMsg';

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  modalView: {
    backgroundColor: BaseColor.whiteColor,
    borderTopLeftRadius: 40,
    width: BaseSetting.nWidth,
    padding: 20,
    alignItems: 'center',
    shadowColor: BaseColor.blackColor,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  modalTopEmptyView: {
    width: BaseSetting.nWidth / 6,
    height: 5,
    borderRadius: 10,
    backgroundColor: BaseColor.grayColor,
    marginBottom: 20,
  },
  alignCenter: {
    alignSelf: 'center',
  },
  starView: {
    flexDirection: 'row',
    paddingVertical: 20,
  },
  txt: {
    paddingBottom: 20,
  },
  paddingR2: {
    paddingRight: 5,
  },
});

const ReviewModal = (props) => {
  const {ismodalVisible, dismiss, navigation} = props;
  const [loader, setLoader] = useState(false);
  const [reviewMessage, setReviewMessage] = useState('');
  console.log(
    '🚀 ~ file: ReviewModal.js ~ line 66 ~ ReviewModal ~ reviewMessage',
    reviewMessage,
  );
  const {isConnected, userData} = useSelector((state) => state.auth);
  const userToken = checkObject(userData, 'access_token');
  const userId = checkObject(userData, 'id');
  const header = {
    Authorization: `Bearer ${userToken}`,
  };

  function Validate() {
    console.log('function called');
    let valid = true;
    if (_.isEmpty(reviewMessage)) {
      valid = false;
      CAlert(StaticAlertMsg.fillAllData, StaticHeader.Alert);
      return false;
    }
    if (_.trim(reviewMessage).length >= 255) {
      valid = false;
      CAlert('Message should be of 255 chars only');
      return false;
    }
    if (valid === true) {
      Keyboard.dismiss();
      console.log('valid');
      setLoader(true);
      setTimeout(() => {
        reviewActions();
      }, 100);
    }
  }

  async function reviewActions() {
    Keyboard.dismiss();
    setLoader(true);
    if (isConnected === true) {
      const updatedData = {
        'Feedback[order_id]': 39,
        'Feedback[product_id]': 66,
        'Feedback[rating]': 3,
        'Feedback[comment]': reviewMessage,
      };

      try {
        let endPoint = BaseSetting.endpoints.order_review;
        const response = await getApiDataProgress(
          navigation,
          endPoint,
          'POST',
          updatedData,
          header,
        );
        if (
          _.isObject(response) &&
          !_.isEmpty(response) &&
          response.success === true
        ) {
          const msg = checkObject(response, 'message');
          CAlert(msg, StaticHeader.Success, () => {
            setReviewMessage('');
            setLoader(false);
            setModalVisible(false);
          });
        } else {
          const errorMessage = checkObject(response, 'message');
          CAlert(errorMessage, StaticHeader.Oops, () => {
            setLoader(false);
          });
        }
      } catch (err) {
        console.log('Catch Part', err);
        CAlert(StaticAlertMsg.someThing_Went_Wrong, StaticHeader.Oops, () => {
          setLoader(false);
        });
      }
    } else {
      CAlert(StaticAlertMsg.Network_Problem, StaticHeader.Oops, () => {
        setLoader(false);
      });
    }
  }

  const createYellowStar = (no) => {
    let stars = [];

    for (let i = 0; i < no; i++) {
      stars.push(
        <Icon
          name="star"
          size={12}
          color={BaseColor.ThemeYellow}
          style={styles.paddingR2}
        />,
      );
    }
    return stars;
  };

  const createWhiteStar = (no, size) => {
    let stars = [];

    for (let i = 0; i < no; i++) {
      stars.push(
        <Icon
          name="staro"
          size={size}
          color={BaseColor.grayColor}
          style={styles.paddingR2}
        />,
      );
    }
    return stars;
  };

  return (
    <Modal
      transparent={true}
      statusBarTranslucent
      animationType="slide"
      visible={ismodalVisible}
      onRequestClose={() => {
        dismiss();
      }}>
      <View style={styles.centeredView}>
        <KeyboardAvoidingView>
          <View style={styles.modalView}>
            <View style={styles.modalTopEmptyView} />
            <Text
              bold
              body1
              blackColor
              textAlignCenter
              style={styles.alignCenter}>
              What is your review?
            </Text>
            <View style={styles.starView}>{createWhiteStar(5, 36)}</View>
            <Text
              style={styles.txt}
              semibold
              body1
              blackColor
              textAlignCenter
              numberOfLines={2}>
              Please share your opinion{'\n'}
              about the product
            </Text>
            <TextInput
              placeholder="Your reviews"
              onChangeText={(t) => {
                setReviewMessage(t);
              }}
              value={reviewMessage}
              keyboardType={'default'}
              textArea
              otherCon={{
                borderWidth: 0,
                backgroundColor: BaseColor.whiteColor,
                elevation: 5,
                borderRadius: 0,
                shadowColor: BaseColor.blackColor,
                shadowOffset: {
                  width: 0,
                  height: 2,
                },
                shadowOpacity: 0.25,
                shadowRadius: 3.84,
              }}
            />
            <Button
              loading={loader}
              buttonText={'Send Review'}
              onPress={() => {
                Validate();
              }}
            />
          </View>
        </KeyboardAvoidingView>
      </View>
    </Modal>
  );
};

export default ReviewModal;
