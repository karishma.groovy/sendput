import React, {useEffect} from 'react';
import Icon from 'react-native-vector-icons/Ionicons';
import PropTypes from 'prop-types';
import {TouchableOpacity, View, StyleSheet} from 'react-native';
import {BaseColor} from '../../config';
import Text from '@components/Text';

const styles = StyleSheet.create({
  contain: {
    height: 55,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: '#0000',
    position: 'relative',
  },
  contentLeft: {
    width: '15%',
    height: 55,
    justifyContent: 'center',
    position: 'absolute',
    top: 0,
    left: 20,
    zIndex: 10,
    backgroundColor: '#0000',
  },
  contentCenter: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  lnrGrnd: {
    height: 55,
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  txtInputView: {
    flex: 1,
    paddingRight: 15,
  },
  icon: {
    width: '10%',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
export default function TopBars(props) {
  const {
    style,
    styleContentLeft,
    styleContentCenter,
    title,
    onPressLeft,
    otherCss,
    leftIcon,
  } = props;
  return (
    <View style={[styles.contain, style]}>
      {leftIcon ? (
        <TouchableOpacity
          activeOpacity={0.8}
          style={[styles.contentLeft, styleContentLeft]}
          onPress={onPressLeft}>
          <Icon name="arrow-back" size={25} color={BaseColor.blackColor} />
        </TouchableOpacity>
      ) : null}

      <View style={[styles.contentCenter, styleContentCenter]}>
        <Text headline bold numberOfLines={1} style={otherCss}>
          {title}
        </Text>
      </View>
    </View>
  );
}

TopBars.propTypes = {};

TopBars.defaultProps = {};
