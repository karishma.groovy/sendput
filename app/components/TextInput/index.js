import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import _ from 'lodash';
import {
  Button,
  StyleSheet,
  Platform,
  View,
  Modal,
  Image,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';
import Icon from 'react-native-vector-icons/FontAwesome';
import CountryPicker from 'react-native-country-picker-modal';
import { BaseColor } from "../../config/theme";
import { FontFamily } from '../../config/typography';
import Text from '../Text/index';
import {CustomIcon} from '../../config/icons';
import IconR from 'react-native-vector-icons/AntDesign';
import {Images} from '../../config/images';
import {BaseSetting} from '../../config';

const isIOS = Platform.OS === 'ios';
const nHeight = BaseSetting.nHeight;

const styles = StyleSheet.create({
  mainCon: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderColor: '#000',
    borderRadius: 25,
    overflow: 'hidden',
    backgroundColor: '#0000',
    marginTop: 10,
  },
  leftIconView: {
    width: '14%',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#0000',
  },
  leftIconSty: {
    color: BaseColor.ThemeBlue,
    backgroundColor: '#0000',
  },
  inputCon: {
    flex: 1,
    fontFamily: FontFamily.medium,
    fontSize: 16,
    letterSpacing:1,
    backgroundColor: '#0000',
    paddingRight: 20,
  },
});
class CInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      datePickerVisible: false,
      datePickerMode: props.mode || 'date',
      focused: false,
      openPicker: false,
      phoneState: {
        cca2: 'US',
        pCode: '1',
      },
    };
  }

  onFocus() {
    this.setState({ focused: true }, () => {
      if (this.props.onFocus) {
        this.props.onFocus();
      }
    });
  }

  onBlur() {
    this.setState({ focused: false }, () => {
      if (this.props.onBlur) {
        this.props.onBlur();
      }
    });
  }

  blur = () => {
    if (this.input) {
      this.input.blur();
    }
  };

  focus() {
    if (this.input !== undefined && this.input !== null) {
      if (this.props.datePicker) {
        this.input.onPressDate();
      } else {
        this.input.focus();
      }
    }
  }

  handlePicker = (val) => {
  console.log("🚀 ~ file: index.js ~ line 83 ~ CInput ~ val", val);
  let obj = {
    cca2: _.has(val, 'cca2') ? val.cca2 : 'US',
    pCode: _.has(val, 'callingCode') && !_.isEmpty(val.callingCode) ? val.callingCode[0] : '1',
  };
    this.setState({ openPicker: false, phoneState: obj });
  };

  onDatePickerPress = () => {
    this.setState({
      datePickerVisible: true,
    });
  };

  onDateChange = (dDate) => {
    const { type, nativeEvent: { timestamp } } = dDate;
    const { datePickerMode } = this.state;
    const { mode, onChangeText } = this.props;

    if (!isIOS && type !== 'set') return;
    if (!isIOS) {
      if (mode === 'datetime' && datePickerMode !== 'time') {
        this.selectedDate = timestamp;
        this.setState({
          datePickerMode: 'time',
        });
      } else if (mode === 'datetime') {
        const newDate = `${moment(this.selectedDate).format('YYYY-MM-DD')} ${moment(timestamp).format('HH:mm')}`;
        this.setState({ datePickerVisible: false, datePickerMode: mode }, () => {
          onChangeText(newDate.valueOf());
        });
      } else {
        this.setState({ datePickerVisible: false }, () => {
          onChangeText(timestamp);
        });
      }
    } else {
      this.selectedDate = timestamp;
    }
  }

  onDateSelectDone = () => {
    const { onChangeText } = this.props;
    this.setState({ datePickerVisible: false }, () => {
      onChangeText(this.selectedDate);
    });
  }

  renderDatePicker = () => {
    const { datePickerVisible, datePickerMode } = this.state;
    const { value, maxDate, minDate } = this.props;
    const datePickerHtml = (<DateTimePicker
      style={{ width: '100%', backgroundColor: BaseColor.whiteColor }}
      testID="dateTimePicker"
      value={isIOS ? moment(value).toDate() : moment(value).valueOf()}
      mode={datePickerMode}
      is24Hour
      display="default"
      onChange={this.onDateChange}
    />);

    if (!isIOS) {
      return datePickerHtml;
    }

    return (
      <Modal
        animationType="fade"
        transparent
        visible={datePickerVisible}
        presentationStyle="overFullScreen"
      >
        <View
          style={{
              flex: 1,
              height: nHeight,
              backgroundColor: 'rgba(0,0,0,0.2)',
              justifyContent: 'flex-end',
              flexDirection: 'column',
            }}
        >
          <View
            style={{
                backgroundColor: BaseColor.whiteColor,
                padding: 15,
                paddingBottom: 40,
              }}
          >
            <View>
              { datePickerHtml }
            </View>
            <View style={{ alignItems: 'center' }}>
              <Button title="Done" text="Done" onPress={this.onDateSelectDone} />
            </View>
          </View>
        </View>
      </Modal>
    );
  };

  render() {
    const { focused, openPicker, phoneState, date } = this.state;
    const {
      inputStyle,
      textArea,
      editable,
      leftIconName,
      iconSize,
      otherCon,
      phone,
      rightIcon,
      rightIconName,
      otherIconCss,
      datePicker,
      value,
      placeholder,
      minDate,
      maxDate,
      onChangeText,
      onSubmitEditing,
    } = this.props;

    const height = textArea ? 125 : 50;
    const { datePickerVisible } = this.state;

    return (
      <View style={[styles.mainCon, {borderWidth: focused ? 2: 1, height: height}, otherCon]}>
        {leftIconName && !phone ? 
          <View style={styles.leftIconView}>
            <Icon name={leftIconName} size={20} style={[styles.leftIconSty, otherIconCss]} />
            {/* <CustomIcon name={'key'} size={35} color={BaseColor.ThemeBlue} /> */}
          </View>
        : null}

        {phone ? (
          // Currently as per doc & Functionality its static Only for US and code is 1 //
          <View style={{
            width: '14%',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: '#0000',
          }}>
            <Image source={Images.usFlag} style={{width: 20, height: 20}} />
          </View>
          

          // <TouchableOpacity
          //   activeOpacity={0.8}
          //   style={styles.leftIconView}
          //   onPress={() => this.setState({ openPicker: true })}
          // >
          //   <CountryPicker
          //     {...{
          //       translation: 'eng',
          //       onSelect: (val) => this.handlePicker(val),
          //       countryCode: phoneState.cca2,
          //       cca2: phoneState.cca2,
          //       withFilter: true,
          //       withAlphaFilter: true,
          //       withFlagButton: true,
          //       withFlag: true,
          //       onClose: () => this.setState({ openPicker: false }),
          //       placeholder: '',
          //     }}
          //     visible={openPicker}
          //   />
          // </TouchableOpacity>

        ) : null}

        {phone ? 
          <Text style={{
            fontFamily: FontFamily.medium,
            fontSize: 16,
            letterSpacing:1,
            backgroundColor: '#0000',
            paddingRight: 5,
          }}>
            {`+${phoneState.pCode}`}
          </Text>
        : null}

        {datePickerVisible && this.renderDatePicker() }

        {datePicker ?
          <TouchableOpacity
            style={[
              styles.inputCon,
              inputStyle,
              textArea ? { textAlignVertical: 'top' } : null,
              { paddingLeft: 20, justifyContent: 'center' },
            ]}
            onPress={this.onDatePickerPress}
          >
            <Text subhead blackColor style={inputStyle}>{value}</Text>
          </TouchableOpacity>
          :
          <TextInput
            {...this.props}
            allowFontScaling={false}
            ref={(o) => { this.input = o; }}
            multiline={textArea}
            numberOfLines={textArea ? 4 : 1}
            placeholderTextColor={focused ? '#0000' : BaseColor.grayColor}
            underlineColorAndroid="#0000"
            onFocus={() => this.onFocus()}
            onBlur={() => this.onBlur()}
            autoCapitalize="none"
            autoCorrect={false}
            blurOnSubmit={false}
            editable={editable}
            style={[
              styles.inputCon,
              inputStyle,
              textArea ? { textAlignVertical: 'top', paddingLeft: 20 } : null,
            ]}
          />
        }
        {
          rightIcon? <View style={styles.leftIconView}>
          <IconR name={rightIconName} size={iconSize ? iconSize : 25} style={styles.leftIconSty} />
        </View>:null
        }
      </View>
    );
  }
}

CInput.propTypes = {
};

CInput.defaultProps = {
};

export default CInput;
