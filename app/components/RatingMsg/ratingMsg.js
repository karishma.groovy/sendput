import React from 'react';
import {View, StyleSheet, Image} from 'react-native';
import Text from '../../components/Text';
import {Images} from '../../config/images';
import {BaseColor} from '../../config/theme';
import Icon from 'react-native-vector-icons/AntDesign';
import {BaseSetting} from '../../config';

export const Rating = ({name, yellowStar, whiteStar, date, message}) => {
  const createYellowStar = (no) => {
    let stars = [];

    for (let i = 0; i < no; i++) {
      stars.push(
        <Icon
          name="star"
          size={12}
          color={BaseColor.ThemeYellow}
          style={styles.paddingR2}
        />,
      );
    }
    return stars;
  };
  const createWhiteStar = (no) => {
    let stars = [];

    for (let i = 0; i < no; i++) {
      stars.push(
        <Icon
          name="staro"
          size={12}
          color={BaseColor.grayColor}
          style={styles.paddingR2}
        />,
      );
    }
    return stars;
  };

  return (
    // <View style={styles.centerView}>
    //   <Image source={Images.hero} style={styles.img}></Image>
    //   <Text bold blackColor body1>
    //     {name}
    //   </Text>
    //   <View style={styles.bottomView}>
    //     <View style={styles.flexRow}>
    //       {createYellowStar(yellowStar)}
    //       {createWhiteStar(whiteStar)}
    //     </View>
    //     <Text>{date}</Text>
    //   </View>
    //   <ScrollView nestedScrollEnabled={true}>

    //   <Text>{message}</Text>
    //   </ScrollView>
    // </View>
    <View style={styles.centerView}>
      <View style={{flexDirection: 'row', alignItems: 'center'}}>
        <Image source={Images.hero} style={styles.img} />
        <Text bold blackColor body1 style={{paddingLeft: 20}}>
          {name}
        </Text>
      </View>
      <View style={styles.flexRow}>
        {createYellowStar(yellowStar)}
        {createWhiteStar(whiteStar)}
        <Text style={{paddingLeft: 10}}>{'5.0'}</Text>
      </View>
      <Text numberOfLines={8}>{message}</Text>
      <Text textAlignRight style={{paddingTop: 20}}>
        {date}
      </Text>
    </View>
  );
};
const styles = StyleSheet.create({
  main: {
    flex: 1,
  },
  ratingTopView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 30,
  },
  starsView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 10,
  },
  listFooter: {
    width: '100%',
    height: 30,
  },
  oopsTxt: {
    alignSelf: 'center',
  },
  oopsView: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  redViews: {
    backgroundColor: BaseColor.ThemeOrange,
    width: 90,
    height: 8,
    borderRadius: 5,
  },
  redViews2: {
    backgroundColor: BaseColor.ThemeOrange,
    width: 70,
    height: 8,
    borderRadius: 5,
  },
  redViews3: {
    backgroundColor: BaseColor.ThemeOrange,
    width: 50,
    height: 8,
    borderRadius: 5,
  },
  redViews4: {
    backgroundColor: BaseColor.ThemeOrange,
    width: 30,
    height: 8,
    borderRadius: 5,
  },
  redViews5: {
    backgroundColor: BaseColor.ThemeOrange,
    width: 10,
    height: 8,
    borderRadius: 5,
  },
  paddingR2: {
    paddingRight: 5,
  },
  oopsImage: {
    width: BaseSetting.nWidth / 2,
    height: BaseSetting.nHeight / 2,
    resizeMode: 'contain',
  },
  centerView: {
    // elevation: 5,
    backgroundColor: BaseColor.whiteColor,
    // borderRadius: 10,
    marginTop: 15,
    marginHorizontal: 10,
    marginBottom: 10,
    borderWidth: 1,
    // position: 'relative',
    // paddingLeft: 20,
    // paddingRight: 20,
    paddingVertical: 20,
    paddingHorizontal: 20,
    borderColor: BaseColor.grayColor,
    // height:BaseSetting.nHeight/4
  },
  img: {
    // position: 'absolute',
    width: 50,
    height: 50,
    borderRadius: 25,
    // zIndex: 11,
    // top: -10,
    // left: -15,
  },
  bottomView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingBottom: 10,
  },
  flexRow: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 10,
  },
});
