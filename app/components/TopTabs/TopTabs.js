import React from 'react';
import PropTypes from 'prop-types';
import {ScrollView, StyleSheet, TouchableOpacity} from 'react-native';
import {BaseColor} from '../../config';
import Text from '../Text/index';
import {checkObject} from '../../utils/commonFunction';

const styles = StyleSheet.create({
  btnView: {
    flexGrow: 1,
  },
  btnStyle: {
    zIndex: 10,
    paddingVertical: 15,
    paddingHorizontal: 20,
    borderRadius: 50,
    overflow: 'hidden',
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 10,
    borderWidth: StyleSheet.hairlineWidth,
  },
  textFont: {
    fontSize: 11,
  },
});

const TopTabs = (props) => {
  const {onPress, active, btnArray, otherSty, otherMain, otherFontSty} = props;

  return (
    <ScrollView
      bounces={false}
      horizontal
      showsHorizontalScrollIndicator={false}
      contentContainerStyle={[styles.btnView, otherMain]}>
      {btnArray.map((obj, index) => {
        const isActive = active === index;
        const textClr = isActive ? BaseColor.whiteColor : BaseColor.blackColor;
        const btnBgClr = isActive ? BaseColor.ThemeBlue : BaseColor.whiteColor;
        const buttonName = checkObject(obj, 'category_name');
        return (
          <TouchableOpacity
            activeOpacity={0.8}
            onPress={() => {
              onPress(index);
            }}
            style={[styles.btnStyle, {backgroundColor: btnBgClr}, otherSty]}>
            <Text
              numberOfLines={1}
              subhead
              style={[styles.textFont, {color: textClr}, otherFontSty]}>
              {buttonName}
            </Text>
          </TouchableOpacity>
        );
      })}
    </ScrollView>
  );
};

TopTabs.propTypes = {
  btnArray: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  otherSty: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  otherFontSty: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  otherMain: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  onPress: PropTypes.func,
  active: PropTypes.number,
};

TopTabs.defaultProps = {
  btnArray: [],
  otherSty: {},
  otherMain: {},
  otherFontSty: {},
  active: null,
  onPress: () => {},
};

export default TopTabs;
