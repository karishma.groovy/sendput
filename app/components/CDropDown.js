import Icon from 'react-native-vector-icons/Ionicons';
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import {View, Text, StyleSheet, Dimensions} from 'react-native';
import {BaseColor} from '../config/theme';
import {FontFamily} from '../config/typography';
import {Dropdown} from '../libs/react-native-material-dropdown/index';

const drpH = 40;
const sDH = 30;
const sdrpH = 20;

const styles = StyleSheet.create({
  container: {
    height: 40,
    width: '100%',
    flexDirection: 'row',
    backgroundColor: BaseColor.lightGrey,
    position: 'relative',
    paddingLeft: 10,
    borderBottomWidth: 1,
    borderColor: '#ccc',
  },
  smallCont: {
    flex: 1,
    height: sDH,
    width: '100%',
    flexDirection: 'row',
    backgroundColor: '#0000',
    borderRadius: sDH / 2,
    marginVertical: 5,
    position: 'relative',
    paddingLeft: sDH / 2,
  },
  sCont: {
    flex: 1,
    height: 20,
    width: '100%',
    margin: 5,
    position: 'relative',
  },
  dropDownCont: {
    flex: 1,
    height: drpH,
    backgroundColor: '#0000',
    position: 'relative',
  },
  leftIconView: {
    width: 'auto',
    alignItems: 'center',
    justifyContent: 'center',
  },
  leftIcon: {
    color: '#FFF',
    marginTop: -2,
    fontSize: 18,
  },
});

const dropdownStyles = StyleSheet.create({
  container: {
    height: drpH,
    width: '100%',
    flexDirection: 'row',
  },
  sContainer: {
    height: sdrpH,
    width: '100%',
    flexDirection: 'row',
  },
  dTextView: {
    flex: 1,
    justifyContent: 'center',
  },
  dText: {
    color: '#444',
    fontFamily: FontFamily.regular,
    fontSize: 14,
    textAlign: 'left',
  },
  dRightView: {
    height: drpH,
    width: drpH,
    alignItems: 'center',
    justifyContent: 'center',
  },
  sDRightView: {
    height: sdrpH,
    width: sdrpH,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

class CDropDown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedItems: [],
      orientation: 1,
    };
  }

  componentDidMount() {
    Dimensions.addEventListener('change', this.onOrientationChange);
  }

  componentWillReceiveProps(nextProps) {
    const {selectedItems} = nextProps;
    if (selectedItems) {
      if (this.state.selectedItems.length !== selectedItems.length) {
        this.setState({selectedItems});
      }
    }
  }

  componentWillUnmount() {
    Dimensions.removeEventListener('change', this.onOrientationChange);
  }

  onOrientationChange = () => {
    const {orientation} = this.state;
    this.setState({
      orientation: orientation === 1 ? 0 : 1,
    });
  };

  onSelectedItemsChange(selectedItems) {
    const {onChangeText} = this.props;
    this.setState({selectedItems}, () => {
      if (onChangeText) {
        onChangeText(selectedItems);
      }
    });
  }

  renderBase(pData) {
    const title = typeof pData.label === 'string' ? pData.label : pData.value;
    const {bTextStyle, smallhof, label, bTitleStyle} = this.props;
    return (
      <View
        style={[
          smallhof ? dropdownStyles.sContainer : dropdownStyles.container,
        ]}>
        <View style={dropdownStyles.dTextView}>
          <Text
            numberOfLines={1}
            style={[
              dropdownStyles.dText,
              !_.isEmpty(title) ? bTextStyle : bTitleStyle,
            ]}>
            {title ? pData.label : label}
          </Text>
        </View>
        <View
          style={
            smallhof ? dropdownStyles.sDRightView : dropdownStyles.dRightView
          }>
          {pData.renderAccessory()}
        </View>
      </View>
    );
  }

  render() {
    const {
      label,
      dropdownData,
      onChangeText,
      leftIcon,
      value,
      disabled,
      borderNone,
      smallhof,
      upside,
      viewStyle,
      pickerTextStyle,
      iconStyle,
      customLabel,
      dTextStyle,
      pickerContainerStyle,
      numberOfLines,
    } = this.props;
    return (
      <View
        style={[
          smallhof ? styles.smallCont : styles.container,
          borderNone ? {borderBottomWidth: 0} : null,
          viewStyle,
        ]}>
        {leftIcon ? (
          <View style={[styles.leftIconView, iconStyle]}>
            <Icon name={leftIcon} style={styles.leftIcon} />
          </View>
        ) : null}
        <Dropdown
          numberOfLines={numberOfLines}
          label={label || customLabel}
          value={value && value.value ? value.value : ''}
          data={dropdownData}
          baseColor="#ccc"
          disabled={disabled}
          containerStyle={[
            smallhof ? styles.sCont : styles.dropDownCont,
            pickerContainerStyle,
          ]}
          pickerStyle={[
            {backgroundColor: BaseColor.whiteColor},
            pickerTextStyle,
          ]} // height: 'auto'
          itemColor={BaseColor.ThemeBlue}
          selectedItemColor={BaseColor.ThemeBlue}
          itemTextStyle={[
            dropdownStyles.dText,
            dTextStyle,
            {color: BaseColor.ThemeBlue, flexShrink: 1},
          ]}
          renderBase={(obj) => this.renderBase(obj)}
          fontSize={14}
          rippleInsets={{
            left: 0,
            top: 0,
            bottom: smallhof ? 18 : 0,
            right: 0,
          }}
          dropdownPosition={upside ? 4 : 0}
          itemPadding={10}
          onChangeText={(dV) => {
            if (onChangeText) {
              onChangeText(dV);
            }
          }}
        />
      </View>
    );
  }
}

CDropDown.propTypes = {
  label: PropTypes.string,
  customLabel: PropTypes.string,
  dropdownData: PropTypes.arrayOf(PropTypes.any),
  onChangeText: PropTypes.func,
  leftIcon: PropTypes.string,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.objectOf(PropTypes.any),
  ]),
  disabled: PropTypes.bool,
  bTextStyle: PropTypes.objectOf(PropTypes.any),
  pickerContainerStyle: PropTypes.objectOf(PropTypes.any),
  dTextStyle: PropTypes.objectOf(PropTypes.any),
  smallhof: PropTypes.bool,
  viewStyle: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.objectOf(PropTypes.any),
  ]),
  bTitleStyle: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.objectOf(PropTypes.any),
  ]),
};

CDropDown.defaultProps = {
  label: '',
  customLabel: '',
  dropdownData: [],
  onChangeText: null,
  leftIcon: null,
  value: {},
  disabled: false,
  bTextStyle: {},
  dTextStyle: {},
  smallhof: false,
  viewStyle: {},
  pickerContainerStyle: {},
  bTitleStyle: {},
};

export default CDropDown;
