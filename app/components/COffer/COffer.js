import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import {Image, StyleSheet, TouchableOpacity} from 'react-native';
import {BaseColor} from '../../config/theme';
import {checkObject} from '../../utils/commonFunction';
import {BaseSetting} from '../../config';

const nWidth = BaseSetting.nWidth;

const styles = StyleSheet.create({
  mainCon: {
    flex: 1,
    elevation: 3,
    marginTop: 10,
    borderRadius: 5,
    position: 'relative',
    marginHorizontal: 10,
    shadowColor: BaseColor.blackColor,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    backgroundColor: BaseColor.whiteColor,
  },
  courasalImage: {
    width: '100%',
    height: nWidth / 2,
    resizeMode: 'contain',
  },
});

export default function COffer(props) {
  const {data, onPress} = props;
  const imgUrl = checkObject(data, 'offer_image');

  return (
    <TouchableOpacity
      activeOpacity={0.8}
      style={styles.mainCon}
      onPress={() => {
        onPress();
      }}>
      <Image style={styles.courasalImage} source={{uri: imgUrl}} />
    </TouchableOpacity>
  );
}

COffer.propTypes = {
  data: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  onPress: PropTypes.func,
};

COffer.defaultProps = {
  data: {},
  onPress: () => {},
};
