import React from 'react';
import _ from 'lodash';
import {StyleSheet, View} from 'react-native';
import Button from '../Button';
import Text from '../Text/index';
import {BaseColor, BaseSetting} from '../../config';
import {checkObject, getOrderStatusClr} from '../../utils/commonFunction';

const styles = StyleSheet.create({
  mainCon: {
    flex: 1,
    elevation: 5,
    padding: 10,
    marginBottom: 5,
    borderRadius: 0,
    marginHorizontal: 5,
    backgroundColor: BaseColor.whiteColor,
    shadowColor: BaseColor.blackColor,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
  },
  btnStyle: {
    width: BaseSetting.nWidth / 4,
    height: 40,
    marginTop: 0,
    borderWidth: StyleSheet.hairlineWidth,
    backgroundColor: 'skyblue',
  },
  commonFlex: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  addressText: {
    color: BaseColor.lightBlack,
    lineHeight: 22,
  },
  rightCon: {
    flex: 1,
  },
  colorBlck: {
    fontSize: 11,
    color: BaseColor.blackColor,
  },
  PT20: {
    paddingTop: 10,
  },
  firstView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  PB10: {
    paddingBottom: 10,
  },
  addressTxt: {
    lineHeight: 22,
  },
  PT10: {
    paddingTop: 10,
  },
  width50: {
    width: '50%',
  },
});

const OrderDetailCard = (props) => {
  const {data, from, onShowOrderDetail} = props;

  const orderStatus = checkObject(data, 'order_status');
  const orderNo = checkObject(data, 'order_no');
  const orderUSDCode = checkObject(data, 'order_amount');
  const deliverAdd = checkObject(data, 'delivery_address');
  const dateTime = checkObject(data, 'order_date');
  const deliveryDate = checkObject(data, 'deliver_date');

  const storeData = checkObject(data, 'store');
  const storeName = checkObject(storeData, 'store_name');

  const isUserOrder = from === 'User';

  return (
    // <View style={styles.mainCon}>
    //   <View style={styles.commonFlex}>
    //     <Text numberOfLines={1} bold body1>{`Order No. ${orderNo}`}</Text>
    //     <Text numberOfLines={1} bold body2>{`${orderUSDCode}`}</Text>
    //   </View>

    //   <Text numberOfLines={2} caption1 style={styles.addressText}>{deliverAdd}</Text>

    //   <View style={[styles.commonFlex, styles.PT20]}>
    //     <Button
    //       styleText={styles.colorBlck}
    //       style={styles.btnStyle}
    //       colorAry={[BaseColor.whiteColor, BaseColor.whiteColor]}
    //       buttonText={'Details'}
    //       onPress={() => {
    //         onShowOrderDetail();
    //       }}
    //     />
    //     <View style={styles.rightCon}>
    //       <Text bold body2 textAlignRight style={{color: getOrderStatusClr(orderStatus)}}>{_.toUpper(orderStatus)}</Text>
    //       <Text semibold caption1 grayColor textAlignRight>{dateTime}</Text>
    //     </View>
    //   </View>
    // </View>

    <View style={styles.mainCon}>
      <View style={[styles.firstView, styles.PB10]}>
        <Text numberOfLines={1} bold body1>{`Order No. ${orderNo}`}</Text>
        <Text numberOfLines={1} bold body1>{`${orderUSDCode}`}</Text>
      </View>

      <Text numberOfLines={1} grayColor caption1>
        {isUserOrder ? 'Store' : 'Delivery Address'}
      </Text>
      <Text numberOfLines={2} body2 bold style={styles.addressTxt}>
        {isUserOrder ? storeName : deliverAdd}
      </Text>

      <View style={[styles.firstView, styles.PT10]}>
        <View style={styles.width50}>
          <Text numberOfLines={1} grayColor caption1 textAlignLeft>
            Delivery Date & Time
          </Text>
          <Text numberOfLines={1} bold body2 blackColor textAlignLeft>
            {deliveryDate}
          </Text>
        </View>
        <View style={styles.width50}>
          <Text numberOfLines={1} caption1 grayColor textAlignRight>
            Order Date
          </Text>
          <Text numberOfLines={1} bold body2 textAlignRight>
            {dateTime}
          </Text>
        </View>
      </View>

      <View style={[styles.commonFlex, styles.PT20]}>
        <Button
          styleText={styles.colorBlck}
          style={styles.btnStyle}
          otherGredientCss={styles.btnStyle}
          colorAry={[BaseColor.whiteColor, BaseColor.whiteColor]}
          buttonText={'Details'}
          onPress={() => {
            onShowOrderDetail();
          }}
        />

        <View style={styles.rightCon}>
          <Text
            bold
            body2
            textAlignRight
            style={{color: getOrderStatusClr(orderStatus)}}>
            {_.toUpper(orderStatus)}
          </Text>
        </View>
      </View>
    </View>
  );
};

export default OrderDetailCard;
