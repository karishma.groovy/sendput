/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import {
  View,
  StatusBar,
  Platform,
  StyleSheet,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/Ionicons';
import FIcon from 'react-native-vector-icons/FontAwesome';
import {BaseColor} from '../../config/theme';
import Text from '../Text/index';

import {store} from '../../redux/store/configureStore';

const styles = StyleSheet.create({
  IconView: {
    width: '10%',
    height: 90,
    alignItems: 'center',
    justifyContent: 'center',
  },
  iconStyle: {
    fontSize: 25,
    color: BaseColor.whiteColor,
  },
  titleView: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default function GredientHeader(props) {
  const {
    auth: {statusBarHeight},
  } = store.getState();

  const {
    leftIcon,
    leftIconName,
    rightIcon,
    rightIconName,
    title,
    onLeftAction,
    onRightAction,
    rightLoader,
    leftLoader,
    homeRightIcon,
    onNotification,
    onCartAction,
    onHeartAction,
    productDetailIcon,
    onShareAction,
    orderCount,
    notificationCount,
    onTitlePress,
    otherTextSty,
    heartFilled,
    pageLoader,
    cartBadgeNo,
  } = props;

  const sHeight = 50 + statusBarHeight;

  if (homeRightIcon) {
    return (
      <View>
        <StatusBar
          barStyle={'light-content'}
          translucent
          backgroundColor="transparent"
        />
        <LinearGradient
          start={{x: 0.3, y: 0.25}}
          end={{x: 0.8, y: 1.0}}
          colors={[BaseColor.ThemeOrange, BaseColor.ThemeYellow]}
          style={{
            height: sHeight,
            width: '100%',
            flexDirection: 'row',
            alignItems: 'center',
            paddingTop: statusBarHeight,
            justifyContent: 'space-between',
            paddingHorizontal: 10,
          }}>
          <View style={{flex: 1}}>
            <Text title3 bold whiteColor>
              SendPut
            </Text>
          </View>
          {pageLoader ? null : (
            <TouchableOpacity
              activeOpacity={0.8}
              onPress={() => {
                onTitlePress();
              }}
              style={{
                width: '30%',
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'flex-end',
              }}>
              <FIcon name="map-marker" size={20} color={BaseColor.whiteColor} />
              <Text
                body1
                bold
                whiteColor
                numberOfLines={1}
                style={[{letterSpacing: 1, paddingLeft: 5}, otherTextSty]}>
                {title}
              </Text>
            </TouchableOpacity>
          )}
        </LinearGradient>
      </View>
    );
  }

  return (
    <View>
      <StatusBar
        barStyle={'light-content'}
        translucent
        backgroundColor="transparent"
      />
      <LinearGradient
        start={{x: 0.3, y: 0.25}}
        end={{x: 0.8, y: 1.0}}
        colors={[BaseColor.ThemeOrange, BaseColor.ThemeYellow]}
        style={{
          height: sHeight,
          width: '100%',
          flexDirection: 'row',
          alignItems: 'center',
          paddingTop: statusBarHeight,
          justifyContent: 'space-between',
        }}>
        {leftIcon ? (
          <TouchableOpacity
            activeOpacity={0.8}
            onPress={
              leftLoader || rightLoader
                ? null
                : () => {
                    onLeftAction();
                  }
            }
            style={styles.IconView}>
            {leftLoader ? (
              <ActivityIndicator
                size="small"
                color={BaseColor.whiteColor}
                animating
              />
            ) : (
              <Icon name={leftIconName} style={styles.iconStyle} />
            )}
          </TouchableOpacity>
        ) : (
          <View style={styles.IconView} />
        )}

          <Text
            title3
            bold
            whiteColor
            numberOfLines={1}
            style={[{letterSpacing: 1}, otherTextSty]}>
            {_.toUpper(title)}
          </Text>


        {/* {homeRightIcon ?
          <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end'}}>
            <TouchableOpacity
              activeOpacity={0.8}
              onPress={() => {
                onNotification();
              }}
              style={{width: '17%', position: 'relative'}}
            >
              <Icon name={'notifications-outline'} style={{fontSize: 25, color: BaseColor.whiteColor}} />
              {notificationCount > 0 ?
                <View
                  style={{
                    position: 'absolute',
                    top: -5,
                    left: 10,
                    width: 18,
                    height: 18,
                    borderRadius: 9,
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: BaseColor.ThemeRedColor,
                  }}
                >
                  <Text whiteColor caption2 >{notificationCount}</Text>
                </View>
              : null}
            </TouchableOpacity>

            <TouchableOpacity
              activeOpacity={0.8}
              onPress={() => {
                onHeartAction();
              }}
              style={{width: '17%'}}
            >
              <Icon name={'heart-outline'} style={{fontSize: 25, color: BaseColor.whiteColor}} />
            </TouchableOpacity>
            <TouchableOpacity
              activeOpacity={0.8}
              onPress={() => {
                onCartAction();
              }}
              style={{width: '17%', position: 'relative'}}
            >
              <Icon name={'cart-outline'} style={{fontSize: 25, color: BaseColor.whiteColor}} />
              {orderCount > 0 ?
                <View
                  style={{
                    position: 'absolute',
                    top: -5,
                    left: 10,
                    width: 18,
                    height: 18,
                    borderRadius: 9,
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: BaseColor.ThemeRedColor,
                  }}
                >
                  <Text whiteColor caption2 >{orderCount}</Text>
                </View>
              : null}
            </TouchableOpacity>
          </View>
        : null} */}

        {productDetailIcon ? (
          <View
            style={{
              width: '17%',
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'flex-end',
            }}>
            <TouchableOpacity
              activeOpacity={0.8}
              onPress={() => {
                onHeartAction();
              }}
              style={{width: '50%'}}>
              <Icon
                name={heartFilled === 'Yes' ? 'heart' : 'heart-outline'}
                style={{fontSize: 25, color: BaseColor.whiteColor}}
              />
            </TouchableOpacity>
            {/* <TouchableOpacity
              activeOpacity={0.8}
              onPress={() => {
                onShareAction();
              }}
              style={{width: '50%'}}
            >
              <Icon name={'share-social-outline'} style={{fontSize: 25, color: BaseColor.whiteColor}} />
            </TouchableOpacity> */}
          </View>
        ) : null}

        {productDetailIcon ? null : rightIcon ? (
          <TouchableOpacity
            activeOpacity={0.8}
            onPress={
              rightLoader || leftLoader
                ? null
                : () => {
                    onRightAction();
                  }
            }
            style={[styles.IconView, {
              position: 'relative',
            }]}>
            {rightLoader ? (
              <ActivityIndicator
                size="small"
                color={BaseColor.whiteColor}
                animating
              />
            ) : (
              <View>
                <Icon name={rightIconName} style={styles.iconStyle} />
                {cartBadgeNo > 0 ?
                  <View style={{
                    width: 16,
                    height: 16,
                    borderRadius: 8,
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: BaseColor.ThemeRedColor,
                    position: 'absolute',
                    top: -5,
                    right: 0,
                  }}>
                    <Text whiteColor caption2 numberOfLines={1}>{cartBadgeNo}</Text>
                  </View>
                : null}
              </View>
            )}
          </TouchableOpacity>
        ) : (
          <View style={styles.IconView} />
        )}
      </LinearGradient>
    </View>
  );
}

GredientHeader.propTypes = {
  cartBadgeNo: PropTypes.number,
  leftIcon: PropTypes.bool,
  leftIconName: PropTypes.string,
  rightIcon: PropTypes.bool,
  rightIconName: PropTypes.string,
  title: PropTypes.string,
  onLeftAction: PropTypes.func,
  onRightAction: PropTypes.func,
  rightLoader: PropTypes.bool,
  leftLoader: PropTypes.bool,
  homeRightIcon: PropTypes.bool,
  onNotification: PropTypes.func,
  onCartAction: PropTypes.func,
  onHeartAction: PropTypes.func,
  productDetailIcon: PropTypes.bool,
  onShareAction: PropTypes.func,
  orderCount: PropTypes.number,
  onTitlePress: PropTypes.func,
  heartFilled: PropTypes.bool,
};

GredientHeader.defaultProps = {
  leftIcon: false,
  leftIconName: 'arrow-back',
  rightIcon: false,
  rightIconName: 'refresh',
  title: '',
  rightLoader: false,
  leftLoader: false,
  onLeftAction: () => {},
  onRightAction: () => {},
  homeRightIcon: false,
  onNotification: () => {},
  onCartAction: () => {},
  onHeartAction: () => {},
  productDetailIcon: false,
  onShareAction: () => {},
  orderCount: 0,
  onTitlePress: () => {},
  heartFilled: false,
  cartBadgeNo: 0,
};
