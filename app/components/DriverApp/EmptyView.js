import React from 'react';
import _ from 'lodash';
import {View, StyleSheet} from 'react-native';
import PropTypes from 'prop-types';
import Text from '../Text/index';
import Button from '../Button/index';

const styles = StyleSheet.create({
  emptyList: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  refreshBtnSty: {
    height: 70,
    width: 70,
    borderRadius: 35,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default function EmptyView(props) {
  const {onPress, emptyText, btnLoader, centerIconName} = props;

  return (
    <View style={styles.emptyList}>
      <Button
        style={[styles.refreshBtnSty, {marginBottom: 20}]}
        otherGredientCss={styles.refreshBtnSty}
        loading={btnLoader}
        displayIcon
        centerIconName={centerIconName}
        onPress={() => {
          onPress();
        }}
      />
      <Text bold textAlignCenter body1 lightGrey>
        {emptyText}
      </Text>
    </View>
  );
}

EmptyView.propTypes = {
  emptyText: PropTypes.string,
  btnLoader: PropTypes.bool,
  onPress: PropTypes.func,
  centerIconName: PropTypes.string,
};

EmptyView.defaultProps = {
  emptyText: '',
  btnLoader: false,
  onPress: () => {},
  centerIconName: 'refresh',
};
