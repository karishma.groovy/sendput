import React from 'react';
import _ from 'lodash';
import {StyleSheet, View} from 'react-native';
import PropTypes from 'prop-types';
import {BaseColor, BaseSetting} from '../../config';
import Text from '../Text/index';
import Button from '../Button/index';
import {checkObject, getOrderStatusClr} from '../../utils/commonFunction';

const styles = StyleSheet.create({
  mainCon: {
    flex: 1,
    elevation: 5,
    padding: 10,
    marginBottom: 10,
    shadowColor: BaseColor.blackColor,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    marginHorizontal: 5,
    backgroundColor: BaseColor.whiteColor,
  },
  firstView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  PB10: {
    paddingBottom: 10,
  },
  PT10: {
    paddingTop: 10,
  },
  width50: {
    width: '50%',
  },
  btnStyle: {
    width: BaseSetting.nWidth / 4,
    height: 40,
    borderWidth: StyleSheet.hairlineWidth,
  },
  addressTxt: {
    lineHeight: 22,
  },
  extraBtnCss: {
    borderWidth: 0,
  },
  MR10: {
    marginRight: 5,
  },
});

function JobFeedCard(props) {
  const {
    data,
    onShowOrderDetail,
    onAcceptOrder,
    onRejectOrder,
    acceptLodaer,
    rejectLoader,
  } = props;

  const jobStatus = checkObject(data, 'job_status');
  const deliverAdd = checkObject(data, 'order_delivery_address');
  const jobDate = checkObject(data, 'job_date');
  const orderNo = checkObject(data, 'order_no');
  const orderUSDCode = checkObject(data, 'order_amount');
  const dateTime = checkObject(data, 'order_delivery_time');

  const isOrderPending = jobStatus === 'pending';

  return (
    <View style={styles.mainCon}>
      {isOrderPending ? (
        <Text numberOfLines={2} grayColor textAlignCenter>
          {
            'You have to accept the order in 3 min otherwise it will consider auto reject'
          }
        </Text>
      ) : null}

      <View
        style={[
          styles.firstView,
          styles.PB10,
          {paddingTop: isOrderPending ? 15 : 0},
        ]}>
        <Text numberOfLines={1} bold body1>{`Order No. ${orderNo}`}</Text>
        <Text numberOfLines={1} bold body1>{`${orderUSDCode}`}</Text>
      </View>

      <Text numberOfLines={1} grayColor caption1>
        Delivery Address
      </Text>
      <Text numberOfLines={2} body2 bold style={styles.addressTxt}>
        {deliverAdd}
      </Text>

      <View style={[styles.firstView, styles.PT10]}>
        <View style={styles.width50}>
          <Text numberOfLines={1} grayColor caption1 textAlignLeft>
            Delivery Date & Time
          </Text>
          <Text numberOfLines={1} bold body2 blackColor textAlignLeft>
            {dateTime}
          </Text>
        </View>
        <View style={styles.width50}>
          <Text numberOfLines={1} caption1 grayColor textAlignRight>
            Job Date
          </Text>
          <Text numberOfLines={1} bold body2 textAlignRight>
            {jobDate}
          </Text>
        </View>
      </View>

      <View style={styles.firstView}>
        <Button
          styleText={{color: BaseColor.blackColor, fontSize: 11}}
          style={styles.btnStyle}
          otherGredientCss={styles.btnStyle}
          colorAry={[BaseColor.whiteColor, BaseColor.whiteColor]}
          buttonText={'DETAIL'}
          onPress={() => {
            onShowOrderDetail();
          }}
        />

        {isOrderPending ? 
          <View style={styles.firstView}>
            <Button
              styleText={{fontSize: 11}}
              style={[styles.btnStyle, styles.extraBtnCss, styles.MR10]}
              otherGredientCss={[styles.btnStyle, styles.extraBtnCss, styles.MR10]}
              buttonText={'ACCEPT'}
              onPress={
                acceptLodaer || rejectLoader
                  ? null
                  : () => {
                      onAcceptOrder();
                    }
              }
            />
            <Button
              styleText={{fontSize: 11}}
              style={[styles.btnStyle, styles.extraBtnCss]}
              otherGredientCss={[styles.btnStyle, styles.extraBtnCss]}
              colorAry={[BaseColor.ThemeBlue, BaseColor.ThemeBlue]}
              buttonText={'REJECT'}
              onPress={
                rejectLoader || acceptLodaer
                  ? null
                  : () => {
                      onRejectOrder();
                    }
              }
            />
          </View>
        : null}

        {!isOrderPending ? (
          <View style={{paddingTop: 20}}>
            <Text numberOfLines={1} caption1 grayColor textAlignRight>
              Job Status
            </Text>
            <Text
              numberOfLines={1}
              bold
              style={{color: getOrderStatusClr(jobStatus)}}>
              {_.toUpper(jobStatus)}
            </Text>
          </View>
        ) : null}
      </View>
    </View>
  );
}

JobFeedCard.propTypes = {
  data: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  onShowOrderDetail: PropTypes.func,
  onAcceptOrder: PropTypes.func,
  onRejectOrder: PropTypes.func,
  acceptLodaer: PropTypes.bool,
  rejectLoader: PropTypes.bool,
};

JobFeedCard.defaultProps = {
  data: {},
  onShowOrderDetail: () => {},
  onAcceptOrder: () => {},
  onRejectOrder: () => {},
  acceptLodaer: false,
  rejectLoader: false,
};

export default JobFeedCard;
