import React from 'react';
import {StyleSheet, View, TouchableOpacity, Image} from 'react-native';
import Text from '../../components/Text/index';
import LinearGradient from 'react-native-linear-gradient';
import {BaseColor, BaseSetting} from '../../config';
import Icon from 'react-native-vector-icons/AntDesign';
import {checkObject} from '../../utils/commonFunction';

const styles = StyleSheet.create({
  productListImage: {
    width: BaseSetting.nWidth / 2.5,
    height: BaseSetting.nHeight / 4.5,
    alignSelf: 'center',
    position: 'relative',
  },
  productListBtmView: {
    paddingLeft: 15,
  },
  pullTextView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop: 10,
  },
  pTop5: {
    paddingTop: 5,
  },
  tickView: {
    flexDirection: 'row',
    paddingTop: 5,
    alignItems: 'center',
    paddingBottom: 15,
  },
  itemContainer: {
    width: BaseSetting.nWidth / 2,
    borderWidth: 0.3,
    borderColor: '#dcdcdc',
    alignItems: 'flex-start',
    padding: 10,
  },
  iconView: {
    position: 'absolute',
    backgroundColor: 'red',
    height: 20,
    width: 20,
    borderRadius: 10,
    top: 15,
    right: 25,
    alignItems: 'center',
    justifyContent: 'center',
  },
  paddingLeft: {
    paddingLeft: 10,
  },
});
const Favorites = ({item, index}) => {
  const image = checkObject(item, 'product_image');
  const productName = checkObject(item, 'product_name');
  const price = checkObject(item, 'price');
  const brand = checkObject(item, 'brand_name');

  return (
    <TouchableOpacity
      onPress={() => {
        return null;
      }}
      style={{
        ...styles.itemContainer,
        marginTop: index === 0 || index === 1 ? 20 : 0,
        borderTopWidth: index === 0 || index === 1 ? 0 : 0.3,
      }}>
      <Image source={{uri: image}} style={styles.productListImage} />
      <LinearGradient
        start={{x: 0.5, y: 0.25}}
        end={{x: 1, y: 1.0}}
        colors={[BaseColor.ThemeOrange, BaseColor.ThemeYellow]}
        style={styles.iconView}>
        <Icon name="heart" size={12} color={BaseColor.whiteColor} />
      </LinearGradient>
      <View style={styles.productListBtmView}>
        {/* <View style={styles.pullTextView}>
          <Text>{`Pull`}</Text>
          <Text>{`${item.length}mi`}</Text>
        </View> */}
        <Text bold body2 style={styles.pTop5}>
          {productName}
        </Text>
        <Text bold body2 style={styles.pTop5}>{`Brand  ${brand}`}</Text>

        <Text bold body2 style={styles.pTop5}>{`Price  ${price}`}</Text>
        {/* <View style={styles.tickView}>
          <Icon name="checkcircle" size={20} color="skyblue"></Icon>
          <Text style={styles.paddingLeft}>{item.product_name}</Text>
        </View> */}
      </View>
    </TouchableOpacity>
  );
};
export default Favorites;
