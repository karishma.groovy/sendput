import React from 'react';
import _ from 'lodash';
import {View, Image, StyleSheet} from 'react-native';
import PropTypes from 'prop-types';
import Text from '../Text/index';
import {BaseColor} from '../../config/theme';
import {checkObject} from '../../utils/commonFunction';
import {BaseSetting} from '../../config';

const nWidth = BaseSetting.nWidth;

const styles = StyleSheet.create({
  // centerView: {
  //   flex:1,
  //   paddingVertical: 10,
  //   flexDirection: 'row',
  //   alignItems: 'center',
  //   borderBottomWidth:0.5,
  //   justifyContent: 'space-between',
  //   borderColor:BaseColor.grayColor
  // },
  // img: {
  //   width: nWidth / 4,
  //   height: nWidth / 4,
  //   resizeMode: 'contain',
  // },
  // infoCon: {
  //   flex: 1,
  //   paddingHorizontal: 10,
  //   paddingVertical: 5,
  //   alignItems: 'flex-start',
  //   justifyContent: 'center',
  // },
  // cmnFlex: {
  //   flexDirection: 'row',
  //   alignItems: 'flex-end',
  //   justifyContent: 'space-between',
  //   paddingBottom: 10,
  // },

  centerView: {
    elevation: 5,
    backgroundColor: BaseColor.whiteColor,
    borderRadius: 10,
    marginHorizontal: 15,
    marginBottom: 10,
    padding: 10,
    shadowColor: BaseColor.blackColor,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
  },
  flxjustCenter: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop: 10,
  },
  width50: {
    width: '50%',
  },
});

function OrderedItems(props) {
  const {data} = props;

  const imgUrl = checkObject(data, 'product_image');
  const name = checkObject(data, 'product_name');
  const price = checkObject(data, 'unit_price');
  const unit = checkObject(data, 'unit');

  return (
    // <View style={styles.centerView}>
    //   <Image source={{uri: imgUrl}} style={styles.img}></Image>
    //   <View style={styles.infoCon}>
    //     <View style={styles.cmnFlex}>
    //       <View style={{flex: 1}}>
    //         <Text numberOfLines={2} body1 blackColor bold>{name}</Text>
    //       </View>
    //       <View style={{width: '20%'}}>
    //         {/* <Text bold>{`USD: ${price}`}</Text> */}
    //         <Text bold>{price}</Text>
    //       </View>
    //     </View>
    //     <Text>{`Unit: ${unit}`}</Text>
    //   </View>
    // </View>

    <View style={styles.centerView}>
      <Text numberOfLines={1} blackColor bold body2>
        {name}
      </Text>
      <View style={styles.flxjustCenter}>
        <View style={styles.width50}>
          <Text numberOfLines={1} semibold caption grayColor textAlignLeft>
            Units
          </Text>
          <Text numberOfLines={1} bold caption textAlignLeft style={{paddingTop: 2}}>
            {unit}
          </Text>
        </View>
        <View style={styles.width50}>
          <Text numberOfLines={1} semibold caption grayColor textAlignRight>
            Unit Price
          </Text>
          <Text numberOfLines={1} bold caption textAlignRight style={{paddingTop: 2}}>
            {price}
          </Text>
        </View>
      </View>
    </View>
  );
}

OrderedItems.propTypes = {
  data: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
};

OrderedItems.defaultProps = {
  data: {},
};

export default OrderedItems;
