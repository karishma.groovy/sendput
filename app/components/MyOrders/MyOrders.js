import React, {useCallback, useEffect, useRef, useState} from 'react';
import PropTypes from 'prop-types';
import Moment from 'moment';
import _ from 'lodash';
import {View, StyleSheet} from 'react-native';
import Text from '../Text';
import Button from '../Button/index';
import {BaseColor} from '../../config/theme';
import {checkObject, getOrderStatusClr} from '../../utils/commonFunction';

const styles = StyleSheet.create({
  main: {
    flex: 1,
    padding: 15,
    elevation: 5,
    marginTop: 10,
    borderRadius: 10,
    marginHorizontal: 10,
    shadowColor: BaseColor.blackColor,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    backgroundColor: BaseColor.whiteColor,
  },
  PV10: {
    paddingVertical: 10,
  },
  commonFlexDirection: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  btnStyle: {
    width: '35%',
    height: 40,
    elevation: 0,
    borderWidth: 1,
  },
});

export default function MyOrders(props) {
  const {data, onPress} = props;

  const id = checkObject(data, 'id');
  const orderId = checkObject(data, 'order_id');
  const orderNo = checkObject(data, 'order_no');
  const orderDate = checkObject(data, 'order_date');
  const orderLastDate = checkObject(data, 'last_date');
  const orderStatus = checkObject(data, 'order_status');
  const deliveryAddress = checkObject(data, 'delivery_address');
  const deliverDate = checkObject(data, 'deliver_date');
  const customerName = checkObject(data, 'customer_name');
  const customerPhone = checkObject(data, 'customer_phone');
  const productList = checkObject(data, 'product');
  const totalQty = checkObject(data, 'total_qty');
  const paymentMode = checkObject(data, 'payment_mode');
  const totalAmount = checkObject(data, 'total_amount');
  const discount = checkObject(data, 'discount');
  const orderAmount = checkObject(data, 'order_amount');
  const orderHistory = checkObject(data, 'order_history');

  const orderDateValue = Moment(orderDate, 'DD MMM YYYY hh:mm a').format(
    'DD-MM-YYYY',
  );

  return (
    <View style={styles.main}>
      <View style={styles.commonFlexDirection}>
        <Text bold body1>{`Order No. ${orderNo}`}</Text>
        <Text semibold body1>
          {orderDateValue}
        </Text>
      </View>

      <Text semibold body1 style={styles.PV10}>
        Tracking number: <Text bold>Groovy147852369</Text>
      </Text>

      <View style={styles.commonFlexDirection}>
        <Text semibold body1>
          Quantity: <Text bold>{totalQty}</Text>
        </Text>
        <Text semibold body1>
          Total Amount: <Text bold>{totalAmount}</Text>
        </Text>
      </View>

      <View style={styles.commonFlexDirection}>
        <Button
          styleText={{color: BaseColor.blackColor, fontWeight: 'normal'}}
          style={styles.btnStyle}
          colorAry={[BaseColor.whiteColor, BaseColor.whiteColor]}
          buttonText={'Details'}
          onPress={() => {
            onPress();
          }}
        />
        <Text bold body2 style={{color: getOrderStatusClr(orderStatus)}}>
          {_.toUpper(orderStatus)}
        </Text>
      </View>
    </View>
  );
}

MyOrders.propTypes = {
  data: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  onPress: PropTypes.func,
};

MyOrders.defaultProps = {
  data: {},
  onPress: () => {},
};
