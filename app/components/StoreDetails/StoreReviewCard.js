import React, {useState} from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import {View, Image, StyleSheet, TouchableOpacity, Modal} from 'react-native';
import {BaseColor} from '../../config/theme';
import Text from '../../components/Text/index';
import {checkObject} from '../../utils/commonFunction';
import Icon from 'react-native-vector-icons/Ionicons';
import {BaseSetting} from '../../config';

const nWidth = BaseSetting.nWidth;

const styles = StyleSheet.create({
  mainCon: {
    flex: 1,
    padding: 10,
    elevation: 5,
    marginTop: 15,
    borderRadius: 10,
    flexDirection: 'row',
    marginHorizontal: 10,
    alignItems: 'flex-start',
    justifyContent: 'space-between',
    backgroundColor: BaseColor.whiteColor,
    shadowColor: BaseColor.blackColor,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
  },
  reviewImgSty: {
    width: nWidth / 5,
    height: nWidth / 5,
    borderRadius: 5,
    resizeMode: 'cover',
  },
  reviewRightInfoCon: {
    flex: 1,
    justifyContent: 'space-around',
    paddingLeft: 10,
    paddingRight: 5,
    height: nWidth / 5,
  },
  rateStarCon: {
    paddingRight: 2,
    paddingVertical: 5,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  centeredView: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  modalView: {
    borderTopLeftRadius: 40,
    backgroundColor: BaseColor.whiteColor,
    width: BaseSetting.nWidth,
    paddingHorizontal: 20,
    paddingTop: 10,
    paddingBottom: 40,
    alignItems: 'center',
    shadowColor: BaseColor.blackColor,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  modalMainCon: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.3)',
    justifyContent: 'flex-end',
  },
  modalSubCon: {
    width: '100%',
    height: 'auto',
    paddingHorizontal: 30,
    paddingTop: 15,
    paddingBottom: 30,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    backgroundColor: BaseColor.whiteColor,
  },
  userImgSty: {
    width: 50,
    height: 50,
    resizeMode: 'cover',
    borderRadius: 25,
    alignSelf: 'center',
    marginBottom: 10,
  },
  userInfoCon: {
    paddingHorizontal: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  userStarCon: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderTopWidth: StyleSheet.hairlineWidth,
    borderTopColor: BaseColor.grayColor,
  },
  MT5: {
    marginTop: 5,
  },
  LH18: {
    lineHeight: 18,
  },
  cmnF: {
    flex: 1,
  },
});

export default function StoreReviewCard(props) {
  const {data} = props;

  const userName = checkObject(data, 'customer_name');
  const userImg = checkObject(data, 'customer_img');
  const rateMessage = checkObject(data, 'comment');
  const startCount = checkObject(data, 'rating');
  const reviewDate = checkObject(data, 'created_at');
  const imgUrl = !_.isEmpty(userImg) && userImg !== '-' ? userImg : '';

  const startArray = [1, 2, 3, 4, 5];
  const [modalVisible, setModalVisible] = useState(false);

  function renderDetailModal() {
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(false);
        }}>
        <View style={styles.modalMainCon}>
          <TouchableOpacity
            activeOpacity={0.8}
            style={styles.cmnF}
            onPress={() => {
              setModalVisible(false);
            }}
          />
          <View style={styles.modalSubCon}>
            <Image style={styles.userImgSty} source={{uri: imgUrl}} />
            <View style={styles.userInfoCon}>
              <Text body2 bold textAlignCenter>
                {userName}{' '}
              </Text>
              <Text caption1 style={styles.MT5}>
                {rateMessage}
              </Text>
            </View>
          </View>
        </View>
      </Modal>
    );
  }

  return (
    <TouchableOpacity
      style={styles.mainCon}
      onPress={() => {
        setModalVisible(true);
      }}>
      <Image source={{uri: imgUrl}} style={styles.reviewImgSty} />
      <View style={styles.reviewRightInfoCon}>
        <View>
          <Text subhead bold>
            {userName}
          </Text>
          <Text footnote grayColor numberOfLines={2}>
            {rateMessage}
          </Text>
        </View>
        <View style={styles.userStarCon}>
          <View style={styles.rateStarCon}>
            {startArray.map((obj, index) => {
              const isBool = obj <= _.toNumber(startCount);
              return (
                <Icon
                  name={isBool ? 'star' : 'star-outline'}
                  size={12}
                  color={BaseColor.ThemeYellow}
                  style={styles.PR3}
                />
              );
            })}
          </View>
          <Text footnote grayColor style={styles.LH18} numberOfLines={3}>
            {reviewDate}
          </Text>
        </View>
      </View>
      {renderDetailModal()}
    </TouchableOpacity>
  );
}

StoreReviewCard.propTypes = {
  data: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
};

StoreReviewCard.defaultProps = {
  data: {},
};
