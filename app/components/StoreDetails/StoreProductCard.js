import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import {View, Image, StyleSheet, TouchableOpacity} from 'react-native';
import {BaseColor} from '../../config/theme';
import Text from '../../components/Text/index';
import {checkObject} from '../../utils/commonFunction';
import {BaseSetting} from '../../config';

const nWidth = BaseSetting.nWidth;

const styles = StyleSheet.create({
  // mainCon: {
  //   flex: 1,
  //   padding: 10,
  //   flexDirection: 'row',
  //   borderBottomWidth: 0.5,
  //   alignItems: 'flex-start',
  //   justifyContent: 'space-between',
  //   borderColor:BaseColor.grayColor,
  // },
  // infoCon: {
  //   flex: 1,
  //   paddingHorizontal: 10,
  //   justifyContent: 'space-around',
  // },
  // ProductImage: {
  //   width: nWidth / 4,
  //   height: nWidth / 4,
  //   borderRadius: 5,
  //   resizeMode: 'cover',
  //   borderWidth: StyleSheet.hairlineWidth,
  //   borderColor: BaseColor.grayColor,
  // },

  mainCon: {
    flex: 1,
    padding: 10,
    width: nWidth / 2,
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: BaseColor.grayColor,
  },
  ProductImage: {
    width: '100%',
    height: nWidth / 2,
    resizeMode: 'contain',
  },
});

export default function StoreProductCard(props) {
  const {data, onPress} = props;

  const createdAt = checkObject(data, 'created_at');
  const productId = checkObject(data, 'id');
  const productCost = checkObject(data, 'cost');
  const productPrice = checkObject(data, 'price');
  const productDesc = checkObject(data, 'product_description');
  const productHighLight = checkObject(data, 'product_highlight');
  const productImg = checkObject(data, 'product_image');
  const productName = checkObject(data, 'product_name');
  const productSku = checkObject(data, 'product_sku');
  const shelfLife = checkObject(data, 'shelf_life');
  const productStatus = checkObject(data, 'status');

  const imgUrl =
    _.isArray(productImg) && !_.isEmpty(productImg) ? productImg[0] : '-';

  return (
    <TouchableOpacity
      activeOpacity={0.8}
      style={styles.mainCon}
      onPress={() => {
        onPress();
      }}>
      <Image source={{uri: imgUrl}} style={styles.ProductImage} />
      <View style={styles.infoCon}>
        <Text numberOfLines={2} body2 bold>
          {productName}
        </Text>
        <Text
          footnote
          style={{
            color: BaseColor.ThemeRedColor,
            paddingVertical: 5,
          }}>{`$${productPrice}`}</Text>
      </View>
    </TouchableOpacity>
  );
}

StoreProductCard.propTypes = {
  data: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  onPress: PropTypes.func,
};

StoreProductCard.defaultProps = {
  data: {},
  onPress: () => {},
};
