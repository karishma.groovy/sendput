/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import Clipboard from '@react-native-clipboard/clipboard';
import PropTypes from 'prop-types';
import _ from 'lodash';
import {View, Image, StyleSheet, TouchableOpacity} from 'react-native';
import {BaseColor} from '../../config/theme';
import Text from '../../components/Text/index';
import {checkObject} from '../../utils/commonFunction';
import {BaseSetting} from '../../config';

const nWidth = BaseSetting.nWidth;

const styles = StyleSheet.create({
  storeInfoSty: {
    width: '100%',
    height: nWidth / 3,
    resizeMode: 'cover',
    borderRadius: 5,
  },
});

export default function StoreDealsCard(props) {
  const {data, onPress, onCopyCode} = props;

  const offerCode = checkObject(data, 'offer_code');
  const offerTitle = checkObject(data, 'title');
  const startDate = checkObject(data, 'start_date');
  const expiryDate = checkObject(data, 'expiry_date');
  const offerImg = checkObject(data, 'offer_image');
  const offerDesc = checkObject(data, 'description');

  const imgUrl = !_.isEmpty(offerImg) && offerImg !== '-' ? offerImg : '';

  const copyToClipboard = () => {
    Clipboard.setString(offerCode);
    onCopyCode();
  };

  return (
    <View
      // activeOpacity={0.8}
      style={{
        flex: 1,
        marginTop: 10,
        padding: 10,
        marginHorizontal: 10,
        borderRadius: 5,
        overflow: 'hidden',
        borderColor: BaseColor.grayColor,
        borderWidth: StyleSheet.hairlineWidth,
        elevation: 5,
        shadowColor: BaseColor.blackColor,
        shadowOffset: {
          width: 0,
          height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        backgroundColor: BaseColor.whiteColor,
      }}
      // onPress={()=>{
      //   onPress();
      // }}
    >
      <Image source={{uri: imgUrl}} style={styles.storeInfoSty} />
      <Text body2 bold style={{marginTop: 5}}>
        {offerTitle}
      </Text>
      <TouchableOpacity
        activeOpacity={0.8}
        onPress={() => {
          copyToClipboard();
        }}
        style={{
          marginVertical: 10,
          alignItems: 'flex-start',
          justifyContent: 'center',
        }}>
        <Text style={{color: BaseColor.ThemeOrange}}>{offerCode}</Text>
      </TouchableOpacity>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
        }}>
        <Text caption1 grayPrimaryColor>
          {'Valid From '}
          <Text bold>{startDate}</Text>
          {' To '}
          <Text bold>{expiryDate}</Text>
        </Text>
        <TouchableOpacity
          onPress={() => {
            onPress();
          }}>
          <Text
            caption1
            bold
            textAlignRight
            style={{
              textDecorationLine: 'underline',
              color: BaseColor.ThemeOrange,
            }}>
            Details
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

StoreDealsCard.propTypes = {
  data: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  onPress: PropTypes.func,
  onCopyCode: PropTypes.func,
};

StoreDealsCard.defaultProps = {
  data: {},
  onPress: () => {},
  onCopyCode: () => {},
};
