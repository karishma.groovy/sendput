import {View, StyleSheet, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import AIcon from 'react-native-vector-icons/AntDesign';
import Swipeable from 'react-native-swipeable';
import React, {useState} from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import Text from '../Text';
import {BaseColor} from '../../config/theme';
import {checkObject} from '../../utils/commonFunction';

const styles = StyleSheet.create({
  mainCardView: {
    flex: 1,
    padding: 10,
    elevation: 5,
    marginHorizontal: 10,
    borderRadius: 5,
    shadowColor: BaseColor.blackColor,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    backgroundColor: BaseColor.whiteColor,
  },
  PV5: {
    paddingVertical: 5,
  },
  cmnFlex: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  PL5: {
    paddingLeft: 5,
  },
  swipeCon: {
    width: '20%',
    height: '100%',
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: BaseColor.ThemeRedColor,
  },
  selfCon: {
    alignSelf: 'center',
  },
});

export default function CShippingAddress(props) {
  const [currentlyOpenSwipeable, setCurrentlyOpenSwipeable] = useState(null);
  const {data, isCheck, onCheck, onDelete, onUpdate} = props;
  const city = checkObject(data, 'city');
  const address = checkObject(data, 'address');
  const zipcode = checkObject(data, 'zipcode');
  const state = checkObject(data, 'state');

  function onOpen(event, gestureState, swipeable) {
    if (currentlyOpenSwipeable && currentlyOpenSwipeable !== swipeable) {
      currentlyOpenSwipeable.recenter();
    }
    setCurrentlyOpenSwipeable(swipeable);
  }

  function onClose() {
    setCurrentlyOpenSwipeable(null);
  }

  return (
    <Swipeable
      rightButtons={[
        <TouchableOpacity
          activeOpacity={0.8}
          onPress={() => {
            onUpdate();
          }}
          style={[styles.swipeCon, {backgroundColor: BaseColor.greenColor}]}>
          <AIcon
            style={styles.selfCon}
            name="edit"
            size={20}
            color={BaseColor.whiteColor}
          />
        </TouchableOpacity>,
        <TouchableOpacity
          activeOpacity={0.8}
          onPress={() => {
            onDelete();
          }}
          style={styles.swipeCon}>
          <Icon
            style={styles.selfCon}
            name="trash-sharp"
            size={20}
            color={BaseColor.whiteColor}
          />
        </TouchableOpacity>,
      ]}
      onRightButtonsOpenRelease={onOpen}
      onRightButtonsCloseRelease={onClose}>
      <View style={styles.mainCardView}>
        <Text bold body2 style={styles.PV5}>
          {address} {city},
        </Text>
        <Text semibold body2 style={styles.PV5}>
          {zipcode} {state}
        </Text>

        <TouchableOpacity
          activeOpacity={0.8}
          onPress={() => {
            onCheck();
          }}
          style={styles.cmnFlex}>
          <Icon
            name={isCheck ? 'checkmark-circle' : 'radio-button-off'}
            size={22}
            color={BaseColor.blackColor}
          />
          <Text body2 style={styles.PL5}>
            {'Use as the shipping address'}
          </Text>
        </TouchableOpacity>
      </View>
    </Swipeable>
  );
}

CShippingAddress.propTypes = {
  data: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  isCheck: PropTypes.bool,
  onCheck: PropTypes.func,
  onDelete: PropTypes.func,
  onUpdate: PropTypes.func,
};

CShippingAddress.defaultProps = {
  data: {},
  isCheck: false,
  onCheck: () => {},
  onDelete: () => {},
  onUpdate: () => {},
};
