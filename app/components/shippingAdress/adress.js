import React from 'react';
import {View, StyleSheet} from 'react-native';
import Text from '../../components/Text';
import Icon from 'react-native-vector-icons/AntDesign';
import {BaseColor} from '../../config/theme';

const ShippingAdress = ({name, address1, address2}) => {
  return (
    <View style={styles.centerView}>
      <View style={styles.firstView}>
        <Text bold body1>
          {name}
        </Text>
        <Icon name="edit" color={BaseColor.blackColor} size={22} />
      </View>
      <Text>
        {address1} {'\n'}
        {address2}
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  centerView: {
    elevation: 5,
    backgroundColor: BaseColor.whiteColor,
    borderRadius: 10,
    marginTop: 15,
    shadowColor: BaseColor.blackColor,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    marginBottom: 10,
    paddingHorizontal: 10,
    paddingVertical: 10,
    paddingLeft: 20,
  },
  firstView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingBottom: 10,
    paddingRight: 20,
  },
});

export default ShippingAdress;
