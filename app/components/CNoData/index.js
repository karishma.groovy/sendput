import LottieView from 'lottie-react-native';
import React, {useRef} from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import {View, StyleSheet} from 'react-native';
import Text from '../Text/index';
import {BaseColor} from '../../config/theme';
import {BaseSetting} from '../../config';

const styles = StyleSheet.create({
  smallFont: {
    textAlign: 'center',
    fontSize: 15,
    fontWeight: 'bold',
    paddingBottom: 10,
    color: BaseColor.ThemeOrange,
  },
  animationWrap: {
    height: BaseSetting.nHeight / 2,
    width: BaseSetting.nWidth,
    justifyContent: 'center',
    alignItems: 'center',
  },
  animation: {
    height: 150,
    width: 200,
  },
});

export default function CNoData(props) {
  const animation = useRef();
  const {
    msgNoData,
    imageSource,
    style,
    imageStyle,
    onAnimationFinish,
    textColor,
  } = props;

  return (
    <View style={[styles.animationWrap, style]}>
      <LottieView
        ref={animation}
        onAnimationFinish={onAnimationFinish}
        autoSize={false}
        style={[styles.animation, imageStyle]}
        source={imageSource}
        autoPlay={true}
        loop={true}
      />
      <Text style={[styles.smallFont, textColor]}>{msgNoData}</Text>
    </View>
  );
}

CNoData.propTypes = {
  headTitle: PropTypes.string,
};

CNoData.defaultProps = {};
