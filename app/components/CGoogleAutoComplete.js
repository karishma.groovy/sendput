/* eslint-disable react-native/no-inline-styles */
import {GooglePlacesAutocomplete} from 'react-native-google-places-autocomplete';
import Icon from 'react-native-vector-icons/MaterialIcons';
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import {
  Modal,
  View,
  StyleSheet,
  TouchableOpacity,
  Platform,
} from 'react-native';
import CAlert from './CAlert';
import Button from '../components/Button/index';
import {BaseColor} from '../config/theme';
import {FontFamily} from '../config/typography';
import {BaseSetting} from '../config';
import {store} from '../redux/store/configureStore';

const isIOS = Platform.OS === 'ios';

const styles = StyleSheet.create({
  BackIconSty: {
    color: BaseColor.blackColor,
    fontSize: 25,
  },
  buttonSty: {
    height: 50,
    width: 70,
    marginTop: 0,
    borderRadius: 0,
    alignItems: 'center',
    justifyContent: 'center',
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: BaseColor.lightGrey,
  },
});

export default class CGoogleAutoComplete extends Component {
  constructor(props) {
    super(props);
    this.state = {
      addAddrModal: false,
      location: {},
    };
  }

  closeModal = () => {
    const {onClose} = this.props;
    this.setState({addAddrModal: false}, () => {
      if (onClose) {
        onClose();
      }
    });
  };

  openModal = () => {
    this.setState({addAddrModal: true});
  };

  onClickSave = () => {
    const {onSave} = this.props;
    const {location} = this.state;
    if (_.isEmpty(location)) {
      CAlert('Please Select Address');
    } else {
      onSave(location);
    }
  };

  GooglePlacesInput = () => {
    return (
      <GooglePlacesAutocomplete
        placeholder="Search"
        minLength={2} // minimum length of text to search
        autoFocus
        returnKeyType="search" // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
        listViewDisplayed="auto" // true/false/undefined
        fetchDetails
        renderDescription={(row) => row.description} // custom description render
        onPress={(data, details = null) => {
          // 'details' is provided when fetchDetails = true
          console.log(data, details);
          this.setState({location: details});
        }}
        onFail={(e) => {
          console.log(e);
        }}
        getDefaultValue={() => ''}
        query={{
          // available options: https://developers.google.com/places/web-service/autocomplete
          key: BaseSetting.googleMapAPIKey,
          language: 'en', // language of the results
          // types: '(cities)', // default: 'geocode'
        }}
        styles={{
          textInputContainer: {
            width: '100%',
            backgroundColor: BaseColor.whiteColor,
            height: 50,
            borderBottomWidth: StyleSheet.hairlineWidth,
            borderBottomColor: BaseColor.lightGrey,
            borderTopWidth: StyleSheet.hairlineWidth,
            borderTopColor: BaseColor.lightGrey,
          },
          container: {
            flex: 1,
            backgroundColor: BaseColor.whiteColor,
          },
          description: {
            fontWeight: 'bold',
          },
          textInput: {
            height: 50,
            backgroundColor: '#0000',
            marginTop: 0,
            color: BaseColor.blackColor,
            fontFamily: FontFamily.regular,
          },
        }}
        placeholderTextColor="#f9f9f9"
        currentLocation={false} // Will add a 'Current location' button at the top of the predefined places list
        // currentLocationLabel=""
        nearbyPlacesAPI="GooglePlacesSearch" // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
        GoogleReverseGeocodingQuery={
          {
            // available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro
          }
        }
        GooglePlacesSearchQuery={{
          // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
          rankby: 'distance',
          //types: 'food',
        }}
        filterReverseGeocodingByTypes={[
          'locality',
          'administrative_area_level_3',
        ]} // filter the reverse geocoding results by types - ['locality', 'administrative_area_level_3'] if you want to display only cities
        debounce={100} // debounce the requests in ms. Set to 0 to remove debounce. By default 0ms.
        renderLeftButton={() => this.renderBackIcon()}
        renderRightButton={() => this.renderSave()}
      />
    );
  };

  renderBackIcon = () => (
    <TouchableOpacity
      activeOpacity={0.8}
      style={{
        height: 50,
        width: 50,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: BaseColor.whiteColor,
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderBottomColor: BaseColor.lightGrey,
      }}
      onPress={() => this.closeModal()}>
      <Icon name="close" style={styles.BackIconSty} />
    </TouchableOpacity>
  );

  renderSave = () => {
    const {saveAddLoader} = this.props;
    return (
      <Button
        style={styles.buttonSty}
        otherGredientCss={{
          borderRadius: 0,
          height: 50,
          marginTop: 0,
          borderBottomWidth: StyleSheet.hairlineWidth,
          borderBottomColor: BaseColor.lightGrey,
        }}
        loading={saveAddLoader}
        buttonText={'set'}
        onPress={() => {
          this.onClickSave();
        }}
      />
    );
  };

  render() {
    const {
      auth: {statusBarHeight},
    } = store.getState();
    const {addAddrModal} = this.state;
    return (
      <Modal
        animationType="slide"
        visible={addAddrModal}
        supportedOrientations={[
          'portrait',
          'landscape',
          'landscape-left',
          'landscape-right',
          'portrait-upside-down',
        ]}
        onRequestClose={() => {
          this.setState({addAddrModal: false});
        }}>
        <View
          style={{
            flex: 1,
            backgroundColor: '#0000',
            paddingTop: isIOS ? statusBarHeight : 0,
          }}>
          {this.GooglePlacesInput()}
        </View>
      </Modal>
    );
  }
}

CGoogleAutoComplete.propTypes = {
  onSave: PropTypes.func,
  saveAddLoader: PropTypes.bool,
};

CGoogleAutoComplete.defaultProps = {
  onSave: null,
  saveAddLoader: false,
};
