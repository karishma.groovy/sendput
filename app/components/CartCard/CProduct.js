import React, {useCallback, useEffect, useRef, useState} from 'react';
import FIcon from 'react-native-vector-icons/AntDesign';
import IoIcon from 'react-native-vector-icons/Ionicons';
import {useDispatch, useSelector} from 'react-redux';
import PropTypes from 'prop-types';
import _ from 'lodash';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import CAlert from '../CAlert';
import {BaseColor} from '../../config/theme';
import Text from '../../components/Text/index';
import {BaseSetting} from '../../config/setting';
import {checkObject} from '../../utils/commonFunction';
import {getApiDataProgress} from '../../utils/apiHelper';
import authActions from '../../redux/reducers/auth/actions';
import {StaticAlertMsg, StaticHeader} from '../../config/StaticAlertMsg';

const {setCartBadgeCount} = authActions;

const nWidth = BaseSetting.nWidth;

const styles = StyleSheet.create({
  mainCon: {
    padding: 10,
    marginTop: 5,
    elevation: 5,
    borderRadius: 5,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowRadius: 3.84,
    shadowOpacity: 0.25,
    marginHorizontal: 10,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    shadowColor: BaseColor.blackColor,
    backgroundColor: BaseColor.whiteColor,
  },
  productNameCon: {
    flex: 1,
    paddingRight: 10,
  },
  updateQtyCon: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignContent: 'center',
    alignItems: 'center',
    width: '30%',
    marginRight: 10,
  },
  displayQtyCon: {
    width: nWidth / 15,
    height: nWidth / 15,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: BaseColor.grayColor,
    borderTopWidth: StyleSheet.hairlineWidth,
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
  btnCon: {
    width: nWidth / 15,
    height: nWidth / 15,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: BaseColor.grayColor,
    borderWidth: StyleSheet.hairlineWidth,
  },
  W20: {
    padding: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default function CProduct(props) {
  const {isConnected, userData} = useSelector((state) => state.auth);
  const [deleteLoad, setDeletedLoad] = useState(false);
  const [QTYbtnLoad, setQTYbtnLoad] = useState(false);
  const userToken = checkObject(userData, 'access_token');

  const dispatch = useDispatch();

  const header = {
    Authorization: `Bearer ${userToken}`,
  };

  const {data, parentData, navigation, onRefreshList} = props;
  // const productName = checkObject(data, 'product_name');
  // const productPrice = checkObject(data ,'price')
  const productName =
    data && data.product && data.product.product_name
      ? data.product.product_name
      : '';
  const productPrice =
    data && data.product && data.product.price ? data.product.price : '';
  const productQty = data && data.qty ? data.qty : '';

  // const cartData = checkObject(data, 'carts');
  // const productQty = checkObject(cartData, 'qty');

  const finalqty = _.toNumber(productQty) > 0 ? _.toNumber(productQty) : 1;

  useEffect(() => {
    setQty(finalqty);
  }, [data]);

  const [qty, setQty] = useState(finalqty);

  const storeData = checkObject(parentData, 'store');
  const storeId = checkObject(storeData, 'id');

  // const productId = checkObject(data, 'id');
  const productId =
    data && data.product && data.product.id ? data.product.id : '';

  async function updateQty(no) {
    setQTYbtnLoad(true);
    if (isConnected === true) {
      const qtyData = {
        'Cart[product_id]': productId,
        'Cart[qty]': no,
        'Cart[store_id]': storeId,
      };

      try {
        let endPoint = BaseSetting.endpoints.update_cart;
        const response = await getApiDataProgress(
          navigation,
          endPoint,
          'POST',
          qtyData,
          header,
        );
        if (
          _.isObject(response) &&
          !_.isEmpty(response) &&
          response.success === true
        ) {
          const successMessage = checkObject(response, 'message');
          setQty(no);
          setQTYbtnLoad(false);
          onRefreshList(successMessage);
        } else {
          const errorMessage = checkObject(response, 'message');
          CAlert(errorMessage, StaticHeader.Oops, () => {
            setQTYbtnLoad(false);
          });
        }
      } catch (err) {
        console.log('Catch Part', err);
        CAlert(StaticAlertMsg.someThing_Went_Wrong, StaticHeader.Oops, () => {
          setQTYbtnLoad(false);
        });
      }
    } else {
      CAlert(StaticAlertMsg.Network_Problem, StaticHeader.Oops, () => {
        setQTYbtnLoad(false);
      });
    }
  }

  async function deleteProduct() {
    if (isConnected === true) {
      const removeData = {
        product_id: productId,
        store_id: storeId,
      };

      try {
        let endPoint = BaseSetting.endpoints.remove_cart;
        const response = await getApiDataProgress(
          navigation,
          endPoint,
          'POST',
          removeData,
          header,
        );
        if (
          _.isObject(response) &&
          !_.isEmpty(response) &&
          response.success === true
        ) {
          const successMessage = checkObject(response, 'message');
          const cBadgeNo = checkObject(response, 'data');
          dispatch(setCartBadgeCount(_.toNumber(cBadgeNo)));
          onRefreshList(successMessage);
          setDeletedLoad(false);
        } else {
          const errorMessage = checkObject(response, 'message');
          CAlert(errorMessage, StaticHeader.Oops, () => {
            setDeletedLoad(false);
          });
        }
      } catch (err) {
        console.log('Catch Part', err);
        CAlert(StaticAlertMsg.someThing_Went_Wrong, StaticHeader.Oops, () => {
          setDeletedLoad(false);
        });
      }
    } else {
      CAlert(StaticAlertMsg.Network_Problem, StaticHeader.Oops, () => {
        setDeletedLoad(false);
      });
    }
  }

  function onUpdateQtyAction(str) {
    let answer = null;
    if (str === 'minus') {
      answer = qty - 1;
      if (answer >= 1) {
        updateQty(answer);
      }
    } else {
      answer = qty + 1;
      if (answer <= 10) {
        updateQty(answer);
      }
    }
  }

  const isDisableMinusBtn = qty <= 1 || deleteLoad;
  const isDisablePlusBtn = qty >= 10 || deleteLoad;

  const minusBtnBGClr = {
    backgroundColor: isDisableMinusBtn ? '#DCDCDC' : BaseColor.whiteColor,
  };
  const minusIconClr = isDisableMinusBtn ? '#C0C0C0' : BaseColor.blackColor;
  const plusBtnBGClr = {
    backgroundColor: isDisablePlusBtn ? '#DCDCDC' : BaseColor.whiteColor,
  };
  const plusIconClr = isDisablePlusBtn ? '#C0C0C0' : BaseColor.blackColor;

  return (
    <View style={styles.mainCon}>
      <View style={styles.productNameCon}>
        <Text numberOfLines={2} footnote>
          {productName}
        </Text>
        <Text caption1 style={{paddingTop: 3}}>{`$${productPrice}`}</Text>
      </View>
      <View style={styles.updateQtyCon}>
        <TouchableOpacity
          activeOpacity={isDisableMinusBtn || QTYbtnLoad ? 1 : 0.5}
          onPress={
            isDisableMinusBtn || QTYbtnLoad
              ? null
              : () => {
                  onUpdateQtyAction('minus');
                }
          }
          style={[styles.btnCon, minusBtnBGClr]}>
          <FIcon name="minus" size={15} color={minusIconClr} />
        </TouchableOpacity>
        <View style={styles.displayQtyCon}>
          <Text footnote style={{textAlign: 'center'}}>
            {qty}
          </Text>
        </View>
        <TouchableOpacity
          activeOpacity={isDisablePlusBtn || QTYbtnLoad ? 1 : 0.5}
          onPress={
            isDisablePlusBtn || QTYbtnLoad
              ? null
              : () => {
                  onUpdateQtyAction('plus');
                }
          }
          style={[styles.btnCon, plusBtnBGClr]}>
          <FIcon name="plus" size={15} color={plusIconClr} />
        </TouchableOpacity>
      </View>
      <TouchableOpacity
        activeOpacity={0.8}
        style={styles.W20}
        onPress={
          deleteLoad
            ? null
            : () => {
                CAlert(
                  StaticAlertMsg.removeProductFromCart,
                  StaticHeader.Confirm,
                  () => {
                    setDeletedLoad(true);
                    setTimeout(() => {
                      deleteProduct();
                    }, 200);
                  },
                  () => {
                    return null;
                  },
                  'YES',
                  'NO',
                );
              }
        }>
        {deleteLoad ? (
          <ActivityIndicator size={'small'} color={BaseColor.ThemeOrange} />
        ) : (
          <IoIcon
            name="trash-outline"
            size={15}
            color={BaseColor.ThemeRedColor}
          />
        )}
      </TouchableOpacity>
    </View>
  );
}

CProduct.propTypes = {
  data: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
};

CProduct.defaultProps = {
  data: {},
};
