/* eslint-disable react-native/no-inline-styles */
import Icon from 'react-native-vector-icons/FontAwesome';
import IoIcon from 'react-native-vector-icons/Ionicons';
import React, {useState, useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import PropTypes from 'prop-types';
import moment from 'moment';
import _ from 'lodash';
import {
  View,
  Image,
  Keyboard,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import CAlert from '../CAlert';
import CProduct from './CProduct';
import CDropDown from '../CDropDown';
import Button from '../Button/index';
import TextInput from '../TextInput/index';
import {BaseColor} from '../../config/theme';
import Text from '../../components/Text/index';
import {BaseSetting} from '../../config/setting';
import {FontFamily} from '../../config/typography';
import {getApiDataProgress} from '../../utils/apiHelper';
import {StaticAlertMsg, StaticHeader} from '../../config/StaticAlertMsg';
import {
  checkObject,
  getTimeStempFromDate,
  enableAnimateInEaseOut,
} from '../../utils/commonFunction';
import authActions from '../../redux/reducers/auth/actions';

const {setCartBadgeCount} = authActions;

const nWidth = BaseSetting.nWidth;
const today = new Date();

const dType = [
  {
    value: 1,
    label: 'Now',
  },
  {
    value: 2,
    label: 'Later',
  },
];

const pType = [
  {
    value: 1,
    label: 'Card',
  },
  {
    value: 2,
    label: 'Cash',
  },
];

const styles = StyleSheet.create({
  mainCon: {
    flex: 1,
    marginTop: 10,
    marginHorizontal: 10,
    elevation: 5,
    borderRadius: 10,
    backgroundColor: BaseColor.whiteColor,
    shadowColor: BaseColor.blackColor,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    position: 'relative',
  },
  storeInfoCon: {
    padding: 10,
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'space-between',
  },
  storeImgCon: {
    width: nWidth / 5,
    height: nWidth / 5,
    borderRadius: 5,
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: BaseColor.grayColor,
    overflow: 'hidden',
  },
  storeImgSty: {
    width: '100%',
    height: '100%',
    resizeMode: 'contain',
  },
  storeInfoRightCon: {
    flex: 1,
    paddingLeft: 15,
  },
  cartConTitle: {
    paddingLeft: 10,
    paddingTop: 5,
  },
  couponmainCon: {
    height: 50,
    width: '100%',
    borderWidth: 1,
    borderRadius: 50,
    overflow: 'hidden',
    marginVertical: 20,
    alignSelf: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderColor: BaseColor.ThemeBlue,
  },
  couponTextInputCon: {
    flex: 1,
    paddingRight: 10,
  },
  PH10: {
    paddingHorizontal: 10,
  },
  mainInputCon: {
    marginTop: 0,
    borderWidth: 0,
  },
  inputCon: {
    paddingLeft: 20,
    fontWeight: '700',
  },
  applyBtnSty: {
    width: '25%',
    height: 50,
    marginTop: 0,
    borderRadius: 0,
  },
  totalAmountCon: {
    paddingHorizontal: 10,
    paddingVertical: 15,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderBottomColor: BaseColor.dimGreyColor,
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
  cmnFlexSty: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  PV8: {
    paddingVertical: 8,
  },
  borderSty: {
    paddingVertical: 15,
    borderTopColor: BaseColor.grayColor,
    borderTopWidth: StyleSheet.hairlineWidth,
    borderBottomColor: BaseColor.grayColor,
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
  PT10: {
    paddingTop: 10,
  },
  P10: {
    padding: 10,
  },
  PB3: {
    paddingBottom: 3,
  },
  dateTimeMainCon: {
    height: 45,
    borderRadius: 3,
    marginTop: 0,
    borderColor: '#ddd',
    borderWidth: 1,
    backgroundColor: BaseColor.whiteColor,
  },
  dateTimeOtherCon: {
    borderWidth: 1,
    borderColor: BaseColor.dimGreyColor,
    backgroundColor: BaseColor.whiteColor,
    borderBottomWidth: 1,
  },
  shippingCon: {
    paddingHorizontal: 10,
    paddingBottom: 10,
  },
  W50: {
    width: '50%',
  },
  W45: {
    width: '45%',
  },
  dropDown: {
    height: 45,
    borderWidth: 1,
    borderRadius: 3,
    borderColor: '#ddd',
    alignSelf: 'center',
    backgroundColor: BaseColor.whiteColor,
  },
  pickerStyle: {
    backgroundColor: BaseColor.whiteColor,
    flex: 1,
  },
  dropdownTitleSty: {
    color: BaseColor.grayColor,
    fontFamily: FontFamily.regular,
  },
  PT0: {
    paddingTop: 0,
  },
  paymentMianCon: {
    borderTopWidth: StyleSheet.hairlineWidth,
    borderTopColor: BaseColor.dimGreyColor,
    padding: 10,
  },
  MV8: {
    marginVertical: 8,
  },
  addAddressText: {
    color: BaseColor.ThemeRedColor,
    padding: 5,
  },
  addAddressBtn: {
    width: '100%',
    padding: 10,
    borderRadius: 5,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: BaseColor.whiteColor,
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: BaseColor.grayColor,
    elevation: 2,
    marginTop: 5,
    shadowColor: BaseColor.blackColor,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
  },
  addressBtnText: {
    color: BaseColor.lightBlack,
    paddingLeft: 5,
  },
  selectCardBtn: {
    width: '100%',
    paddingVertical: 15,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderRadius: 5,
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: BaseColor.grayColor,
    backgroundColor: BaseColor.whiteColor,
  },
});

export default function CartCard(props) {
  const {isConnected, userData} = useSelector((state) => state.auth);
  const dispatch = useDispatch();
  const {
    data,
    navigation,
    onRefreshList,
    paymentList,
    onlyRefreshData,
    onAddAddressAction,
    onPaymentCard,
    onDeleteStore,
  } = props;

  const userToken = checkObject(userData, 'access_token');
  const header = {
    Authorization: `Bearer ${userToken}`,
  };

  const [deliverType, setDType] = useState(dType[0]);
  const [couponNumber, setCouponNumber] = useState('');
  const [paymentType, setPaymentType] = useState(pType[1]);
  const [finalAmountData, setFinalAmountData] = useState({});
  const [placeOrderLoad, setPlaceOrderLoad] = useState(false);
  const [applyCouponCode, setApplyCouponCode] = useState(false);
  const [showPaymentList, setShowPaymentList] = useState(false);
  const [selectedPaymentCard, setSelectedCard] = useState({});
  const [orderDateTime, setOrderDateTime] = useState(
    moment(today).format('YYYY-MM-DD HH:mm'),
  );

  useEffect(() => {
    setCouponNumber('');
    setFinalAmountData({});
  }, [data]);

  const storeData = checkObject(data, 'store');
  const productList = checkObject(data, 'product');
  const addressList = checkObject(data, 'address');

  const findAddressIndex = addressList.findIndex(
    (v) => _.toUpper(v.is_Active) === 'YES',
  );

  const selectedAddress =
    findAddressIndex >= 0 ? addressList[findAddressIndex] : {};

  const [country, setCountry] = useState(selectedAddress);

  const storeId = checkObject(storeData, 'id');
  const storeName = checkObject(storeData, 'store_name');
  const storeAddress = checkObject(storeData, 'address');
  const storeImg = checkObject(storeData, 'store_thumb');

  const cardName = !_.isEmpty(selectedPaymentCard)
    ? `**** **** **** ${selectedPaymentCard.last4}`
    : 'Select your card';

  async function placeOrder() {
    Keyboard.dismiss();
    if (isConnected === true) {
      const orderPlaceData = {
        'Order[delivery_address]': checkObject(country, 'value'),
        'Order[delivery_type]': _.toLower(deliverType.label),
        'Order[delivery_time]': getTimeStempFromDate(orderDateTime),
        'Order[mode_of_payment]': _.toLower(paymentType.label),
        'Order[offer_code]': couponNumber,
        'Order[store_id]': storeId,
      };

      if (_.toLower(paymentType.label) === 'card') {
        const cardId = checkObject(selectedPaymentCard, 'id');
        orderPlaceData['Order[payment_card_token]'] = cardId;
      }

      if (_.isArray(productList) && !_.isEmpty(productList)) {
        productList.map((obj, index) => {
          // const productId = checkObject(obj, 'id');
          const productId =
            obj && obj.product && obj.product.id ? obj.product.id : '';
          // const cartData = checkObject(obj, 'carts');
          // const productQty = checkObject(cartData, 'qty');
          const productQty = checkObject(obj, 'qty');
          orderPlaceData[`Order[item][${index}][product_id]`] = productId;
          orderPlaceData[`Order[item][${index}][qty]`] = productQty;
        });
      }

      try {
        let endPoint = BaseSetting.endpoints.create_order;
        const response = await getApiDataProgress(
          navigation,
          endPoint,
          'POST',
          orderPlaceData,
          header,
        );
        if (
          _.isObject(response) &&
          !_.isEmpty(response) &&
          response.success === true
        ) {
          const orderData = checkObject(response, 'data');
          const cBadgeNo = checkObject(orderData, 'cart_count');
          dispatch(setCartBadgeCount(_.toNumber(cBadgeNo)));
          setPlaceOrderLoad(false);
          onRefreshList(orderData);
        } else {
          const errorMessage = checkObject(response, 'message');
          CAlert(errorMessage, StaticHeader.Oops, () => {
            setPlaceOrderLoad(false);
          });
        }
      } catch (err) {
        console.log('Catch Part', err);
        CAlert(StaticAlertMsg.someThing_Went_Wrong, StaticHeader.Oops, () => {
          setPlaceOrderLoad(false);
        });
      }
    } else {
      CAlert(StaticAlertMsg.Network_Problem, StaticHeader.Oops, () => {
        setPlaceOrderLoad(false);
      });
    }
  }

  function checkCoupon() {
    if (_.isEmpty(couponNumber)) {
      CAlert(StaticAlertMsg.couponCode, StaticHeader.Oops);
    } else {
      setApplyCouponCode(true);
      setTimeout(() => {
        applyCouponAction();
      }, 100);
    }
  }

  async function applyCouponAction() {
    Keyboard.dismiss();
    if (isConnected === true) {
      const couponData = {
        offer_code: couponNumber,
        store_id: storeId,
      };

      if (_.isArray(productList) && !_.isEmpty(productList)) {
        productList.map((obj, index) => {
          const productId =
            obj && obj.product && obj.product.id ? obj.product.id : '';
          const productQty = checkObject(obj, 'qty');
          // const productId = checkObject(obj, 'id');
          // const cartData = checkObject(obj, 'carts');
          // const productQty = checkObject(cartData, 'qty');
          couponData[`item[${index}][product_id]`] = productId;
          couponData[`item[${index}][qty]`] = _.toNumber(productQty);
        });
      }

      try {
        let endPoint = BaseSetting.endpoints.apply_offer_code;
        const response = await getApiDataProgress(
          navigation,
          endPoint,
          'POST',
          couponData,
          header,
        );
        if (
          _.isObject(response) &&
          !_.isEmpty(response) &&
          response.success === true
        ) {
          const amountData = checkObject(response, 'data');
          setFinalAmountData(amountData);
          setTimeout(() => {
            setApplyCouponCode(false);
          }, 1000);
        } else {
          const errorMessage = checkObject(response, 'message');
          CAlert(errorMessage, StaticHeader.Oops, () => {
            setApplyCouponCode(false);
          });
        }
      } catch (err) {
        console.log('Catch Part', err);
        CAlert(StaticAlertMsg.someThing_Went_Wrong, StaticHeader.Oops, () => {
          setApplyCouponCode(false);
        });
      }
    } else {
      CAlert(StaticAlertMsg.Network_Problem, StaticHeader.Oops, () => {
        setApplyCouponCode(false);
      });
    }
  }

  function renderCheckOutBtn() {
    return (
      <View style={{paddingHorizontal: 10, paddingBottom: 20}}>
        <TouchableOpacity
          activeOpacity={0.8}
          style={{
            alignSelf: 'flex-end',
            paddingVertical: 5,
          }}
          onPress={() => {
            navigation.navigate('DispensariesDetail', storeData);
          }}>
          <Text
            caption1
            style={{
              textDecorationLine: 'underline',
              color: BaseColor.ThemeOrange,
            }}>
            Add More Item?
          </Text>
        </TouchableOpacity>
        <Button
          loading={placeOrderLoad}
          style={{marginTop: 0}}
          buttonText={'Check Out'}
          onPress={() => {
            setPlaceOrderLoad(true);
            setTimeout(() => {
              placeOrder();
            }, 1000);
          }}
        />
      </View>
    );
  }

  function renderPatmentCardList() {
    const isDisplayList =
      paymentType && paymentType.value && paymentType.value === 1;
    if (isDisplayList) {
      return (
        <View>
          {_.isArray(paymentList) && !_.isEmpty(paymentList) ? (
            <View>
              <TouchableOpacity
                activeOpacity={0.8}
                onPress={() => {
                  enableAnimateInEaseOut();
                  setShowPaymentList(!showPaymentList);
                }}
                style={styles.selectCardBtn}>
                <View style={{flex: 1, paddingLeft: 10}}>
                  <Text caption1 greyColor>
                    {cardName}
                  </Text>
                </View>
                <View style={{width: 24}}>
                  <Icon
                    name={showPaymentList ? 'caret-up' : 'caret-down'}
                    size={15}
                    color={'#CCC'}
                  />
                </View>
              </TouchableOpacity>

              {showPaymentList ? (
                <View style={{width: '100%', height: nWidth / 3, marginTop: 5}}>
                  <ScrollView
                    bounces={false}
                    nestedScrollEnabled
                    contentContainerStyle={{flexGrow: 1}}>
                    {paymentList.map((obj, index) => {
                      const cardNo = checkObject(obj, 'last4');
                      return (
                        <TouchableOpacity
                          key={index}
                          onPress={() => {
                            enableAnimateInEaseOut();
                            setShowPaymentList(false);
                            setSelectedCard(obj);
                          }}
                          style={{
                            width: '98%',
                            height: 40,
                            paddingHorizontal: 10,
                            alignItems: 'flex-start',
                            justifyContent: 'center',
                            alignSelf: 'center',
                            elevation: 2,
                            shadowColor: BaseColor.blackColor,
                            shadowOffset: {
                              width: 0,
                              height: 2,
                            },
                            shadowOpacity: 0.25,
                            shadowRadius: 3.84,
                            marginTop: 5,
                            marginBottom:
                              paymentList.length - 1 === index ? 10 : 0,
                            borderRadius: 5,
                            backgroundColor: BaseColor.whiteColor,
                          }}>
                          <Text
                            subhead
                            bold
                            style={{
                              letterSpacing: 4,
                              color: '#4c4c4c',
                            }}>{`**** **** **** ${cardNo}`}</Text>
                        </TouchableOpacity>
                      );
                    })}
                  </ScrollView>
                </View>
              ) : null}
            </View>
          ) : (
            <View style={styles.shippingCon}>
              <Text
                caption1
                textAlignCenter
                greyColor
                style={styles.addAddressText}>
                {
                  "You haven't add any payment card details yet.\nClick on Add Button and add your first payment card."
                }
              </Text>
              <TouchableOpacity
                activeOpacity={0.8}
                onPress={() => {
                  onPaymentCard();
                }}
                style={styles.addAddressBtn}>
                <IoIcon
                  name="add-outline"
                  size={20}
                  color={BaseColor.lightBlack}
                />
                <Text semibold style={styles.addressBtnText}>
                  Add Card
                </Text>
              </TouchableOpacity>
            </View>
          )}
        </View>
      );
    } else {
      return null;
    }
  }

  function renderPaymentCon() {
    return (
      <View style={styles.paymentMianCon}>
        <Text subhead bold>
          PAYMENT
        </Text>
        <View style={styles.MV8}>
          <CDropDown
            dropdownData={pType}
            value={paymentType}
            onChangeText={(value) => {
              setPaymentType(value);
            }}
            label="Payment type"
            viewStyle={styles.dropDown}
            pickerTextStyle={styles.pickerStyle}
            dTextStyle={styles.PT0}
            bTitleStyle={styles.dropdownTitleSty}
          />
        </View>
        {renderPatmentCardList()}
      </View>
    );
  }

  function renderDeliveryCon() {
    return (
      <View>
        <Text subhead bold style={[styles.PH10, styles.PT10]}>
          DELIVERY
        </Text>

        <View style={[styles.cmnFlexSty, styles.P10]}>
          <View style={{width: '27%'}}>
            <Text caption1 greyColor style={styles.PB3}>
              Delivery Type
            </Text>
            <CDropDown
              dropdownData={dType}
              value={deliverType}
              onChangeText={(value) => {
                setDType(value);
              }}
              label="Delivery type"
              viewStyle={styles.dropDown}
              pickerTextStyle={styles.pickerStyle}
              dTextStyle={styles.PT0}
              bTitleStyle={styles.dropdownTitleSty}
            />
          </View>

          <View style={{width: '70%'}}>
            <Text caption1 greyColor style={styles.PB3}>
              Delivery Datetime
            </Text>
            <TextInput
              onChangeText={(t) => {
                setOrderDateTime(moment(t).format('YYYY-MM-DD HH:mm'));
              }}
              otherCon={styles.dateTimeMainCon}
              inputStyle={{fontSize: 13, color: '#444', paddingLeft: 5}}
              value={orderDateTime}
              datePicker
              mode="datetime"
              returnKeyType="go"
              format="YYYY-MM-DD HH:mm"
              placeholder="Choose date"
              placeholderColor={BaseColor.ThemeBlue}
              inputContainerStyle={styles.dateTimeOtherCon}
            />
          </View>
        </View>

        {_.isArray(addressList) && !_.isEmpty(addressList) ? (
          <View style={styles.shippingCon}>
            <Text caption1 greyColor style={styles.PB3}>
              Select your shipping address
            </Text>
            <CDropDown
              dropdownData={addressList}
              value={country}
              onChangeText={(value) => {
                setCountry(value);
              }}
              label="Select shipping address"
              viewStyle={styles.dropDown}
              pickerTextStyle={styles.pickerStyle}
              dTextStyle={styles.PT0}
              bTitleStyle={styles.dropdownTitleSty}
            />
          </View>
        ) : (
          <View style={styles.shippingCon}>
            <Text
              caption1
              textAlignCenter
              greyColor
              style={styles.addAddressText}>
              {
                "You haven't add any shipping address yet.\nClick on Add Button and add your first shipping address."
              }
            </Text>
            <TouchableOpacity
              activeOpacity={0.8}
              onPress={() => {
                onAddAddressAction();
              }}
              style={styles.addAddressBtn}>
              <IoIcon
                name="add-outline"
                size={20}
                color={BaseColor.lightBlack}
              />
              <Text semibold style={styles.addressBtnText}>
                Add Address
              </Text>
            </TouchableOpacity>
          </View>
        )}
      </View>
    );
  }

  function renderTotalAmount() {
    const totalAmount = checkObject(data, 'total_count');
    return (
      <View style={styles.totalAmountCon}>
        <Text subhead bold>
          TOTAL
        </Text>
        <Text subhead bold>
          {`$${totalAmount}`}{' '}
        </Text>
      </View>
    );
  }

  function renderNewAmount() {
    const totalAmount = checkObject(finalAmountData, 'total_amount');
    const discountAmount = checkObject(finalAmountData, 'discount_amount');
    const netAmount = checkObject(finalAmountData, 'net_amount');

    return (
      <View>
        <View style={[styles.cmnFlexSty, styles.PH10, styles.PV8]}>
          <Text subhead>Total Amount</Text>
          <Text subhead>{`$${totalAmount}`}</Text>
        </View>

        <View style={[styles.cmnFlexSty, styles.PH10, styles.PV8]}>
          <Text subhead>Discount Amount</Text>
          <Text subhead>{`$${discountAmount}`}</Text>
        </View>

        <View style={[styles.cmnFlexSty, styles.PH10, styles.borderSty]}>
          <Text subhead bold>
            Net Amount
          </Text>
          <Text subhead bold>{`$${netAmount}`}</Text>
        </View>
      </View>
    );
  }

  function renderCouponCon() {
    return (
      <View style={styles.PH10}>
        <View style={styles.couponmainCon}>
          <View style={styles.couponTextInputCon}>
            <TextInput
              value={couponNumber}
              otherCon={styles.mainInputCon}
              placeholder="Apply Coupon Code"
              returnKeyType={'done'}
              inputStyle={styles.inputCon}
              onSubmitEditing={() => {
                checkCoupon();
              }}
              onChangeText={
                applyCouponCode
                  ? null
                  : (t) => {
                      setCouponNumber(t);
                    }
              }
            />
          </View>
          <Button
            onPress={
              applyCouponCode
                ? null
                : () => {
                    checkCoupon();
                  }
            }
            buttonText={'Apply'}
            loading={applyCouponCode}
            colorAry={[BaseColor.ThemeBlue, BaseColor.ThemeBlue]}
            style={styles.applyBtnSty}
            otherGredientCss={{
              borderRadius: 0,
              height: 50,
            }}
          />
        </View>
      </View>
    );
  }

  function renderCartList() {
    return (
      <View>
        <Text subhead bold style={styles.cartConTitle}>
          ITEMS IN CART
        </Text>

        {productList.map((obj, index) => {
          return (
            <CProduct
              key={index}
              data={obj}
              navigation={navigation}
              parentData={data}
              onRefreshList={(msg) => {
                onlyRefreshData(msg);
              }}
            />
          );
        })}
      </View>
    );
  }

  function storeInfoCon() {
    return (
      <View style={styles.storeInfoCon}>
        <View style={styles.storeImgCon}>
          <Image source={{uri: storeImg}} style={styles.storeImgSty} />
        </View>

        <View style={styles.storeInfoRightCon}>
          <Text subhead bold>
            {storeName}
          </Text>
          <Text caption1 greyColor>
            {storeAddress}
          </Text>
        </View>
      </View>
    );
  }

  return (
    <View style={styles.mainCon}>
      {storeInfoCon()}
      {renderCartList()}
      {renderCouponCon()}
      {!_.isEmpty(finalAmountData) ? renderNewAmount() : renderTotalAmount()}
      {renderDeliveryCon()}
      {renderPaymentCon()}
      {renderCheckOutBtn()}
      <TouchableOpacity
        activeOpacity={0.8}
        onPress={() => {
          onDeleteStore();
        }}
        style={{
          position: 'absolute',
          top: -8,
          right: -8,
          zIndex: 10,
        }}>
        <IoIcon name="close-circle" size={25} color={BaseColor.ThemeOrange} />
      </TouchableOpacity>
    </View>
  );
}

CartCard.propTypes = {
  data: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
};

CartCard.defaultProps = {
  data: {},
};
