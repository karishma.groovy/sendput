import React from 'react';
import PropTypes from 'prop-types';
import {
  TouchableOpacity,
  StatusBar,
  View,
  Image,
  Vibration,
} from 'react-native';
import { getStatusBarHeight, isIphoneX } from 'react-native-iphone-x-helper';
import GestureRecognizer, { swipeDirections } from 'react-native-swipe-gestures';
import { BaseColor } from '../../../../config/theme';
import Text from '../../../../components/Text/index';

const styles = {
  root: {
    flex: 1,
    backgroundColor: BaseColor.whiteColor,
  },
  container: {
    position: 'absolute',
    top: isIphoneX() && getStatusBarHeight(),
    bottom: 0,
    left: 0,
    right: 0,
    width: '95%',
    height: 65,
    alignSelf: 'center',
    borderRadius: 5,
    alignSelf: 'center',
    paddingHorizontal: 8,
    backgroundColor: BaseColor.whiteColor,
    shadowColor: BaseColor.blackColor,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
  },
  content: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: BaseColor.whiteColor,
  },
  logoImg: {
    width: 54,
    height: 54,
    borderRadius: 27,
    overflow: 'hidden',
  },
  textContainer: {
    flex: 1,
    paddingHorizontal: 10,
  },
  title: {
    color: BaseColor.blackColor,
    fontWeight: 'bold',
  },
  message: {
    color: BaseColor.lightBlack,
  },
};

class DefaultNotificationBody extends React.Component {
  constructor() {
    super();

    this.onNotificationPress = this.onNotificationPress.bind(this);
    this.onSwipe = this.onSwipe.bind(this);
  }

  componentDidUpdate(prevProps) {
    if (this.props.isOpen !== prevProps.isOpen) {
      StatusBar.setHidden(this.props.isOpen);
    }

    if (
      (prevProps.vibrate || this.props.vibrate)
      && this.props.isOpen
      && !prevProps.isOpen
    ) {
      Vibration.vibrate();
    }
  }

  onNotificationPress() {
    const { onPress, onClose } = this.props;

    onClose();
    onPress();
  }

  onSwipe(direction) {
    const { SWIPE_UP } = swipeDirections;

    if (direction === SWIPE_UP) {
      this.props.onClose();
    }
  }

  render() {
    const { title, message } = this.props;
    return (
      <View style={styles.root}>
        <GestureRecognizer onSwipe={this.onSwipe} style={styles.container}>
          <TouchableOpacity
            style={styles.content}
            activeOpacity={0.3}
            underlayColor="transparent"
            onPress={this.onNotificationPress}
          >
            <Image
              resizeMode={'cover'}
              style={styles.logoImg}
              source={require('../../../../assets/images/ic_launcher_round.png')}
            />
            <View style={styles.textContainer}>
              <Text subhead numberOfLines={1} style={styles.title}>
                {title}
              </Text>
              <Text caption1 numberOfLines={1} style={styles.message}>
                {message}
              </Text>
            </View>
          </TouchableOpacity>
        </GestureRecognizer>
      </View>
    );
  }
}

DefaultNotificationBody.propTypes = {
  title: PropTypes.string,
  message: PropTypes.string,
  vibrate: PropTypes.bool,
  isOpen: PropTypes.bool,
  onPress: PropTypes.func,
  onClose: PropTypes.func,
};

DefaultNotificationBody.defaultProps = {
  title: 'Notification',
  message: 'This is a test notification',
  vibrate: true,
  isOpen: false,
  onPress: () => null,
  onClose: () => null,
};

export default DefaultNotificationBody;
