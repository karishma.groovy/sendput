/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import PropTypes from 'prop-types';
import {TouchableOpacity, View, Image, Vibration, NativeModules, Platform} from 'react-native';
import GestureRecognizer, {swipeDirections} from 'react-native-swipe-gestures';
import { BaseColor } from '../../../../config/theme';
import Text from '../../../../components/Text/index';
const { StatusBarManager } = NativeModules;

const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 0 : StatusBarManager.HEIGHT;

const styles = {
  container: {
    width: '97%',
    height: 65,
    elevation: 5,
    borderRadius: 5,
    alignSelf: 'center',
    paddingHorizontal: 8,
    marginTop: STATUSBAR_HEIGHT - 5,
    backgroundColor: BaseColor.whiteColor,
  },
  content: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: BaseColor.whiteColor,
  },
  logoImg: {
    width: 54,
    height: 54,
    borderRadius: 27,
    overflow: 'hidden',
  },
  textContainer: {
    flex: 1,
    paddingHorizontal: 10,
  },
  title: {
    color: BaseColor.blackColor,
    fontWeight: 'bold',
  },
  message: {
    color: BaseColor.lightBlack,
  },
};

class DefaultNotificationBody extends React.Component {
  constructor() {
    super();

    this.onNotificationPress = this.onNotificationPress.bind(this);
    this.onSwipe = this.onSwipe.bind(this);
  }

  componentDidUpdate(prevProps) {
    if (
      (prevProps.vibrate || this.props.vibrate) &&
      this.props.isOpen &&
      !prevProps.isOpen
    ) {
      Vibration.vibrate();
    }
  }

  onNotificationPress() {
    const {onPress, onClose} = this.props;

    onClose();
    onPress();
  }

  onSwipe(direction) {
    const {onClose} = this.props;
    const {SWIPE_LEFT, SWIPE_RIGHT} = swipeDirections;

    if (direction === SWIPE_RIGHT || direction === SWIPE_LEFT) {
      onClose();
    }
  }

  render() {
    const {title, message, iconApp, icon} = this.props;
    return (
      <GestureRecognizer onSwipe={this.onSwipe} style={styles.container}>
        <TouchableOpacity
          style={styles.content}
          activeOpacity={0.3}
          underlayColor="transparent"
          onPress={this.onNotificationPress}>
          <Image
            resizeMode={'cover'}
            style={styles.logoImg}
            source={require('../../../../assets/images/ic_launcher_round.png')}
          />
          <View style={styles.textContainer}>
            <Text subhead numberOfLines={1} style={styles.title}>
              {title}
            </Text>
            <Text caption1 numberOfLines={1} style={styles.message}>
              {message}
            </Text>
          </View>
        </TouchableOpacity>
      </GestureRecognizer>
    );
  }
}

DefaultNotificationBody.propTypes = {
  title: PropTypes.string,
  message: PropTypes.string,
  vibrate: PropTypes.bool,
  isOpen: PropTypes.bool,
  onPress: PropTypes.func,
  onClose: PropTypes.func,
  iconApp: Image.propTypes.source,
  icon: Image.propTypes.source,
};

DefaultNotificationBody.defaultProps = {
  title: 'Notification',
  message: 'This is a test notification',
  vibrate: true,
  isOpen: false,
  iconApp: null,
  icon: null,
  onPress: () => null,
  onClose: () => null,
};

export default DefaultNotificationBody;
